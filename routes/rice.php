<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Travel Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {

    /////* Warehouse */////
    Route::get('/rice/warehouse', 'Rice\RefWarehouseController@index')->name('rice.warehouse');
    Route::get('/rice/warehouse/form_add', 'Rice\RefWarehouseController@formAdd')->name('rice.warehouse.form_add');
    Route::post('/rice/warehouse/add_new', 'Rice\RefWarehouseController@addNew')->name('rice.warehouse.add_new');
    Route::get('/rice/warehouse/form_edit/{id}', 'Rice\RefWarehouseController@formEdit')->name('rice.warehouse.form_edit');
    Route::post('/rice/warehouse/edit/{id}', 'Rice\RefWarehouseController@edit')->name('rice.warehouse.edit');

    /////* Car */////
    Route::get('/rice/car', 'Rice\RefCarController@index')->name('rice.car');
    Route::get('/rice/car/form_add', 'Rice\RefCarController@formAdd')->name('rice.car.form_add');
    Route::post('/rice/car/add_new', 'Rice\RefCarController@addNew')->name('rice.car.add_new');
    Route::get('/rice/car/form_edit/{id}', 'Rice\RefCarController@formEdit')->name('rice.car.form_edit');
    Route::post('/rice/car/edit/{id}', 'Rice\RefCarController@edit')->name('rice.car.edit');

    /////* Driver */////
    Route::get('/rice/driver', 'Rice\RefDriverController@index')->name('rice.driver');
    Route::get('/rice/driver/form_add', 'Rice\RefDriverController@formAdd')->name('rice.driver.form_add');
    Route::post('/rice/driver/add_new', 'Rice\RefDriverController@addNew')->name('rice.driver.add_new');
    Route::get('/rice/driver/form_edit/{id}', 'Rice\RefDriverController@formEdit')->name('rice.driver.form_edit');
    Route::post('/rice/driver/edit/{id}', 'Rice\RefDriverController@edit')->name('rice.driver.edit');

    /////* Helper */////
    Route::get('/rice/helper', 'Rice\RefHelperController@index')->name('rice.helper');
    Route::get('/rice/helper/form_add', 'Rice\RefHelperController@formAdd')->name('rice.helper.form_add');
    Route::post('/rice/helper/add_new', 'Rice\RefHelperController@addNew')->name('rice.helper.add_new');
    Route::get('/rice/helper/form_edit/{id}', 'Rice\RefHelperController@formEdit')->name('rice.helper.form_edit');
    Route::post('/rice/helper/edit/{id}', 'Rice\RefHelperController@edit')->name('rice.helper.edit');

    /////* Product */////
    Route::get('/rice/product', 'Rice\RefProductController@index')->name('rice.product');
    Route::get('/rice/product/form_add', 'Rice\RefProductController@formAdd')->name('rice.product.form_add');
    Route::post('/rice/product/add_new', 'Rice\RefProductController@addNew')->name('rice.product.add_new');
    Route::get('/rice/product/form_edit/{id}', 'Rice\RefProductController@formEdit')->name('rice.product.form_edit');
    Route::post('/rice/product/edit/{id}', 'Rice\RefProductController@edit')->name('rice.product.edit');

    /////* Purchase */////
    Route::get('/rice/purchase', 'Rice\TrnsctPurchaseController@index')->name('rice.purchase');
    Route::get('/rice/purchase/list_data', 'Rice\TrnsctPurchaseController@listData')->name('rice.purchase.list_data');
    Route::get('/rice/purchase/detail/{id}', 'Rice\TrnsctPurchaseController@detail')->name('rice.purchase.detail');
    Route::get('/rice/purchase/form_add', 'Rice\TrnsctPurchaseController@formAdd')->name('rice.purchase.form_add');
    Route::post('/rice/purchase/add_new', 'Rice\TrnsctPurchaseController@addNew')->name('rice.purchase.add_new');
    Route::get('/rice/purchase/form_add_item/{id}', 'Rice\TrnsctPurchaseController@formAddItem')->name('rice.purchase.form_add_item');
    Route::post('/rice/purchase/add_new_item/{id}', 'Rice\TrnsctPurchaseController@addNewItem')->name('rice.purchase.add_new_item');
    Route::get('/rice/purchase/form_edit/{id}', 'Rice\TrnsctPurchaseController@formEdit')->name('rice.purchase.form_edit');
    Route::post('/rice/purchase/edit/{id}', 'Rice\TrnsctPurchaseController@edit')->name('rice.purchase.edit');
    Route::get('/rice/purchase/view_list_item/{id}', 'Rice\TrnsctPurchaseController@viewListItem')->name('rice.purchase.view_list_item');
    Route::post('/rice/purchase/delete_purchase_item', 'Rice\TrnsctPurchaseController@deletePurchaseItem')->name('rice.purchase.delete_purchase_item');

    /////* Processing */////
    Route::get('/rice/processing', 'Rice\TrnsctProcessingController@index')->name('rice.processing');
    Route::get('/rice/processing/list_data', 'Rice\TrnsctProcessingController@listData')->name('rice.processing.list_data');
    Route::get('/rice/processing/detail/{id}', 'Rice\TrnsctProcessingController@detail')->name('rice.processing.detail');
    Route::get('/rice/processing/form_add', 'Rice\TrnsctProcessingController@formAdd')->name('rice.processing.form_add');
    Route::post('/rice/processing/add_new', 'Rice\TrnsctProcessingController@addNew')->name('rice.processing.add_new');
    Route::get('/rice/processing/form_add_item/{id}', 'Rice\TrnsctProcessingController@formAddItem')->name('rice.processing.form_add_item');
    Route::post('/rice/processing/add_new_item/{id}', 'Rice\TrnsctProcessingController@addNewItem')->name('rice.processing.add_new_item');
    Route::get('/rice/processing/form_edit/{id}', 'Rice\TrnsctProcessingController@formEdit')->name('rice.processing.form_edit');
    Route::post('/rice/processing/edit/{id}', 'Rice\TrnsctProcessingController@edit')->name('rice.processing.edit');
    Route::get('/rice/processing/view_list_item/{id}', 'Rice\TrnsctProcessingController@viewListItem')->name('rice.processing.view_list_item');
    Route::post('/rice/processing/delete_processing_item', 'Rice\TrnsctProcessingController@deleteProcessingItem')->name('rice.processing.delete_processing_item');

    /////* Delivery */////
    Route::get('/rice/delivery', 'Rice\TrnsctDeliveryController@index')->name('rice.delivery');
    Route::get('/rice/delivery/list_data', 'Rice\TrnsctDeliveryController@listData')->name('rice.delivery.list_data');
    Route::get('/rice/delivery/detail/{id}', 'Rice\TrnsctDeliveryController@detail')->name('rice.delivery.detail');
    Route::get('/rice/delivery/form_add', 'Rice\TrnsctDeliveryController@formAdd')->name('rice.delivery.form_add');
    Route::post('/rice/delivery/add_new', 'Rice\TrnsctDeliveryController@addNew')->name('rice.delivery.add_new');
    Route::get('/rice/delivery/form_add_item/{id}', 'Rice\TrnsctDeliveryController@formAddItem')->name('rice.delivery.form_add_item');
    Route::post('/rice/delivery/add_new_item/{id}', 'Rice\TrnsctDeliveryController@addNewItem')->name('rice.delivery.add_new_item');
    Route::get('/rice/delivery/form_edit/{id}', 'Rice\TrnsctDeliveryController@formEdit')->name('rice.delivery.form_edit');
    Route::post('/rice/delivery/edit/{id}', 'Rice\TrnsctDeliveryController@edit')->name('rice.delivery.edit');
    Route::get('/rice/delivery/view_list_item/{id}', 'Rice\TrnsctDeliveryController@viewListItem')->name('rice.delivery.view_list_item');
    Route::post('/rice/delivery/delete_item', 'Rice\TrnsctDeliveryController@deletePurchaseItem')->name('rice.delivery.delete_item');
});
