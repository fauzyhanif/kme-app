<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Travel Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/travel/booking/search-cars-page-public', 'Travel\TrnsctBookingPublicController@searchCarsPagePublic')->name('travel.search_cars_page_public');
Route::post('/travel/booking/search-cars', 'Travel\TrnsctBookingPublicController@searchCars')->name('travel.booking.search.cars');

Route::middleware(['auth'])->group(function () {

    /////* Kategori Kendaraan */////
    Route::get('/travel/kategori-kendaraan', 'Travel\RefCarCategoryController@index')->name('travel.kategorikendaraan');
    Route::get('/travel/kategori-kendaraan/json-data', 'Travel\RefCarCategoryController@jsonData')->name('travel.kategorikendaraan.jsondata');
    Route::get('/travel/kategori-kendaraan/form-add', 'Travel\RefCarCategoryController@formAdd')->name('travel.kategorikendaraan.formadd');
    Route::post('/travel/kategori-kendaraan/add-new', 'Travel\RefCarCategoryController@addNew')->name('travel.kategorikendaraan.addnew');
    Route::get('/travel/kategori-kendaraan/form-edit/{id}', 'Travel\RefCarCategoryController@formEdit')->name('travel.kategorikendaraan.formedit');
    Route::post('/travel/kategori-kendaraan/edit/{id}', 'Travel\RefCarCategoryController@edit')->name('travel.kategorikendaraan.edit');

    /////* Kendaraan */////
    Route::get('/travel/kendaraan', 'Travel\RefCarController@index')->name('travel.kendaraan');
    Route::get('/travel/kendaraan/json-data', 'Travel\RefCarController@jsonData')->name('travel.kendaraan.jsondata');
    Route::get('/travel/kendaraan/detail/{id}', 'Travel\RefCarController@detail')->name('travel.kendaraan.detail');
    Route::get('/travel/kendaraan/form-add', 'Travel\RefCarController@formAdd')->name('travel.kendaraan.formadd');
    Route::post('/travel/kendaraan/add-new', 'Travel\RefCarController@addNew')->name('travel.kendaraan.addnew');
    Route::get('/travel/kendaraan/form-edit/{id}', 'Travel\RefCarController@formEdit')->name('travel.kendaraan.formedit');
    Route::post('/travel/kendaraan/edit/{id}', 'Travel\RefCarController@edit')->name('travel.kendaraan.edit');
    Route::post('/travel/kendaraan/service', 'Travel\RefCarController@addService')->name('travel.kendaraan.service');
    Route::post('/travel/kendaraan/get-five-car-history', 'Travel\RefCarController@getFiveCarHistory')->name('travel.kendaraan.get-five-car-history');
    Route::get('/travel/kendaraan/all-car-history/{id}', 'Travel\RefCarController@allCarHistory')->name('travel.kendaraan.all-car-history');
    Route::post('/travel/kendaraan/get-all-car-history-data-json', 'Travel\RefCarController@getAllCarHistoryDataJson')->name('travel.kendaraan.get-all-car-history-data-json');
    Route::post('/travel/kendaraan/get-five-car-service-data', 'Travel\RefCarController@getFiveCarServiceData')->name('travel.kendaraan.getfivecarservicedata');
    Route::get('/travel/kendaraan/all-car-service/{id}', 'Travel\RefCarController@allCarService')->name('travel.kendaraan.allcarservice');
    Route::post('/travel/kendaraan/get-all-car-service-data-json', 'Travel\RefCarController@getAllCarServiceDataJson')->name('travel.kendaraan.getallcarservicedatajson');
    Route::post('/travel/kendaraan/get-all-trnsct', 'Travel\RefCarController@getAllTrnsct')->name('travel.kendaraan.get-all-trnsct');
    Route::post('/travel/kendaraan/get-all-trnsct-this-month', 'Travel\RefCarController@getAllTrnsctThisMonth')->name('travel.kendaraan.get-all-trnsct-this-month');

    /////* Driver */////
    Route::get('/travel/driver', 'Travel\RefDriverController@index')->name('travel.driver');
    Route::get('/travel/driver/json-data', 'Travel\RefDriverController@jsonData')->name('travel.driver.jsondata');
    Route::get('/travel/driver/detail/{id}', 'Travel\RefDriverController@detail')->name('travel.driver.detail');
    Route::get('/travel/driver/form-add', 'Travel\RefDriverController@formAdd')->name('travel.driver.formadd');
    Route::post('/travel/driver/add-new', 'Travel\RefDriverController@addNew')->name('travel.driver.addnew');
    Route::get('/travel/driver/form-edit/{id}', 'Travel\RefDriverController@formEdit')->name('travel.driver.formedit');
    Route::post('/travel/driver/edit/{id}', 'Travel\RefDriverController@edit')->name('travel.driver.edit');
    Route::post('/travel/driver/get-five-driver-saving-data', 'Travel\RefDriverController@getFiveDriverSavingData')->name('travel.driver.getfivedriversavingdata');
    Route::post('/travel/driver/get-five-driver-journey-data', 'Travel\RefDriverController@getFiveDriverJourneyData')->name('travel.driver.getfivedriverjourneydata');
    Route::get('/travel/driver/all-driver-saving/{id}', 'Travel\RefDriverController@allDriverSaving')->name('travel.driver.alldriversaving');
    Route::post('/travel/driver/get-all-driver-saving-data', 'Travel\RefDriverController@allDriverSavingData')->name('travel.driver.getalldriversavingdata');
    Route::post('/travel/driver/saving-retrieval', 'Travel\RefDriverController@savingRetrieval')->name('travel.driver.savingretrieval');
    Route::get('/travel/driver/all-trnsct-page/{id}', 'Travel\RefDriverController@allTrnsctPage')->name('travel.driver.all-trnsct-page');
    Route::post('/travel/driver/get-all-driver-history-data-json', 'Travel\RefDriverController@getAllDriverHistoryDataJson')->name('travel.driver.get-all-driver-history-data-json');
    Route::post('/travel/driver/initial-saving', 'Travel\RefDriverController@initialSaving')->name('travel.driver.initial-saving');
    Route::post('/travel/driver/delete-initial-saving', 'Travel\RefDriverController@deleteInitialSaving')->name('travel.driver.delete-initial-saving');

    /////* Helper */////
    Route::get('/travel/helper', 'Travel\RefHelperController@index')->name('travel.helper');
    Route::get('/travel/helper/json-data', 'Travel\RefHelperController@jsonData')->name('travel.helper.jsondata');
    Route::get('/travel/helper/detail/{id}', 'Travel\RefHelperController@detail')->name('travel.helper.detail');
    Route::get('/travel/helper/form-add', 'Travel\RefHelperController@formAdd')->name('travel.helper.formadd');
    Route::post('/travel/helper/add-new', 'Travel\RefHelperController@addNew')->name('travel.helper.addnew');
    Route::get('/travel/helper/form-edit/{id}', 'Travel\RefHelperController@formEdit')->name('travel.helper.formedit');
    Route::post('/travel/helper/edit/{id}', 'Travel\RefHelperController@edit')->name('travel.helper.edit');
    Route::get('/travel/driver/all-helper-saving/{id}', 'Travel\RefHelperController@allHelperSaving')->name('travel.helper.alldriversaving');
    Route::post('/travel/helper/get-five-helper-saving-data', 'Travel\RefHelperController@getFiveHelperSavingData')->name('travel.helper.getfivehelpersavingdata');
    Route::get('/travel/helper/all-helper-saving/{id}', 'Travel\RefHelperController@allHelperSaving')->name('travel.helper.allhelpersaving');
    Route::post('/travel/helper/saving-retrieval', 'Travel\RefHelperController@savingRetrieval')->name('travel.helper.savingretrieval');
    Route::post('/travel/helper/get-all-helper-saving-data', 'Travel\RefHelperController@allHelperSavingData')->name('travel.helper.getallhelpersavingdata');
    Route::get('/travel/helper/all-trnsct-page/{id}', 'Travel\RefHelperController@allTrnsctPage')->name('travel.helper.all-trnsct-page');
    Route::post('/travel/helper/initial-saving', 'Travel\RefHelperController@initialSaving')->name('travel.helper.initial-saving');
    Route::post('/travel/helper/delete-initial-saving', 'Travel\RefHelperController@deleteInitialSaving')->name('travel.helper.delete-initial-saving');

    /////* Customer */////
    Route::get('/travel/customer', 'Travel\RefCustomerController@index')->name('travel.customer');
    Route::get('/travel/customer/json-data', 'Travel\RefCustomerController@jsonData')->name('travel.customer.jsondata');
    Route::get('/travel/customer/detail/{id}', 'Travel\RefCustomerController@detail')->name('travel.customer.detail');
    Route::post('/travel/customer/datatable-detail-page', 'Travel\RefCustomerController@datatableDetailPage')->name('travel.customer.datatable-detail-page');
    Route::get('/travel/customer/form-add', 'Travel\RefCustomerController@formAdd')->name('travel.customer.formadd');
    Route::post('/travel/customer/add-new', 'Travel\RefCustomerController@addNew')->name('travel.customer.addnew');
    Route::get('/travel/customer/form-edit/{id}', 'Travel\RefCustomerController@formEdit')->name('travel.customer.formedit');
    Route::post('/travel/customer/edit/{id}', 'Travel\RefCustomerController@edit')->name('travel.customer.edit');

    /////* Agen */////
    Route::get('/travel/agen', 'Travel\RefAgentController@index')->name('travel.agent');
    Route::get('/travel/agen/json-data', 'Travel\RefAgentController@jsonData')->name('travel.agent.jsondata');
    Route::get('/travel/agen/detail/{id}', 'Travel\RefAgentController@detail')->name('travel.agent.detail');
    Route::post('/travel/agen/datatable-detail-page', 'Travel\RefAgentController@datatableDetailPage')->name('travel.agent.datatable-detail-page');
    Route::get('/travel/agen/form-add', 'Travel\RefAgentController@formAdd')->name('travel.agent.formadd');
    Route::post('/travel/agen/add-new', 'Travel\RefAgentController@addNew')->name('travel.agent.addnew');
    Route::get('/travel/agen/form-edit/{id}', 'Travel\RefAgentController@formEdit')->name('travel.agent.formedit');
    Route::post('/travel/agen/edit/{id}', 'Travel\RefAgentController@edit')->name('travel.agent.edit');

    /////* Booking */////
    Route::get('/travel/booking', 'Travel\TrnsctBookingController@bookingList')->name('travel.booking');
    Route::get('/travel/booking/leaving_this_week', 'Travel\TrnsctBookingController@leavingThisWeek')->name('travel.leaving_this_week');
    Route::get('/travel/booking/json-data', 'Travel\TrnsctBookingController@bookingListData')->name('travel.booking.listdata');
    Route::get('/travel/booking/search-cars-page', 'Travel\TrnsctBookingController@searchCarsPage')->name('travel.booking.search.cars.page');
    Route::post('/travel/booking/form-add', 'Travel\TrnsctBookingController@formAdd')->name('travel.booking.formadd');
    Route::post('/travel/booking/add-new', 'Travel\TrnsctBookingController@addNew')->name('travel.booking.addnew');
    Route::post('/travel/booking/get-customers', 'Travel\TrnsctBookingController@getCustomers')->name('travel.booking.getcustomers');
    Route::get('/travel/booking/detail/{id}', 'Travel\TrnsctBookingController@detail')->name('travel.booking.detail');
    Route::get('/travel/booking/form-edit/{id}', 'Travel\TrnsctBookingController@formEdit')->name('travel.booking.formedit');
    Route::post('/travel/booking/edit', 'Travel\TrnsctBookingController@edit')->name('travel.booking.edit');
    Route::post('/travel/booking/get-old-customer', 'Travel\RefCustomerController@getDataForRemoteSelect')->name('travel.booking.get-old-customer');
    Route::post('/travel/booking/get-agent', 'Travel\RefAgentController@getDataForRemoteSelect')->name('travel.booking.get-agent');
    Route::post('/travel/booking/pencairan_komisi', 'Travel\TrnsctBookingController@pencairanKomisi')->name('travel.booking.pencairan_komisi');
    Route::post('/travel/booking/cancel_pencairan_komisi', 'Travel\TrnsctBookingController@cancelPencairanKomisi')->name('travel.booking.cancel_pencairan_komisi');
    Route::post('/travel/booking/cancel_booking', 'Travel\TrnsctBookingController@cancelBooking')->name('travel.booking.cancel_booking');
    Route::post('/travel/booking/cancel_booking_second', 'Travel\TrnsctBookingController@cancelBookingSecond')->name('travel.booking.cancel_booking_second');
    Route::post('/travel/booking/delete_booking', 'Travel\TrnsctBookingController@deleteBooking')->name('travel.booking.delete_booking');
    Route::post('/travel/booking/get_detail_cancel', 'Travel\TrnsctBookingController@getDetailCancel')->name('travel.booking.get_detail_cancel');
    Route::get('/travel/booking/print_cancel/{id}', 'Travel\TrnsctBookingController@printCancel')->name('travel.booking.print_cancel');
    

    /////* SPJ */////
    Route::get('/travel/spj', 'Travel\TrnsctSpjController@index')->name('travel.spj.index');
    Route::get('/travel/spj/json-data', 'Travel\TrnsctSpjController@spjListData')->name('travel.spj.jsondata');
    Route::get('/travel/spj/form-add/{booking}/{category}', 'Travel\TrnsctSpjController@formAdd')->name('travel.spj.formadd');
    Route::post('/travel/spj/add-new', 'Travel\TrnsctSpjController@addNew')->name('travel.spj.addnew');
    Route::get('/travel/spj/form-edit/{id}', 'Travel\TrnsctSpjController@formEdit')->name('travel.spj.formedit');
    Route::post('/travel/spj/edit', 'Travel\TrnsctSpjController@edit')->name('travel.spj.edit');
    Route::get('/travel/spj/print/{id}', 'Travel\TrnsctSpjController@print')->name('travel.spj.print');
    Route::post('/travel/spj/change_status', 'Travel\TrnsctSpjController@changeStatus')->name('travel.spj.change_status');
    Route::post('/travel/spj/delete', 'Travel\TrnsctSpjController@delete')->name('travel.spj.delete');

    /////* Payment */////
    Route::get('/travel/payment/print/{id}', 'Travel\TrnsctPaymentController@print')->name('travel.payment.print');
    Route::get('/travel/payment', 'Travel\TrnsctPaymentController@index')->name('travel.payment');
    Route::get('/travel/payment/json-data', 'Travel\TrnsctPaymentController@jsonData')->name('travel.payment.jsondata');
    Route::get('/travel/payment/form-add/{id?}', 'Travel\TrnsctPaymentController@formAdd')->name('travel.payment.formadd');
    Route::post('/travel/payment/add-new', 'Travel\TrnsctPaymentController@addNew')->name('travel.payment.addnew');
    Route::get('/travel/payment/detail', 'Travel\TrxPaymentController@detail')->name('travel.payment.detail');
    Route::post('/travel/payment/cancel', 'Travel\TrnsctPaymentController@cancel')->name('travel.payment.cancel');

    /////* Financial account  */////
    Route::get('/travel/finance/account', 'Travel\RefFinancialAccountController@index')->name('travel.finance.account');
    Route::get('/travel/finance/account/get-parent/{id}', 'Travel\RefFinancialAccountController@getParent')->name('travel.finance.account.get-parent');
    Route::get('/travel/finance/account/json-data', 'Travel\RefFinancialAccountController@jsonData')->name('travel.finance.account.jsondata');
    Route::get('/travel/finance/account/detail/{id}', 'Travel\RefFinancialAccountController@detail')->name('travel.finance.account.detail');
    Route::get('/travel/finance/account/form-add', 'Travel\RefFinancialAccountController@formAdd')->name('travel.finance.account.formadd');
    Route::post('/travel/finance/account/add-new', 'Travel\RefFinancialAccountController@addNew')->name('travel.finance.account.addnew');
    Route::get('/travel/finance/account/form-edit/{id}', 'Travel\RefFinancialAccountController@formEdit')->name('travel.finance.account.formedit');
    Route::post('/travel/finance/account/edit/{id}', 'Travel\RefFinancialAccountController@edit')->name('travel.finance.account.edit');

    /////* Financial account group  */////
    Route::get('/travel/finance/account-group', 'Travel\RefFinancialAccountGroupController@index')->name('travel.finance.account-group');
    Route::get('/travel/finance/account-group/json-data', 'Travel\RefFinancialAccountGroupController@jsonData')->name('travel.finance.account-group.jsondata');
    Route::get('/travel/finance/account-group/detail/{id}', 'Travel\RefFinancialAccountGroupController@detail')->name('travel.finance.account-group.detail');
    Route::get('/travel/finance/account-group/form-add', 'Travel\RefFinancialAccountGroupController@formAdd')->name('travel.finance.account-group.formadd');
    Route::post('/travel/finance/account-group/add-new', 'Travel\RefFinancialAccountGroupController@addNew')->name('travel.finance.account-group.addnew');
    Route::get('/travel/finance/account-group/form-edit/{id}', 'Travel\RefFinancialAccountGroupController@formEdit')->name('travel.finance.account-group.formedit');
    Route::post('/travel/finance/account-group/edit/{id}', 'Travel\RefFinancialAccountGroupController@edit')->name('travel.finance.account-group.edit');

    /////* Income */////
    Route::get('/travel/income', 'Travel\TrnsctIncomeController@index')->name('travel.income');
    Route::get('/travel/income/json-data', 'Travel\TrnsctIncomeController@jsonData')->name('travel.income.jsondata');
    Route::get('/travel/income/form-add', 'Travel\TrnsctIncomeController@formAdd')->name('travel.income.formadd');
    Route::post('/travel/income/add-new', 'Travel\TrnsctIncomeController@addNew')->name('travel.income.addnew');
    Route::post('/travel/income/remove-jurnal-item', 'Travel\TrnsctIncomeController@removeJurnalItem')->name('travel.income.remove-jurnal-item');
    Route::get('/travel/income/detail/{id}', 'Travel\TrnsctIncomeController@detail')->name('travel.income.detail');
    Route::post('/travel/income/edit/{id}', 'Travel\TrnsctIncomeController@edit')->name('travel.income.edit');
    Route::get('/travel/income/print/{id}', 'Travel\TrnsctIncomeController@print')->name('travel.income.print');

    /////* Spending */////
    Route::get('/travel/spending', 'Travel\TrnsctSpendingController@index')->name('travel.spending');
    Route::get('/travel/spending/json-data', 'Travel\TrnsctSpendingController@jsonData')->name('travel.spending.jsondata');
    Route::get('/travel/spending/form-add', 'Travel\TrnsctSpendingController@formAdd')->name('travel.spending.formadd');
    Route::post('/travel/spending/add-new', 'Travel\TrnsctSpendingController@addNew')->name('travel.spending.addnew');
    Route::get('/travel/spending/detail/{id}', 'Travel\TrnsctSpendingController@detail')->name('travel.spending.detail');
    Route::post('/travel/spending/edit/{id}', 'Travel\TrnsctSpendingController@edit')->name('travel.spending.edit');
    Route::post('/travel/spending/remove-jurnal-item', 'Travel\TrnsctSpendingController@removeJurnalItem')->name('travel.spending.remove-jurnal-item');
    Route::get('/travel/spending/print/{id}', 'Travel\TrnsctSpendingController@print')->name('travel.spending.print');

    /////* Receive Money */////
    Route::get('/travel/receive_money', 'Travel\TrnsctReceiveMoneyController@index')->name('travel.receive_money');
    Route::get('/travel/receive_money/json_data', 'Travel\TrnsctReceiveMoneyController@jsonData')->name('travel.receive_money.json_data');
    Route::get('/travel/receive_money/form_add', 'Travel\TrnsctReceiveMoneyController@formAdd')->name('travel.receive_money.form_add');
    Route::post('/travel/receive_money/add_new', 'Travel\TrnsctReceiveMoneyController@addNew')->name('travel.receive_money.add_new');
    Route::post('/travel/receive_money/remove_jurnal_item', 'Travel\TrnsctReceiveMoneyController@removeJurnalItem')->name('travel.receive_money.remove_jurnal_item');
    Route::get('/travel/receive_money/detail/{id}', 'Travel\TrnsctReceiveMoneyController@detail')->name('travel.receive_money.detail');
    Route::post('/travel/receive_money/edit/{id}', 'Travel\TrnsctReceiveMoneyController@edit')->name('travel.receive_money.edit');
    Route::get('/travel/receive_money/print/{id}', 'Travel\TrnsctReceiveMoneyController@print')->name('travel.receive_money.print');

    /////* Deposit Money */////
    Route::get('/travel/deposit_money', 'Travel\TrnsctDepositMoneyController@index')->name('travel.deposit_money');
    Route::get('/travel/deposit_money/json_data', 'Travel\TrnsctDepositMoneyController@jsonData')->name('travel.deposit_money.json_data');
    Route::get('/travel/deposit_money/form_add', 'Travel\TrnsctDepositMoneyController@formAdd')->name('travel.deposit_money.form_add');
    Route::post('/travel/deposit_money/add_new', 'Travel\TrnsctDepositMoneyController@addNew')->name('travel.deposit_money.add_new');
    Route::post('/travel/deposit_money/remove_jurnal_item', 'Travel\TrnsctDepositMoneyController@removeJurnalItem')->name('travel.deposit_money.remove_jurnal_item');
    Route::get('/travel/deposit_money/detail/{id}', 'Travel\TrnsctDepositMoneyController@detail')->name('travel.deposit_money.detail');
    Route::post('/travel/deposit_money/edit/{id}', 'Travel\TrnsctDepositMoneyController@edit')->name('travel.deposit_money.edit');
    Route::get('/travel/deposit_money/print/{id}', 'Travel\TrnsctDepositMoneyController@print')->name('travel.deposit_money.print');

    /////* Calendar */////
    Route::match(['get', 'post'], '/travel/kalender-booking', 'Travel\RefCalendarController@index')->name('travel.calendar');

    /////* Report Agenda Unit */////
    Route::match(['get', 'post'] ,'/travel/laporan/agenda-armada', 'Travel\ReportAgendaUnitController@index')->name('travel.report.agendaarmada');
    Route::post('/travel/agenda-armada', 'Travel\ReportAgendaUnitController@print')->name('travel.report.agenda-armada.print');

    /////* Report Income */////
    Route::match(['get', 'post'] ,'/travel/laporan/pendapatan-sewa-bus', 'Travel\ReportIncomeController@index')->name('travel.report.pendapatan-sewa-bus');
    Route::get('/travel/pendapatan-sewa-bus/print', 'Travel\ReportIncomeController@print')->name('travel.report.pendapatan-sewa-bus.print');

    /////* Report Laba Rugi */////
    Route::get('/travel/laba-rugi', 'Travel\ReportLabaRugiController@index')->name('travel.laba-rugi');
    Route::post('/travel/laba-rugi/print', 'Travel\ReportLabaRugiController@print')->name('travel.laba-rugi.print');

    /////* Report Neraca */////
    Route::get('/travel/neraca', 'Travel\ReportNeracaController@index')->name('travel.neraca');
    Route::get('/travel/neraca/print', 'Travel\ReportNeracaController@print')->name('travel.neraca.print');
    Route::get('/travel/neraca/rincian/{id}', 'Travel\ReportNeracaController@detail')->name('travel.neraca.rincian');

    /////* Report Expense Monthly */////
    Route::match(['get', 'post'] ,'/travel/report/expense_monthly', 'Travel\ReportExpenseMonthlyController@index')->name('travel.report.expense_monthly');
    Route::match(['get', 'post'] ,'/travel/report/expense_monthly/print', 'Travel\ReportExpenseMonthlyController@print')->name('travel.report.expense_monthly.print');

    /////* Report Income Monthly */////
    Route::match(['get', 'post'] ,'/travel/report/income_monthly', 'Travel\ReportIncomeMonthlyController@index')->name('travel.report.income_monthly');
    Route::match(['get', 'post'] ,'/travel/report/income_monthly/print', 'Travel\ReportIncomeMonthlyController@print')->name('travel.report.income_monthly.print');

    /////* Report Finance Monthly */////
    Route::match(['get', 'post'] ,'/travel/report/finance_monthly', 'Travel\ReportFinanceMonthlyController@index')->name('travel.report.finance_monthly');
    Route::match(['get', 'post'] ,'/travel/report/finance_monthly/print', 'Travel\ReportFinanceMonthlyController@print')->name('travel.report.finance_monthly.print');

    /////* Payrol */////
    Route::match(['get', 'post'] ,'/travel/penggajian', 'Travel\TrnsctPayrolController@index')->name('travel.penggajian');
    Route::match(['get', 'post'] ,'/travel/penggajian/print', 'Travel\TrnsctPayrolController@print')->name('travel.penggajian.print');
    Route::post('/travel/penggajian/generate', 'Travel\TrnsctPayrolController@generate')->name('travel.penggajian.generate');
    Route::get('/travel/penggajian/form_pencairan/{id}', 'Travel\TrnsctPayrolController@formPencairan')->name('travel.penggajian.form_pencairan');
    Route::get('/travel/penggajian/print_slip_gaji/{id}', 'Travel\TrnsctPayrolController@printSlipGaji')->name('travel.penggajian.print_slip_gaji');
    Route::post('/travel/penggajian/pencairan', 'Travel\TrnsctPayrolController@pencairan')->name('travel.penggajian.pencairan');
    Route::post('/travel/penggajian/pembatalan_pencairan', 'Travel\TrnsctPayrolController@PembatalanPencairan')->name('travel.penggajian.pembatalan_pencairan');


    /////* Kasbon */////
    Route::get('/travel/kasbon', 'Travel\TrnsctDebtController@index')->name('travel.kasbon');
    Route::get('/travel/kasbon/list_data', 'Travel\TrnsctDebtController@listData')->name('travel.kasbon.list_data');
    Route::get('/travel/kasbon/detail/{id}', 'Travel\TrnsctDebtController@detail')->name('travel.kasbon.detail');
    Route::get('/travel/kasbon/form_add', 'Travel\TrnsctDebtController@formAdd')->name('travel.kasbon.form_add');
    Route::post('/travel/kasbon/add_new', 'Travel\TrnsctDebtController@addNew')->name('travel.kasbon.add_new');
    Route::get('/travel/kasbon/form_pembayaran/{id}', 'Travel\TrnsctDebtController@formPembayaran')->name('travel.kasbon.form_pembayaran');
    Route::post('/travel/kasbon/pembayaran', 'Travel\TrnsctDebtController@pembayaran')->name('travel.kasbon.pembayaran');
    Route::post('/travel/kasbon/pembatalan', 'Travel\TrnsctDebtController@pembatalan')->name('travel.kasbon.pembatalan');
    Route::post('/travel/kasbon/pembatalan_pembayaran', 'Travel\TrnsctDebtController@pembatalanPembayaran')->name('travel.kasbon.pembatalan_pembayaran');
    Route::post('/travel/kasbon/print', 'Travel\TrnsctDebtController@print')->name('travel.kasbon.print');


});
