<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::post('/login/doAuth', 'Auth\LoginController@doAuth')->name('login.doauth');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

////////////////////////
///////* System *///////
////////////////////////
Route::get('/gate', 'System\GateController@index')->name('gate');
Route::get('/gate/show-role/{id}', 'System\GateController@showRole')->name('gate.show.role');
Route::get('/gate/set-auth-role-and-module/{role}/{module}', 'System\GateController@setAuth')->name('gate.set.auth.role.and.module');
Route::get('/get-menu', 'System\GateController@getMenu')->name('get.menu');

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'IndexController@index')->name('home');

    /////* Change Password */////
    Route::match(['get', 'post'], '/auth/change_password', 'Auth\LoginController@changePassword')->name('auth.change_password');

    /////* Module */////
    Route::get('/system/module', 'System\ModuleController@index')->name('system.module');
    Route::get('/system/module/form-add', 'System\ModuleController@formAdd')->name('system.module.form.add');
    Route::post('/system/module/add-new', 'System\ModuleController@addNew')->name('system.module.add.new');
    Route::get('/system/module/form-edit/{id}', 'System\ModuleController@formEdit')->name('system.module.form.edit');
    Route::post('/system/module/edit/{id}', 'System\ModuleController@edit')->name('system.module.edit');

    /////* Role */////
    Route::get('/system/role', 'System\RoleController@index')->name('system.role');
    Route::get('/system/role/form-add', 'System\RoleController@formAdd')->name('system.role.form.add');
    Route::post('/system/role/add-new', 'System\RoleController@addNew')->name('system.role.add.new');
    Route::get('/system/role/form-edit/{id}', 'System\RoleController@formEdit')->name('system.role.form.edit');
    Route::post('/system/role/edit/{id}', 'System\RoleController@edit')->name('system.role.edit');

    /////* User */////
    Route::get('/system/user', 'System\UserController@index')->name('system.user');
    Route::get('/system/user/form-add', 'System\UserController@formAdd')->name('system.user.form.add');
    Route::post('/system/user/add-new', 'System\UserController@addNew')->name('system.user.add.new');
    Route::get('/system/user/form-edit/{id}', 'System\UserController@formEdit')->name('system.user.form.edit');
    Route::post('/system/user/edit/{id}', 'System\UserController@edit')->name('system.user.edit');

    /////* Group menu */////
    Route::get('/system/group-menu', 'System\GroupMenuController@index')->name('system.groupmenu');
    Route::get('/system/group-menu/form-add', 'System\GroupMenuController@formAdd')->name('system.groupmenu.form.add');
    Route::post('/system/group-menu/add-new', 'System\GroupMenuController@addNew')->name('system.groupmenu.add.new');
    Route::get('/system/group-menu/form-edit/{id}', 'System\GroupMenuController@formEdit')->name('system.groupmenu.form.edit');
    Route::post('/system/group-menu/edit/{id}', 'System\GroupMenuController@edit')->name('system.groupmenu.edit');

    /////* Acl */////
    Route::get('/system/acl', 'System\AclController@index')->name('system.acl');
    Route::get('/system/acl/form-add', 'System\AclController@formAdd')->name('system.acl.form.add');
    Route::post('/system/acl/add-new', 'System\AclController@addNew')->name('system.acl.add.new');
    Route::get('/system/acl/form-edit/{id}', 'System\AclController@formEdit')->name('system.acl.form.edit');
    Route::post('/system/acl/edit/{id}', 'System\AclController@edit')->name('system.acl.edit');
    Route::get('/system/acl/delete/{id}', 'System\AclController@delete')->name('system.acl.delete');

    /////* SDM Position */////
    Route::get('/system/jabatan', 'System\RefSdmPositionController@index')->name('system.jabatan');
    Route::get('/system/jabatan/json-data', 'System\RefSdmPositionController@jsonData')->name('system.jabatan.json-data');
    Route::get('/system/jabatan/form-add', 'System\RefSdmPositionController@formAdd')->name('system.jabatan.form-add');
    Route::post('/system/jabatan/add-new', 'System\RefSdmPositionController@addNew')->name('system.jabatan.add-new');
    Route::get('/system/jabatan/form-edit/{id}', 'System\RefSdmPositionController@formEdit')->name('system.jabatan.form-edit');
    Route::post('/system/jabatan/edit/{id}', 'System\RefSdmPositionController@edit')->name('system.jabatan.edit');

    /////* SDM */////
    Route::get('/system/sdm', 'System\RefSdmController@index')->name('system.sdm');
    Route::get('/system/sdm/json-data', 'System\RefSdmController@jsonData')->name('system.sdm.json-data');
    Route::get('/system/sdm/form-add', 'System\RefSdmController@formAdd')->name('system.sdm.form-add');
    Route::post('/system/sdm/add-new', 'System\RefSdmController@addNew')->name('system.sdm.add-new');
    Route::get('/system/sdm/form-edit/{id}', 'System\RefSdmController@formEdit')->name('system.sdm.form-edit');
    Route::post('/system/sdm/edit/{id}', 'System\RefSdmController@edit')->name('system.sdm.edit');
});
