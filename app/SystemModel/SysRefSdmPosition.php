<?php

namespace App\SystemModel;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class SysRefSdmPosition extends Model
{
    use Compoships;
    protected $table = "sys_ref_sdm_position";
    protected $primaryKey = "position_id";
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        "module_id",
        "position_id",
        "name",
        "wages",
        "active",
    ];
}
