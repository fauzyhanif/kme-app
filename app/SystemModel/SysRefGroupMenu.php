<?php

namespace App\SystemModel;

use Illuminate\Database\Eloquent\Model;

class SysRefGroupMenu extends Model
{
    protected $table = "sys_ref_group_menu";
    protected $primaryKey = "id_group_menu";
    protected $fillable = [
        "id_group_menu",
        "id_module",
        "id_role",
        "nama",
        "urut",
        "aktif",
    ];
}
