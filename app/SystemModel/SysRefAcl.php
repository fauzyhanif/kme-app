<?php

namespace App\SystemModel;

use Illuminate\Database\Eloquent\Model;

class SysRefAcl extends Model
{
    protected $table = "sys_ref_acl";
    protected $fillable = [
        "id",
        "id_module",
        "id_role",
        "id_group_menu",
        "is_menu",
        "icon",
        "label",
        "is_child",
        "parent",
        "method",
        "route",
        "url",
        "controller",
        "function",
        "middleware",
        "urut",
        "aktif",
    ];
}
