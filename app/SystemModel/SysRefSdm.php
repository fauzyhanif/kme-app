<?php

namespace App\SystemModel;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class SysRefSdm extends Model
{
    use Compoships;

    protected $table = "sys_ref_sdm";
    protected $primaryKey = "sdm_id";

    protected $fillable = [
        "sdm_id",
        "module_id",
        "nip",
        "name",
        "phone_num",
        "address",
        "position_id",
        "active",
    ];

    public function position()
    {
        return $this->belongsTo(SysRefSdmPosition::class, ['module_id', 'position_id'], ['module_id', 'position_id']);
    }
}
