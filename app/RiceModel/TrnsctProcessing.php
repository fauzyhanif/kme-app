<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctProcessing extends Model
{
    protected $table = "rice_trnsct_processing";
    protected $primaryKey = "process_id";
    protected $fillable = [
        "process_id",
        "date",
        "type",
        "warehouse_id",
        "pj",
        "product_id",
        "qty",
    ];

    public function warehouse()
    {
        return $this->belongsTo(RefWarehouse::class, 'warehouse_id', 'warehouse_id');
    }

    public function product()
    {
        return $this->belongsTo(RefProduct::class, 'product_id', 'product_id');
    }
}
