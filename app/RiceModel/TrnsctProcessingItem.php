<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctProcessingItem extends Model
{
    protected $table = "rice_trnsct_processing_item";
    protected $primaryKey = "procitem_id";
    protected $fillable = [
        "procitem_id",
        "process_id",
        "product_id",
        "qty",
    ];

    public function product()
    {
        return $this->belongsTo(RefProduct::class, 'product_id', 'product_id');
    }
}
