<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class RefHelper extends Model
{
    protected $table = "rice_ref_helper";
    protected $primaryKey = "helper_id";
    protected $fillable = [
        "helper_id",
        "name",
        "phone_num",
        "address",
        "active",
    ];
}
