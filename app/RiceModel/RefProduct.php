<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class RefProduct extends Model
{
    protected $table = "rice_ref_product";
    protected $primaryKey = "product_id";
    protected $fillable = [
        "product_id",
        "name",
        "active",
    ];
}
