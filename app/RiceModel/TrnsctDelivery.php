<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctDelivery extends Model
{
    protected $table = "rice_trnsct_delivery";
    protected $primaryKey = "delivery_id";
    protected $fillable = [
        "delivery_id",
        "delivery_date",
        "back_date",
        "car_id",
        "car_sub_id",
        "driver_id",
        "driver_sub_id",
        "helper_id",
        "helper_sub_id",
        "driver_commission",
        "helper_commission",
        "destination",
        "status_id",
    ];

    public function mainCar()
    {
        return $this->belongsTo(RefCar::class, 'car_id', 'car_id');
    }

    public function subCar()
    {
        return $this->belongsTo(RefCar::class, 'car_sub_id', 'car_id');
    }

    public function mainDriver()
    {
        return $this->belongsTo(RefDriver::class, 'driver_id', 'driver_id');
    }

    public function subDriver()
    {
        return $this->belongsTo(RefDriver::class, 'driver_sub_id', 'driver_id');
    }

    public function mainHelper()
    {
        return $this->belongsTo(RefHelper::class, 'helper_id', 'helper_id');
    }

    public function subHelper()
    {
        return $this->belongsTo(RefHelper::class, 'helper_sub_id', 'helper_id');
    }
}
