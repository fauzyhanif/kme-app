<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class RefWarehouse extends Model
{
    protected $table = "rice_ref_warehouse";
    protected $primaryKey = "warehouse_id";
    protected $fillable = [
        "warehouse_id",
        "name",
        "address",
        "active",
    ];
}
