<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctPurchaseItem extends Model
{
    protected $table = "rice_trnsct_purchase_item";
    protected $primaryKey = "purcitem_id";
    protected $fillable = [
        "purcitem_id",
        "purchase_id",
        "product_id",
        "qty",
        "price",
        "total",
    ];

    public function product()
    {
        return $this->belongsTo(RefProduct::class, 'product_id', 'product_id');
    }
}
