<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class RefDriver extends Model
{
    protected $table = "rice_ref_driver";
    protected $primaryKey = "driver_id";
    protected $fillable = [
        "driver_id",
        "name",
        "sim_num",
        "phone_num",
        "address",
        "active",
    ];
}
