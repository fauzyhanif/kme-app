<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class RefCar extends Model
{
    protected $table = 'rice_ref_car';
    protected $primaryKey = "car_id";
    protected $fillable = [
        "car_id",
        "police_num",
        "owner",
        "brand",
        "type",
        "color",
        "framework_num",
        "machine_num",
        "bpkb_num",
        "kir_date",
        "stnk_tax_date",
        "active",
    ];
}
