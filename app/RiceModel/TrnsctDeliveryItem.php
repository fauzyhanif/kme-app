<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctDeliveryItem extends Model
{
    protected $table = "rice_trnsct_delivery_item";
    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "delivery_id",
        "product_id",
        "price",
        "begin_qty",
        "sales_qty",
        "sales_end",
        "sales_total",
        "end_qty"
    ];

    public function product()
    {
        return $this->belongsTo(RefProduct::class, 'product_id', 'product_id');
    }
}
