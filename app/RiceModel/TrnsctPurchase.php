<?php

namespace App\RiceModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctPurchase extends Model
{
    protected $table = "rice_trnsct_purchase";
    protected $primaryKey = "purchase_id";
    protected $fillable = [
        "purchase_id",
        "purchase_date",
        "pj",
        "farmer_name",
        "farmer_phone",
        "farmer_address",
        "agent_name",
        "agent_phone",
    ];

    public function warehouse()
    {
        return $this->belongsTo(RefWarehouse::class, 'warehouse_id', 'warehouse_id');
    }
}
