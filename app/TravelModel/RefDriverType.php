<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefDriverType extends Model
{
    protected $table = "trvl_ref_driver_type";
    protected $primaryKey = "type_id";
    protected $fillable = [
        "type_id",
        "name"
    ];
}
