<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefDriverSaving extends Model
{
    protected $table = "trvl_ref_driver_saving";
    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "type",
        "driver_id",
        "booking_id",
        "trnsct_date",
        "information",
        "in",
        "out",
        "create_manual"
    ];

    public function driver()
    {
        return $this->belongsTo(RefDriver::class, 'user_id', 'driver_id');
    }

    public function helper()
    {
        return $this->belongsTo(RefHelper::class, 'user_id', 'helper_id');
    }
}
