<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefPaymentMethod extends Model
{
    protected $table = "trvl_ref_payment_method";
    protected $primaryKey = "payment_method_id";
    protected $fillable = ["payment_method_id", "name"];
}
