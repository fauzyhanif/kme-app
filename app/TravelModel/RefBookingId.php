<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefBookingId extends Model
{
    protected $table = "trvl_ref_booking_id";
    protected $fillable = [
        "id",
        "month",
        "serial_num",
    ];
}
