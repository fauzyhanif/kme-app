<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefFinanceAccountGroup extends Model
{
    protected $table = "trvl_ref_finance_account_group";
    protected $primaryKey = 'group_id';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        "group_id",
        "name",
        "active"
    ];
}
