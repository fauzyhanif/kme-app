<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;

class TrnsctDebt extends Model
{
    protected $table = "trvl_trnsct_debt";
    protected $fillable = [
        "id",
        "sdm_type",
        "sdm_id",
        "date",
        "amount",
        "amount_paid",
        "desc",
        "account_id",
        "status",
        "admin",
    ];

    public function sdm()
    {
        return $this->belongsTo(\App\SystemModel\SysRefSdm::class, 'sdm_id', 'sdm_id');
    }

    public function payment()
    {
        return $this->hasMany(\App\TravelModel\TrnsctDebtPayment::class, 'debt_id', 'id')->where('status', 'SUCCESS');
    }

    public function cashAccount()
    {
        return $this->belongsTo(\App\TravelModel\RefFinanceAccount::class, 'account_id', 'account_id');
    }

    public static function addNew($post)
    {
        $new = new \App\TravelModel\TrnsctDebt();
        $new->sdm_type = 'SDM';
        $new->sdm_id = explode('-', $post['sdm_id'])[0];
        $new->date = $post['date'];
        $new->amount = str_replace('.', '', $post['amount']);
        $new->desc = $post['desc'];
        $new->account_id = $post['account_id'];
        $new->admin = Session::get('auth_nama');

        if ($new->save()) {
            parent::addNewJurnal($post, $new->id);
        }
    }

    public static function addNewJurnal($post, $id)
    {
        $proof_id = \App\TravelModel\TrnsctJurnal::getProofId();

        // CASH ACCOUNT
        $cashAccount = new \App\TravelModel\TrnsctJurnal();
        $cashAccount->proof_id = $proof_id;
        $cashAccount->trnsct_date = $post['date'];
        $cashAccount->related_person = explode('-', $post['sdm_id'])[1];
        $cashAccount->account_id = $post['account_id'];
        $cashAccount->description = $post['desc'];
        $cashAccount->credit = str_replace('.', '', $post['amount']);
        $cashAccount->debit = 0;
        $cashAccount->type = "OUT";
        $cashAccount->is_main = "N";
        $cashAccount->debt_id = $id;
        $cashAccount->user = Session::get('auth_nama');
        $cashAccount->save();

        // SECOND ACCOUNT
        $secondAccount = new \App\TravelModel\TrnsctJurnal();
        $secondAccount->proof_id = $proof_id;
        $secondAccount->trnsct_date = $post['date'];
        $secondAccount->related_person = explode('-', $post['sdm_id'])[1];
        $secondAccount->account_id = $post['kasbon_account'];
        $secondAccount->description = $post['desc'];
        $secondAccount->debit = str_replace('.', '', $post['amount']);
        $secondAccount->credit = 0;
        $secondAccount->type = "OUT";
        $secondAccount->is_main = "Y";
        $secondAccount->debt_id = $id;
        $secondAccount->user = Session::get('auth_nama');
        $secondAccount->save();
    }

    public static function pembatalan($id)
    {
        $data = parent::findOrFail($id);
        $data->status = 'CANCEL';
        $data->save();
    }

    public static function updatePaid($id, $jml)
    {
        $data = parent::findOrFail($id);
        $data->amount_paid = $data->amount_paid + $jml;
        $data->save();
    }

    public static function updatePaidSubtraction($id, $jml)
    {
        $data = parent::findOrFail($id);
        $data->amount_paid = $data->amount_paid - $jml;
        $data->save();
    }

    public static function getDataJson($status)
    {
        if ($status == 1) { // piutang baru
            $datas = parent::listDataBaru();
        } elseif ($status == 2) { // sebagian bayar
            $datas = parent::listDataSebagianBayar();
        } elseif ($status == 3) { // selesai
            $datas = parent::listDataSelesai();
        } elseif ($status == 4) { // dibatalkan
            $datas = parent::listDataBatal();
        }

        return DataTables::of($datas)
            ->addColumn('date', function($datas)
            {
                $text = date("d/m/Y", strtotime($datas->date));
                return $text;
            })
            ->addColumn('jml_kasbon', function($datas)
            {
                return number_format($datas->amount,0,',','.');;
            })
            ->addColumn('sisa', function($datas)
            {
                return number_format($datas->amount - $datas->amount_paid,0,',','.');;
            })
            ->addColumn('actions_link', function($datas)
            {
                $btn = '';
                if ($datas->status != 'CANCEL') {
                    // edits link
                    $btn .= "<a href='" . route('travel.kasbon.form_pembayaran', $datas->id) . "' class='btn btn-info btn-xs'>";
                    $btn .= "<i class='fas fa-edit'></i> Bayar";
                    $btn .= "</a> ";

                    if ($datas->amount_paid == '0') {
                        $btn .= "<button onclick=\"modalCancel('$datas->id')\"";
                        $btn .= "class='btn btn-danger btn-xs'>";
                        $btn .= "<i class='fas fa-times' style='padding: 3px'></i> Batalkan";
                        $btn .= "</button> ";
                    }
                }

                return $btn;
            })
            ->rawColumns(['actions_link', 'date', 'sisa', 'jml_kasbon'])
            ->toJson();
    }

    public static function listDataBaru()
    {
        $datas = parent::with('sdm')
            ->where('amount_paid', "=", '0')
            ->where('status', 'SUCCESS')
            ->orderBy('date', 'ASC')
            ->get();

        return $datas;
    }

    public static function listDataSebagianBayar()
    {
        $datas = parent::with('sdm')
            ->where('amount_paid', ">", '0')
            ->where('amount_paid', "<", DB::raw('amount'))
            ->where('status', 'SUCCESS')
            ->orderBy('date', 'ASC')
            ->get();

        return $datas;
    }

    public static function listDataSelesai()
    {
        $datas = parent::with('sdm')
            ->where('amount', DB::raw('amount_paid'))
            ->where('status', 'SUCCESS')
            ->orderBy('date', 'ASC')
            ->get();

        return $datas;
    }

    public static function listDataBatal()
    {
       $datas = parent::with('sdm')
            ->where('status', 'CANCEL')
            ->orderBy('date', 'ASC')
            ->get();

        return $datas;
    }
}
