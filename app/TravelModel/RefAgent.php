<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefAgent extends Model
{
    protected $table = "trvl_ref_agent";
    protected $primaryKey = "agent_id";
    protected $fillable = [
        "agent_id",
        "agent_type",
        "name",
        "phone_num",
        "address",
        "ttl_trnsct",
        "ttl_commission",
        "active",
    ];
}
