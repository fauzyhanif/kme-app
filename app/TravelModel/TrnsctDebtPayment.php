<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TrnsctDebtPayment extends Model
{
    protected $table = "trvl_trnsct_debt_payment";
    protected $fillable = [
        "id",
        "debt_id",
        "account_id",
        "amount",
        "payment_date",
        "status",
    ];

    public function debt()
    {
        return $this->belongsTo(\App\TravelModel\TrnsctDebt::class, 'debt_id', 'id');
    }

    public function cashAccount()
    {
        return $this->belongsTo(\App\TravelModel\RefFinanceAccount::class, 'account_id', 'account_id');
    }

    public function payrolPayment()
    {
        return $this->belongsTo(\App\TravelModel\TrnsctPayrolPayment::class, 'payrol_payment_id', 'id');
    }

    public static function addNew($post)
    {
        $data = new \App\TravelModel\TrnsctDebtPayment();
        $data->debt_id = $post['debt_id'];
        $data->account_id = $post['cash_account'];
        $data->amount = str_replace('.', '',$post['kasbon_amount']);
        $data->payment_date = $post['payment_date'];
        $data->payrol_payment_id = (isset($post['payrol_payment_id'])) ? $post['payrol_payment_id'] : '';
        $data->status = 'SUCCESS';
        $data->admin = Session::get('auth_nama');

        if ($data->save()) {
            parent::addNewJurnal($post, str_replace('.', '',$post['kasbon_amount']), $data->id);

            // update table debt kolom total_paid
            \App\TravelModel\TrnsctDebt::updatePaid($data->debt_id, $data->amount);
        }
    }

    public static function addNewJurnal($post, $jml, $debt_payment_id)
    {
        $proof_id = \App\TravelModel\TrnsctJurnal::getProofId();

        // CASH ACCOUNT
        $cashAccount = new \App\TravelModel\TrnsctJurnal();
        $cashAccount->proof_id = $proof_id;
        $cashAccount->trnsct_date = $post['payment_date'];
        $cashAccount->related_person = $post['related_person'];
        $cashAccount->account_id = $post['cash_account'];
        $cashAccount->description = "Angsuran kasbon " . $post['related_person'];
        $cashAccount->debit = $jml;
        $cashAccount->credit = 0;
        $cashAccount->type = "OUT";
        $cashAccount->is_main = "N";
        $cashAccount->debt_payment_id = $debt_payment_id;
        $cashAccount->user = Session::get('auth_nama');
        $cashAccount->save();

        // SECOND ACCOUNT
        $secondAccount = new \App\TravelModel\TrnsctJurnal();
        $secondAccount->proof_id = $proof_id;
        $secondAccount->trnsct_date = $post['payment_date'];
        $secondAccount->related_person = $post['related_person'];
        $secondAccount->account_id = $post['kasbon_account'];
        $secondAccount->description = "Angsuran kasbon " . $post['related_person'];
        $secondAccount->credit = $jml;
        $secondAccount->debit = 0;
        $secondAccount->type = "OUT";
        $secondAccount->is_main = "Y";
        $secondAccount->debt_payment_id = $debt_payment_id;
        $secondAccount->user = Session::get('auth_nama');
        $secondAccount->save();
    }

    public static function pembatalan($id)
    {
        // cek dulu ada atau ngga
        $check = parent::findOrFail($id);
        $check->status = 'CANCEL';
        $check->save();

        // cancel jurnal
        \App\TravelModel\TrnsctJurnal::where('debt_payment_id', "$id")->update(["status" => "BATAL"]);

        // kurangi amount_paid table debt
        \App\TravelModel\TrnsctDebt::updatePaidSubtraction($check->debt_id, $check->amount);
    }

    public static function cancelPaymentFromPayrol($payrol_payment_id)
    {
        // cek dulu ada atau ngga
        $check = parent::where('payrol_payment_id', "$payrol_payment_id")->get();

        if ($check->count() == 1) {
            parent::where('payrol_payment_id', "$payrol_payment_id")->update(["status" => "CANCEL"]);

            // cancel jurnal
            $id = $check[0]->id;
            $debt_id = $check[0]->debt_id;
            \App\TravelModel\TrnsctJurnal::where('debt_payment_id', "$id")->update(["status" => "BATAL"]);

            // kurangi amount_paid table debt
            \App\TravelModel\TrnsctDebt::updatePaidSubtraction($debt_id, $check[0]->amount);
        }
    }
}
