<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctSpj extends Model
{
    protected $table = "trvl_trnsct_spj";
    protected $primaryKey = "spj_id";
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        // "id",
        "spj_id",
        "booking_id",
        "police_num",
        "category_id",
        "main_driver",
        "second_driver",
        "helper_id",
        "premi_main_driver",
        "premi_second_driver",
        "premi_helper",
        "km_start",
        "km_end",
        "admin",
        "status",
    ];

    public function booking()
    {
        return $this->belongsTo(TrnsctBooking::class, 'booking_id', 'booking_id');
    }

    public function mainDriver()
    {
        return $this->belongsTo(RefDriver::class, 'main_driver', 'driver_id');
    }

    public function secondDriver()
    {
        return $this->belongsTo(RefDriver::class, 'second_driver', 'driver_id');
    }

    public function helper()
    {
        return $this->belongsTo(RefHelper::class, 'helper_id', 'helper_id');
    }

    public function carCategory()
    {
        return $this->belongsTo(RefCarCategory::class, 'category_id', 'category_id');
    }

}
