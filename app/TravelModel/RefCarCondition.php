<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefCarCondition extends Model
{
    protected $table = 'trvl_ref_car_condition';
    protected $fillable = [
        "id",
        "name"
    ];

    public function car()
    {
        return $this->hasMany(RefCar::class, 'id', 'condition_id');
    }
}
