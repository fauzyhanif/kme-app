<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefFinanceAccount extends Model
{
    protected $table = "trvl_ref_finance_account";
    protected $fillable = [
        "group_id",
        "is_parent",
        "parent",
        "account_id",
        "name",
        "post_report",
        "post_sub_report",
        "cash_flow_group",
        "debit",
        "credit",
        "active"
    ];

    public function group()
    {
        return $this->belongsTo(RefFinanceAccountGroup::class, 'group_id', 'group_id');
    }
}
