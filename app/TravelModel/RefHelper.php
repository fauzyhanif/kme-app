<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefHelper extends Model
{
    protected $table = "trvl_ref_helper";
    protected $primaryKey = "helper_id";
    protected $fillable = [
        "helper_id",
        "name",
        "phone_num",
        "address",
        "ttl_trnsct",
        "ttl_saving",
        "active",
    ];
}
