<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TrnsctpayrolPayment extends Model
{
    protected $table = "trvl_trnsct_payrol_payment";
    protected $fillable = [
        "id",
        "payrol_id",
        "account_id",
        "amount",
        "payment_date",
        "status",
    ];

    public function account()
    {
        return $this->belongsTo(\App\TravelModel\RefFinanceAccount::class, 'account_id', 'account_id');
    }

    public function payrol()
    {
        return $this->belongsTo(\App\TravelModel\TrnsctPayrol::class, 'payrol_id', 'id');
    }

    public function debtPayment()
    {
        return $this->belongsTo(\App\TravelModel\TrnsctDebtPayment::class, 'id', 'payrol_payment_id');
    }

    public static function addNew($post)
    {
        $payrol = \App\TravelModel\TrnsctPayrol::findOrFail($post['payrol_id']);
        $gaji = $payrol->total - $payrol->total_paid;
        $sisaGaji = $gaji - $post['total_kasbon_bayar'];

        $data = new \App\TravelModel\TrnsctPayrolPayment();
        $data->payrol_id = $post['payrol_id'];
        $data->account_id = $post['cash_account'];
        $data->amount = $sisaGaji;
        $data->payment_date = $post['payment_date'];
        $data->status = 'SUCCESS';
        $data->admin = Session::get('auth_nama');

        if ($data->save()) {
            // add new jurnal
            parent::addNewJurnal($post, $sisaGaji);

            // update paid
            \App\TravelModel\TrnsctPayrol::updatePaid($post['payrol_id'], $sisaGaji);

            // cek apakah ada bayar kasbon
            if ($post['total_kasbon_bayar'] != '0') {

                // jika ada maka get total kasbon yg dibayar dan masukkan ke potongan kasbon table payrol
                \App\TravelModel\TrnsctPayrol::updatePotonganKasbon($post['payrol_id'], $post['total_kasbon_bayar']);

                // looping
                foreach ($post['item_kasbon'] as $key => $value) {

                    // insert table debt_payment, sisipkan value payrol_payment_id
                    $post['debt_id'] = $key;
                    $post['kasbon_amount'] = $value;
                    $post['payrol_payment_id'] = $data->id;
                    \App\TravelModel\TrnsctDebtPayment::addNew($post);

                }
            }
        }

    }

    public static function addNewJurnal($post, $gaji)
    {
        $proof_id = \App\TravelModel\TrnsctJurnal::getProofId();

        // CASH ACCOUNT
        $cashAccount = new \App\TravelModel\TrnsctJurnal();
        $cashAccount->proof_id = $proof_id;
        $cashAccount->trnsct_date = $post['payment_date'];
        $cashAccount->related_person = $post['related_person'];
        $cashAccount->account_id = $post['cash_account'];
        $cashAccount->description = $post['desc'];
        $cashAccount->credit = $gaji;
        $cashAccount->debit = 0;
        $cashAccount->type = "OUT";
        $cashAccount->is_main = "N";
        $cashAccount->payrol_id = $post['payrol_id'];
        $cashAccount->user = Session::get('auth_nama');
        $cashAccount->save();

        // SECOND ACCOUNT
        $secondAccount = new \App\TravelModel\TrnsctJurnal();
        $secondAccount->proof_id = $proof_id;
        $secondAccount->trnsct_date = $post['payment_date'];
        $secondAccount->related_person = $post['related_person'];
        $secondAccount->account_id = $post['operasional_account'];
        $secondAccount->description = $post['desc'];
        $secondAccount->debit = $gaji;
        $secondAccount->credit = 0;
        $secondAccount->type = "OUT";
        $secondAccount->is_main = "Y";
        $secondAccount->payrol_id = $post['payrol_id'];
        $secondAccount->user = Session::get('auth_nama');
        $secondAccount->save();
    }

    public static function pembatalan($post)
    {
        $data = parent::findOrFail($post['payrol_payment_id']);
        $data->status = 'CANCEL';


        if ($data->save()) {
            \App\TravelModel\TrnsctPayrol::updatePaidSubtraction($data->payrol_id, $data->amount);

            // cancel debt payment
            \App\TravelModel\TrnsctDebtPayment::cancelPaymentFromPayrol($post['payrol_payment_id']);

            // cancel jurnal
            \App\TravelModel\TrnsctJurnal::where('payrol_id', "$data->payrol_id")->update(["status" => "BATAL"]);
        }
    }
}
