<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefPaymentId extends Model
{
    protected $table = "trvl_ref_payment_id";
    protected $fillable = [
        "id",
        "month",
        "serial_num",
    ];
}
