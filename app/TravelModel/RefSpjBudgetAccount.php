<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefSpjBudgetAccount extends Model
{
    protected $table = "trvl_ref_spj_budget_account";
    protected $primaryKey = "account_id";
    protected $fillable = [
        "account_id",
        "name",
        "active",
    ];
}
