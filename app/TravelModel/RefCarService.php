<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefCarService extends Model
{
    protected $table = "trvl_ref_car_service";
    protected $fillable = [
        "id",
        "police_num",
        "repair_date",
        "repair_place",
        "information",
    ];
}
