<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TrnsctPayrol extends Model
{
    protected $table = "trvl_trnsct_payrol";
    protected $fillable = [
        "id",
        "sdm_type",
        "sdm_id",
        "year",
        "month",
        "upah_bulanan",
        "upah_lainlain",
        "potongan_kasbon",
        "potongan_lainlain",
        "total",
        "total_paid",
        "status",
    ];

    public function sdm()
    {
        return $this->belongsTo(\App\SystemModel\SysRefSdm::class, 'sdm_id', 'sdm_id');
    }

    public static function getData($year, $month)
    {
        return DB::table('trvl_trnsct_payrol as a')
            ->leftJoin('sys_ref_sdm as b', function($join){
                $join->where('a.sdm_type', '=', 'SDM');
                $join->on('a.sdm_id', '=', 'b.sdm_id');
            })
            ->leftJoin('trvl_ref_driver as c', function($join){
                $join->where('a.sdm_type', '=', 'DRIVER');
                $join->on('a.sdm_id', '=', 'c.driver_id');
            })
            ->leftJoin('trvl_ref_helper as d', function($join){
                $join->where('a.sdm_type', '=', 'HELPER');
                $join->on('a.sdm_id', '=', 'd.helper_id');
            })
            ->leftJoin('sys_ref_sdm_position as e', function($join){
                $join->where('a.sdm_type', '=', 'SDM');
                $join->on('b.module_id', '=', 'e.module_id');
                $join->on('b.position_id', '=', 'e.position_id');
            })
            ->select(
                'a.id', 'a.sdm_id', 'a.sdm_type', 'a.upah_bulanan', 'a.upah_lainlain', 'a.potongan_kasbon', 'a.potongan_lainlain', 'a.total_paid',
                'a.total', 'b.name as sdm_name', 'c.name as driver_name', 'd.name as helper_name', 'e.name as position_name'
            )
            ->where('a.month', "$month")
            ->where('a.year', "$year")
            ->get();
    }

    public static function addNew($post)
    {
        $sdm = \App\SystemModel\SysRefSdm::with(['position'])->where('active', 'Y')->get();

        foreach ($sdm as $key => $value) {

            $checExist = parent::checkExist($value->sdm_id, "SDM", $post['year'], $post['month']);
            if ($checExist == false) {
                $data = new \App\TravelModel\TrnsctPayrol();
                $data->sdm_type = "SDM";
                $data->sdm_id = $value->sdm_id;
                $data->year = $post['year'];
                $data->month = $post['month'];
                $data->upah_bulanan = $value->position->wages;
                $data->upah_lainlain = 0;
                $data->potongan_kasbon = 0;
                $data->potongan_lainlain = 0;
                $data->total = $value->position->wages;
                $data->total_paid = 0;
                $data->status = 'SUCCESS';
                $data->save();
            }

        }

    }

    public static function checkExist($sdm_id, $sdm_type, $year, $month)
    {
        $data = \App\TravelModel\TrnsctPayrol::where("sdm_type", "$sdm_type")
            ->where("sdm_id", "$sdm_id")
            ->where("year", "$year")
            ->where("month", "$month")
            ->get();

        $res = false;
        if ($data->count() > 0) {
            $res = true;
        }

        return $res;

    }

    public static function updatePaid($payrol_id, $jml_cair)
    {
        $data = parent::findOrFail($payrol_id);
        $data->total_paid = $data->total_paid + $jml_cair;
        $data->save();
    }

    public static function updatePaidSubtraction($payrol_id, $jml)
    {
        $data = parent::findOrFail($payrol_id);
        $data->total_paid = 0;
        $data->potongan_kasbon = 0;
        $data->total = $data->upah_bulanan;
        $data->save();
    }

    public static function updatePotonganKasbon($payrol_id, $jml)
    {
        $data = parent::findOrFail($payrol_id);
        $data->potongan_kasbon = $jml;
        $data->total = $data->upah_bulanan - $jml;
        $data->save();
    }
}
