<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctSpjBudget extends Model
{
    protected $table = "trvl_trnsct_spj_budget";
    protected $primaryKey = "spjbudget_id";
    protected $fillable = [
        "spjbudget_id",
        "spj_id",
        "account_id",
        "qty",
        "duration",
        "amount",
        "ttl_amount",
        "information",
        "is_saving",
    ];

    public function budgetAccount()
    {
        return $this->belongsTo(RefSpjBudgetAccount::class, 'account_id', 'account_id');
    }
}
