<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctBooking extends Model
{
    protected $table = "trvl_trnsct_booking";
    protected $primaryKey = "booking_id";
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        "trnsct_date",
        "booking_id",
        "cust_id",
        "cust_name",
        "cust_phone_num",
        "cust_address",
        "booking_start_date",
        "booking_end_date",
        "date_not_for_sale",
        "destination",
        "pick_up_location",
        "standby_time",
        "information",
        "booking_from",
        "agent_id",
        "agent_commission",
        "agent_commission_paid",
        "st_commission",
        "ttl_unit",
        "ttl_trnsct",
        "ttl_trnsct_paid",
        "charge",
        "discount",
        "status",
    ];

    public function customer()
    {
        return $this->belongsTo(RefCustomer::class, 'cust_id', 'cust_id');
    }

    public function agent()
    {
        return $this->belongsTo(RefAgent::class, 'agent_id', 'agent_id');
    }

    public function cars()
    {
        return $this->hasMany(TrnsctBookingDtl::class, 'booking_id', 'booking_id');
    }
}
