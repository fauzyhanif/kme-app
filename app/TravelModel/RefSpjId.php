<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefSpjId extends Model
{
    protected $table = "trvl_ref_spj_id";
    protected $fillable = [
        "id",
        "mount",
        "serial_num",
    ];
}
