<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefDriver extends Model
{
  protected $table = "trvl_ref_driver";
  protected $primaryKey = "driver_id";
  protected $fillable = [
    "driver_id",
    "name",
    "sim_num",
    "address",
    "driver_type",
    "police_num",
    "car_permit",
    "ttl_trnsct",
    "ttl_saving",
    "active",
  ];

  public function car()
  {
    return $this->belongsTo(RefCar::class, 'police_num', 'police_num');
  }
}