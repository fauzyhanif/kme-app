<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctPayment extends Model
{
    protected $table = "trvl_trnsct_payment";
    protected $fillable = [
        "id",
        "payment_id",
        "booking_id",
        "account_id",
        "payment_date",
        "received_from",
        "amount",
        "payment_method",
        "information",
        "type",
        "is_canceled",
        "admin",
    ];

    public function booking()
    {
        return $this->belongsTo(TrnsctBooking::class, 'booking_id', 'booking_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(RefPaymentMethod::class, 'payment_method', 'payment_method_id');
    }

    public function coa()
    {
        return $this->belongsTo(RefFinanceAccount::class, 'payment_method', 'account_id');
    }
}
