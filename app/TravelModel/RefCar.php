<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefCar extends Model
{
    protected $table = "trvl_ref_car";
    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "category_id",
        "conditions_id",
        "police_num",
        "owner",
        "owners_address",
        "brand",
        "type",
        "color",
        "raft_year",
        "purchase_year",
        "machine_cc",
        "framework_num",
        "machine_num",
        "bpkb_num",
        "nominal_tax",
        "kir_date",
        "kps_permit_date",
        "insurance_date",
        "stnk_tax_date",
        "active",
    ];

    public function carCategory()
    {
        return $this->belongsTo(RefCarCategory::class, 'category_id', 'category_id');
    }

    public function carCondition()
    {
        return $this->belongsTo(RefCarCondition::class, 'condition_id', 'id');
    }

    public function driver()
    {
        return $this->hasMany(RefDriver::class, 'police_num', 'police_num');
    }
}
