<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctBookingDtl extends Model
{
  protected $table = "trvl_trnsct_booking_dtl";
  protected $primaryKey = "trnsctdtl_id";
  protected $fillable = [
    "trnsctdtl_id",
    "booking_id",
    "booking_start_date",
    "booking_end_date",
    "date_not_for_sale",
    "category_id",
    "unit_qty",
    "unit_qty_spj",
    "price",
    "total",
    "status",
  ];

  public function carCategory()
  {
    return $this->belongsTo(RefCarCategory::class, 'category_id', 'category_id');
  }
}
