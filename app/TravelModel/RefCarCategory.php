<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefCarCategory extends Model
{
    protected $table = "trvl_ref_car_category";
    protected $fillable = [
        "category_id",
        "name",
        "seat",
        "active",
    ];

    public function car()
    {
      return $this->oneToMany(RefCar::class, 'category_id', 'category_id');
    }

}