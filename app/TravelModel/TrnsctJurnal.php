<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class TrnsctJurnal extends Model
{
    protected $table = "trvl_trnsct_jurnal";
    protected $primaryKey = "jurnal_id";
    protected $fillable = [
        "jurnal_id",
        "proof_id",
        "trnsct_date",
        "related_person",
        "account_id",
        "description",
        "debit",
        "credit",
        "type",
        "trnsct_type",
        "post_net_profit",
        "is_main",
        "user",
        "spj_id",
        "payrol_id",
        "debt_id",
        "debt_payment_id"
    ];

    public function coa()
    {
        return $this->belongsTo(RefFinanceAccount::class, 'account_id', 'account_id');
    }

    public static function getProofId()
    {
        $thisMonth = date('Y-m');
        $paymentID = 'INV';

        $lastPaymentID = \App\TravelModel\RefPaymentId::where('month', '=', "$thisMonth")->first();

        // check exist or not
        if ( $lastPaymentID === null ) {
            $orderId = "0001";

            $new = new \App\TravelModel\RefPaymentId();
            $new->month = "$thisMonth";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $orderId = sprintf('%04d', $lastPaymentID->serial_num);
            $update = \App\TravelModel\RefPaymentId::where('month', '=', "$thisMonth")->update(["serial_num" => $orderId + 1]);
        }

        $paymentID .= str_replace("-", "", $thisMonth) . $orderId;
        return $paymentID;
    }
}
