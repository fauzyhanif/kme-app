<?php

namespace App\TravelModel;

use Illuminate\Database\Eloquent\Model;

class RefCustomer extends Model
{
    protected $table = "trvl_ref_customer";
    protected $primaryKey = "cust_id";
    protected $fillable = [
        "cust_id",
        "name",
        "phone_num",
        "address",
        "ttl_trnsct",
        "ttl_payment",
        "active",
    ];

}
