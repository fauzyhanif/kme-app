<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use App\Helpers\GeneralHelper;
use Illuminate\Http\Request;
use Redirect,Response,DB;
use Illuminate\Support\Facades\File;
use DataTables;

class RefCarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.RefCar.index');
    }

    public function jsonData()
    {
        $cars = \App\TravelModel\RefCar::with(['carCategory', 'carCondition'])->get();
        return DataTables::of($cars)
            ->editColumn('stnk_tax_date', function($cars)
            {
                $result = "";
                if ($cars->stnk_tax_date != "") {
                    $result = date_format(date_create($cars->stnk_tax_date), 'd/m/Y');
                }

                return $result;
            })
            ->addColumn('actions_link', function($cars)
            {
                // details link
                $btn = "<a href='" . route('travel.kendaraan.detail', $cars->id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                // edits link
                $btn .= "<a href='" . route('travel.kendaraan.formedit', $cars->id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function detail($id)
    {
        $car = \App\TravelModel\RefCar::findOrFail($id);

        return view('travel.RefCar.detail', \compact('car'));
    }

    public function formAdd()
    {
        $categories = \App\TravelModel\RefCarCategory::where('active', 'Y')->get();
        $conditions = \App\TravelModel\RefCarCondition::all();
        return view('travel.RefCar.formAdd', \compact('categories', 'conditions'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'category_id'=>'required',
            'police_num'=>'required|unique:trvl_ref_car',
            'condition_id'=>'required',
            'owner'=>'required',
            'bpkb_num'=>'required',
            'nominal_tax'=>'required',
            'color'=>'required',
            'stnk_tax_date'=>'required',
        ]);

        $new = new \App\TravelModel\RefCar();
        $new->category_id = $request->get('category_id');
        $new->police_num = $request->get('police_num');
        $new->condition_id = $request->get('condition_id');
        $new->owner = $request->get('owner');
        $new->owners_address = $request->get('owners_address');
        $new->bpkb_num = $request->get('bpkb_num');
        $new->nominal_tax = str_replace(".", "", $request->get('nominal_tax'));
        $new->brand = $request->get('brand');
        $new->type = $request->get('type');
        $new->color = $request->get('color');
        $new->raft_year = $request->get('raft_year');
        $new->purchase_year = $request->get('purchase_year');
        $new->machine_cc = $request->get('machine_cc');
        $new->framework_num = $request->get('framework_num');
        $new->machine_num = $request->get('machine_num');
        $new->kir_date = $request->get('kir_date');
        $new->kps_permit_date = $request->get('kps_permit_date');
        $new->insurance_date = $request->get('insurance_date');
        $new->stnk_tax_date = $request->get('stnk_tax_date');
        $new->save();
        $response = [
            "type" => "success",
            "text" => " Kendaraan berhasil disimpan"
        ];

        return response()->json($response);
    }

    public function formEdit($id)
    {
        $car = \App\TravelModel\RefCar::findOrFail($id);
        $categories = \App\TravelModel\RefCarCategory::where('active', 'Y')->get();
        $conditions = \App\TravelModel\RefCarCondition::all();
        return view('travel.RefCar.formEdit', \compact("categories", "car", "conditions"));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'police_num'=>'required|max:10|unique:trvl_ref_car,police_num,' . $request->get('police_num') . ',police_num',
            'category_id'=>'required',
            'condition_id'=>'required',
            'owner'=>'required',
            'bpkb_num'=>'required',
            'nominal_tax'=>'required',
            'brand'=>'required',
            'color'=>'required',
            'machine_cc'=>'required',
            'framework_num'=>'required',
            'machine_num'=>'required',
            'stnk_tax_date'=>'required',
        ]);

        $old = \App\TravelModel\RefCar::find($id);
        $old->category_id = $request->get('category_id');
        $old->police_num = $request->get('police_num');
        $old->condition_id = $request->get('condition_id');
        $old->owner = $request->get('owner');
        $old->owners_address = $request->get('owners_address');
        $old->bpkb_num = $request->get('bpkb_num');
        $old->nominal_tax = str_replace(".", "", $request->get('nominal_tax'));
        $old->brand = $request->get('brand');
        $old->type = $request->get('type');
        $old->color = $request->get('color');
        $old->raft_year = $request->get('raft_year');
        $old->purchase_year = $request->get('purchase_year');
        $old->machine_cc = $request->get('machine_cc');
        $old->framework_num = $request->get('framework_num');
        $old->machine_num = $request->get('machine_num');
        $old->kir_date = $request->get('kir_date');
        $old->kps_permit_date = $request->get('kps_permit_date');
        $old->insurance_date = $request->get('insurance_date');
        $old->stnk_tax_date = $request->get('stnk_tax_date');
        $old->update();
        $response = [
            "type" => "success",
            "text" => " Kendaraan berhasil diubah."
        ];

        return response()->json($response);
    }

    public function addService(Request $request)
    {
        $new = new \App\TravelModel\RefCarService();
        $new->police_num = $request['police_num'];
        $new->repair_date = $request['repair_date'];
        $new->repair_place = $request['repair_place'];
        $new->information = $request['information'];
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function getAllTrnsct(Request $request)
    {
        $post = $request->all();
        $column = array_keys($post)[0];
        $value = $post[$column];

        $result = \App\TravelModel\TrnsctSpj::groupBy('police_num')
            ->select(DB::raw('count(*) as total'))
            ->where("$column", "$value")
            ->get();

        return (count($result) > 0) ? $result[0]->total : 0;
    }

    public function getFiveCarHistory(Request $request)
    {
        $post = $request->all();
        $column = array_keys($post)[0];
        $value = $post[$column];

        $histories = \App\TravelModel\TrnsctSpj::with('booking')
                    ->where("$column", "$value")
                    ->orderBy('booking_id', 'DESC')
                    ->limit(5)
                    ->get();

        return view('travel.RefCar.displayCarHistory', \compact("histories"));
    }

    public function allCarHistory($car_id)
    {
        $car = \App\TravelModel\RefCar::where("id", $car_id)
                ->with(["carCategory"])
                ->first();

        return view('travel.RefCar.allCarHistory', \compact("car"));
    }

    public function getAllCarHistoryDataJson(Request $request)
    {
        $police_num = $request['police_num'];
        $histories = \App\TravelModel\TrnsctSpj::with('booking')
                    ->where("police_num", "$police_num")
                    ->orderBy('booking_id', 'DESC')
                    ->get();

        return DataTables::of($histories)
            ->addColumn('booking_date', function($histories)
            {
                $result = GeneralHelper::konversiTgl($histories->booking->booking_start_date, 'slash');

                return $result;
            })
            ->toJson();
    }

    public function getFiveCarServiceData(Request $request)
    {
        $police_num = $request['police_num'];
        $services = \App\TravelModel\RefCarService::where("police_num", "$police_num")
                    ->orderBy('repair_date', 'DESC')
                    ->limit(5)
                    ->get();
        return view('travel.RefCar.displayCarServiceData', \compact("services"));
    }

    public function allCarService($car_id)
    {
        $car = \App\TravelModel\RefCar::where("id", $car_id)
                ->with(["carCategory"])
                ->first();

        return view('travel.RefCar.allCarService', \compact("car"));
    }

    public function getAllCarServiceDataJson(Request $request)
    {
        $police_num = $request['police_num'];
        $services = \App\TravelModel\RefCarService::where("police_num", "$police_num")
                    ->orderBy('repair_date', 'DESC')
                    ->get();
        return DataTables::of($services)
            ->editColumn('repair_date', function($services)
            {
                $result = GeneralHelper::konversiTgl($services->repair_date, 'slash');

                return $result;
            })
            ->toJson();
    }

}
