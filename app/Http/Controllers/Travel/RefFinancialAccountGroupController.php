<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class RefFinancialAccountGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.RefFinancialAccountGroup.index');
    }

    public function jsonData()
    {
        $groups = \App\TravelModel\RefFinanceAccountGroup::all();
        return DataTables::of($groups)
            ->addColumn('actions_link', function($groups)
            {
                // edits link
                $btn = "<a href='" . route('travel.finance.account-group.formedit', $groups->group_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formAdd()
    {
        return view('travel.RefFinancialAccountGroup.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'group_id'=>'required',
            'name'=>'required',
        ]);

        $new = new \App\TravelModel\RefFinanceAccountGroup();
        $new->group_id = $request->get('group_id');
        $new->name = $request->get('name');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $group = \App\TravelModel\RefFinanceAccountGroup::findOrFail($id);

        return view('travel.RefFinancialAccountGroup.formEdit', \compact('group'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'group_id'=>'required',
            'name'=>'required',
        ]);

        $new = \App\TravelModel\RefFinanceAccountGroup::findOrFail($id);
        $new->group_id = $request->get('group_id');
        $new->name = $request->get('name');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
