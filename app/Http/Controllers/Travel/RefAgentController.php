<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class RefAgentController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    return view('travel.RefAgent.index');
  }

  public function jsonData()
  {
    $agents = \App\TravelModel\RefAgent::all();
    return DataTables::of($agents)
      ->addColumn('actions_link', function($agents)
      {
        // details link
        $btn = "<a href='" . route('travel.agent.detail', $agents->agent_id) . "' class='btn btn-info btn-xs'>";
        $btn .= "<i class='fas fa-eye'></i> Detail";
        $btn .= "</a> ";

        // edits link
        $btn .= "<a href='" . route('travel.agent.formedit', $agents->agent_id) . "' class='btn btn-primary btn-xs'>";
        $btn .= "<i class='fas fa-edit'></i> Edit";
        $btn .= "</a> ";

        return $btn;
      })
      ->rawColumns(['actions_link'])
      ->toJson();
  }

    public function detail($agent_id)
    {
        $agent = \App\TravelModel\RefAgent::findOrFail($agent_id);
        return view('travel.RefAgent.detail', \compact('agent'));
    }

    public function datatableDetailPage(Request $request)
    {
        $agentId = $request['agent_id'];
        $trnscts = \App\TravelModel\TrnsctBooking::where("agent_id", "$agentId")
            ->orderBy("booking_id", "DESC")
            ->get();

        return DataTables::of($trnscts)
            ->addColumn('booking_date', function($trnscts){
                return date_format(date_create($trnscts->booking_start_date), 'd/m/Y');
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

  public function formAdd()
  {
      return view('travel.RefAgent.formAdd');
  }

  public function addNew(Request $request)
  {
      $request->validate([
          'name'=>'required',
          'phone_num'=>'required',
      ]);

      $new = new \App\TravelModel\RefAgent();
      $new->name = $request->get('name');
      $new->agent_type = $request->get('agent_type');
      $new->phone_num = $request->get('phone_num');
      $new->address = $request->get('address');
      $new->save();

      $response = "Berhasil, Data berhasil disimpan.";
      return response()->json($response);
  }

  public function formEdit($agent_id)
  {
      $agent = \App\TravelModel\RefAgent::findOrFail($agent_id);
      return view('travel.RefAgent.formEdit', \compact('agent'));
  }

    public function edit(Request $request, $agent_id)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
        ]);

        $new = \App\TravelModel\RefAgent::findOrFail($agent_id);
        $new->name = $request->get('name');
        $new->agent_type = $request->get('agent_type');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }

    public function getDataForRemoteSelect(Request $request){

        $search = $request->search;

        if($search == ''){
            $employees = \App\TravelModel\RefAgent::orderBy('name','asc')
                ->select('agent_id','name')
                ->limit(10)
                ->get();
        }else{
            $employees = \App\TravelModel\RefAgent::orderBy('name','asc')
                ->select('agent_id','name')
                ->where('name', 'like', '%' .$search . '%')
                ->limit(10)
                ->get();
        }

        $response[] = [
            "id" => "",
            "text" => "-- Pilih Agen / Biro --"
        ];

        $response = array();
        foreach($employees as $employee){
            $response[] = array(
                "id"=> $employee->agent_id,
                "text"=> $employee->name
            );
        }

        echo json_encode($response);
        exit;
   }


}
