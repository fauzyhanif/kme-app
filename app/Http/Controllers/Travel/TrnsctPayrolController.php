<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use PDF;
use DataTables;

class TrnsctPayrolController extends Controller
{
    protected $TrnsctBookingController;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (request()->exists('month')) {
            $this_month = $request->get('month');
            $this_year = $request->get('year');
        } else {
            $this_month = date('m');
            $this_year = date('Y');
        }

        $year_min = 2019;
        $year_max = date('Y');
        $years = [];
        for ($i=$year_max; $i > $year_min; $i--) {
            $years[] = $i;
        }

        $payrols = \App\TravelModel\TrnsctPayrol::getData($this_year, $this_month);

        return view('travel.TrnsctPayrol.index', compact('years', 'this_month', 'this_year', 'payrols'));
    }

    public function print(Request $request)
    {
        $this_month = $request->get('month');
        $this_year = $request->get('year');
        $payrols = \App\TravelModel\TrnsctPayrol::getData($this_year, $this_month);

        return view('travel.TrnsctPayrol.print', compact('years', 'this_month', 'this_year', 'payrols'));
    }

    public function printSlipGaji($id)
    {
        $payrol = \App\TravelModel\TrnsctPayrolPayment::with(['payrol', 'payrol.sdm', 'payrol.sdm.position'])->findOrFail($id);

        return view('travel.TrnsctPayrol.printSlipGaji', compact('payrol'));
    }

    public function generate(Request $request)
    {
        \App\TravelModel\TrnsctPayrol::addNew($request->all());

        return redirect()->back();
    }

    public function formPencairan($id)
    {
        $payrol = \App\TravelModel\TrnsctPayrol::with(['sdm', 'sdm.position'])->findOrFail($id);
        $debts = \App\TravelModel\TrnsctDebt::where("sdm_id", "$payrol->sdm_id")->where("amount_paid", "<", DB::raw("amount"))->where('status', 'SUCCESS')->orderBy("date", "ASC")->get();
        $cashAccounts = \App\TravelModel\RefFinanceAccount::where("account_id", "LIKE", "%1-%")->where("parent", "!=", "null")->orderBy("account_id", "ASC")->get();
        $operasionalAccounts = \App\TravelModel\RefFinanceAccount::where("account_id", "LIKE", "%8-%")->orderBy("account_id", "ASC")->get();
        $payrolPayments = \App\TravelModel\TrnsctPayrolPayment::with(['account', 'debtPayment', 'debtPayment.debt'])->where("payrol_id", "$id")->where("status", "SUCCESS")->get();

        return view('travel.TrnsctPayrol.formPencairan', compact('payrol', 'debts', 'cashAccounts', 'operasionalAccounts', 'payrolPayments'));
    }

    public function pencairan(Request $request)
    {
        \App\TravelModel\TrnsctPayrolPayment::addNew($request->all());

        return redirect()->back();
    }

    public function pembatalanPencairan(Request $request)
    {
        \App\TravelModel\TrnsctPayrolPayment::pembatalan($request->all());
        return redirect()->back();
    }
}
