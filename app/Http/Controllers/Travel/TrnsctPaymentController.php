<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PDF;
use DataTables;

use App\Http\Controllers\Travel\TrnsctBookingController;

class TrnsctPaymentController extends Controller
{
    protected $TrnsctBookingController;
    public function __construct(TrnsctBookingController $TrnsctBookingController)
    {
        $this->TrnsctBookingController = $TrnsctBookingController;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.TrnsctPayment.index');
    }

    public function jsonData()
    {
        $payments = \App\TravelModel\TrnsctPayment::with('coa')
            ->where('is_canceled', '0')
            ->orderBy("payment_id", "desc")
            ->limit(100)
            ->get();

        return DataTables::of($payments)
        ->editColumn('payment_date', function($payments) {
            $payment_date = date_format(date_create($payments->payment_date), 'd/m/Y');

            return $payment_date;
        })
        ->editColumn('amount', function($payments) {
            $amount = number_format($payments->amount,2,',','.');

            return $amount;
        })
        ->editColumn('payment_method', function($payments) {
            $result = $payments->coa->name;

            return $result;
        })
        ->addColumn('actions_link', function($payments) {
            // print link
            $btn = "<a href='" . route('travel.payment.print', $payments->payment_id) . "' class='btn btn-warning btn-xs' target='_blank'>";
            $btn .= "<i class='fas fa-print'></i> Cetak";
            $btn .= "</a> ";

            return $btn;
        })
        ->rawColumns(['actions_link'])
        ->toJson();
    }

    public function formAdd($bookingID = "")
    {
        $methods = \App\TravelModel\RefPaymentMethod::all();
        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])->findOrFail($bookingID);

        // get cash & bank account
        $cashAccount = \App\TravelModel\RefFinanceAccount::where("account_id", "LIKE", "%1-%")
            ->where("parent", "!=", "null")
            ->orderBy("account_id", "ASC")
            ->get();

        return view('travel.TrnsctPayment.formAdd', compact(
            'methods',
            'booking',
            'cashAccount'
        ));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'booking_id'=>'required',
            'payment_date'=>'required',
            'received_from'=>'required',
            'payment_method'=>'required'
        ]);

        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])->findOrFail($request['booking_id']);
        $bookingStartDate = date("d-m-Y", strtotime($booking->booking_start_date));
        $customerName = $booking->customer->name;

        $jenisBayar = (str_replace(".", "", $request['amount']) == ($booking->ttl_trnsct - $booking->ttl_trnsct_paid)) ? 'Pelunasan' : 'DP';

        $description = "$jenisBayar ";

        $carCategories = \App\TravelModel\TrnsctBookingDtl::with(['carCategory'])->where('booking_id', $request['booking_id'])->get();
        foreach ($carCategories as $key => $value) {
            $description .= $value->carCategory->name;
        }


        $description .= " Tgl $bookingStartDate Tujuan $booking->destination A/N $customerName";

        $paymentID = $this->TrnsctBookingController->getPaymentID();

        $new = new \App\TravelModel\TrnsctPayment();
        $new->booking_id = $request->get('booking_id');
        $new->payment_id = $paymentID;
        $new->payment_date = $request->get('payment_date');
        $new->received_from = $request->get('received_from');
        $new->amount = str_replace(".", "", $request['amount']);
        $new->payment_method = $request->get('payment_method');
        $new->information = $description;
        $new->admin = Session::get('auth_nama');
        $new->save();

        // subtraction bill
        $this->subtractionBill($request->all());

        // save jurnal
        $this->saveJurnal($request->all(), $paymentID, $description);

        $notice = [
            "booking_id" => $new->booking_id,
            "payment_id" => $paymentID,
            "type" => "success",
            "text" => "Data berhasil disimpan.",
        ];

        return response()->json($notice);
    }

    public function subtractionBill($request)
    {
        $booking_id = $request['booking_id'];
        $booking = \App\TravelModel\TrnsctBooking::findOrFail($request['booking_id']);

        // ambil selisih
        $selisih = $booking->ttl_trnsct_paid - str_replace(".", "", $request['amount']);

        $booking->ttl_trnsct_paid += str_replace(".", "", $request['amount']);
        $booking->save();

        // if repayment then save second jurnal
        if ($booking->ttl_trnsct_paid == $booking->ttl_trnsct) {

            // penetapan nominal yg akan diinput ke jurnal
            // cek apakah akun 1-130 post kredit ada uangnya atau pembayaran sebelumnya lunas
            // jika belum ada maka jumlah pembayaran baru jadi nominal yg diinput

            $cek_exis_uang_muka_sewa = \App\TravelModel\TrnsctJurnal::where("booking_id", "$booking_id")->where("account_id", "1-130")->where("credit", "!=", "0")->get();
            if ($cek_exis_uang_muka_sewa->count() == 0) {
                $nominal = str_replace(".", "", $request['amount']);
            } else {
                $nominal = $booking->ttl_trnsct_paid;
            }

            $description = "Pelunasan Kode Booking " . $request['booking_id'];
            $paymentID = $this->TrnsctBookingController->getPaymentID();

            // store to trnsct_jurnal => CASH ACCOUNT
            $cashAccount = new \App\TravelModel\TrnsctJurnal();
            $cashAccount->booking_id = $request['booking_id'];
            $cashAccount->proof_id = $paymentID;
            $cashAccount->trnsct_date = $request['payment_date'];
            $cashAccount->related_person = $request['received_from'];
            $cashAccount->account_id = "1-130";
            $cashAccount->description = $description;
            $cashAccount->debit = $nominal;
            $cashAccount->credit = 0;
            $cashAccount->type = "IN";
            $cashAccount->is_main = "N";
            $cashAccount->user = Session::get('auth_nama');
            $cashAccount->save();

            // store to trnsct_jurnal => INCOME ACCOUNT
            $incomeAccount = new \App\TravelModel\TrnsctJurnal();
            $incomeAccount->booking_id = $request['booking_id'];
            $incomeAccount->proof_id = $paymentID;
            $incomeAccount->trnsct_date = $request['payment_date'];
            $incomeAccount->related_person = $request['received_from'];
            $incomeAccount->account_id = "4-100";
            $incomeAccount->description = $description;
            $incomeAccount->debit = 0;
            $incomeAccount->credit = $nominal;
            $incomeAccount->type = "IN";
            $incomeAccount->is_main = "Y";
            $incomeAccount->post_net_profit = "LABARUGI";
            $incomeAccount->user = Session::get('auth_nama');
            $incomeAccount->save();
        }
    }

    public function saveJurnal($request, $paymentID, $description)
    {
        // store to trnsct_jurnal => CASH ACCOUNT
        $cashAccount = new \App\TravelModel\TrnsctJurnal();
        $cashAccount->booking_id = $request['booking_id'];
        $cashAccount->proof_id = $paymentID;
        $cashAccount->trnsct_date = $request['payment_date'];
        $cashAccount->related_person = $request['received_from'];
        $cashAccount->account_id = $request['payment_method'];
        $cashAccount->description = $description;
        $cashAccount->debit = str_replace(".", "", $request['amount']);
        $cashAccount->credit = 0;
        $cashAccount->type = "IN";
        $cashAccount->is_main = "N";
        $cashAccount->user = Session::get('auth_nama');
        $cashAccount->save();

        // store to trnsct_jurnal => INCOME ACCOUNT
        $incomeAccount = new \App\TravelModel\TrnsctJurnal();
        $incomeAccount->booking_id = $request['booking_id'];
        $incomeAccount->proof_id = $paymentID;
        $incomeAccount->trnsct_date = $request['payment_date'];
        $incomeAccount->related_person = $request['received_from'];
        $incomeAccount->account_id = "1-130";
        $incomeAccount->description = $description;
        $incomeAccount->debit = 0;
        $incomeAccount->credit = str_replace(".", "", $request['amount']);
        $incomeAccount->type = "IN";
        $incomeAccount->is_main = "Y";
        $incomeAccount->user = Session::get('auth_nama');
        $incomeAccount->save();

        $notice = [
            "type" => "success",
            "text" => "Data berhasil disimpan.",
        ];

        return response()->json($notice);
    }

    public function print($paymentID)
    {
        $payment = \App\TravelModel\TrnsctPayment::with(['coa', 'booking', 'booking.customer'])
            ->where('payment_id', "$paymentID")
            ->first();

        $payments = \App\TravelModel\TrnsctPayment::where('booking_id', "$payment->booking_id")
            ->where('is_canceled', '0')
            ->get();

        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])
            ->where('booking_id', "$payment->booking_id")
            ->first();

        $bookingDtl = \App\TravelModel\TrnsctBookingDtl::
            with('carCategory')
            ->where('booking_id', "$payment->booking_id")
            ->get();

        $pdf = PDF::loadView('travel.TrnsctPayment.print', compact(
            'payment',
            'payments',
            'booking',
            'bookingDtl'
        ));
        return $pdf->stream($paymentID.".pdf");
    }

    public function cancel(Request $request)
    {
        $paymentId = $request->get('payment_id');
        $bookingId = $request->get('booking_id');

        // get jurnal
        $jurnals = \App\TravelModel\TrnsctJurnal::where('proof_id', "$paymentId")->where('is_main', 'N')->get();

        // get amount untuk pengurangan kolom trnsct_paid table booking
        $amount = ($jurnals->count() > 0) ? $jurnals[0]->debit : 0;

        // pengurangan ttl_trnsct_paid di table booking
        $booking = \App\TravelModel\TrnsctBooking::findOrFail($bookingId);
        $booking->ttl_trnsct_paid = $booking->ttl_trnsct_paid - $amount;
        $booking->save();

        // delete data di table payment
        \App\TravelModel\TrnsctPayment::where('payment_id', "$paymentId")
            ->where('booking_id', "$bookingId")
            ->update([
                'is_canceled' => '1'
            ]);

        // cancel data di table jurnal
        \App\TravelModel\TrnsctJurnal::where('proof_id', "$paymentId")
            ->update([
                'status' => 'BATAL'
            ]);

        // cancel data di table jurnal (jika lunas / telah terjadi pendapatan sewa)
        \App\TravelModel\TrnsctJurnal::where('booking_id', "$bookingId")
            ->where('account_id', '1-130')
            ->where('debit', '!=', '0')
            ->where('credit', '0')
            ->update([
                'status' => 'BATAL'
            ]);

        \App\TravelModel\TrnsctJurnal::where('booking_id', "$bookingId")
            ->where('account_id', '4-100')
            ->where('credit', '!=', '0')
            ->where('debit', '0')
            ->update([
                'status' => 'BATAL'
            ]);

        return redirect()->back()->with(['success' => 'Pembatalan pembayaran berhasil!']);
    }
}
