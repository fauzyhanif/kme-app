<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\Cache;

class TrnsctBookingPublicController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Bangkok");
    }

    public function searchCarsPagePublic()
    {
        return view('travel.TrnsctBooking.form.searchCarsPublic');
    }

    public function searchCars(Request $request)
    {
        $page = $request['page'];
        $date = explode(" - ", $request['date']);
        $startDate = $date[0];
        $endDate = $date[1];
        $isPublic = (isset($request['is_public'])) ? 'Y' : 'N';


        // cari kendaraan yg siap pakai dan group by di table ref_car
        $carReady = $this->getCarReady();

        // cari kedararaan yg sudah dibooking dan group by di trnsct_dtl
        if (isset($request['booking_id'])) {
            $carBooked = $this->getCarBookedWithBookingId($startDate, $endDate, $request['booking_id']);
        } else {
            $carBooked = $this->getCarBooked($startDate, $endDate);
        }

        $arrBooked = [];
        foreach ($carBooked as $key => $value) {
            $arrBooked[$value->category_id] = $value->ttl_booked;
        }

        $view = ($page == "FORM_ADD") ? 'displayResultSearchCarsSecond' : 'displayResultSearchCars';

        return view('travel.TrnsctBooking.form.' . $view, \compact(
            'carReady',
            'arrBooked',
            'isPublic',
            'startDate',
            'endDate'
        ));
    }

    public function getCarReady()
    {
        $datas = DB::table('trvl_ref_car')
            ->leftJoin('trvl_ref_car_category', 'trvl_ref_car.category_id', '=', 'trvl_ref_car_category.category_id')
            ->select('trvl_ref_car_category.category_id', 'trvl_ref_car_category.name', 'trvl_ref_car_category.seat', DB::raw('count(trvl_ref_car.id) as ttl_ready'))
            ->where('trvl_ref_car.condition_id', '1')
            ->groupBy('trvl_ref_car.category_id')
            ->get();

        return $datas;
    }

    public function getCarBooked($startDate, $endDate)
    {
        $datas = DB::table('trvl_trnsct_booking_dtl as a')
            ->leftJoin('trvl_trnsct_booking as b', 'a.booking_id', '=', 'b.booking_id')
            ->select('a.category_id', DB::raw('sum(a.unit_qty) as ttl_booked'))
            ->where('b.status', '!=', 'BATAL')
            ->where(function($query) use  ($startDate, $endDate){
                $query->where([['b.booking_start_date', '<=', "$startDate"], ['b.date_not_for_sale' , '>=', "$startDate"]])
                    ->orWhere([['b.booking_start_date', '<=', "$endDate"], ['b.date_not_for_sale' , '>=', "$endDate"]])
                    ->orWhere([['b.booking_start_date', '>=', "$startDate"], ['b.date_not_for_sale' , '<=', "$endDate"]]);
            })
            ->groupBy('a.category_id')
            ->get();

        return $datas;
    }

    public function getCarBookedWithBookingId($startDate, $endDate, $bookingId)
    {
        $datas = DB::table('trvl_trnsct_booking_dtl as a')
            ->leftJoin('trvl_trnsct_booking as b', 'a.booking_id', '=', 'b.booking_id')
            ->select('a.category_id', DB::raw('sum(a.unit_qty) as ttl_booked'))
            ->where('b.status', '!=', 'BATAL')
            ->where('b.booking_id', '!=', "$bookingId")
            ->where(function($query) use  ($startDate, $endDate){
                $query->where([['b.booking_start_date', '<=', "$startDate"], ['b.date_not_for_sale' , '>=', "$startDate"]])
                    ->orWhere([['b.booking_start_date', '<=', "$endDate"], ['b.date_not_for_sale' , '>=', "$endDate"]])
                    ->orWhere([['b.booking_start_date', '>=', "$startDate"], ['b.date_not_for_sale' , '<=', "$endDate"]]);
            })
            ->groupBy('a.category_id')
            ->get();



        return $datas;
    }
}
