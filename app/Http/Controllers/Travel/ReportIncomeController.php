<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ReportIncomeController extends Controller
{
    public function index(Request $request)
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $request->get('date'))[0];
            $lastDay = explode(" - ", $request->get('date'))[1];
        }

        // set session for print report
        Session::put('REPORTS_INCOME_START_DATE', $firstDay);
        Session::put('REPORTS_INCOME_END_DATE', $lastDay);

        // get data report
        $reports = $this->getData($firstDay, $lastDay);

        return view('travel.ReportIncome.index', \compact(
            'firstDay',
            'lastDay',
            'reports'
        ));
    }

    public function print()
    {
        // get session first and end date
        $firstDay = Session::get('REPORTS_INCOME_START_DATE');
        $lastDay = Session::get('REPORTS_INCOME_END_DATE');

        // get data report
        $reports = $this->getData($firstDay, $lastDay);

        return view('travel.ReportIncome.print', \compact(
            'firstDay',
            'lastDay',
            'reports'
        ));
    }

    public function getData($firstDay, $lastDay)
    {
        $reports = DB::table('trvl_trnsct_booking_dtl as a')
            ->leftJoin('trvl_ref_car_category as b', 'a.category_id', '=', 'b.category_id')
            ->leftJoin('trvl_trnsct_booking as c', 'a.booking_id', '=', 'c.booking_id')
            ->select('b.name', DB::raw('sum(a.total) as total'))
            ->whereBetween('a.booking_start_date', [$firstDay, $lastDay])
            ->where('c.status', '!=', 'BATAL')
            ->groupBy('a.category_id', 'b.name')
            ->get();

        return $reports;
    }
}
