<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use App\TravelModel\RefCustomer;
use App\TravelModel\TrnsctBooking;
use App\TravelModel\TrnsctJurnal;
use App\TravelModel\TrnsctPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;
use Carbon\Carbon;
use Exception;
use PDF;
use Illuminate\Support\Facades\Cache;

class TrnsctBookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set("Asia/Bangkok");
    }

    public function searchCarsPage()
    {
        return view('travel.TrnsctBooking.form.searchCars');
    }

    public function searchCarsPagePublic()
    {
        return view('travel.TrnsctBooking.form.searchCarsPublic');
    }

    public function searchCars(Request $request)
    {
        $page = $request['page'];
        $date = explode(" - ", $request['date']);
        $startDate = $date[0];
        $endDate = $date[1];
        $isPublic = (isset($request['is_public'])) ? 'Y' : 'N';


        // cari kendaraan yg siap pakai dan group by di table ref_car
        $carReady = $this->getCarReady();


        // cari kedararaan yg sudah dibooking dan group by di trnsct_dtl
        $carBooked = $this->getCarBooked($startDate, $endDate);

        $arrBooked = [];
        foreach ($carBooked as $key => $value) {
            $arrBooked[$value->category_id] = $value->ttl_booked;
        }

        $view = ($page == "FORM_ADD") ? 'displayResultSearchCarsSecond' : 'displayResultSearchCars';

        return view('travel.TrnsctBooking.form.' . $view, \compact(
            'carReady',
            'arrBooked',
            'isPublic',
            'startDate',
            'endDate'
        ));
    }

    public function getCarReady()
    {
        $datas = DB::table('trvl_ref_car')
                ->leftJoin('trvl_ref_car_category', 'trvl_ref_car.category_id', '=', 'trvl_ref_car_category.category_id')
                ->select('trvl_ref_car_category.category_id', 'trvl_ref_car_category.name', 'trvl_ref_car_category.seat', DB::raw('count(trvl_ref_car.id) as ttl_ready'))
                ->where('trvl_ref_car.condition_id', '1')
                ->groupBy('trvl_ref_car.category_id')
                ->get();

        return $datas;
    }

    public function getCarBooked($startDate, $endDate)
    {
        $datas = DB::table('trvl_trnsct_booking_dtl as a')
            ->leftJoin('trvl_trnsct_booking as b', 'a.booking_id', '=', 'b.booking_id')
            ->select('a.category_id', DB::raw('sum(a.unit_qty) as ttl_booked'))
            ->where('b.status', '!=', 'BATAL')
            ->where(function($query) use  ($startDate, $endDate){
                $query->where([['b.booking_start_date', '<=', "$startDate"], ['b.date_not_for_sale' , '>=', "$startDate"]])
                    ->orWhere([['b.booking_start_date', '<=', "$endDate"], ['b.date_not_for_sale' , '>=', "$endDate"]])
                    ->orWhere([['b.booking_start_date', '>=', "$startDate"], ['b.date_not_for_sale' , '<=', "$endDate"]]);
            })
            ->groupBy('a.category_id')
            ->get();

        return $datas;
    }

    public function formAdd(Request $request)
    {
        $startDate = $request['booking_start_date'];
        $endDate = $request['booking_end_date'];
        $bookingItems = $request['booking_item'];
        $carbon = new Carbon($endDate);
        $dateNotForSale = $carbon->addDays(1)->toDateString();

        // get cash & bank account
        $cashAccount = \App\TravelModel\RefFinanceAccount::where("account_id", "LIKE", "%1-%")
            ->where("parent", "!=", "null")
            ->orderBy("account_id", "ASC")
            ->get();

        // cari kendaraan yg siap pakai dan group by di table ref_car
        $carReady = $this->getCarReady();

        // cari kedararaan yg sudah dibooking dan group by di trnsct_dtl
        $carBooked = $this->getCarBooked($startDate, $endDate);

        $arrBooked = [];
        foreach ($carBooked as $key => $value) {
            $arrBooked[$value->category_id] = $value->ttl_booked;
        }

        return view('travel.TrnsctBooking.form.formAdd', \compact(
            'startDate',
            'endDate',
            'dateNotForSale',
            'cashAccount',
            'carReady',
            'arrBooked',
            'bookingItems'
        ));
    }

    public function addNew(Request $request)
    {
        $dataPost = $request->all();

        // get booking ID
        $bookingID = $this->getBookingID();
        $dataPost['booking_id'] = $bookingID;

        // customer
        if ($request->get('sudah_pernah')[0] == '0') {
            $custID = $this->saveNewCustomer($dataPost);
            $dataPost['cust_id'] = $custID;
        } else {
            // explode customer value
            $explCust = explode(" | ", $dataPost['cust_id']);
            $dataPost['cust_id'] = $explCust[0];
            $dataPost['cust_name'] = $explCust[1];
            $dataPost['cust_phone_num'] = $explCust[2];
            $dataPost['cust_address'] = $explCust[3];
        }

        $dataPost['ttl_trnsct'] = str_replace(".", "", $dataPost['ttl_trnsct']) - str_replace(".", "", $dataPost['discount']);

        // get total unit
        $ttl_unit = $this->getTtlUnit($dataPost);
        $dataPost['ttl_unit'] = $ttl_unit;

        // save new booking
        $responseSaveNewBooking = $this->saveNewBooking($dataPost);

        $notice = [];

        if ($responseSaveNewBooking == "1") {
            // save new booking detail
            $this->saveNewBookingDtl($dataPost);

            // if amount payment not 0
            if ($dataPost['ttl_trnsct_paid'] != 0) {
                // get payment ID
                $paymentID = $this->getPaymentID();
                $dataPost['payment_id'] = $paymentID;

                // save payment
                $this->saveNewPayment($dataPost);

                // save jurnal
                $this->saveJurnal($dataPost);

                $notice = [
                    "booking_id" => $bookingID,
                    "payment_id" => $paymentID,
                    "page" => "store",
                    "type" => "success",
                    "text" => "Data berhasil disimpan.",
                    "url" => "{{ route('travel.booking.detail', $bookingID) }}"
                ];
            } else {
                $notice = [
                    "booking_id" => $bookingID,
                    "page" => "store",
                    "type" => "success_with_no_payment",
                    "text" => "Data berhasil disimpan.",
                    "url" => "{{ route('travel.booking.detail', $bookingID) }}"
                ];
            }
        } else {
            $notice = [
                "booking_id" => 0,
                "type" => "error",
                "text" => "Data gagal disimpan.",
            ];
        }

        return response()->json($notice);
    }

    public function getTtlUnit($request)
    {
        $ttl_unit = 0;
        foreach ($request['item_booking'] as $key => $value) {
            $ttl_unit += $value;
        }

        return $ttl_unit;
    }

    public function getTtlTrnsct($request)
    {
        $ttl_trnsct = 0;
        foreach ($request['total'] as $key => $value) {
            $ttl_trnsct += str_replace(".", "", $value);
        }

        return $ttl_trnsct;
    }

    public function saveNewBooking($request)
    {
        $new = new \App\TravelModel\TrnsctBooking();
        $new->trnsct_date = \date('Y-m-d H:i:s');
        $new->booking_id = $request['booking_id'];
        $new->cust_id = $request['cust_id'];
        $new->cust_name = $request['cust_name'];
        $new->cust_phone_num = $request['cust_phone_num'];
        $new->cust_address = $request['cust_address'];
        $new->booking_start_date = explode(" - ", $request['date'])[0];
        $new->booking_end_date = explode(" - ", $request['date'])[1];
        $new->date_not_for_sale = $request['date_not_for_sale'];
        $new->destination = $request['destination'];
        $new->pick_up_location = $request['pick_up_location'];
        $new->standby_time = $request['standby_time'];
        $new->information = $request['information'];
        $new->booking_from = $request['booking_from'];
        if ($request['booking_from'] != "UMUM") {
            $new->agent_id = $request['agent_id'];
        }
        $new->agent_commission = str_replace(".", "", $request['agent_commission']);
        $new->ttl_unit = $request['ttl_unit'];
        $new->ttl_trnsct = str_replace(".", "", $request['ttl_trnsct']);
        $new->ttl_trnsct_paid = str_replace(".", "", $request['ttl_trnsct_paid']);
        $new->charge = 0;
        $new->discount = str_replace(".", "", $request['discount']);

        if ($new->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function saveNewBookingDtl($request)
    {
        foreach ($request['item_booking'] as $key => $value) {
            if ($value != 0) {
                $new = new \App\TravelModel\TrnsctBookingDtl();
                $new->booking_id = $request['booking_id'];
                $new->booking_start_date = explode(" - ", $request['date'])[0];
                $new->booking_end_date = explode(" - ", $request['date'])[1];
                $new->date_not_for_sale = $request['date_not_for_sale'];
                $new->category_id = $key;
                $new->unit_qty = $value;
                $new->unit_qty_spj = 0;
                $new->price = str_replace(".", "", $request['price'][$key]);
                $new->total = str_replace(".", "", $request['total'][$key]);
                $new->save();
            }
        }
    }

    public function saveNewPayment($request)
    {
        $jenisBayar = (str_replace(".", "", $request['ttl_trnsct']) == str_replace(".", "", $request['ttl_trnsct_paid'])) ? 'Sewa' : 'DP';
        $description = "$jenisBayar ";

        $carCategories = \App\TravelModel\TrnsctBookingDtl::with(['carCategory'])->where('booking_id', $request['booking_id'])->get();
        foreach ($carCategories as $key => $value) {
            $description .= $value->carCategory->name;
        }

        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])->findOrFail($request['booking_id']);
        $bookingStartDate = date("d-m-Y", strtotime($booking->booking_start_date));
        $customerName = $booking->customer->name;

        $description .= " Tgl $bookingStartDate Tujuan $booking->destination A/N $customerName";

        $new = new \App\TravelModel\TrnsctPayment();
        $new->payment_id = $request['payment_id'];
        $new->booking_id = $request['booking_id'];
        $new->payment_date = $request['payment_date'] . date('H:i:s');
        $new->received_from = $request['cust_name'];
        $new->amount = str_replace(".", "", $request['ttl_trnsct_paid']);
        $new->payment_method = $request['payment_method'];
        $new->information = $description;
        $new->is_canceled = 0;
        $new->admin = Session::get('auth_nama');
        $new->save();
    }

    public function getBookingID()
    {
        $thisMonth = date('Y-m');
        $bookingID = 'BK';

        $lastBookingID = \App\TravelModel\RefBookingId::where('month', '=', "$thisMonth")->first();

        // check exist or not
        if ( $lastBookingID === null ) {
            $orderId = "0001";

            $new = new \App\TravelModel\RefBookingId();
            $new->month = "$thisMonth";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $orderId = sprintf('%04d', $lastBookingID->serial_num);
            $update = \App\TravelModel\RefBookingId::where('month', '=', "$thisMonth")->update(["serial_num" => $orderId + 1]);
        }

        $bookingID .= str_replace("-", "", $thisMonth) . $orderId;
        return $bookingID;
    }

    public function getPaymentID()
    {
        $thisMonth = date('Y-m');
        $paymentID = 'INV';

        $lastPaymentID = \App\TravelModel\RefPaymentId::where('month', '=', "$thisMonth")->first();

        // check exist or not
        if ( $lastPaymentID === null ) {
            $orderId = "0001";

            $new = new \App\TravelModel\RefPaymentId();
            $new->month = "$thisMonth";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $orderId = sprintf('%04d', $lastPaymentID->serial_num);
            $update = \App\TravelModel\RefPaymentId::where('month', '=', "$thisMonth")->update(["serial_num" => $orderId + 1]);
        }

        $paymentID .= str_replace("-", "", $thisMonth) . $orderId;
        return $paymentID;
    }

    public function saveNewCustomer($request)
    {
        $custID = 0;

        $new = new \App\TravelModel\RefCustomer();
        $new->name = $request['cust_name'];
        $new->phone_num = $request['cust_phone_num'];
        $new->address = $request['cust_address'];
        $new->ttl_trnsct = 1;
        $new->ttl_payment = str_replace(".", "", $request['ttl_trnsct']);
        $new->save();

        $custID = $new->cust_id;

        return $custID;
    }

    public function saveNewAgentBiro($request)
    {
        $agenttID = 0;
        $searchAgent = \App\TravelModel\RefAgent::where('name', $request['agent_name'])
                        ->where('phone_num', $request['agent_phone_num'])
                        ->where('agent_type', $request['booking_from'])
                        ->first();

        if ($searchAgent === null) {
            $new = new \App\TravelModel\RefAgent();
            $new->agent_type = $request['booking_from'];
            $new->name = $request['agent_name'];
            $new->phone_num = $request['agent_phone_num'];
            $new->ttl_trnsct = 1;
            $new->ttl_commission = $request['agent_commission'];
            $new->save();

            $agentID = $new->cust_id;
        } else {
            $searchAgent->ttl_trnsct = $searchAgent->ttl_trnsct +1;
            $searchAgent->ttl_commission = $searchAgent->ttl_commission + $request['agent_commission'];
            $searchAgent->save();

            $agentID = $searchAgent->cust_id;
        }

        return $agentID;
    }

    public function saveJurnal($request)
    {
        $jenisBayar = (str_replace(".", "", $request['ttl_trnsct']) == str_replace(".", "", $request['ttl_trnsct_paid'])) ? 'Sewa' : 'DP';
        $description = "$jenisBayar ";

        $carCategories = \App\TravelModel\TrnsctBookingDtl::with(['carCategory'])->where('booking_id', $request['booking_id'])->get();
        foreach ($carCategories as $key => $value) {
            $description .= $value->carCategory->name;
        }

        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])->findOrFail($request['booking_id']);
        $bookingStartDate = date("d-m-Y", strtotime($booking->booking_start_date));
        $customerName = $booking->customer->name;

        $description .= " Tgl $bookingStartDate Tujuan $booking->destination A/N $customerName";


        // store to trnsct_jurnal => CASH ACCOUNT
        $cashAccount = new \App\TravelModel\TrnsctJurnal();
        $cashAccount->booking_id = $request['booking_id'];
        $cashAccount->proof_id = $request['payment_id'];
        $cashAccount->trnsct_date = date('Y-m-d');
        $cashAccount->related_person = $request['cust_name'];
        $cashAccount->account_id = $request['payment_method'];
        $cashAccount->description = $description;
        $cashAccount->debit = str_replace(".", "", $request['ttl_trnsct_paid']);
        $cashAccount->credit = 0;
        $cashAccount->type = "IN";
        $cashAccount->is_main = "N";
        $cashAccount->user = Session::get('auth_nama');
        $cashAccount->save();

        // store to trnsct_jurnal => INCOME ACCOUNT
        $account_id = (str_replace(".", "", $request['ttl_trnsct_paid']) == str_replace(".", "",$request['ttl_trnsct'])) ? '4-100' : '1-130';
        $post_net_profit = (str_replace(".", "", $request['ttl_trnsct_paid']) == str_replace(".", "",$request['ttl_trnsct'])) ? 'LABARUGI' : '';

        $incomeAccount = new \App\TravelModel\TrnsctJurnal();
        $incomeAccount->booking_id = $request['booking_id'];
        $incomeAccount->proof_id = $request['payment_id'];
        $incomeAccount->trnsct_date = date('Y-m-d');
        $incomeAccount->related_person = $request['cust_name'];
        $incomeAccount->account_id = $account_id;
        $incomeAccount->description = $description;
        $incomeAccount->debit = 0;
        $incomeAccount->credit = str_replace(".", "", $request['ttl_trnsct_paid']);
        $incomeAccount->type = "IN";
        $incomeAccount->is_main = "Y";
        $incomeAccount->post_net_profit = $post_net_profit;
        $incomeAccount->user = Session::get('auth_nama');
        $incomeAccount->save();

        $notice = [
            "type" => "success",
            "text" => "Data berhasil disimpan.",
        ];

        return response()->json($notice);
    }

    public function cancelPayment($booking_id, $invoice_id)
    {
        // kurangi ttl_trnsct_paid di table trvl_trnsct_booking by booking_id

        // update is_canceled = 1 di table trvl_trnsct_payment by invoice_id

        // update status = CANCEL di table trvl_trnsct_jurnal by proof_id
    }


    public function bookingList(Request $request)
    {
        $cashAccount = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->orderBy("account_id", "ASC")
            ->get();

        $status = ($request->get('status')) ? $request->get('status') : 'ALL';



        return view('travel.TrnsctBooking.list.index', compact(
            'cashAccount',
            'status'
        ));
    }

    public function bookingListData(Request $request)
    {
        $status = $request->get('status');

        if ($status == 'ALL') { // Semua data

            $bookings = $this->getAllBooking();

        } elseif ($status == 'NOT_YET') { // belum berangkat

            $bookings = $this->getBookingNotYet();

        } elseif ($status == 'DONE') { // sudah berangkat

            $bookings = $this->getBookingDone();

        } elseif ($status == 'CANCEL') { // dibatalkan

            $bookings = $this->getBookingCancel();

        }

        return DataTables::of($bookings)
            ->addColumn('booking_date', function($bookings) {
                $booking_start_date = date_format(date_create($bookings->booking_start_date), 'd/m/Y');
                $booking_end_date = date_format(date_create($bookings->booking_end_date), 'd/m/Y');

                return $booking_start_date . " - " . $booking_end_date;
            })
            ->addColumn('status', function($bookings) {
                $status = "<span class='text-red'>BATAL</span>";
                if ($bookings->status == 'SUKSES') {
                    $status = "<span class='text-green'>SUKSES</span>";
                }

                return $status;
            })
            ->addColumn('status_pembayaran', function($bookings) {
                $sttusLunas = "<span class='text-red'>Belum Lunas</span>";
                if ($bookings->ttl_trnsct <= $bookings->ttl_trnsct_paid) {
                    $sttusLunas = "<span class='text-green'>Sudah Lunas</span>";
                }

                $sttusKomisi = "<i class='fas fa-user-tag'></i> Belum diambil";
                if ($bookings->ttl_trnsct <= $bookings->ttl_trnsct_paid) {
                    $sttusKomisi = "<i class='fas fa-user-tag'></i> Sudah diambil";
                }

                return $sttusLunas;
            })
            ->addColumn('actions_link', function($bookings) {
                // details button
                $btn = "<a href='" . route('travel.booking.detail', $bookings->booking_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                // payment button
                $btn .= "<a href='" . route('travel.payment.formadd', $bookings->booking_id) . "' class='btn btn-success btn-xs'>";
                $btn .= "<i class='fas fa-money-bill-wave'></i> Bayar";
                $btn .= "</a> ";

                if ($bookings->status == "SUKSES") {
                    // edits button
                    $btn .= "<a href='" . route('travel.booking.formedit', $bookings->booking_id) . "'";
                    $btn .= " class='btn btn-primary btn-xs'>";
                    $btn .= "<i class='fas fa-edit' style='padding: 1px'></i> Edit";
                    $btn .= "</a> ";

                    // cancel button
                    $statusCancel = ($bookings->status == 'BATAL') ? 'HAS_BEEN_CANCELED' : 'NOT_CANCELED';
                    $btn .= "<button onclick=\"modalCancel('$bookings->booking_id', ";
                    $btn .= "'$bookings->ttl_trnsct_paid', '$bookings->ttl_trnsct', '$statusCancel')\"";
                    $btn .= "class='btn btn-danger btn-xs'>";
                    $btn .= "<i class='fas fa-times' style='padding: 3px'></i> Batal";
                    $btn .= "</button> ";
                }

                return $btn;
            })
            ->rawColumns(['booking_date', 'actions_link', 'status', 'status_pembayaran'])
            ->toJson();
    }

    public function getAllBooking()
    {
        $bookings = \App\TravelModel\TrnsctBooking::with(['customer'])
            ->where('status', '!=', 'BATAL')
            ->orderBy("booking_start_date", "asc")
            ->get();

        return $bookings;
    }

    public function getBookingNotYet()
    {
        $today = date('Y-m-d');
        $bookings = \App\TravelModel\TrnsctBooking::with(['customer'])
            ->where('status', '!=', 'BATAL')
            ->whereDate('booking_start_date', '>=', $today)
            ->orderBy("booking_start_date", "asc")
            ->get();

        return $bookings;
    }

    public function getBookingDone()
    {
        $today = date('Y-m-d');
        $bookings = \App\TravelModel\TrnsctBooking::with(['customer'])
            ->where('status', '!=', 'BATAL')
            ->whereDate('booking_start_date', '<', $today)
            ->orderBy("booking_start_date", "asc")
            ->get();

        return $bookings;
    }

    public function getBookingCancel()
    {
        $today = date('Y-m-d');
        $bookings = \App\TravelModel\TrnsctBooking::with(['customer'])
            ->where('status', 'BATAL')
            ->orderBy("booking_start_date", "asc")
            ->get();

        return $bookings;
    }

    public function detail($bookingID)
    {
        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])->where('booking_id', "$bookingID")->first();
        $bookingItems = \App\TravelModel\TrnsctBookingDtl::with(['carCategory'])
            ->where('booking_id', "$bookingID")
            ->get();

        $spj = \App\TravelModel\TrnsctSpj::with(['mainDriver', 'carCategory'])
            ->where('booking_id', "$bookingID")
            ->get();

        $payments = \App\TravelModel\TrnsctPayment::where('booking_id', "$bookingID")
            ->where('is_canceled', '0')
            ->orderBy('payment_id', 'desc')
            ->get();

        $cashAccount = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->orderBy("account_id", "ASC")
            ->get();

        $listDisbursementCommission = \App\TravelModel\TrnsctJurnal::where('booking_id', "$bookingID")
            ->where('trnsct_type', 'DISBURSEMENT_COMMISSION')
            ->where('is_main', 'N')
            ->orderBy('booking_id', 'ASC')
            ->get();

        $moneyHasBeenRefund = \App\TravelModel\TrnsctJurnal::where('booking_id', "$bookingID")
            ->where('is_refund', 'Y')
            ->orderBy('trnsct_date', 'ASC')
            ->get();


        return view('travel.TrnsctBooking.list.detail', \compact(
            'booking',
            'bookingItems',
            'payments',
            'cashAccount',
            'spj',
            'listDisbursementCommission',
            'moneyHasBeenRefund'
        ));
    }

    public function formEdit($bookingID)
    {
        $booking = \App\TravelModel\TrnsctBooking::with(['agent'])->where('booking_id', $bookingID)->first();

        // cari kendaraan yg siap pakai
        $carReady = $this->getCarReady();

        // cari kedararaan yg sudah dibooking
        $carBooked = $this->getCarBooked($booking->booking_start_date, $booking->date_not_for_sale);

        // cari kendaraan yg dibooking oleh booking ID ini
        $bookingList = \App\TravelModel\TrnsctBookingDtl::where('booking_id', $bookingID)->get();

        $arrBooked = [];
        foreach ($carBooked as $key => $value) {
            $arrBooked[$value->category_id] = $value->ttl_booked;
        }

        $bookingItems = [];
        foreach ($bookingList as $key => $value) {
            $bookingItems[$value->category_id] = [
                "category_id_$value->category_id" => $value->unit_qty,
                "price_$value->category_id" => $value->price,
                "total_$value->category_id" => $value->total,
            ];
        }

        return view('travel.TrnsctBooking.form.formEdit', \compact(
            'booking',
            'carReady',
            'carBooked',
            'arrBooked',
            'bookingItems'
        ));
    }

    public function edit(Request $request)
    {
        $dataPost = $request->all();
        $bookingID = $request['booking_id'];

        // get total unit
        $ttl_unit = $this->getTtlUnit($dataPost);
        $dataPost['ttl_unit'] = $ttl_unit;

        // get total trnsct
        $ttl_trnsct = $this->getTtlTrnsct($dataPost);
        $dataPost['ttl_trnsct'] = $ttl_trnsct - str_replace(".", "", $dataPost['discount']);

        // update trnsct_booking
        $resUpdateBooking = $this->updateBooking($dataPost);

        // update status spj
        // $this->updateStatusSpj($bookingID, $dataPost['status']);

        if ($resUpdateBooking == "1") {
            $this->updateBookingDtl($dataPost);

            $booking = TrnsctBooking::with(['cars', 'cars.carCategory'])->findOrFail($request->booking_id);
            $description = '';
            foreach ($booking->cars as $car) {
                $description .= $car->carCategory->name;
            }

            $bookingStartDate = date("d-m-Y", strtotime($booking->booking_start_date));
            $customerName = $booking->cust_name;
            $description .= " Tgl $bookingStartDate Tujuan $booking->destination A/N $customerName";

            $jurnals = TrnsctJurnal::where('type', 'IN')
                ->where('booking_id', $request->booking_id)
                ->where('account_id', '!=', '4-100')
                ->where('is_refund', 'N')
                ->whereNull('spj_id')
                ->whereNull('payrol_id')
                ->whereNull('debt_id')
                ->whereNull('debt_payment_id')
                ->get();

            foreach ($jurnals as $jurnal) {
                $updateJurnal = TrnsctJurnal::find($jurnal->jurnal_id);
                $updateJurnal->description = explode(' ', $updateJurnal->description)[0] . ' ' . $description;
                $updateJurnal->save();
            }

            $payments = TrnsctPayment::where('booking_id', $request->booking_id)
                ->where('is_canceled', '0')
                ->get();

            foreach ($payments as $payment) {
                $updatePayment = TrnsctPayment::where('payment_id', $payment->payment_id)
                    ->update(['information' => explode(' ', $payment->information)[0] . ' ' . $description]);
            }

            $notice = [
                "booking_id" => $bookingID,
                "page" => "update",
                "type" => "success",
                "text" => "Data berhasil disimpan.",
                "url" => "{{ route('travel.booking.detail', $bookingID) }}"
            ];
        } else {
            $notice = [
                "booking_id" => 0,
                "type" => "error",
                "text" => "Data gagal disimpan.",
            ];
        }

        return response()->json($notice);
    }

    public function updateBooking($request)
    {
        $bookingID = $request['booking_id'];
        $ttl_trnsct = str_replace(".", "", $request['ttl_trnsct']);
        $old = \App\TravelModel\TrnsctBooking::find($bookingID);
        $old->booking_start_date = explode(" - ", $request['date'])[0];
        $old->booking_end_date = explode(" - ", $request['date'])[1];
        $old->date_not_for_sale = $request['date_not_for_sale'];
        $old->destination = $request['destination'];
        $old->pick_up_location = $request['pick_up_location'];
        $old->standby_time = $request['standby_time'];
        $old->information = $request['information'];
        $old->booking_from = $request['booking_from'];

        if ($request['cust_name'] != $old->cust_name || $request['cust_phone_num'] != $old->cust_phone_num || $request['cust_address'] != $old->cust_address ) {
            $customer = RefCustomer::find($old->cust_id);
            $customer->name = $request['cust_name'];
            $customer->phone_num = $request['cust_phone_num'];
            $customer->address = $request['cust_address'];
            $customer->save();
        }

        $old->cust_name = $request['cust_name'];
        $old->cust_phone_num = $request['cust_phone_num'];
        $old->cust_address = $request['cust_address'];
        $old->ttl_unit = $request['ttl_unit'];
        $old->ttl_trnsct = $ttl_trnsct;

        if ($request['booking_from'] == 'UMUM') {

            if ($old->booking_form != 'UMUM' && $old->agent_commission_paid > 0) {
                throw new Exception('Perubahan Booking dari Agen/Biro ke Umum gagal karna sudah ada pencairan komisi ke Agen/Biro', 400);
            }

            $old->agent_id = '';
            $old->agent_commission = 0;
        } else {
            $old->agent_id = $request['agent_id'];
            $old->agent_commission = str_replace(".","", $request['agent_commission']);
        }

        if ($old->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updateBookingDtl($request)
    {
        $bookingID = $request['booking_id'];
        foreach ($request['item_booking'] as $key => $value) {

            // cek apakah ada di db atau  belum
            $check = \App\TravelModel\TrnsctBookingDtl::where('category_id', $key)
                ->where('booking_id', $bookingID)
                ->first();

            // jika ada maka update
            if ($check !== null) {
                if ($value <= 0) {
                    $check->delete();
                } else {
                    $check->update([
                        "booking_start_date" => explode(" - ", $request['date'])[0],
                        "booking_end_date" => explode(" - ", $request['date'])[1],
                        "date_not_for_sale" => $request["date_not_for_sale"],
                        "unit_qty" => $value,
                        "price" => str_replace(".", "", $request['price'][$key]),
                        "total" => str_replace(".", "", $request['total'][$key]),
                    ]);
                }
            }

            // jika value != 0 dan di db tidak ada maka insert
            if ($value != 0 && $check === null) {
                $new = new \App\TravelModel\TrnsctBookingDtl();
                $new->booking_id = $request['booking_id'];
                $new->booking_start_date = explode(" - ", $request['date'])[0];
                $new->booking_end_date = explode(" - ", $request['date'])[1];
                $new->date_not_for_sale = $request['date_not_for_sale'];
                $new->category_id = $key;
                $new->unit_qty = $value;
                $new->unit_qty_spj = 0;
                $new->price = str_replace(".", "", $request['price'][$key]);
                $new->total = str_replace(".", "", $request['total'][$key]);
                $new->save();
            }
        }
    }

    public function updateStatusSpj($bookingID, $status)
    {
        $update = \App\TravelModel\TrnsctSpj::where("booking_id", $bookingID)
            ->update(["status" => "$status"]);
    }

    public function pencairanKomisi(Request $request)
    {
        $data = \App\TravelModel\TrnsctBooking::findOrFail($request['booking_id']);
        $data->st_commission = "SUDAH DICAIRKAN";
        $data->agent_commission_paid = $data->agent_commission_paid + str_replace(".", "", $request['amount']);

        if ($data->save()) {
            $paymentID = $this->getPaymentID();
            $description = "Pencairan Komisi " . $request['booking_id'];

            // store to trnsct_jurnal => FIRST JURNAL
            $firstJurnal = new \App\TravelModel\TrnsctJurnal();
            $firstJurnal->booking_id = $request['booking_id'];
            $firstJurnal->proof_id = $paymentID;
            $firstJurnal->trnsct_date = $request['trnsct_date'];
            $firstJurnal->related_person = $request['related_person'];
            $firstJurnal->account_id = $request['payment_method'];
            $firstJurnal->description = $description;
            $firstJurnal->credit = str_replace(".", "", $request['amount']);
            $firstJurnal->debit = 0;
            $firstJurnal->type = "OUT";
            $firstJurnal->trnsct_type = "DISBURSEMENT_COMMISSION";
            $firstJurnal->is_main = "N";
            $firstJurnal->user = Session::get('auth_nama');
            $firstJurnal->save();

            // store to trnsct_jurnal => SECOND JURNAL
            $secondJurnal = new \App\TravelModel\TrnsctJurnal();
            $secondJurnal->booking_id = $request['booking_id'];
            $secondJurnal->proof_id = $paymentID;
            $secondJurnal->trnsct_date = $request['trnsct_date'];
            $secondJurnal->related_person = $request['related_person'];
            $secondJurnal->account_id = "6-020";
            $secondJurnal->description = $description;
            $secondJurnal->debit = str_replace(".", "", $request['amount']);
            $secondJurnal->credit = 0;
            $secondJurnal->type = "OUT";
            $secondJurnal->trnsct_type = "DISBURSEMENT_COMMISSION";
            $secondJurnal->is_main = "Y";
            $secondJurnal->post_net_profit = "LABARUGI";
            $secondJurnal->user = Session::get('auth_nama');
            $secondJurnal->save();

            $notice = [
                "booking_id" => $request['booking_id'],
                "title" => "Berhasil!",
                "type" => "success",
                "text" => "Data berhasil disimpan.",
            ];

            return response()->json($notice);
        }
    }

    public function cancelPencairanKomisi(Request $request)
    {
        DB::beginTransaction();
        try {
            $jurnal = TrnsctJurnal::where('proof_id', $request->proof_id)
                ->where('is_main', 'Y')
                ->first();

            $booking = TrnsctBooking::findOrFail($request->booking_id);
            $booking->agent_commission_paid = $booking->agent_commission_paid - $jurnal->debit;
            $booking->save();

            $jurnal = TrnsctJurnal::where('proof_id', $request->proof_id);
            $jurnal->delete();

            DB::commit();
            return true;
        } catch (Exception $th) {
            DB::rollBack();
            throw new Exception("Error Processing Request", 400);
        }
    }

    public function cancelBooking(Request $request)
    {
        $booking = \App\TravelModel\TrnsctBooking::findOrFail($request['booking_id']);
        $booking->status = "BATAL";
        $booking->cancel_date = $request->get('cancel_date');
        $booking->cancel_by = $request->get('cancel_by');
        $booking->money_return = str_replace(".", "", $request->get('refund_now'));
        $booking->money_refundable_plan = str_replace(".", "", $request->get('money_refundable_plan'));
        $booking->save();

        // cek apakah pembayaran sudah lunas atau belum

        /*
        - jika belum lunas (Uang muka) maka
        - DP 3.000.000
        - Rencana dikembalikan 2.000.000
        - Dikembalikan skrg 1.000.000
        - Potongan 1.000.000
        -
        - 1. akun uang muka sewa dikurangi uang dikembalikan sekarang (debet) 1.000.000
        - 2. uang dikembalikan sekarang diambil dri akun kas mana (credit) 1.000.000
        - 3. potongan untuk perusahaan masuk akun fee refund (credit) 1.000.000
        - 4. potongan untuk perusahaan diambil dari uang muka sewa (debet) 1.000.000
        */

        $paymentID = $this->getPaymentID();
        if ($booking->ttl_trnsct_paid < $booking->ttl_trnsct) {

            // 1. akun uang muka sewa dikurangi uang dikembalikan sekarang (debet) 1.000.000
            $postData = [
                'proof_id' => $paymentID,
                'booking_id' => $booking->booking_id,
                'trnsct_date' => $request['cancel_date'],
                'related_person' => $request['cancel_by'],
                'account_id' => '1-130',
                'description' => 'Pembatalan sewa kode boking'.$booking->booking_id,
                'credit' => 0,
                'debit' => str_replace(".", "", $request->get('refund_now')),
                'type' => 'OUT',
                'is_main' => 'Y',
                'is_refund' => 'N',
                'post_net_profit' => 'NERACA',
            ];
            $this->saveJurnalBenchmark($postData);

            // 2. uang dikembalikan sekarang diambil dri akun kas mana (credit) 1.000.000
            $postData = [
                'proof_id' => $paymentID,
                'booking_id' => $booking->booking_id,
                'trnsct_date' => $request['cancel_date'],
                'related_person' => $request['cancel_by'],
                'account_id' => $request['money_get_from'],
                'description' => 'Pembatalan sewa kode boking'.$booking->booking_id,
                'debit' => 0,
                'credit' => str_replace(".", "", $request->get('refund_now')),
                'type' => 'OUT',
                'is_main' => 'Y',
                'is_refund' => 'Y',
                'post_net_profit' => 'NERACA',
            ];
            $this->saveJurnalBenchmark($postData);

            // 3. potongan untuk perusahaan masuk akun fee refund (credit) 1.000.000
            $postData = [
                'proof_id' => $paymentID,
                'booking_id' => $booking->booking_id,
                'trnsct_date' => $request['cancel_date'],
                'related_person' => $request['cancel_by'],
                'account_id' => '7-400',
                'description' => 'Potongan pembatalan sewa kode boking'.$booking->booking_id,
                'debit' => 0,
                'credit' => str_replace(".", "", $request->get('refund_cut')),
                'type' => 'IN',
                'is_main' => 'Y',
                'is_refund' => 'N',
                'post_net_profit' => 'LABARUGI',
            ];
            $this->saveJurnalBenchmark($postData);

            // 4. potongan untuk perusahaan diambil dari uang muka sewa (debet) 1.000.000
            $postData = [
                'proof_id' => $paymentID,
                'booking_id' => $booking->booking_id,
                'trnsct_date' => $request['cancel_date'],
                'related_person' => $request['cancel_by'],
                'account_id' => '1-130',
                'description' => 'Potongan pembatalan sewa kode boking'.$booking->booking_id,
                'debit' => str_replace(".", "", $request->get('refund_cut')),
                'credit' => 0,
                'type' => 'IN',
                'is_main' => 'N',
                'is_refund' => 'N',
                'post_net_profit' => 'NERACA',
            ];
            $this->saveJurnalBenchmark($postData);
        } else {
            /*
            - jika sudah lunas maka
            - SUDAH BAYAR 5.000.000
            - OTOMATIS ADA PENDAPATAN 5.000.000
            - RENCANA UANG DIKAMBALIKAN 4.000.000
            - POTONGAN PERUSAHAAN 1.000.000
            - UANG DIKEMBALIKAN SEKARANG 2.000.000
            - SISA 2.000.000 UANG CUST YG BELUM DIKEMBALIKAM

            - 1. batalkan jurnal yg masuk ke pendapata sewa (5.000.000)
            - 2. kurangi laba besih (4.000.000)
            - 3. potongan untuk perusahaan masuk akun fee refund (credit) 1.000.000
            - 4. uang muka sewa bertambah (credit 2.000.000)
            - 5. uang dikembalikan sekarang diambil dri akun kas mana (debit 2.000.000)
            */

            // 1. batalkan jurnal yg masuk ke pendapatan sewa (5.000.000)
            $cancelJurnal = \App\TravelModel\TrnsctJurnal::
                where("booking_id", $request->get('booking_id'))
                ->where("account_id", "4-100")
                ->update(["status_booking" => "BATAL"]);

            // 2. kurangi laba besih (4.000.000)
            // Pake trigger mysql

            // 3. potongan untuk perusahaan masuk akun fee refund (credit) 1.000.000
            $postData = [
                'proof_id' => $paymentID,
                'booking_id' => $booking->booking_id,
                'trnsct_date' => $request['cancel_date'],
                'related_person' => $request['cancel_by'],
                'account_id' => '7-400',
                'description' => 'Potongan pembatalan sewa kode boking'.$booking->booking_id,
                'debit' => 0,
                'credit' => str_replace(".", "", $request->get('refund_cut')),
                'type' => 'IN',
                'is_main' => 'Y',
                'is_refund' => 'N',
                'post_net_profit' => 'LABARUGI',
            ];
            $this->saveJurnalBenchmark($postData);

            // 4. uang muka sewa bertambah (credit 2.000.000)
            $postData = [
                'proof_id' => $paymentID,
                'booking_id' => $booking->booking_id,
                'trnsct_date' => $request['cancel_date'],
                'related_person' => $request['cancel_by'],
                'account_id' => '1-130',
                'description' => 'Pembatalan sewa kode boking'.$booking->booking_id,
                'debit' => 0,
                'credit' => str_replace(".", "", $request->get('money_refundable_plan')) - str_replace(".", "", $request->get('refund_now')),
                'type' => 'IN',
                'is_main' => 'Y',
                'is_refund' => 'N',
                'post_net_profit' => 'NERACA',
            ];
            $this->saveJurnalBenchmark($postData);

            // 5. uang dikembalikan sekarang diambil dri akun kas mana (debit 2.000.000)
            $postData = [
                'proof_id' => $paymentID,
                'booking_id' => $booking->booking_id,
                'trnsct_date' => $request['cancel_date'],
                'related_person' => $request['cancel_by'],
                'account_id' => $request['money_get_from'],
                'description' => 'Pembatalan sewa kode boking'.$booking->booking_id,
                'debit' => 0,
                'credit' => str_replace(".", "", $request->get('refund_now')),
                'type' => 'OUT',
                'is_main' => 'N',
                'is_refund' => 'Y',
                'post_net_profit' => 'NERACA',
            ];
            $this->saveJurnalBenchmark($postData);
        }

        $bookingId =$request->get('booking_id');
        // batalkan SPJ jika ada
        $cancelSpj = \App\TravelModel\TrnsctSpj::where("booking_id", "$bookingId")->update(["status" => "BATAL"]);

        $notice = [
            "title" => "Berhasil!",
            "type" => "success",
            "text" => "Booking berhasil dibatalkan.",
            "payment_id" => $paymentID,
        ];

        return response()->json($notice);
    }

    public function saveJurnalBenchmark($request)
    {
        $jurnal = new \App\TravelModel\TrnsctJurnal();
        $jurnal->booking_id = $request['booking_id'];
        $jurnal->proof_id = $request['proof_id'];
        $jurnal->trnsct_date = $request['trnsct_date'];
        $jurnal->related_person = $request['related_person'];
        $jurnal->account_id = $request['account_id'];
        $jurnal->description = $request['description'];
        $jurnal->debit = $request['debit'];
        $jurnal->credit = $request['credit'];
        $jurnal->type = $request['type'];
        $jurnal->is_main = $request['is_main'];
        $jurnal->is_refund = $request['is_refund'];
        $jurnal->post_net_profit = $request['post_net_profit'];
        $jurnal->user = Session::get('auth_nama');
        $jurnal->save();
    }

    public function printCancel($paymentID)
    {
        $payment = \App\TravelModel\TrnsctJurnal::
            with('coa')
            ->where('proof_id', "$paymentID")
            ->where('is_refund', 'Y')
            ->first();

        $payments = \App\TravelModel\TrnsctJurnal::
            where('proof_id', "$paymentID")
            ->where('is_refund', 'Y')
            ->get();

        $booking = \App\TravelModel\TrnsctBooking::
            where('booking_id', "$payment->booking_id")
            ->first();

        $bookingDtl = \App\TravelModel\TrnsctBookingDtl::
            with('carCategory')
            ->where('booking_id', "$payment->booking_id")
            ->get();

        $pdf = PDF::loadView('travel.TrnsctBooking.print.print', compact(
            'payment',
            'payments',
            'booking',
            'bookingDtl'
        ));

        return $pdf->stream("kwitansi_pembatalan_$paymentID.pdf");
    }

    public function getDetailCancel(Request $request)
    {
        $booking = \App\TravelModel\TrnsctBooking::find($request['bookingId']);

        $res = [
            'cancel_by' => $booking->cancel_by,
            'money_refundable_plan' => $booking->money_refundable_plan,
            'money_return' => $booking->money_return,
        ];

        return response()->json($res);
    }

    public function cancelBookingSecond(Request $request)
    {
        // update money return
        $booking = \App\TravelModel\TrnsctBooking::find($request['booking_id']);
        $booking->money_return = $booking->money_return + str_replace(".", "", $request->get('refund_now'));
        $booking->save();

        // get payment id
        $paymentID = $this->getPaymentID();

        // store to jurnal table
        $postData = [
            'proof_id' => $paymentID,
            'booking_id' => $booking->booking_id,
            'trnsct_date' => $request['refund_date'],
            'related_person' => $request['related_person'],
            'account_id' => $request['money_get_from'],
            'description' => 'Angsuran refund kode boking'.$booking->booking_id,
            'debit' => 0,
            'credit' => str_replace(".", "", $request->get('refund_now')),
            'type' => 'OUT',
            'is_main' => 'N',
            'is_refund' => 'Y',
            'post_net_profit' => 'NERACA',
        ];
        $this->saveJurnalBenchmark($postData);

        $postData = [
            'proof_id' => $paymentID,
            'booking_id' => $booking->booking_id,
            'trnsct_date' => $request['refund_date'],
            'related_person' => $request['related_person'],
            'account_id' => '1-130',
            'description' => 'Angsuran refund kode boking'.$booking->booking_id,
            'credit' => 0,
            'debit' => str_replace(".", "", $request->get('refund_now')),
            'type' => 'OUT',
            'is_main' => 'Y',
            'is_refund' => 'Y',
            'post_net_profit' => 'NERACA',
        ];
        $this->saveJurnalBenchmark($postData);

        $postData = [
            'proof_id' => $paymentID,
            'booking_id' => $booking->booking_id,
            'trnsct_date' => $request['refund_date'],
            'related_person' => $request['related_person'],
            'account_id' => $request['money_get_from'],
            'description' => 'Angsuran refund kode boking'.$booking->booking_id,
            'debit' => 0,
            'credit' => str_replace(".", "", $request->get('refund_now')),
            'type' => 'OUT',
            'is_main' => 'N',
            'is_refund' => 'Y',
            'post_net_profit' => 'NERACA',
        ];
        $this->saveJurnalBenchmark($postData);

        $notice = [
            "title" => "Berhasil!",
            "type" => "success",
            "text" => "Booking berhasil dibatalkan.",
            "payment_id" => $paymentID,
        ];

        return response()->json($notice);
    }

    public function leavingThisWeek()
    {
        $monday = strtotime("last monday");
        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
        $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
        $this_week_sd = date("Y-m-d",$monday);
        $this_week_ed = date("Y-m-d",$sunday);
        $ar = [
            'this_week_sd' => $this_week_sd,
            'this_week_ed' => $this_week_ed,
        ];

        $bookings = \App\TravelModel\TrnsctBooking::with(['cars', 'cars.carCategory'])
            ->where('booking_start_date', '<=', $this_week_ed)
            ->where('booking_start_date', '>=', $this_week_sd)
            ->orderBy('booking_start_date', 'ASC')
            ->get();

        $cashAccount = Cache::rememberForever('list_finance_account', function () {
            return \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->orderBy("account_id", "ASC")
            ->get();
        });

        return view('travel.TrnsctBooking.list.leavingThisWeek', compact('bookings', 'cashAccount'));
    }

    public function deleteBooking(Request $request)
    {
        $bookingId = $request->get('booking_id');
        $res = [];

        try {
            \App\TravelModel\TrnsctBooking::findOrFail($bookingId)->delete();
            \App\TravelModel\TrnsctBookingDtl::where('booking_id', "$bookingId")->delete();

            $res = [
                'type' => 'success',
                'title' => 'Berhasil',
                'text' => 'Booking berhasil dihapus'
            ];
        } catch (\Throwable $th) {
            $res = [
                'type' => 'error',
                'title' => 'Gagal',
                'text' => 'Gagal berhasil dihapus'
            ];
        }

        return response()->json($res);
    }
}
