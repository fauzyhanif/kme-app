<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RefCalendarController extends Controller
{
    protected $TrnsctBookingController;

    public function __construct(TrnsctBookingController $TrnsctBookingController)
    {
        $this->TrnsctBookingController = $TrnsctBookingController;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $thisMonth = date('Y-m');
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if ($request->method() == 'POST') {
            $type = $request->get('type');
            $today = $request->get('this_month');

            if ($type == 'minus') {
                $datestring = $today . ' first day of last month';
                $dt = date_create($datestring);
                $thisMonth = $dt->format('Y-m');
                $firstDay = $dt->format('Y-m-01');
                $lastDay = $dt->format('Y-m-t');
            } else {
                $datestring = $today . ' first day of +1 month';
                $dt = date_create($datestring);
                $thisMonth = $dt->format('Y-m');
                $firstDay = $dt->format('Y-m-01');
                $lastDay = $dt->format('Y-m-t');
            }
        }

        $a_month = explode("-", $thisMonth)[1];

        // get num days of month
        $numOfDays = cal_days_in_month(CAL_GREGORIAN, substr($thisMonth, -2), substr($thisMonth, 0, 4));

        // get unit ready
        $units = $this->TrnsctBookingController->getCarReady();

        // get car booked
        $carBooked = $this->getCarBooked($firstDay, $lastDay);
        $arrBooked = [];

        foreach ($carBooked as $key => $value) {
            $day_diff = strtotime($value->date_not_for_sale) - strtotime($value->booking_start_date);
            $day_diff = ($day_diff == 0) ? 8460 : $day_diff;
            $num = floor($day_diff/(60*60*24));
            $num = ($value->date_not_for_sale == $value->booking_start_date) ? 1 : $num;

            $startTime = strtotime( "$value->booking_start_date 00:01" );
            $endTime = strtotime( "$value->date_not_for_sale 23:59" );

            // Loop between timestamps, 24 hours at a time
            for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {
                $thisDate = date( 'm_d', $i ); //
                if (array_key_exists($value->category_id."_".$thisDate, $arrBooked)) {
                    $arrBooked[$value->category_id."_".$thisDate] = $arrBooked[$value->category_id."_".$thisDate] + $value->unit_qty;
                } else {
                    $arrBooked[$value->category_id."_".$thisDate] = $value->unit_qty;
                }
            }
        }

        return view('travel.RefCalendar.index', compact(
            'firstDay',
            'lastDay',
            'units',
            'numOfDays',
            'thisMonth',
            'a_month',
            'arrBooked'
        ));
    }

    public function getCarBooked($startDate, $endDate)
    {
        $datas = DB::table('trvl_trnsct_booking_dtl as a')
        ->leftJoin('trvl_trnsct_booking as b', 'a.booking_id', '=', 'b.booking_id')
        ->select('a.*')
        ->where('b.status', '!=', 'BATAL')
        ->where(function($query) use  ($startDate, $endDate){
            $query->where('a.booking_start_date', '<=', "$startDate")
                ->where('a.date_not_for_sale' , '>=', "$startDate");
        })
        ->orWhere(function($query) use  ($startDate, $endDate){
            $query->where('a.booking_start_date', '<=', "$endDate")
                ->where('a.date_not_for_sale' , '>=', "$endDate");
        })
        ->orWhere(function($query) use  ($startDate, $endDate){
            $query->where('a.booking_start_date', '>=', "$startDate")
                ->where('a.date_not_for_sale' , '<=', "$endDate");
        })

        ->get();

        return $datas;
    }
}
