<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class ReportFinanceMonthlyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexBackup(Request $request)
    {
        $month = date('m');
        $year = date('Y');

        if ($request->input('year')) {
            $year = $request->input('year');
            $month = $request->input('month');
        }

        $convert_date = $year . '-' . $month . '-01';

        $incomes = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('type', '=', 'IN')
            ->where(function ($query) {
                $query->where('account_id', '=', '1-110')
                    ->orWhere('account_id', '=', '1-120');
            })
            ->sum('debit');

        $incomes_kantor = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->where('type', '=', 'IN')
            ->sum('debit');

        $incomes_bri = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->where('type', '=', 'IN')
            ->sum('debit');

        $expenses = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('type', '=', 'OUT')
            ->where(function ($query) {
                $query->where('account_id', '=', '1-110')
                    ->orWhere('account_id', '=', '1-120');
            })
            ->sum('credit');

        $expenses_kantor = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->where('type', '=', 'OUT')
            ->sum('credit');

        $expenses_bri = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->where('type', '=', 'OUT')
            ->sum('credit');

        $saldo_kantor = \App\TravelModel\TrnsctJurnal::where('trnsct_date', '<', $convert_date)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->sum(DB::raw('debit - credit'));

        $saldo_bri = \App\TravelModel\TrnsctJurnal::where('trnsct_date', '<', $convert_date)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->sum(DB::raw('debit - credit'));


        return view('travel.ReportFinanceMonthly.index', compact(
            'month',
            'year',
            'incomes',
            'incomes_kantor',
            'incomes_bri',
            'expenses',
            'expenses_kantor',
            'expenses_bri',
            'saldo_kantor',
            'saldo_bri'
        ));
    }

    public function index(Request $request)
    {
        $month = date('m');
        $year = date('Y');

        if ($request->input('year')) {
            $year = $request->input('year');
            $month = $request->input('month');
        }

        $convert_date = $year . '-' . $month . '-01';

        $car_category = \App\TravelModel\RefCarCategory::where('active', 'Y')->orderBy('seat', 'DESC')->get();
        $car_category_income = [];

        foreach ($car_category as $key => $value) {
            $car_category_income[$value->category_id] = [
                'name' => 'Sewa ' . $value->name . ' Seat ' . $value->seat,
                'amount' => 0
            ];
        }

        $incomes_object = DB::table('trvl_trnsct_spj as a')
            ->leftJoin('trvl_trnsct_booking_dtl as b', function($join)
            {
                $join->on('a.booking_id', '=', 'b.booking_id');
                $join->on('a.category_id', '=', 'b.category_id');
            })
            ->leftJoin('trvl_trnsct_booking as c', 'a.booking_id', '=', 'c.booking_id')
            ->leftJoin('trvl_ref_driver as d', 'a.main_driver', '=', 'd.driver_id')
            ->select('a.category_id', 'a.budget as kas_jalan', 'b.price as harga')
            ->whereYear('.c.booking_start_date', '=', $year)
            ->whereMonth('c.booking_start_date', '=', $month)
            ->orderBy('b.booking_id', 'DESC')
            ->get();

        foreach ($incomes_object as $key => $value) {
            $car_category_income[$value->category_id]['amount'] += $value->harga - $value->kas_jalan;
        }

        $incomes = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('type', '=', 'IN')
            ->where(function ($query) {
                $query->where('account_id', '=', '1-110')
                    ->orWhere('account_id', '=', '1-120');
            })
            ->sum('debit');

        $incomes_kantor = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->where('type', '=', 'IN')
            ->sum('debit');

        $incomes_bri = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->where('type', '=', 'IN')
            ->sum('debit');

        $expenses = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->whereHas('coa', function ($query) {
                $query->where('post_sub_report', '=', 'BIAYA')
                        ->orWhere('post_sub_report', '=', 'BIAYA_LAIN');
            })
            ->sum('debit');

        $expenses_kantor = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->where('type', '=', 'OUT')
            ->sum('credit');

        $expenses_bri = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->where('type', '=', 'OUT')
            ->sum('credit');

        $saldo_kantor = \App\TravelModel\TrnsctJurnal::where('trnsct_date', '<', $convert_date)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->sum(DB::raw('debit - credit'));

        $saldo_bri = \App\TravelModel\TrnsctJurnal::where('trnsct_date', '<', $convert_date)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->sum(DB::raw('debit - credit'));


        return view('travel.ReportFinanceMonthly.index', compact(
            'month',
            'year',
            'incomes',
            'car_category_income',
            'incomes_kantor',
            'incomes_bri',
            'expenses',
            'expenses_kantor',
            'expenses_bri',
            'saldo_kantor',
            'saldo_bri'
        ));
    }

    public function print(Request $request)
    {
        $month = date('m');
        $year = date('Y');

        if ($request->input('year')) {
            $year = $request->input('year');
            $month = $request->input('month');
        }

        $convert_date = $year . '-' . $month . '-01';

        $car_category = \App\TravelModel\RefCarCategory::where('active', 'Y')->orderBy('seat', 'DESC')->get();
        $car_category_income = [];

        foreach ($car_category as $key => $value) {
            $car_category_income[$value->category_id] = [
                'name' => 'Sewa ' . $value->name . ' Seat ' . $value->seat,
                'amount' => 0
            ];
        }

        $incomes_object = DB::table('trvl_trnsct_spj as a')
            ->leftJoin('trvl_trnsct_booking_dtl as b', function($join)
            {
                $join->on('a.booking_id', '=', 'b.booking_id');
                $join->on('a.category_id', '=', 'b.category_id');
            })
            ->leftJoin('trvl_trnsct_booking as c', 'a.booking_id', '=', 'c.booking_id')
            ->leftJoin('trvl_ref_driver as d', 'a.main_driver', '=', 'd.driver_id')
            ->select('a.category_id', 'a.budget as kas_jalan', 'b.price as harga')
            ->whereYear('.c.booking_start_date', '=', $year)
            ->whereMonth('c.booking_start_date', '=', $month)
            ->orderBy('b.booking_id', 'DESC')
            ->get();

        foreach ($incomes_object as $key => $value) {
            $car_category_income[$value->category_id]['amount'] += $value->harga - $value->kas_jalan;
        }

        $incomes = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('type', '=', 'IN')
            ->where(function ($query) {
                $query->where('account_id', '=', '1-110')
                    ->orWhere('account_id', '=', '1-120');
            })
            ->sum('debit');

        $incomes_kantor = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->where('type', '=', 'IN')
            ->sum('debit');

        $incomes_bri = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->where('type', '=', 'IN')
            ->sum('debit');

        $expenses = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->whereHas('coa', function ($query) {
                $query->where('post_sub_report', '=', 'BIAYA')
                        ->orWhere('post_sub_report', '=', 'BIAYA_LAIN');
            })
            ->sum('debit');

        $expenses_kantor = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->where('type', '=', 'OUT')
            ->sum('credit');

        $expenses_bri = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->where('type', '=', 'OUT')
            ->sum('credit');

        $saldo_kantor = \App\TravelModel\TrnsctJurnal::where('trnsct_date', '<', $convert_date)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-110')
            ->sum(DB::raw('debit - credit'));

        $saldo_bri = \App\TravelModel\TrnsctJurnal::where('trnsct_date', '<', $convert_date)
            ->where('status', '!=', 'BATAL')
            ->where('account_id', '=', '1-120')
            ->sum(DB::raw('debit - credit'));

        $pdf = PDF::loadView('travel.ReportFinanceMonthly.print', compact(
            'month',
            'year',
            'car_category_income',
            'incomes',
            'incomes_kantor',
            'incomes_bri',
            'expenses',
            'expenses_kantor',
            'expenses_bri',
            'saldo_kantor',
            'saldo_bri'
        ));

        return $pdf->stream();
    }
}
