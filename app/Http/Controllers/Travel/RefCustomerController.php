<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class RefCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.RefCustomer.index');
    }

    public function jsonData()
    {
        $customers = \App\TravelModel\RefCustomer::all();
        return DataTables::of($customers)
            ->addColumn('actions_link', function($customers)
            {
                // details link
                $btn = "<a href='" . route('travel.customer.detail', $customers->cust_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                // edits link
                $btn .= "<a href='" . route('travel.customer.formedit', $customers->cust_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function detail($customer_id)
    {
        $customer = \App\TravelModel\RefCustomer::findOrFail($customer_id);
        return view('travel.RefCustomer.detail', \compact('customer'));
    }

    public function datatableDetailPage(Request $request)
    {
        $custId = $request['cust_id'];
        $trnscts = \App\TravelModel\TrnsctBooking::where("cust_id", "$custId")
            ->orderBy("booking_id", "DESC")
            ->get();

        return DataTables::of($trnscts)
            ->addColumn('booking_date', function($trnscts){
                return date_format(date_create($trnscts->booking_start_date), 'd/m/Y');
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formAdd()
    {
        return view('travel.RefCustomer.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
        ]);

        $new = new \App\TravelModel\RefCustomer();
        $new->name = $request->get('name');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($customer_id)
    {
        $customer = \App\TravelModel\RefCustomer::findOrFail($customer_id);

        return view('travel.RefCustomer.formEdit', \compact('customer'));
    }

    public function edit(Request $request, $customer_id)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
        ]);

        $new = \App\TravelModel\RefCustomer::findOrFail($customer_id);
        $new->name = $request->get('name');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }

    public function getDataForRemoteSelect(Request $request){

        $search = $request->search;

        if($search == ''){
            $employees = \App\TravelModel\RefCustomer::orderBy('name','asc')
                ->select('cust_id','name', 'phone_num', 'address')
                ->limit(10)
                ->get();
        }else{
            $employees = \App\TravelModel\RefCustomer::orderBy('name','asc')
                ->select('cust_id','name', 'phone_num', 'address')
                ->where('name', 'like', '%' .$search . '%')
                ->limit(10)
                ->get();
        }

        $response = array();
        foreach($employees as $employee){
            $response[] = array(
                "id"=> "$employee->cust_id | $employee->name | $employee->phone_num | $employee->address",
                "text"=>$employee->name
            );
        }

        echo json_encode($response);
        exit;
   }
}
