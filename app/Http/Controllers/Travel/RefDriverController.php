<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use App\TravelModel\RefDriver;
use App\TravelModel\RefDriverSaving;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class RefDriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.RefDriver.index');
    }

    public function jsonData()
    {
        $drivers = DB::table('trvl_ref_driver as a')
            ->leftJoin('trvl_ref_car as b', 'a.police_num', '=', 'b.police_num')
            ->leftJoin('trvl_ref_car_category as c', 'b.category_id', '=', 'c.category_id')
            ->select('a.name as driver_nm', 'a.phone_num', 'a.address', 'a.driver_id',
                    'a.driver_type', 'a.police_num', 'c.name as category_nm')
            ->where('a.active', 'Y')
            ->orderBy('a.name', 'ASC')
            ->get();

        return DataTables::of($drivers)
            ->addColumn('driver_for_car', function($drivers)
            {
                $result = $drivers->police_num . " (" . $drivers->category_nm . ")";
                return $result;
            })
            ->addColumn('actions_link', function($drivers)
            {
                // details link
                $btn = "<a href='" . route('travel.driver.detail', $drivers->driver_id) . "' class='btn btn-info btn-xs'>";
                $btn .= "<i class='fas fa-eye'></i> Detail";
                $btn .= "</a> ";

                // edits link
                $btn .= "<a href='" . route('travel.driver.formedit', $drivers->driver_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['driver_for_car', 'actions_link'])
            ->toJson();
    }

    public function detail($driver_id)
    {
        $driver = RefDriver::with(['car.carCategory'])
                    ->where("driver_id", $driver_id)
                    ->first();


        $categories = \App\TravelModel\RefCarCategory::where('active', 'Y')->get();
        return view('travel.RefDriver.detail', \compact('driver', 'categories'));
    }

    public function allTrnsctPage($driverId)
    {
        $driver = RefDriver::where("driver_id", $driverId)->first();

        return view('travel.RefDriver.allDriverHistory', \compact("driver"));
    }

    public function getAllDriverHistoryDataJson(Request $request)
    {
        $driverId = $request['driver_id'];
        $histories = \App\TravelModel\TrnsctSpj::with('booking')
                    ->where("main_driver", "$driverId")
                    ->orWhere("second_driver", "$driverId")
                    ->orderBy('booking_id', 'DESC')
                    ->get();

        return DataTables::of($histories)
            ->addColumn('booking_date', function($histories)
            {
                return date_format(date_create($histories->booking->booking_start_date), 'd/m/Y');
            })
            ->toJson();
    }

    public function formAdd()
    {
        $categories = \App\TravelModel\RefCarCategory::where('active', 'Y')->get();
        $cars = \App\TravelModel\RefCar::with('carCategory')
            ->orderBy('category_id', 'ASC')
            ->get();

        return view('travel.RefDriver.formAdd', \compact('categories', 'cars'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
            'driver_type'=>'required',
            'car_permit'=>'required',
        ]);

        $new = new RefDriver();
        $new->name = $request->get('name');
        $new->sim_num = $request->get('sim_num');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->driver_type = $request->get('driver_type');
        $new->police_num = $request->get('police_num');
        $new->car_permit = \json_encode($request->get('car_permit'));
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($driver_id)
    {
        $categories = \App\TravelModel\RefCarCategory::where('active', 'Y')->get();
        $cars = \App\TravelModel\RefCar::all();
        $driver = RefDriver::findOrFail($driver_id);

        return view('travel.RefDriver.formEdit', \compact('categories', 'cars', 'driver'));
    }

    public function edit(Request $request, $driver_id)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
            'driver_type'=>'required',
            'car_permit'=>'required',
        ]);

        $new = RefDriver::findOrFail($driver_id);
        $new->name = $request->get('name');
        $new->sim_num = $request->get('sim_num');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->driver_type = $request->get('driver_type');
        $new->police_num = $request->get('police_num');
        $new->car_permit = \json_encode($request->get('car_permit'));
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }

    public function getFiveDriverSavingData(Request $request)
    {
        $post = $request->all();
        $column = array_keys($post)[0];
        $value = $post[$column];

        $savings = RefDriverSaving::where("$column", "$value")
                    ->where('type', $post['user_type'])
                    ->limit(5)
                    ->get();

        return view('travel.RefDriver.displayFiveDriverSavingData', \compact("savings"));
    }

    public function getFiveDriverJourneyData(Request $request)
    {
        $post = $request->all();
        $driver_id = $request->get('main_driver');

        $journeys = \App\TravelModel\TrnsctSpj::with(['booking'])
                    ->where("main_driver", "$driver_id")
                    ->orWhere("second_driver", "$driver_id")
                    ->limit(5)
                    ->get();

        return view('travel.RefDriver.displayFiveDriverJourneyData', \compact("journeys"));
    }

    public function allDriverSaving($driver_id)
    {
        $driver = RefDriver::findOrFail($driver_id);
        return view('travel.RefDriver.allDriverSaving', \compact("driver"));

    }

    public function allDriverSavingData(Request $request)
    {
        $driver_id = $request['driver_id'];
        $savings = RefDriverSaving::where('user_id', $driver_id)
                    ->where('type', 'DRIVER')
                    ->orderBy('trnsct_date', 'DESC')
                    ->get();

        return DataTables::of($savings)
            ->editColumn('trnsct_date', function($savings)
            {
                $result = date_format(date_create($savings->trnsct_date), 'd/m/Y');
                return $result;
            })
            ->editColumn('in', function($savings)
            {
                $result = number_format($savings->in,2,',','.');;
                return $result;
            })
            ->editColumn('out', function($savings)
            {
                $result = number_format($savings->out,2,',','.');;
                return $result;
            })
            ->addColumn('actions_link', function($savings)
            {
                if ($savings->create_manual == 1) {
                    $btn = "<button type='button' class='btn btn-danger btn-xs' onclick=\"showFormDeleteSaving('$savings->id')\">";
                    $btn .= "<i class='fas fa-trash'></i> Hapus";
                    $btn .= "</button> ";

                    return $btn;
                }
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function savingRetrieval(Request $request)
    {
        $amount = str_replace(".", "", $request['out']);
        $saving = new RefDriverSaving();
        $saving->type = "DRIVER";
        $saving->trnsct_date = $request['trnsct_date'];
        $saving->user_id = $request['user_id'];
        $saving->out = $amount;
        $saving->information = $request['information'];
        $saving->save();

        $this->savingReduction($request['user_id'], $amount);

        $response = "Berhasil. Data berhasil disimpan.";
        return response()->json($response);
    }

    public function savingReduction($driver_id, $amount)
    {
        $driver = RefDriver::findOrFail($driver_id);
        $driver->ttl_saving = $driver->ttl_saving - $amount;
        $driver->save();
    }

    public function initialSaving(Request $request)
    {
        DB::beginTransaction();
        try {
            $driver = RefDriver::findOrFail($request->driver_id);
            $driver->ttl_saving = $driver->ttl_saving + (int) str_replace('.', '', $request->in);
            $driver->save();

            $saving = new RefDriverSaving();
            $saving->type = 'DRIVER';
            $saving->user_id = $request->driver_id;
            $saving->trnsct_date = date('Y-m-d');
            $saving->information = 'Tabungan awal ditambahkan secara manual';
            $saving->in = (int) str_replace('.', '', $request->in);
            $saving->create_manual = 1;
            $saving->save();

            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Data berhasil diinput']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $th->getMessage()]);
        }    
    }

    public function deleteInitialSaving(Request $request)
    {
        DB::beginTransaction();
        try {
            $saving = RefDriverSaving::findOrFail($request->id);

            $driver = RefDriver::findOrFail($saving->user_id);
            $driver->ttl_saving = $driver->ttl_saving - $saving->in;
            $driver->save();

            $saving->delete(); 

            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Data berhasil dihapus']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $th->getMessage()]);
        } 
    }
}
