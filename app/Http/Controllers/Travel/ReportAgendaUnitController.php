<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;

class ReportAgendaUnitController extends Controller
{
    public function index(Request $request)
    {
        $month = date('m');
        $year = date('Y');

        if ($request->input('year')) {
            $year = $request->input('year');
            $month = $request->input('month');
        }

        // get data report
        $reports = $this->getData($month, $year);
        $carCategories = \App\TravelModel\RefCarCategory::where('active', 'Y')->orderBy('category_id')->get();
        return view('travel.ReportAgendaUnit.index', \compact(
            'month',
            'year',
            'reports',
            'carCategories'
        ));
    }

    public function print(Request $request)
    {
        $month = $request->get('month');
        $year = $request->get('year');
        $categoryId = explode('|',$request->get('category_id'))[0];
        $categoryName = explode('|',$request->get('category_id'))[1];

        // get data report
        $reports = $this->getDataByCategoryId($month, $year, $categoryId);

        $pdf = PDF::loadView('travel.ReportAgendaUnit.print', compact(
            'month',
            'year',
            'categoryName',
            'reports'
        ))->setPaper('f4', 'landscape');;

        return $pdf->stream();
    }

    public function getData($month, $year)
    {
        $reports = DB::table('trvl_trnsct_spj as a')
            ->leftJoin('trvl_trnsct_booking_dtl as b', function($join)
            {
                $join->on('a.booking_id', '=', 'b.booking_id');
                $join->on('a.category_id', '=', 'b.category_id');
            })
            ->leftJoin('trvl_trnsct_booking as c', 'a.booking_id', '=', 'c.booking_id')
            ->leftJoin('trvl_ref_driver as d', 'a.main_driver', '=', 'd.driver_id')
            ->leftJoin('trvl_ref_car_category as e', 'a.category_id', '=', 'e.category_id')
            ->select('c.booking_start_date', 'c.booking_end_date', 'a.police_num','d.name as driver_nm',
                    'c.pick_up_location', 'c.standby_time', 'c.destination', 'c.cust_name', 'c.cust_phone_num',
                    'a.budget as kas_jalan', 'b.price as harga', 'e.name as category_name')
            ->whereYear('c.booking_start_date', '=', $year)
            ->whereMonth('c.booking_start_date', '=', $month)
            ->orderBy('b.booking_id', 'DESC')
            ->get();

        return $reports;
    }

    public function getDataByCategoryId($month, $year, $categoryId)
    {
        $reports = DB::table('trvl_trnsct_spj as a')
            ->leftJoin('trvl_trnsct_booking_dtl as b', function($join)
            {
                $join->on('a.booking_id', '=', 'b.booking_id');
                $join->on('a.category_id', '=', 'b.category_id');
            })
            ->leftJoin('trvl_trnsct_booking as c', 'a.booking_id', '=', 'c.booking_id')
            ->leftJoin('trvl_ref_driver as d', 'a.main_driver', '=', 'd.driver_id')
            ->leftJoin('trvl_ref_car_category as e', 'a.category_id', '=', 'e.category_id')
            ->select('c.booking_start_date', 'c.booking_end_date', 'a.police_num','d.name as driver_nm',
                    'c.pick_up_location', 'c.standby_time', 'c.destination', 'c.cust_name', 'c.cust_phone_num',
                    'a.budget as kas_jalan', 'b.price as harga', 'e.name as category_name')
            ->whereYear('c.booking_start_date', '=', $year)
            ->whereMonth('c.booking_start_date', '=', $month)
            ->where('a.category_id', $categoryId)
            ->orderBy('b.booking_id', 'DESC')
            ->get();

        return $reports;
    }
}
