<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class ReportIncomeMonthlyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $month = date('m');
        $year = date('Y');

        if ($request->input('year')) {
            $year = $request->input('year');
            $month = $request->input('month');
        }

        $car_category_income = $this->getData($month, $year);

        return view('travel.ReportIncomeMonthly.index', compact(
            'month',
            'year',
            'car_category_income'
        ));
    }

    public function print(Request $request)
    {
        $year = $request->input('year');
        $month = $request->input('month');

        $car_category_income = $this->getData($month, $year);

        $pdf = PDF::loadView('travel.ReportIncomeMonthly.print', compact(
            'month',
            'year',
            'car_category_income'
        ));

        return $pdf->stream();
    }

    public function getData($month, $year)
    {
        $car_category = \App\TravelModel\RefCarCategory::where('active', 'Y')->orderBy('seat', 'DESC')->get();
        $car_category_income = [];

        foreach ($car_category as $key => $value) {
            $car_category_income[$value->category_id] = [
                'name' => 'Sewa ' . $value->name . ' Seat ' . $value->seat,
                'amount' => 0
            ];
        }

        $incomes_object = DB::table('trvl_trnsct_spj as a')
            ->leftJoin('trvl_trnsct_booking_dtl as b', function($join)
            {
                $join->on('a.booking_id', '=', 'b.booking_id');
                $join->on('a.category_id', '=', 'b.category_id');
            })
            ->leftJoin('trvl_trnsct_booking as c', 'a.booking_id', '=', 'c.booking_id')
            ->leftJoin('trvl_ref_driver as d', 'a.main_driver', '=', 'd.driver_id')
            ->select('a.category_id', 'a.budget as kas_jalan', 'b.price as harga')
            ->whereYear('.c.booking_start_date', '=', $year)
            ->whereMonth('c.booking_start_date', '=', $month)
            ->orderBy('b.booking_id', 'DESC')
            ->get();

        foreach ($incomes_object as $key => $value) {
            $car_category_income[$value->category_id]['amount'] += $value->harga - $value->kas_jalan;
        }

        return $car_category_income;
    }
}
