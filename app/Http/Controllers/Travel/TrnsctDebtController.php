<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use PDF;
use DataTables;

class TrnsctDebtController extends Controller
{
    protected $TrnsctBookingController;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $status = ($request->has('status')) ? $request->input('status') : 1;
        $year_min = 2019;
        $year_max = date('Y');
        $years = [];
        for ($i=$year_max; $i > $year_min; $i--) {
            $years[] = $i;
        }

        return view('travel.TrnsctDebt.index', compact('status', 'years'));
    }

    public function listData(Request $request)
    {
        $status = $request->get('status');
        $result = \App\TravelModel\TrnsctDebt::getDataJson($status);
        return $result;
    }

    public function formAdd()
    {
        $employes = \App\SystemModel\SysRefSdm::orderBy('name', 'ASC')->get();
        $cashAccounts = \App\TravelModel\RefFinanceAccount::where("account_id", "LIKE", "%1-%")->where("parent", "!=", "null")->orderBy("account_id", "ASC")->get();

        return view('travel.TrnsctDebt.formStore', compact('employes', 'cashAccounts'));
    }

    public function addnew(Request $request)
    {
        \App\TravelModel\TrnsctDebt::addNew($request->all());

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formPembayaran($id)
    {
        $debt = \App\TravelModel\TrnsctDebt::with(['sdm', 'sdm.position','payment', 'cashAccount', 'payment.cashAccount', 'payment.payrolPayment', 'payment.payrolPayment.payrol'])->findOrFail($id);
        $cashAccounts = \App\TravelModel\RefFinanceAccount::where("account_id", "LIKE", "%1-%")->where("parent", "!=", "null")->orderBy("account_id", "ASC")->get();
        return view('travel.TrnsctDebt.formPembayaran', compact('debt', 'cashAccounts'));
    }

    public function pembayaran(Request $request)
    {
        \App\TravelModel\TrnsctDebtPayment::addNew($request->all());
        return redirect()->back();
    }

    public function pembatalanPembayaran(Request $request)
    {
        \App\TravelModel\TrnsctDebtPayment::pembatalan($request->get('id'));
        return redirect()->back();
    }

    public function pembatalan(Request $request)
    {
        \App\TravelModel\TrnsctDebt::pembatalan($request->get('debt_id'));
        return redirect()->back();
    }

    public function print(Request $request)
    {
        $month = $request->get('month');
        $year = $request->get('year');

        $debts = \App\TravelModel\TrnsctDebt::with(['sdm'])->whereMonth('date', "$month")->whereYear('date', "$year")->orderBy('date', 'ASC')->get();
        return view('travel.TrnsctDebt.print', compact('debts'));
    }
}
