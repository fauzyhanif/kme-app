<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use App\TravelModel\RefDriverSaving;
use App\TravelModel\RefHelper;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class RefHelperController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.RefHelper.index');
    }

    public function jsonData()
    {
        $helpers = \App\TravelModel\RefHelper::where('active', 'Y')->orderBy('name')->get();
        return DataTables::of($helpers)
                ->addColumn('actions_link', function($helpers)
                {
                    // details link
                    $btn = "<a href='" . route('travel.helper.detail', $helpers->helper_id) . "' class='btn btn-info btn-xs'>";
                    $btn .= "<i class='fas fa-eye'></i> Detail";
                    $btn .= "</a> ";

                    // edits link
                    $btn .= "<a href='" . route('travel.helper.formedit', $helpers->helper_id) . "' class='btn btn-primary btn-xs'>";
                    $btn .= "<i class='fas fa-edit'></i> Edit";
                    $btn .= "</a> ";

                    return $btn;
                })
                ->rawColumns(['actions_link'])
                ->toJson();
    }

    public function detail($helper_id)
    {
        $helper = \App\TravelModel\RefHelper::findOrFail($helper_id);

        return view('travel.RefHelper.detail', \compact('helper'));
    }

    public function formAdd()
    {
        return view('travel.RefHelper.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
        ]);

        $new = new \App\TravelModel\RefHelper();
        $new->name = $request->get('name');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($helper_id)
    {
        $helper = \App\TravelModel\RefHelper::findOrFail($helper_id);

        return view('travel.RefHelper.formEdit', \compact('helper'));
    }

    public function edit(Request $request, $helper_id)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
        ]);

        $new = \App\TravelModel\RefHelper::findOrFail($helper_id);
        $new->name = $request->get('name');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }

    public function getFiveHelperSavingData(Request $request)
    {
        $helper_id = $request['helper_id'];
        $savings = \App\TravelModel\RefDriverSaving::where('user_id', $helper_id)
                    ->where('type', 'HELPER')
                    ->limit(5)
                    ->get();

        return view('travel.RefHelper.displayFiveHelperSavingData', \compact("savings"));
    }

    public function allHelperSaving($helper_id)
    {
        $helper = \App\TravelModel\RefHelper::findOrFail($helper_id);
        return view('travel.RefHelper.allHelperSaving', \compact("helper"));

    }

    public function allhelpersavingData(Request $request)
    {
        $helper_id = $request['helper_id'];
        $savings = \App\TravelModel\RefDriverSaving::where('user_id', $helper_id)
                    ->where('type', 'HELPER')
                    ->orderBy('trnsct_date', 'DESC')
                    ->get();

        return DataTables::of($savings)
            ->editColumn('trnsct_date', function($savings)
            {
                $result = date_format(date_create($savings->trnsct_date), 'd/m/Y');

                return $result;
            })
            ->editColumn('in', function($savings)
            {
                $result = number_format($savings->in,2,',','.');;
                return $result;
            })
            ->editColumn('out', function($savings)
            {
                $result = number_format($savings->out,2,',','.');;
                return $result;
            })
            ->addColumn('actions_link', function($savings)
            {
                if ($savings->create_manual == 1) {
                    $btn = "<button type='button' class='btn btn-danger btn-xs' onclick=\"showFormDeleteSaving('$savings->id')\">";
                    $btn .= "<i class='fas fa-trash'></i> Hapus";
                    $btn .= "</button> ";

                    return $btn;
                }
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function savingRetrieval(Request $request)
    {
        $amount = str_replace(".", "", $request['out']);
        $saving = new \App\TravelModel\RefDriverSaving();
        $saving->type = "HELPER";
        $saving->trnsct_date = $request['trnsct_date'];
        $saving->user_id = $request['user_id'];
        $saving->out = $amount;
        $saving->information = $request['information'];
        $saving->save();

        $this->savingReduction($request['user_id'], $amount);

        $response = "Berhasil. Data berhasil disimpan.";
        return response()->json($response);
    }

    public function savingReduction($helper_id, $amount)
    {
        $helper = \App\TravelModel\RefHelper::findOrFail($helper_id);
        $helper->ttl_saving = $helper->ttl_saving - $amount;
        $helper->save();
    }

    public function initialSaving(Request $request)
    {
        DB::beginTransaction();
        try {
            $helper = RefHelper::findOrFail($request->helper_id);
            $helper->ttl_saving = $helper->ttl_saving + (int) str_replace('.', '', $request->in);
            $helper->save();

            $saving = new RefDriverSaving();
            $saving->type = 'HELPER';
            $saving->user_id = $request->helper_id;
            $saving->trnsct_date = date('Y-m-d');
            $saving->information = 'Tabungan awal ditambahkan secara manual';
            $saving->in = (int) str_replace('.', '', $request->in);
            $saving->create_manual = 1;
            $saving->save();

            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Data berhasil diinput']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $th->getMessage()]);
        }    
    }

    public function deleteInitialSaving(Request $request)
    {
        DB::beginTransaction();
        try {
            $saving = RefDriverSaving::findOrFail($request->id);

            $helper = RefHelper::findOrFail($saving->user_id);
            $helper->ttl_saving = $helper->ttl_saving - $saving->in;
            $helper->save();

            $saving->delete(); 

            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Data berhasil dihapus']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $th->getMessage()]);
        } 
    }
}
