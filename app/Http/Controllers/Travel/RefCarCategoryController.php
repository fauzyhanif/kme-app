<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect,Response,DB;
use Illuminate\Support\Facades\File;
use DataTables;

class RefCarCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.RefCarCategory.index');
    }

    public function jsonData()
    {
        $categories = \App\TravelModel\RefCarCategory::orderBy('category_id')->get();
        return DataTables::of($categories)
            ->editColumn('active', function($categories)
            {
                $result = ($categories->active == 'Y') ? 'Aktif' : 'Nonaktif';
                return $result;
            })
            ->addColumn('actions_link', function($categories)
            {
                $btn = "<a href='" . route('travel.kategorikendaraan.formedit', $categories->id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formAdd()
    {
        return view('travel.RefCarCategory.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'category_id'=>'required|max:2|unique:trvl_ref_car_category',
            'name'=>'required',
            'seat'=>'required',
        ]);

        $new = new \App\TravelModel\RefCarCategory();
        $new->category_id = $request->get('category_id');
        $new->name = $request->get('name');
        $new->seat = $request->get('seat');
        $new->save();
        $response = [
            "type" => "success",
            "text" => "Kategori Kendaraan berhasil disimpan"
        ];

        return response()->json($response);
    }

    public function formEdit($id)
    {
        $category = \App\TravelModel\RefCarCategory::find($id);
        return view('travel.RefCarCategory.formEdit', \compact("category"));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'category_id'=>'required|max:2|unique:trvl_ref_car_category,category_id,' . $request->get('category_id') . ',category_id',
            'name'=>'required',
            'seat'=>'required',
            'active'=>'required',
        ]);

        $old = \App\TravelModel\RefCarCategory::find($id);
        $old->category_id = $request->get('category_id');
        $old->name = $request->get('name');
        $old->seat = $request->get('seat');
        $old->active = $request->get('active')[0];
        $old->update();
        $response = [
            "type" => "success",
            "text" => "Kategori Kendaraan berhasil diubah."
        ];

        return response()->json($response);
    }

}
