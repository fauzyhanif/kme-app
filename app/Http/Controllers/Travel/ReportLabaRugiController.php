<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class ReportLabaRugiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $firstDay = date('Y-m-01');
        $lastDay = date('Y-m-t');

        if (request()->isMethod('post')) {
            $firstDay = explode(" - ", $request->get('date'))[0];
            $lastDay = explode(" - ", $request->get('date'))[1];
        }

        return view('travel.ReportLabaRugi.index', compact(
            "firstDay",
            "lastDay"
        ));
    }

    public function print(Request $request)
    {
        $expl = explode(" - ", $request->get('date'));
        $startDate = $expl[0];
        $endDate = $expl[1];

        // cari pendapatan with 4-
        $mainIncomes = $this->getMainIncomes($startDate, $endDate);

        // cari pendapatan with 7-
        $otherIncomes = $this->getOtherIncomes($startDate, $endDate);

        // cari hpp with 5-
        $capitals = $this->getCapitals($startDate, $endDate);

        // cari biaya with 6-
        $costs = $this->getCosts($startDate, $endDate);

        // cari biaya lain with 7-
        $otherCosts = $this->getOtherCosts($startDate, $endDate);

        $pdf = PDF::loadView('travel.ReportLabaRugi.print', compact(
            "startDate",
            "endDate",
            "mainIncomes",
            "otherIncomes",
            "capitals",
            "costs",
            "otherCosts"
        ));
        return $pdf->stream();
    }

    public function getMainIncomes($startDate, $endDate)
    {
        $result = DB::table('trvl_ref_finance_account as a')
            ->rightJoin('trvl_trnsct_jurnal as b', 'a.account_id', '=', 'b.account_id')
            ->select('a.name', DB::raw('sum(b.credit) as ttl_credit, sum(b.debit) as ttl_debit'))
            ->whereBetween('b.trnsct_date', [$startDate, $endDate])
            ->where('a.post_report', 'LABARUGI')
            ->where('a.post_sub_report', 'PENDAPATAN')
            ->where('b.status', 'OK')
            ->groupBy('b.account_id', 'a.name')
            ->get();

        return $result;
    }

    public function getOtherIncomes($startDate, $endDate)
    {
        $result = DB::table('trvl_ref_finance_account as a')
            ->rightJoin('trvl_trnsct_jurnal as b', 'a.account_id', '=', 'b.account_id')
            ->select('a.name', DB::raw('sum(b.credit) as ttl_credit, sum(b.debit) as ttl_debit'))
            ->whereBetween('b.trnsct_date', [$startDate, $endDate])
            ->where('a.post_report', 'LABARUGI')
            ->where('a.post_sub_report', 'PENDAPATAN_LAIN')
            ->where('b.status', 'OK')
            ->groupBy('b.account_id', 'a.name')
            ->get();

        return $result;
    }

    public function getCapitals($startDate, $endDate)
    {
        $result = DB::table('trvl_ref_finance_account as a')
            ->rightJoin('trvl_trnsct_jurnal as b', 'a.account_id', '=', 'b.account_id')
            ->select('a.name', DB::raw('sum(b.credit) as ttl_credit, sum(b.debit) as ttl_debit'))
            ->whereBetween('b.trnsct_date', [$startDate, $endDate])
            ->where('a.post_report', 'LABARUGI')
            ->where('a.post_sub_report', 'HPP')
            ->where('b.status', 'OK')
            ->groupBy('b.account_id', 'a.name')
            ->get();

        return $result;
    }

    public function getCosts($startDate, $endDate)
    {
        $result = DB::table('trvl_ref_finance_account as a')
            ->rightJoin('trvl_trnsct_jurnal as b', 'a.account_id', '=', 'b.account_id')
            ->select('a.name', DB::raw('sum(b.credit) as ttl_credit, sum(b.debit) as ttl_debit'))
            ->whereBetween('b.trnsct_date', [$startDate, $endDate])
            ->where('a.post_report', 'LABARUGI')
            ->where('a.post_sub_report', 'BIAYA')
            ->where('b.status', 'OK')
            ->groupBy('b.account_id', 'a.name')
            ->get();

        return $result;
    }

    public function getOtherCosts($startDate, $endDate)
    {
        $result = DB::table('trvl_ref_finance_account as a')
            ->rightJoin('trvl_trnsct_jurnal as b', 'a.account_id', '=', 'b.account_id')
            ->select('a.name', DB::raw('sum(b.credit) as ttl_credit, sum(b.debit) as ttl_debit'))
            ->whereBetween('b.trnsct_date', [$startDate, $endDate])
            ->where('a.post_report', 'LABARUGI')
            ->where('a.post_sub_report', 'BIAYA_LAIN')
            ->where('b.status', 'OK')
            ->groupBy('b.account_id', 'a.name')
            ->get();

        return $result;
    }
}
