<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class RefFinancialAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.RefFinancialAccount.index');
    }

    public function jsonData()
    {
        $accounts = \App\TravelModel\RefFinanceAccount::with('group')
            ->where('active', 'Y')
            ->orderBy('account_id', 'ASC')
            ->get();

        return DataTables::of($accounts)
            ->editColumn('name', function($accounts)
            {
                $result = ($accounts->parent != "") ? "--  " . $accounts->name : $accounts->name;
                return $result;
            })
            ->addColumn('actions_link', function($accounts)
            {
                // edits link
                $btn = "<a href='" . route('travel.finance.account.formedit', $accounts->id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function formAdd()
    {
        $groups = \App\TravelModel\RefFinanceAccountGroup::where('active', 'Y')->get();
        return view('travel.RefFinancialAccount.formAdd', compact('groups'));
    }

    public function getParent($id)
    {
        $accounts = \App\TravelModel\RefFinanceAccount::where('group_id', "$id")
            ->where('active', 'Y')
            ->get();

        return view('travel.RefFinancialAccount.parentFormSelect', compact('accounts'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'group_id'=>'required',
            'account_id'=>'required',
            'name'=>'required',
            'post_report'=>'required'
        ]);

        $new = new \App\TravelModel\RefFinanceAccount();
        $new->group_id = $request->get('group_id');
        $new->parent = $request->get('parent');
        $new->account_id = $request->get('account_id');
        $new->name = $request->get('name');
        $new->post_report = $request->get('post_report');
        $new->cash_flow_group = $request->get('cash_flow_group');
        $new->debit = $request->get('debit');
        $new->credit = $request->get('credit');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $account = \App\TravelModel\RefFinanceAccount::findOrFail($id);
        $accounts = \App\TravelModel\RefFinanceAccount::where("group_id", $account->group_id)
            ->where('active', 'Y')
            ->orderBy('account_id', 'ASC')
            ->get();

        $groups = \App\TravelModel\RefFinanceAccountGroup::where('active', 'Y')->get();

        return view('travel.RefFinancialAccount.formEdit', \compact(
            'account',
            'accounts',
            'groups'
        ));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'group_id'=>'required',
            'account_id'=>'required',
            'name'=>'required',
            'post_report'=>'required'
        ]);

        $new = \App\TravelModel\RefFinanceAccount::findOrFail($id);
        $new->group_id = $request->get('group_id');
        $new->parent = $request->get('parent');
        $new->account_id = $request->get('account_id');
        $new->name = $request->get('name');
        $new->post_report = $request->get('post_report');
        $new->cash_flow_group = $request->get('cash_flow_group');
        $new->debit = $request->get('debit');
        $new->credit = $request->get('credit');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
