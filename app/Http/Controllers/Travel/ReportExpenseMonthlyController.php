<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class ReportExpenseMonthlyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $month = date('m');
        $year = date('Y');

        if ($request->input('year')) {
            $year = $request->input('year');
            $month = $request->input('month');
        }

        // $expenses = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
        //     ->whereMonth('trnsct_date', '=', $month)
        //     ->where('status', '!=', 'BATAL')
        //     ->where('type', '=', 'OUT')
        //     ->where(function ($query) {
        //         $query->where('account_id', '=', '1-110')
        //             ->orWhere('account_id', '=', '1-120');
        //     })
        //     ->paginate(25);

        $expenses = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->whereHas('coa', function ($query) {
                $query->where('post_sub_report', '=', 'BIAYA')
                        ->orWhere('post_sub_report', '=', 'BIAYA_LAIN');
            })
            ->get();


        return view('travel.ReportExpenseMonthly.index', compact(
            'month',
            'year',
            'expenses'
        ));
    }

    public function print(Request $request)
    {
        $year = $request->input('year');
        $month = $request->input('month');

        // $expenses = \App\TravelModel\TrnsctJurnal::whereYear('trnsct_date', '=', $year)
        //     ->whereMonth('trnsct_date', '=', $month)
        //     ->where('status', '!=', 'BATAL')
        //     ->where('type', '=', 'OUT')
        //     ->where(function ($query) {
        //         $query->where('account_id', '=', '1-110')
        //             ->orWhere('account_id', '=', '1-120');
        //     })
        //     ->get();

        $expenses = \App\TravelModel\TrnsctJurnal::
            whereYear('trnsct_date', '=', $year)
            ->whereMonth('trnsct_date', '=', $month)
            ->where('status', '!=', 'BATAL')
            ->whereHas('coa', function ($query) {
                $query->where('post_sub_report', '=', 'BIAYA')
                        ->orWhere('post_sub_report', '=', 'BIAYA_LAIN');
            })
            ->get();

        $pdf = PDF::loadView('travel.ReportExpenseMonthly.print', compact(
            'month',
            'year',
            'expenses'
        ));

        return $pdf->stream();
    }
}
