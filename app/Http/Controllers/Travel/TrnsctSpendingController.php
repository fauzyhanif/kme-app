<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PDF;
use DataTables;

class TrnsctSpendingController extends Controller
{
    protected $TrnsctBookingController;
    public function __construct(TrnsctBookingController $TrnsctBookingController)
    {
        $this->TrnsctBookingController = $TrnsctBookingController;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.TrnsctSpending.index');
    }

    public function jsonData()
    {
        $spendings = \App\TravelModel\TrnsctJurnal::with(['coa'])
            ->where("type", "OUT")
            ->where("is_main", "N")
            ->where("status", "!=", "BATAL")
            ->orderBy("proof_id", "desc")
            ->limit(100)
            ->get();

        return DataTables::of($spendings)
        ->editColumn('trnsct_date', function($spendings) {
            $payment_date = date_format(date_create($spendings->trnsct_date), 'd/m/Y');

            return $payment_date;
        })
        ->editColumn('credit', function($spendings) {
            $amount = number_format($spendings->credit,2,',','.');

            return $amount;
        })
        ->addColumn('actions_link', function($spendings) {
            // print link
            $btn = "<a href='" . route('travel.spending.detail', $spendings->proof_id) . "' class='btn btn-info btn-xs'>";
            $btn .= "<i class='fas fa-eye'></i> Detail";
            $btn .= "</a> ";

            return $btn;
        })
        ->rawColumns(['actions_link'])
        ->toJson();
    }

    public function formAdd()
    {
        // get cash & bank account
       $cashAccount = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->where("account_id", "LIKE", "%1-%")
            ->orderBy("account_id", "ASC")
            ->get();

        // get spending account
        $spendingAccount = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->where('account_id', 'LIKE', "%6-%")
            ->orWhere('account_id', 'LIKE', "%8-%")
            ->orderBy("account_id", "ASC")
            ->get();

        return view('travel.TrnsctSpending.formAdd', \compact(
            'cashAccount',
            'spendingAccount'
        ));
    }

    public function addNew(Request $request)
    {
        // get payment ID
        $paymentID = $this->TrnsctBookingController->getPaymentID();

        // store to trnsct_jurnal => CASH ACCOUNT
        $cashAccount = new \App\TravelModel\TrnsctJurnal();
        $cashAccount->proof_id = $paymentID;
        $cashAccount->trnsct_date = $request->get('trnsct_date');
        $cashAccount->related_person = $request->get('related_person');
        $cashAccount->account_id = $request->get('cash_account');
        $cashAccount->description = $request->get('cash_description');
        $cashAccount->debit = 0;
        $cashAccount->credit = $request->get('total-amount');
        $cashAccount->type = "OUT";
        $cashAccount->is_main = "N";
        $cashAccount->user = Session::get('auth_nama');
        $cashAccount->save();

        // store to trnsct_jurnal => spending ACCOUNT
        for ($i=0; $i < count($request->get('spending_account')); $i++) {
            $spendingAccount = new \App\TravelModel\TrnsctJurnal();
            $spendingAccount->proof_id = $paymentID;
            $spendingAccount->trnsct_date = $request->get('trnsct_date');
            $spendingAccount->related_person = $request->get('related_person');
            $spendingAccount->account_id = explode("*", $request->get('spending_account')[$i])[0];
            $spendingAccount->description = $request->get('cash_description');
            $spendingAccount->credit = 0;
            $spendingAccount->debit = $request->get('amount')[$i];
            $spendingAccount->type = "OUT";
            $spendingAccount->is_main = "Y";
            $spendingAccount->post_net_profit = explode("*", $request->get('spending_account')[$i])[1];
            $spendingAccount->user = Session::get('auth_nama');
            $spendingAccount->save();
        }

        $notice = [
            "type" => "success",
            "text" => "Data berhasil disimpan.",
        ];

        return response()->json($notice);
    }

    public function detail($paymentID)
    {
        $cashAccountOld = \App\TravelModel\TrnsctJurnal::with('coa')
            ->where('proof_id', "$paymentID")
            ->where('is_main', 'N')
            ->first();

        $spendingAccountOld = \App\TravelModel\TrnsctJurnal::with('coa')
            ->where('proof_id', "$paymentID")
            ->where('is_main', 'Y')
            ->get();

        // get cash & bank account
        $cashAccount = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->where("account_id", "LIKE", "%1-%")
            ->orderBy("account_id", "ASC")
            ->get();

        // get spending account
        $spendingAccount = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->where('account_id', 'LIKE', "%6-%")
            ->orWhere('account_id', 'LIKE', "%8-%")
            ->orderBy("account_id", "ASC")
            ->get();

        return view('travel.TrnsctSpending.formEdit', \compact(
            'cashAccountOld',
            'spendingAccountOld',
            'cashAccount',
            'spendingAccount'
        ));
    }

    public function edit(Request $request, $proofId)
    {
        // update data jurnal => CASH ACCOUNT
        $cashAccount = \App\TravelModel\TrnsctJurnal::where("proof_id", $proofId)
            ->where("is_main", "N")
            ->update([
                "trnsct_date" => $request->get('trnsct_date'),
                "related_person" => $request->get('related_person'),
                "account_id" => $request->get('cash_account'),
                "description" => $request->get('cash_description'),
                "credit" => $request->get('total-amount'),
                "user" => Session::get('auth_nama')
            ]);

        // update data jurnal => spending ACCOUNT
        foreach ($request->get('spending_account') as $key => $value) {
            if ($key != "x") {
                $oldData = \App\TravelModel\TrnsctJurnal::findOrFail($key);
                $oldData->account_id = explode("*", $request->get('spending_account')[$key])[0];
                $oldData->description = $request->get('cash_description');
                $oldData->debit = $request->get('amount')[$key];
                $oldData->post_net_profit = explode("*", $request->get('spending_account')[$key])[1];
                $oldData->update();
            } else {
                $newData = new \App\TravelModel\TrnsctJurnal();
                $newData->proof_id = $proofId;
                $newData->trnsct_date = $request->get('trnsct_date');
                $newData->related_person = $request->get('related_person');
                $newData->account_id = explode("*", $request->get('spending_account')[$key])[0];
                $newData->description = $request->get('cash_description');
                $newData->debit = $request->get('amount')[$key];
                $newData->credit = 0;
                $newData->type = "OUT";
                $newData->is_main = "Y";
                $oldData->post_net_profit = explode("*", $request->get('spending_account')[$key])[1];
                $newData->user = Session::get('auth_nama');
                $newData->save();
            }
        }

        $notice = [
            "type" => "success",
            "text" => "Data berhasil disimpan.",
        ];

        return response()->json($notice);

    }

    public function removeJurnalItem(Request $request)
    {
        $jurnal = \App\TravelModel\TrnsctJurnal::findOrFail($request->get('jurnal_id'));
        $delete = $jurnal->delete();

        if ($delete) {
            $this->reduceCashBalance($jurnal->proof_id, $jurnal->debit);

            $notice = [
                "type" => "success",
                "text" => "Data berhasil dihapus.",
            ];
        } else {
            $notice = [
                "type" => "error",
                "text" => "Data gagal dihapus.",
            ];
        }

        return response()->json($notice);
    }

    public function reduceCashBalance($proofId, $amount)
    {
        $debitBalance = $this->getDebitBalance($proofId);
        $cashJurnal = \App\TravelModel\TrnsctJurnal::where("proof_id", $proofId)
            ->where("is_main", "N")
            ->update(["credit" => $debitBalance - $amount]);
    }

    public function getDebitBalance($proofId)
    {
        $jurnal = \App\TravelModel\TrnsctJurnal::where("proof_id", $proofId)
            ->where("is_main", "N")
            ->first();

        return $jurnal->credit;
    }

    public function print($proofId)
    {
        $cashAccount = \App\TravelModel\TrnsctJurnal::with('coa')
            ->where('proof_id', "$proofId")
            ->where('is_main', 'N')
            ->first();

        $spendingAccount = \App\TravelModel\TrnsctJurnal::with('coa')
            ->where('proof_id', "$proofId")
            ->where('is_main', 'Y')
            ->get();


        $pdf = PDF::loadView('travel.TrnsctSpending.print', compact(
            'cashAccount',
            'spendingAccount'
        ));
        return $pdf->stream();
    }
}
