<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RefBiroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.dataMaster.biro.index');
    }

    public function detail()
    {
        return view('travel.dataMaster.biro.detail');
    }

    public function formAdd()
    {
        return view('travel.dataMaster.biro.formAdd');
    }

    public function formEdit()
    {
        return view('travel.dataMaster.biro.formEdit');
    }
}
