<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class ReportNeracaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $datas = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->where("post_report", "NERACA")
            ->orderBy("account_id", "ASC")
            ->get();

        return view('travel.ReportNeraca.index', compact(
            "datas"
        ));
    }

    public function print(Request $request)
    {
        $datas = \App\TravelModel\RefFinanceAccount::where("is_parent", "!=", "Y")
            ->where("post_report", "NERACA")
            ->orderBy("account_id", "ASC")
            ->get();

        $pdf = PDF::loadView('travel.ReportNeraca.print', compact(
            "datas"
        ));
        return $pdf->stream();
    }

    public function detail($id)
    {
        $account = \App\TravelModel\RefFinanceAccount::where("account_id", "$id")->first();

        $datas = \App\TravelModel\TrnsctJurnal::where("account_id", "$id")
            ->where('status', 'OK')
            ->orderBy("trnsct_date", "DESC")
            ->paginate(15);

        return view('travel.ReportNeraca.detail', compact('account', 'datas'));
    }

}
