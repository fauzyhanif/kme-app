<?php

namespace App\Http\Controllers\Travel;

use App\Http\Controllers\Controller;
use App\TravelModel\RefDriver;
use App\TravelModel\RefDriverSaving;
use App\TravelModel\RefHelper;
use App\TravelModel\TrnsctBookingDtl;
use App\TravelModel\TrnsctJurnal;
use App\TravelModel\TrnsctSpj;
use App\TravelModel\TrnsctSpjBudget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DataTables;
use PDF;

class TrnsctSpjController extends Controller
{
    protected $TrnsctBookingController;
    public function __construct(TrnsctBookingController $TrnsctBookingController)
    {
        $this->TrnsctBookingController = $TrnsctBookingController;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('travel.TrnsctSpj.index');
    }

    public function spjListData()
    {
        $letters = TrnsctSpj::with(['mainDriver', 'carCategory', 'booking' => function($query){
                $query->orderBy('booking_start_date', 'DESC');
            }])
            ->limit(100)
            ->get();

        return DataTables::of($letters)
        ->editColumn('police_num', function($letters)
        {
            $result = $letters->police_num . " (" . $letters->carCategory->name . ")";
            return $result;
        })
        ->editColumn('main_driver', function($letters)
        {
            $result = $letters->mainDriver->name;
            return $result;
        })
        ->addColumn('booking_date', function($letters) {
            $booking_start_date = date_format(date_create($letters->booking->booking_start_date), 'd/m/Y');
            $booking_end_date = date_format(date_create($letters->booking->booking_end_date), 'd/m/Y');

            return $booking_start_date . " s/d " . $booking_end_date;
        })
        ->addColumn('destination', function($letters) {
            $result = $letters->booking->destination;
            return $result;
        })
        ->addColumn('actions_link', function($letters) {
            $editRoute = route('travel.spj.formedit', $letters->id);
            $printRoute = route('travel.spj.print', $letters->id);

            // edit link
            $btn = "<a href='$editRoute' class='btn btn-primary btn-xs'>";
            $btn .= "<i class='fas fa-edit'></i> Detail";
            $btn .= "</a> ";

            // print link
            $btn .= "<a href='$printRoute' class='btn btn-warning btn-xs' target='_blank'>";
            $btn .= "<i class='fas fa-print'></i> Cetak";
            $btn .= "</a> ";

            if ($letters->status != "BATAL") {
                // status link
                $btn .= "<button type='button' class='btn btn-danger btn-xs' onclick=\"popupStatus('$letters->spj_id', '$letters->status')\">";
                $btn .= "<i class='fas fa-trash'></i> Batalkan";
                $btn .= "</button> ";
            }

            return $btn;
        })
        ->rawColumns(['booking_date', 'actions_link', 'destination'])
        ->toJson();
    }

    public function formAdd($bookingID, $categoryID)
    {
        $booking = \App\TravelModel\TrnsctBooking::where('booking_id', "$bookingID")->first();
        $category = \App\TravelModel\RefCarCategory::where('category_id', "$categoryID")->first();

        // cari unit yang belum dibooking
        $cars = $this->getCarReady($categoryID, $booking->booking_start_date, $booking->booking_end_date);

        // cari driver yang belum dibooking
        $drivers = $this->getDriverReady($categoryID, $booking->booking_start_date, $booking->booking_end_date);

        // cari helper yang belum dibooking
        $helpers = $this->getHelperReady($booking->booking_start_date, $booking->booking_end_date);

        // get akun alokasi pemberangkatan unit
        $budgetAccounts = \App\TravelModel\RefSpjBudgetAccount::where('active', 'Y')->get();

        // get cash & bank account
        $cashAccounts = \App\TravelModel\RefFinanceAccount::where("account_id", "LIKE", "%1-%")
            ->where("parent", "!=", "null")
            ->orderBy("account_id", "ASC")
            ->get();

        return view('travel.TrnsctSpj.formAdd', compact(
            'booking',
            'category',
            'cars',
            'drivers',
            'helpers',
            'budgetAccounts',
            'cashAccounts'
        ));
    }

    public function addNew(Request $request)
    {
        // get SPJ ID
        $spjID = $this->getSpjID();

        // store spj
        $newSpj = new TrnsctSpj();
        $newSpj->spj_id = $spjID;
        $newSpj->booking_id = $request['booking_id'];
        $newSpj->police_num = $request['police_num'];
        $newSpj->category_id = $request['category_id'];
        $newSpj->main_driver = $request['main_driver'];
        $newSpj->second_driver = $request['second_driver'];
        $newSpj->helper_id = $request['helper_id'];
        $newSpj->premi_main_driver = str_replace(".", "", $request['premi_main_driver']);
        $newSpj->premi_second_driver = str_replace(".", "", $request['premi_second_driver']);
        $newSpj->premi_helper = str_replace(".", "", $request['premi_helper']);

        if ($newSpj->save()) {
            // update saving main driver, second driver and helper
            if ($request['main_driver'] != "") {
                $this->updateSavingDriver(
                    $request['main_driver'],
                    str_replace(".", "", $request['premi_main_driver'])
                );
            }

            if ($request['second_driver'] != "") {
                $this->updateSavingDriver(
                    $request['second_driver'],
                    str_replace(".", "", $request['premi_second_driver'])
                );
            }

            if ($request['helper_id'] != "") {
                $this->updateSavingHelper(
                    $request['helper_id'],
                    str_replace(".", "", $request['premi_helper'])
                );
            }

            // store spj budget
            $ttl_budget = 0;
            foreach ($request['account_id'] as $key => $value) {
                $newSpjBudget = new TrnsctSpjBudget();
                $newSpjBudget->spj_id = $spjID;
                $newSpjBudget->account_id = $value;
                $newSpjBudget->qty = $request['qty'][$value];
                $newSpjBudget->duration = $request['duration'][$value];
                $newSpjBudget->amount = str_replace(".", "", $request['amount'][$value]);
                $newSpjBudget->ttl_amount = str_replace(".", "", $request['ttl_amount'][$value]);
                $newSpjBudget->save();
                $ttl_budget += str_replace(".", "", $request['ttl_amount'][$value]);
            }

            $request['ttl_budget'] = $ttl_budget;

            // save jurnal
            $this->saveJurnal($request, $spjID);

            // get ID
            $resNewSpj = TrnsctSpj::findOrFail($spjID);
            $resNewSpj->budget = $ttl_budget;
            $resNewSpj->save();

            $notice = [
                "spj_id" => $resNewSpj->id,
                "booking_id" => $resNewSpj->booking_id,
                "type" => "success",
                "text" => "SPJ berhasil disimpan.",
                "url" => "{{ route('travel.spj.print', $newSpj->id) }}"
            ];
        } else {
            $notice = [
                "spj_id" => 0,
                "type" => "error",
                "text" => "SPJ gagal disimpan.",
            ];
        }

        return response()->json($notice);
    }

    public function formEdit($id)
    {
        $letter = DB::table('trvl_trnsct_spj as a')
                ->select('a.*', 'b.name as main_driver_name', 'c.name as second_driver_name', 'd.name as helper_name')
                ->leftJoin('trvl_ref_driver as b', 'a.main_driver', '=', 'b.driver_id')
                ->leftJoin('trvl_ref_driver as c', 'a.second_driver', '=', 'c.driver_id')
                ->leftJoin('trvl_ref_helper as d', 'a.helper_id', '=', 'd.helper_id')
                ->where('id', $id)
                ->first();

        $budgets = TrnsctSpjBudget::with(['budgetAccount'])
            ->where("spj_id", $letter->spj_id)
            ->get();

        $booking = \App\TravelModel\TrnsctBooking::where('booking_id', "$letter->booking_id")->first();
        $category = \App\TravelModel\RefCarCategory::where('category_id', "$letter->category_id")->first();

        // cari unit yang sudah dibooking
        $cars = $this->getCarReady($letter->category_id, $booking->booking_start_date, $booking->booking_end_date);

        // cari driver yang sudah dibooking
        $drivers = $this->getDriverReady($letter->category_id, $booking->booking_start_date, $booking->booking_end_date);

        // cari helper yang sudah dibooking
        $helpers = $this->getHelperReady($booking->booking_start_date, $booking->booking_end_date);

        return view('travel.TrnsctSpj.formEdit', compact(
            'letter',
            'budgets',
            'booking',
            'category',
            'cars',
            'drivers',
            'helpers'
        ));
    }

    public function edit(Request $request)
    {
        $spjID = $request->get('spj_id');
        $letter = TrnsctSpj::findOrFail($spjID);

        // update saving main driver, second driver and helper
        if ($request->get('main_driver') != "" && $letter->main_driver != $request->get('main_driver')) {
            $this->updateSavingDriver(
                $request['main_driver'],
                str_replace(".", "", $request['premi_main_driver']),
                $letter->main_driver,
                $letter->premi_main_driver
            );
        }

        if ($request->get('second_driver') != "" && $letter->second_driver != $request->get('second_driver')) {
            $this->updateSavingDriver(
                $request['second_driver'],
                str_replace(".", "", $request['premi_second_driver']),
                $letter->second_driver,
                $letter->premi_second_driver
            );
        }

        if ($request->get('helper_id') != "" && $letter->helper_id != $request->get('helper_id')) {
            $this->updateSavingHelper(
                $request['helper_id'],
                str_replace(".", "", $request['premi_helper']),
                $letter->helper_id,
                $letter->premi_helper
            );
        }

        $letter->police_num = $request->get('police_num');
        $letter->main_driver = $request->get('main_driver');
        $letter->second_driver = $request->get('second_driver');
        $letter->helper_id = $request->get('helper_id');
        $letter->premi_main_driver = str_replace(".", "", $request->get('premi_main_driver'));
        $letter->premi_second_driver = str_replace(".", "", $request->get('premi_second_driver'));
        $letter->premi_helper = str_replace(".", "", $request->get('premi_helper'));

        $ttl_budget = 0;
        if ($letter->save()) {

            // store spj budget
            foreach ($request['account_id'] as $key => $value) {
                $budgets = TrnsctSpjBudget::where('spj_id', $spjID)
                    ->where('account_id', $value)
                    ->first();

                if ($budgets !== null) {
                    $budgets->update([
                        "qty" => $request['qty'][$value],
                        "duration" => $request['duration'][$value],
                        "amount" => str_replace(".", "", $request['amount'][$value]),
                        "ttl_amount" => str_replace(".", "", $request['ttl_amount'][$value])
                    ]);
                }

                $ttl_budget += str_replace(".", "", $request['ttl_amount'][$value]);
            }

            $letter->budget = $ttl_budget;
            $letter->save();

            // update jurnal
            $jurnal_1 = \App\TravelModel\TrnsctJurnal::where("spj_id", "$spjID")->where("credit", "!=", "0")->update(["credit" => $ttl_budget]);
            $jurnal_2 = \App\TravelModel\TrnsctJurnal::where("spj_id", "$spjID")->where("debit", "!=", "0")->update(["debit" => $ttl_budget]);

            $notice = [
                "spj_id" => $letter->id,
                "booking_id" => $letter->booking_id,
                "type" => "success",
                "text" => "SPJ berhasil disimpan.",
                "url" => "{{ route('travel.spj.print', $letter->id) }}"
            ];
        } else {
            $notice = [
                "spj_id" => 0,
                "type" => "error",
                "text" => "SPJ gagal disimpan.",
            ];
        }

        return response()->json($notice);
    }

    public function saveJurnal($request, $spjID)
    {
        $paymentID = $this->TrnsctBookingController->getPaymentID();
        $driverName = \App\TravelModel\RefDriver::findOrFail($request['main_driver'])->value('name');
        $carCategoryName = \App\TravelModel\RefCarCategory::findOrFail($request['category_id'])->value('name');
        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])->findOrFail($request['booking_id']);
        $bookingStartDate = date("d-m-Y", strtotime($booking->booking_start_date));
        $customerName = $booking->customer->name;
        $description = "Kas Jalan $carCategoryName Tgl $bookingStartDate Tujuan $booking->destination A/N $customerName";

        // store to trnsct_jurnal => CASH ACCOUNT
        $cashAccount = new \App\TravelModel\TrnsctJurnal();
        $cashAccount->booking_id = $request['booking_id'];
        $cashAccount->proof_id = $paymentID;
        $cashAccount->trnsct_date = date('Y-m-d');
        $cashAccount->related_person = $driverName;
        $cashAccount->account_id = $request['payment_method'];
        $cashAccount->description = $description;
        $cashAccount->debit = 0;
        $cashAccount->credit = $request['ttl_budget'];
        $cashAccount->type = "OUT";
        $cashAccount->is_main = "N";
        $cashAccount->user = Session::get('auth_nama');
        $cashAccount->spj_id = $spjID;
        $cashAccount->save();

        // store to trnsct_jurnal => INCOME ACCOUNT
        $incomeAccount = new \App\TravelModel\TrnsctJurnal();
        $incomeAccount->booking_id = $request['booking_id'];
        $incomeAccount->proof_id = $paymentID;
        $incomeAccount->trnsct_date = date('Y-m-d');
        $incomeAccount->related_person = $driverName;
        $incomeAccount->account_id = "5-100";
        $incomeAccount->description = $description;
        $incomeAccount->credit = 0;
        $incomeAccount->debit = $request['ttl_budget'];
        $incomeAccount->type = "OUT";
        $incomeAccount->is_main = "Y";
        $incomeAccount->user = Session::get('auth_nama');
        $incomeAccount->spj_id = $spjID;
        $incomeAccount->save();

        $notice = [
            "type" => "success",
            "text" => "Data berhasil disimpan.",
        ];

        return response()->json($notice);
    }

    public function updateSavingDriver($newDriver, $premiNewDriver = "", $oldDriver = "", $premiOldDriver = "", $batal = "")
    {
        $driver = \App\TravelModel\RefDriver::findOrFail($newDriver);
        if ($batal == "BATAL") {
            $driver->ttl_saving -= $premiNewDriver;
            $driver->ttl_trnsct -= 1;
            $driver->save();
        } else {
            $driver->ttl_saving += $premiNewDriver;
            $driver->ttl_trnsct += 1;
            $driver->save();
        }

        if ($oldDriver != "") {
            $driver = \App\TravelModel\RefDriver::findOrFail($oldDriver);
            $driver->ttl_saving -= $premiOldDriver;
            $driver->ttl_trnsct -= 1;
            $driver->save();
        }
    }

    public function updateSavingHelper($newHelper, $premiNewHelper = "", $oldHelper = "", $premiOldHelper = "", $batal = "")
    {
        $helper = RefHelper::findOrFail($newHelper);
        if ($batal != "BATAL") {
            $helper->ttl_saving += $premiNewHelper;
            $helper->ttl_trnsct += 1;
            $helper->save();
        } else {
            $helper->ttl_saving -= $premiNewHelper;
            $helper->ttl_trnsct -= 1;
            $helper->save();
        }

        if ($oldHelper != "") {
            $helper = RefHelper::findOrFail($oldHelper);
            $helper->ttl_saving -= $premiOldHelper;
            $helper->ttl_trnsct -= 1;
            $helper->save();
        }
    }

    public function print($id)
    {
        // get spj
        $spj = TrnsctSpj::with(['mainDriver', 'secondDriver', 'helper', 'carCategory'])
            ->where("id", $id)
            ->first();

        // get spj budget
        $spjBudget = TrnsctSpjBudget::with(['budgetAccount'])
            ->where("spj_id", $spj->spj_id)
            ->get();

        // get booking
        $booking = \App\TravelModel\TrnsctBooking::with(['customer'])->findOrFail($spj->booking_id);

        $pdf = PDF::loadView('travel.TrnsctSpj.print', compact(
            'spj',
            'spjBudget',
            'booking'
        ));

        return $pdf->stream($spj->spj_id.".pdf");
    }

    public function getSpjID()
    {
        $thisMonth = date('Y-m');
        $year = date('Y');
        $year = substr($year, -2);
        $month = date('m');

        $lastSpjID = \App\TravelModel\RefSpjId::where('month', '=', "$thisMonth")->first();

        // check exist or not
        if ( $lastSpjID === null ) {
            $orderId = "0001";

            $new = new \App\TravelModel\RefSpjId();
            $new->month = "$thisMonth";
            $new->serial_num = "0002";
            $new->save();
        } else {
            $orderId = sprintf('%04d', $lastSpjID->serial_num);
            $update = \App\TravelModel\RefSpjId::where('month', '=', "$thisMonth")->update(["serial_num" => $orderId + 1]);
        }

        $spjID = "$orderId/SPJ/OPR/PT.KME/$month/$year";
        return $spjID;
    }

    public function getCarReady($categoryID, $startDate, $endDate)
    {
        // get all car by category ID
        $cars = \App\TravelModel\RefCar::where('condition_id', '1')
            ->where('category_id', "$categoryID")
            ->get();

        // get car booked
        $carBooked = $this->getCarBooked($categoryID, $startDate, $endDate);

        $arrayCarBooked = [];
        foreach ($carBooked as $key => $value) {
            $arrayCarBooked[] = $value->police_num;
        }

        $arrayCarReady = [];
        foreach ($cars as $key => $value) {
            if (!in_array($value->police_num, $arrayCarBooked)) {
                $arrayCarReady[] = $value->police_num;
            }
        }

        return $arrayCarReady;
    }

    public function getCarBooked($categoryID, $startDate, $endDate)
    {
        $datas = DB::table('trvl_trnsct_spj')
            ->leftJoin('trvl_trnsct_booking', 'trvl_trnsct_booking.booking_id', '=', 'trvl_trnsct_spj.booking_id')
            ->select('trvl_trnsct_spj.police_num')
            ->where('trvl_trnsct_spj.category_id', $categoryID)
            ->where(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '<=', $startDate)
                    ->where('trvl_trnsct_booking.date_not_for_sale' , '>=', $startDate);
            })
            ->orWhere(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '<=', $endDate)
                    ->where('trvl_trnsct_booking.date_not_for_sale' , '>=', $endDate);
            })
            ->orWhere(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '>=', $startDate)
                    ->where('trvl_trnsct_booking.date_not_for_sale' , '<=', $endDate);
            })
            ->get();

        return $datas;
    }

    public function getDriverReady($categoryID, $startDate, $endDate)
    {
        // get all car by category ID
        $drivers = \App\TravelModel\RefDriver::whereJsonContains('car_permit', "$categoryID")->get();

        // get driver booked
        $driverBooked = $this->getDriverBooked($startDate, $endDate);

        $arrayMainDriverBooked = [];
        foreach ($driverBooked as $key => $value) {
            $arrayMainDriverBooked[] = $value->main_driver;
        }

        $arraySecondDriverBooked = [];
        foreach ($driverBooked as $key => $value) {
            $arraySecondDriverBooked[] = $value->second_driver;
        }

        $arrayDriverReady = [];
        foreach ($drivers as $key => $value) {
            if (
                !in_array($value->driver_id, $arrayMainDriverBooked) &&
                !in_array($value->driver_id, $arraySecondDriverBooked)
            ) {
                $arrayDriverReady[] = $value;
            }
        }

        return $arrayDriverReady;
    }

    public function getDriverBooked($startDate, $endDate)
    {
        $datas = DB::table('trvl_trnsct_spj')
            ->leftJoin('trvl_trnsct_booking', 'trvl_trnsct_booking.booking_id', '=', 'trvl_trnsct_spj.booking_id')
            ->select('trvl_trnsct_spj.main_driver', 'trvl_trnsct_spj.second_driver')
            ->where(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '<=', $startDate)
                    ->where('trvl_trnsct_booking.booking_end_date' , '>=', $startDate);
            })
            ->orWhere(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '<=', $endDate)
                    ->where('trvl_trnsct_booking.booking_end_date' , '>=', $endDate);
            })
            ->orWhere(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '>=', $startDate)
                    ->where('trvl_trnsct_booking.booking_end_date' , '<=', $endDate);
            })
            ->get();

        return $datas;
    }

    public function getHelperReady($startDate, $endDate)
    {
        // get all helper
        $helpers = RefHelper::all();

        // get driver booked
        $driverBooked = $this->getHelperBooked($startDate, $endDate);

        $arrayHelperBooked = [];
        foreach ($driverBooked as $key => $value) {
            $arrayHelperBooked[] = $value->helper_id;
        }

        $arrayHelperReady = [];
        foreach ($helpers as $key => $value) {
            if (!in_array($value->helper_id, $arrayHelperBooked)) {
                $arrayHelperReady[] = $value;
            }
        }

        return $arrayHelperReady;
    }

    public function getHelperBooked($startDate, $endDate)
    {
        $datas = DB::table('trvl_trnsct_spj')
            ->leftJoin('trvl_trnsct_booking', 'trvl_trnsct_booking.booking_id', '=', 'trvl_trnsct_spj.booking_id')
            ->select('trvl_trnsct_spj.helper_id')
            ->where(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '<=', $startDate)
                    ->where('trvl_trnsct_booking.booking_end_date' , '>=', $startDate);
            })
            ->orWhere(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '<=', $endDate)
                    ->where('trvl_trnsct_booking.booking_end_date' , '>=', $endDate);
            })
            ->orWhere(function($query) use  ($startDate, $endDate){
                $query->where('trvl_trnsct_booking.booking_start_date', '>=', $startDate)
                    ->where('trvl_trnsct_booking.booking_end_date' , '<=', $endDate);
            })
            ->get();

        return $datas;
    }

    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        $letter = TrnsctSpj::findOrFail($id);
        $letter->status = $request->get('status');

        if ($letter->save()) {

            // if status is cancel
            if ($request->get('status') == 'BATAL') {

                // kurangi tabungan driver & helper

                $this->updateSavingDriver(
                    $letter->main_driver,
                    $letter->premi_main_driver,
                    "",
                    "",
                    "BATAL"
                );

                if ($letter->second_driver != "") {
                    $this->updateSavingDriver(
                        $letter->second_driver,
                        $letter->premi_second_driver,
                        "",
                        "",
                        "BATAL"
                    );
                }

                if ($letter->helper_id != "") {
                    $this->updateSavingHelper(
                        $letter->helper_id,
                        $letter->premi_helper,
                        "",
                        "",
                        "BATAL"
                    );
                }
            }

            $notice = [
                "type" => "success",
                "text" => "SPJ berhasil diubah.",
            ];
        } else {
            $notice = [
                "type" => "error",
                "text" => "SPJ gagal diubah.",
            ];
        }

        return response()->json($notice);
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();

        try {
            $spj = TrnsctSpj::find($request->spj_id);

            // update premi driver
            if ((int) $spj->main_driver > 0 && (int) $spj->premi_main_driver > 0) {
                $driver = RefDriver::findOrFail($spj->main_driver);
                $driver->ttl_saving = $driver->ttl_saving - $spj->premi_main_driver;
                $driver->save();

                $saving = new RefDriverSaving();
                $saving->type = 'DRIVER';
                $saving->user_id = $spj->main_driver;
                $saving->trnsct_date = date('Y-m-d');
                $saving->information = 'Surat perintah jalan dihapus';
                $saving->out = $spj->premi_main_driver;
                $saving->save();
            }

            // update premi helper
            if ((int) $spj->second_driver > 0 && (int) $spj->premi_second_driver > 0) {
                $driver = RefDriver::findOrFail($spj->second_driver);
                $driver->ttl_saving = $driver->ttl_saving - $spj->premi_second_driver;
                $driver->save();

                $saving = new RefDriverSaving();
                $saving->type = 'DRIVER';
                $saving->user_id = $spj->second_driver;
                $saving->trnsct_date = date('Y-m-d');
                $saving->information = 'Surat perintah jalan dihapus';
                $saving->out = $spj->premi_second_driver;
                $saving->save();
            }

            // create saving out
            if ((int) $spj->helper_id > 0 && (int) $spj->premi_helper > 0) {
                $helper = RefHelper::findOrFail($spj->helper_id);
                $helper->ttl_saving = $helper->ttl_saving - $spj->premi_helper;
                $helper->save();

                $saving = new RefDriverSaving();
                $saving->type = 'HELPER';
                $saving->user_id = $spj->helper_id;
                $saving->trnsct_date = date('Y-m-d');
                $saving->information = 'Surat perintah jalan dihapus';
                $saving->out = $spj->premi_helper;
                $saving->save();
            }

            // delete spj budget
            TrnsctSpjBudget::where('spj_id', $spj->spj_id)->delete();

            // update jurnal to BATAL
            TrnsctJurnal::where('spj_id', $spj->spj_id)->update(['status' => 'BATAL']);

            // update booking detail
            $bookingDtl = TrnsctBookingDtl::where('booking_id', $spj->booking_id)
                ->where('category_id', $spj->category_id)
                ->first();

            if ($bookingDtl) {
                $x = TrnsctBookingDtl::find($bookingDtl->trnsctdtl_id);
                $x->unit_qty_spj = $x->unit_qty_spj - 1;
                $x->save();
            }

            // delete SPJ
            $spj->delete();

            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Data berhasil dihapus']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $th->getMessage()]);
        }      
    }
}
