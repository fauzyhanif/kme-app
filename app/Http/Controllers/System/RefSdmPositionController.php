<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefSdmPositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('system.RefSdmPosition.index');
    }

    public function jsonData()
    {
        $module_id = Session::get('auth_module_aktif');
        $positions = \App\SystemModel\SysRefSdmPosition::where("module_id", $module_id);
        return DataTables::of($positions)
            ->addColumn('wages', function($positions){
                return number_format($positions->wages,2,',','.');
            })
            ->addColumn('actions_link', function($positions)
            {
                // edits link
                $btn = "<a href='" . route('system.jabatan.form-edit', $positions->position_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['wages', 'actions_link'])
            ->toJson();
    }

    public function detail($id)
    {
        $position = \App\TravelModel\SysRefSdmPosition::findOrFail($id);
        return view('system.RefSdmPosition.detail', \compact('position'));
    }

    public function formAdd()
    {
        return view('system.RefSdmPosition.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'position_id'=>'required',
            'name'=>'required',
        ]);

        $module_id = Session::get('auth_module_aktif');

        $new = new \App\SystemModel\SysRefSdmPosition();
        $new->position_id = $request->get('position_id');
        $new->name = $request->get('name');
        $new->wages = str_replace(".", "", $request->get('wages'));
        $new->module_id = $module_id;
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $position = \App\SystemModel\SysRefSdmPosition::findOrFail($id);

        return view('system.RefSdmPosition.formEdit', \compact('position'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'position_id'=>'required',
            'name'=>'required',
        ]);

        $new = \App\SystemModel\SysRefSdmPosition::findOrFail($id);
        $new->position_id = $request->get('position_id');
        $new->name = $request->get('name');
        $new->wages = str_replace(".", "", $request->get('wages'));
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
