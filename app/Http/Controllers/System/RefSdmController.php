<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class RefSdmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('system.RefSdm.index');
    }

    public function jsonData()
    {
        $module_id = Session::get('auth_module_aktif');
        $users = DB::table('sys_ref_sdm as a')
            ->leftJoin('sys_ref_sdm_position as b', function($join)
            {
                $join->on('a.module_id', '=', 'b.module_id');
                $join->on('a.position_id', '=', 'b.position_id');
            })
            ->select('a.sdm_id', 'a.nip', 'a.name', 'b.name as position_name')
            ->where('a.module_id', $module_id)
            ->orderBy('b.position_id', 'ASC')
            ->get();

        return DataTables::of($users)
            ->addColumn('actions_link', function($users)
            {
                // edits link
                $btn = "<a href='" . route('system.sdm.form-edit', $users->sdm_id) . "' class='btn btn-primary btn-xs'>";
                $btn .= "<i class='fas fa-edit'></i> Edit";
                $btn .= "</a> ";

                return $btn;
            })
            ->rawColumns(['actions_link'])
            ->toJson();
    }

    public function detail($id)
    {
        $position = \App\TravelModel\SysRefSdm::findOrFail($id);
        return view('system.RefSdm.detail', \compact('position'));
    }

    public function formAdd()
    {
        $module_id = Session::get('auth_module_aktif');
        $positions = \App\SystemModel\SysRefSdmPosition::where("module_id", $module_id)->get();
        return view('system.RefSdm.formAdd', compact('positions'));
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'nip'=>'required',
            'position_id'=>'required',
            'name'=>'required',
            'address'=>'required',
            'phone_num'=>'required',
        ]);

        $module_id = Session::get('auth_module_aktif');

        $new = new \App\SystemModel\SysRefSdm();
        $new->module_id = $module_id;
        $new->nip = $request->get('nip');
        $new->position_id = $request->get('position_id');
        $new->name = $request->get('name');
        $new->address = $request->get('address');
        $new->phone_num = $request->get('phone_num');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $sdm = \App\SystemModel\SysRefSdm::findOrFail($id);
        $module_id = Session::get('auth_module_aktif');
        $positions = \App\SystemModel\SysRefSdmPosition::where("module_id", $module_id)->get();

        return view('system.RefSdm.formEdit', \compact('sdm', 'positions'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'nip'=>'required',
            'position_id'=>'required',
            'name'=>'required',
            'address'=>'required',
            'phone_num'=>'required',
        ]);

        $new = \App\SystemModel\SysRefSdm::findOrFail($id);
        $new->position_id = $request->get('position_id');
        $new->name = $request->get('name');
        $new->address = $request->get('address');
        $new->phone_num = $request->get('phone_num');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
