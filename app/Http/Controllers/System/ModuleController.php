<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect,Response,DB;
use Illuminate\Support\Facades\File;

class ModuleController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $modules = \App\SystemModel\SysRefModule::all();
        return view('system.module.index', \compact('modules'));
    }

    public function formAdd()
    {
        return view('system.module.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'id_module'=>'required|max:2|unique:sys_ref_module',
            'nama'=>'required',
            'deskripsi'=>'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:1000',
        ]);

        $new = new \App\SystemModel\SysRefModule();
        $new->id_module = $request->get('id_module');
        $new->nama = $request->get('nama');
        $new->deskripsi = $request->get('deskripsi');

        if ($files = $request->file('image')) {
            // path
            $destinationPath = 'public/system/module/';

            // rename file
            $profileImage = date("Ymdhis") . "." . $files->getClientOriginalExtension();

            // upload process
            $files->move($destinationPath, $profileImage);

            // append filename
            $new->image = "$profileImage";
        } else {
            // if file not exist
            $new->image = "default.png";
        }

        $new->save();
        $response = [
            "type" => "success",
            "text" => "Module berhasil disimpan"
        ];

        return response()->json($response);
    }

    public function formEdit($idModule)
    {
        $module = \App\SystemModel\SysRefModule::where("id_module", $idModule)->first();
        return view('system.module.formEdit', \compact("module"));
    }

    public function edit(Request $request, $idModule)
    {
        $request->validate([
            'id_module'=>'required|max:2|unique:sys_ref_module,id_module,' . $idModule . ',id_module',
            'nama'=>'required',
            'deskripsi'=>'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:1000',
        ]);

        $old = \App\SystemModel\SysRefModule::where("id_module", $idModule)->first();
        $old->id_module = $request->get('id_module');
        $old->nama = $request->get('nama');
        $old->deskripsi = $request->get('deskripsi');

        if ($files = $request->file('image')) {
            // path
            $destinationPath = 'public/system/module/';

            if ($old->image != 'default.png') {
                // remove old file
                File::delete($destinationPath . $old->image);
            }

            // rename file
            $filename = date("Ymdhis") . "." . $files->getClientOriginalExtension();

            // upload process
            $files->move($destinationPath, $filename);

            // append filename
            $old->image = "$filename";
        }

        $old->update();
        $response = [
            "type" => "success",
            "text" => "Module berhasil diubah."
        ];

        return response()->json($response);
    }

}
