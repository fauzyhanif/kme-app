<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function doAuth(Request $request)
    {
        $request->validate([
            "email" => "required",
            "password" => "required"
        ]);

        $data_exist = \App\User::where('email', '=', $request->get('email'))->first();
        if ($data_exist === null) {
            return redirect('/login')->withErrors(['Email salah.', '']);
        } else {
            if (!Hash::check($request->get('password'), $data_exist->password)) {
                return redirect('/login')->withErrors(['Email atau password salah.', '']);
            } else {
                // set session
                Session::put('auth_nama', $data_exist->nama);
                Session::put('auth_email', $data_exist->email);
                Session::put('auth_module', $data_exist->id_module);
                Session::put('auth_role', $data_exist->id_role);

                // redirect to gate
                return redirect('/gate');
            }
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/login');
    }

    public function changePassword(Request $request)
    {
        $myEmail = Session::get('auth_email');
        $user = \App\User::where('email', $myEmail)->first();

        return view('auth.changePassword');
    }
}
