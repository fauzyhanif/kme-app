<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefCarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $cars = \App\RiceModel\RefCar::all();
        return view('rice.RefCar.index', compact('cars'));
    }

    public function formAdd()
    {
        return view('rice.RefCar.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'police_num'=>'required',
            'owner'=>'required',
        ]);

        $new = new \App\RiceModel\RefCar();
        $new->police_num = $request->get('police_num');
        $new->owner = $request->get('owner');
        $new->brand = $request->get('brand');
        $new->type = $request->get('type');
        $new->color = $request->get('color');
        $new->framework_num = $request->get('framework_num');
        $new->machine_num = $request->get('machine_num');
        $new->bpkb_num = $request->get('bpkb_num');
        $new->kir_date = $request->get('kir_date');
        $new->stnk_tax_date = $request->get('stnk_tax_date');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $car = \App\RiceModel\RefCar::findOrFail($id);

        return view('rice.RefCar.formEdit', \compact('car'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'police_num'=>'required',
            'owner'=>'required',
        ]);

        $new = \App\RiceModel\RefCar::findOrFail($id);
        $new->police_num = $request->get('police_num');
        $new->owner = $request->get('owner');
        $new->brand = $request->get('brand');
        $new->type = $request->get('type');
        $new->color = $request->get('color');
        $new->framework_num = $request->get('framework_num');
        $new->machine_num = $request->get('machine_num');
        $new->bpkb_num = $request->get('bpkb_num');
        $new->kir_date = $request->get('kir_date');
        $new->stnk_tax_date = $request->get('stnk_tax_date');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
