<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class TrnsctDeliveryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('rice.TrnsctDelivery.index');
    }

    public function formAdd()
    {
        $cars = \App\RiceModel\RefCar::all();
        $drivers = \App\RiceModel\RefDriver::all();
        $helpers = \App\RiceModel\RefHelper::all();

        return view('rice.TrnsctDelivery.formAdd', compact('cars', 'drivers', 'helpers'));
    }

    public function addNew(Request $request)
    {
        $new = new \App\RiceModel\TrnsctDelivery();
        $new->delivery_date = $request->get('delivery_date');
        $new->car_id = $request->get('car_id');
        $new->driver_id = $request->get('driver_id');
        $new->helper_id = $request->get('helper_id');
        $new->destination = $request->get('destination');

        $res = [];
        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
                "id" => $new->delivery_id
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function listData()
    {
        $datas = \App\RiceModel\TrnsctDelivery::with(['car', 'mainDriver', 'subDriver', 'mainHelper', 'subHelper'])
            ->orderBy("delivery_date", "desc")
            ->get();

        return DataTables::of($datas)
        ->editColumn('delivery_date', function($datas){
            return date_format(date_create($datas->delivery_date), 'd/m/Y');
        })
        ->editColumn('back_date', function($datas){
            return ($datas->back_date != "") ? date_format(date_create($datas->back_date), 'd/m/Y') : '';
        })
        ->editColumn('mainHelper.name', function($datas){
            return ($datas->helper_id != "") ? $datas->mainHelper->name : '';
        })
        ->addColumn('actions_link', function($datas) {
            // update button
            $btn = "<a href='" . route('rice.delivery.form_edit', $datas->delivery_id) . "' class='btn btn-primary btn-xs'>";
            $btn .= "<i class='fas fa-edit'></i>";
            $btn .= "</a> ";

            // detail button
            $btn .= "<a href='" . route('rice.delivery.detail', $datas->delivery_id) . "' class='btn btn-info btn-xs'>";
            $btn .= "<i class='fas fa-eye'></i>";
            $btn .= "</a> ";

            return $btn;
        })
        ->rawColumns(['actions_link'])
        ->toJson();
    }

    public function detail($id)
    {
        $deliveryInfo = \App\RiceModel\TrnsctDelivery::with(['car', 'mainDriver', 'subDriver', 'mainHelper', 'subHelper'])
            ->findOrFail($id);

        $deliveryItem = \App\RiceModel\TrnsctDeliveryItem::with('product')
            ->where('delivery_id', $id)
            ->orderBy('product_id', 'ASC')
            ->get();

        return view('rice.TrnsctDelivery.detail', compact('deliveryInfo', 'deliveryItem'));
    }

    public function formAddItem($deliveryId)
    {
        $products = \App\RiceModel\RefProduct::all();

        return view('rice.TrnsctDelivery.formAddItem', compact('products', 'deliveryId'));
    }

    public function addNewItem(Request $request, $deliveryId)
    {
        $new = new \App\RiceModel\TrnsctDeliveryItem();
        $new->delivery_id = $deliveryId;
        $new->product_id = $request->get('product_id');
        $new->begin_qty = $request->get('begin_qty');
        $new->price = str_replace(".", "", $request->get('price'));

        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function viewListItem($deliveryId)
    {
        $items = \App\RiceModel\TrnsctDeliveryItem::with(['product'])
            ->where('delivery_id', $deliveryId)
            ->orderBy('product_id', 'ASC')
            ->get();

        return view('rice.TrnsctDelivery.viewListItem', compact('items'));
    }

    public function deleteItem(Request $request)
    {
        $id = $request->get('id');
        $data = \App\RiceModel\TrnsctDeliveryItem::findOrFail($id);

        if ($data->delete()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil dihapus.",
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil dihapus.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function formEdit($id)
    {
        $data = \App\RiceModel\TrnsctDelivery::findOrFail($id);
        $cars = \App\RiceModel\RefCar::all();
        $drivers = \App\RiceModel\RefDriver::all();
        $helpers = \App\RiceModel\Refhelper::all();

        return view('rice.TrnsctDelivery.formEdit', \compact('data', 'cars', 'drivers', 'helpers'));
    }

    public function edit(Request $request, $id)
    {
        $new = \App\RiceModel\TrnsctDelivery::findOrFail($id);
        $new->car_id = $request->get('car_id');
        $new->car_sub_id = $request->get('car_sub_id');
        $new->driver_id = $request->get('driver_id');
        $new->driver_sub_id = $request->get('driver_sub_id');
        $new->helper_id = $request->get('helper_id');
        $new->helper_sub_id = $request->get('helper_sub_id');
        $new->destination = $request->get('destination');
        $new->status_id = $request->get('status_id');
        $new->save();

        $res = [];
        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
                "id" => $id
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }
}
