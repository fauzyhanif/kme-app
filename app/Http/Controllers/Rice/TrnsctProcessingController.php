<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class TrnsctProcessingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('rice.TrnsctProcessing.index');
    }

    public function formAdd()
    {
        $warehouses = \App\RiceModel\RefWarehouse::all();
        $products = \App\RiceModel\RefProduct::all();
        return view('rice.TrnsctProcessing.formAdd', compact('warehouses', 'products'));
    }

    public function addNew(Request $request)
    {
        $new = new \App\RiceModel\TrnsctProcessing();
        $new->type = $request->get('type');
        $new->warehouse_id = $request->get('warehouse_id');
        $new->date = $request->get('date');
        $new->pj = $request->get('pj');
        $new->product_id = $request->get('product_id');
        $new->qty = $request->get('qty');

        $res = [];
        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
                "id" => $new->process_id
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function listData()
    {
        $datas = \App\RiceModel\TrnsctProcessing::with(['product'])->orderBy("date", "desc")->get();

        return DataTables::of($datas)
        ->editColumn('date', function($datas){
            return date_format(date_create($datas->date), 'd/m/Y');
        })
        ->editColumn('product_id', function($datas){
            return $datas->product->name;
        })
        ->addColumn('actions_link', function($datas) {
            // update button
            $btn = "<a href='" . route('rice.processing.form_edit', $datas->process_id) . "' class='btn btn-primary btn-xs'>";
            $btn .= "<i class='fas fa-edit'></i>";
            $btn .= "</a> ";

            // detail button
            $btn .= "<a href='" . route('rice.processing.detail', $datas->process_id) . "' class='btn btn-info btn-xs'>";
            $btn .= "<i class='fas fa-eye'></i>";
            $btn .= "</a> ";

            return $btn;
        })
        ->rawColumns(['actions_link'])
        ->toJson();
    }

    public function detail($id)
    {
        $processingInfo = \App\RiceModel\TrnsctProcessing::with(['product'])->where('process_id', $id)->first();
        $processingItem = \App\RiceModel\TrnsctProcessingItem::with(['product'])->where('process_id', $id)->orderBy('product_id', 'ASC')->get();

        return view('rice.TrnsctProcessing.detail', compact('processingInfo', 'processingItem'));
    }

    public function formAddItem($processId)
    {
        $products = \App\RiceModel\RefProduct::all();

        return view('rice.TrnsctProcessing.formAddItem', compact('products', 'processId'));
    }

    public function addNewItem(Request $request, $processId)
    {
        $new = new \App\RiceModel\TrnsctProcessingItem();
        $new->process_id = $processId;
        $new->product_id = $request->get('product_id');
        $new->qty = $request->get('qty');

        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function viewListItem($processId)
    {
        $items = \App\RiceModel\TrnsctProcessingItem::with(['product'])
            ->where('process_id', $processId)
            ->orderBy('product_id', 'ASC')
            ->get();

        return view('rice.TrnsctProcessing.viewListItem', compact('items'));
    }

    public function deleteProcessingItem(Request $request)
    {
        $id = $request->get('procitem_id');
        $data = \App\RiceModel\TrnsctProcessingItem::findOrFail($id);

        if ($data->delete()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil dihapus.",
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil dihapus.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function formEdit($id)
    {
        $data = \App\RiceModel\TrnsctProcessing::findOrFail($id);
        $warehouses = \App\RiceModel\RefWarehouse::all();
        $products = \App\RiceModel\RefProduct::all();

        return view('rice.TrnsctProcessing.formEdit', \compact('data', 'warehouses', 'products'));
    }

    public function edit(Request $request, $id)
    {
        $new = \App\RiceModel\TrnsctProcessing::findOrFail($id);
        $new->type = $request->get('type');
        $new->warehouse_id = $request->get('warehouse_id');
        $new->date = $request->get('date');
        $new->pj = $request->get('pj');
        $new->product_id = $request->get('product_id');
        $new->qty = $request->get('qty');

        $res = [];
        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
                "id" => $id
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }
}
