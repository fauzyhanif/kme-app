<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefDriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $drivers = \App\RiceModel\RefDriver::all();
        return view('rice.RefDriver.index', compact('drivers'));
    }

    public function formAdd()
    {
        return view('rice.RefDriver.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
            'address'=>'required',
        ]);

        $new = new \App\RiceModel\RefDriver();
        $new->name = $request->get('name');
        $new->sim_num = $request->get('sim_num');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $driver = \App\RiceModel\RefDriver::findOrFail($id);

        return view('rice.RefDriver.formEdit', \compact('driver'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
            'address'=>'required',
        ]);

        $new = \App\RiceModel\RefDriver::findOrFail($id);
        $new->name = $request->get('name');
        $new->sim_num = $request->get('sim_num');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
