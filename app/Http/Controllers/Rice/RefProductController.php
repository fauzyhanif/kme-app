<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = \App\RiceModel\RefProduct::all();
        return view('rice.RefProduct.index', compact('products'));
    }

    public function formAdd()
    {
        return view('rice.RefProduct.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $new = new \App\RiceModel\RefProduct();
        $new->name = $request->get('name');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $product = \App\RiceModel\RefProduct::findOrFail($id);

        return view('rice.RefProduct.formEdit', \compact('product'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $new = \App\RiceModel\RefProduct::findOrFail($id);
        $new->name = $request->get('name');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
