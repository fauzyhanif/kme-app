<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefWarehouseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $warehouses = \App\RiceModel\RefWarehouse::all();
        return view('rice.RefWarehouse.index', compact('warehouses'));
    }

    public function formAdd()
    {
        return view('rice.RefWarehouse.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'address'=>'required',
        ]);

        $new = new \App\RiceModel\RefWarehouse();
        $new->name = $request->get('name');
        $new->address = $request->get('address');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $warehouse = \App\RiceModel\RefWarehouse::findOrFail($id);

        return view('rice.RefWarehouse.formEdit', \compact('warehouse'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'address'=>'required',
        ]);

        $new = \App\RiceModel\RefWarehouse::findOrFail($id);
        $new->name = $request->get('name');
        $new->address = $request->get('address');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
