<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class TrnsctPurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('rice.TrnsctPurchase.index');
    }

    public function formAdd()
    {
        $warehouses = \App\RiceModel\RefWarehouse::all();
        return view('rice.TrnsctPurchase.formAdd', compact('warehouses'));
    }

    public function addNew(Request $request)
    {
        $new = new \App\RiceModel\TrnsctPurchase();
        $new->warehouse_id = $request->get('warehouse_id');
        $new->purchase_date = $request->get('purchase_date');
        $new->pj = $request->get('pj');
        $new->farmer_name = $request->get('farmer_name');
        $new->farmer_phone = $request->get('farmer_phone');
        $new->farmer_address = $request->get('farmer_address');
        $new->agent_name = $request->get('agent_name');
        $new->agent_phone = $request->get('agent_phone');

        $res = [];
        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
                "id" => $new->purchase_id
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function listData()
    {
        $datas = \App\RiceModel\TrnsctPurchase::orderBy("purchase_date", "desc")->get();

        return DataTables::of($datas)
        ->editColumn('purchase_date', function($datas){
            return date_format(date_create($datas->purchase_date), 'd/m/Y');
        })
        ->addColumn('farmer_info', function($datas) {
            return "$datas->farmer_name ($datas->farmer_address) <br> $datas->farmer_phone";
        })
        ->addColumn('agent_info', function($datas) {
            return "$datas->agent_name <br> $datas->farmer_phone";
        })
        ->addColumn('actions_link', function($datas) {
            // update button
            $btn = "<a href='" . route('rice.purchase.form_edit', $datas->purchase_id) . "' class='btn btn-primary btn-xs'>";
            $btn .= "<i class='fas fa-edit'></i>";
            $btn .= "</a> ";

            // detail button
            $btn .= "<a href='" . route('rice.purchase.detail', $datas->purchase_id) . "' class='btn btn-info btn-xs'>";
            $btn .= "<i class='fas fa-eye'></i>";
            $btn .= "</a> ";

            return $btn;
        })
        ->rawColumns(['farmer_info', 'agent_info', 'actions_link'])
        ->toJson();
    }

    public function detail($id)
    {
        $purchaseInfo = \App\RiceModel\TrnsctPurchase::findOrFail($id);
        $purchaseItem = \App\RiceModel\TrnsctPurchaseItem::where('purchase_id', $id)->orderBy('product_id', 'ASC')->get();

        return view('rice.TrnsctPurchase.detail', compact('purchaseInfo', 'purchaseItem'));
    }

    public function formAddItem($purchaseId)
    {
        $products = \App\RiceModel\RefProduct::all();

        return view('rice.TrnsctPurchase.formAddItem', compact('products', 'purchaseId'));
    }

    public function addNewItem(Request $request, $purchaseId)
    {
        $new = new \App\RiceModel\TrnsctPurchaseItem();
        $new->purchase_id = $purchaseId;
        $new->product_id = $request->get('product_id');
        $new->qty = $request->get('qty');
        $new->price = str_replace(".", "", $request->get('price'));
        $new->total = str_replace(".", "", $request->get('total'));

        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function viewListItem($purchaseId)
    {
        $items = \App\RiceModel\TrnsctPurchaseItem::with(['product'])
            ->where('purchase_id', $purchaseId)
            ->orderBy('product_id', 'ASC')
            ->get();

        return view('rice.TrnsctPurchase.viewListItem', compact('items'));
    }

    public function deletePurchaseItem(Request $request)
    {
        $id = $request->get('purcitem_id');
        $data = \App\RiceModel\TrnsctPurchaseItem::findOrFail($id);

        if ($data->delete()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil dihapus.",
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil dihapus.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }

    public function formEdit($id)
    {
        $data = \App\RiceModel\TrnsctPurchase::findOrFail($id);
        $warehouses = \App\RiceModel\RefWarehouse::all();

        return view('rice.TrnsctPurchase.formEdit', \compact('data', 'warehouses'));
    }

    public function edit(Request $request, $id)
    {
        $new = \App\RiceModel\TrnsctPurchase::findOrFail($id);
        $new->warehouse_id = $request->get('warehouse_id');
        $new->purchase_date = $request->get('purchase_date');
        $new->pj = $request->get('pj');
        $new->farmer_name = $request->get('farmer_name');
        $new->farmer_phone = $request->get('farmer_phone');
        $new->farmer_address = $request->get('farmer_address');
        $new->agent_name = $request->get('agent_name');
        $new->agent_phone = $request->get('agent_phone');
        $new->save();

        $res = [];
        if ($new->save()) {
            $res = [
                "status" => "success",
                "text" => "Berhasil, Data berhasil disimpan.",
                "id" => $id
            ];
        } else {
            $res = [
                "status" => "error",
                "text" => "Gagal, Data berhasil disimpan.",
                "id" => ''
            ];
        }

        return response()->json($res);
    }
}
