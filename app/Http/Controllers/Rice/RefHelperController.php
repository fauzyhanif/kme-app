<?php

namespace App\Http\Controllers\Rice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Session;

class RefHelperController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $helpers = \App\RiceModel\RefHelper::all();
        return view('rice.RefHelper.index', compact('helpers'));
    }

    public function formAdd()
    {
        return view('rice.RefHelper.formAdd');
    }

    public function addNew(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
            'address'=>'required',
        ]);

        $new = new \App\RiceModel\RefHelper();
        $new->name = $request->get('name');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->save();

        $response = "Berhasil, Data berhasil disimpan.";
        return response()->json($response);
    }

    public function formEdit($id)
    {
        $helper = \App\RiceModel\RefHelper::findOrFail($id);

        return view('rice.RefHelper.formEdit', \compact('helper'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'phone_num'=>'required',
            'address'=>'required',
        ]);

        $new = \App\RiceModel\RefHelper::findOrFail($id);
        $new->name = $request->get('name');
        $new->phone_num = $request->get('phone_num');
        $new->address = $request->get('address');
        $new->active = $request->get('active');
        $new->save();

        $response = "Berhasil, Data berhasil diubah.";
        return response()->json($response);
    }
}
