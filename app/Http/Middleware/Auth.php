<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('auth_nama')) {
            if(Session::has('auth_module_aktif')) {
                return $next($request);
            } else {
                return redirect('/gate');
            }
        }

        return redirect('/login');
    }
}
