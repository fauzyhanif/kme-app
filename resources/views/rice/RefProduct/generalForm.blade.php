<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($product)
            value="{{ $product->name }}"
        @endisset
    >
</div>

@isset($product)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($product)
                @if ($product->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($product)
                @if ($product->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
