<div class="form-group">
    <label>Gudang <span class="text-red">*</span></label>
    <select name="warehouse_id" class="form-control" required>
        <option value="">** Pilih Gudang</option>
        @foreach ($warehouses as $warehouse)
            <option value="{{ $warehouse->warehouse_id }}" {{ (isset($data) && $data->warehouse_id == $warehouse->warehouse_id) ? 'selected' : '' }}>{{ $warehouse->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Tanggal <span class="text-red">*</span></label>
    <input
        type="date"
        name="purchase_date"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->purchase_date }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Penanggung Jawab</label>
    <input type="text" name="pj" class="form-control" @isset($data) value="{{ $data->pj }}" @endisset>
</div>

<div class="form-group">
    <label>Petani <span class="text-red">*</span></label>
    <input
        type="text"
        name="farmer_name"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->farmer_name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No Telephone Petani</label>
    <input
        type="text"
        name="farmer_phone"
        class="form-control"
        @isset($data)
            value="{{ $data->farmer_phone }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat Petani</label>
    <input
        type="text"
        name="farmer_address"
        class="form-control"
        @isset($data)
            value="{{ $data->farmer_address }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Nama Calo</label>
    <input
        type="text"
        name="agent_name"
        class="form-control"
        @isset($data)
            value="{{ $data->agent_name }}"
        @endisset>
</div>

<div class="form-group">
    <label>No Telephone Calo</label>
    <input
        type="text"
        name="agent_phone"
        class="form-control"
        @isset($data)
            value="{{ $data->agent_phone }}"
        @endisset>
</div>
