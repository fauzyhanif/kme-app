@php $no = 1; @endphp
@php $total = 0; @endphp
@foreach ($items as $item)
    <tr>
        <td>{{ $no }}.</td>
        <td>{{ $item->product->name }}</td>
        <td>{{ $item->qty }}</td>
        <td>{{ GeneralHelper::rupiah($item->price) }}</td>
        <td>{{ GeneralHelper::rupiah($item->total) }}</td>
        <td>
            <button type="button" class="btn btn-danger btn-xs" onclick="confirmDeleteItem('{{ $item->purcitem_id }}')">
                <i class="fas fa-trash"></i>
            </button>
        </td>
    </tr>
@php $no += 1; @endphp
@php $total += $item->total; @endphp
@endforeach
<tr>
    <td colspan="4" class="font-weight-bold text-center">Total</td>
    <td class="font-weight-bold">{{ GeneralHelper::rupiah($total) }}</td>
    <td></td>
</tr>
