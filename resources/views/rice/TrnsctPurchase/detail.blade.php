@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('rice.purchase') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pembelian
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Pembelian</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Info Pembelian</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <td width="35%">ID Pembelian</td>
                                <td class="font-weight-bold">{{ $purchaseInfo->purchase_id }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Tgl Pembelian</td>
                                <td class="font-weight-bold">{{ GeneralHelper::konversiTgl($purchaseInfo->purchase_date) }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Pabrik</td>
                                <td class="font-weight-bold">{{ $purchaseInfo->warehouse->name }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Petani</td>
                                <td class="font-weight-bold">
                                    {{ $purchaseInfo->farmer_name }} @if ($purchaseInfo->farmer_Address != null) ({{ $purchaseInfo->farmer_Address }}) @endif <br>
                                    {{ $purchaseInfo->farmer_phone }}
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Calo</td>
                                <td class="font-weight-bold">
                                    {{ $purchaseInfo->agent_name }} <br>
                                    {{ $purchaseInfo->agent_phone }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item Pembelian</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-sm">
                            <thead class="bg-info">
                                <th width="5%">No</th>
                                <th>Item</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right" width="15%">Qty</th>
                                <th class="text-right">Subtotal</th>
                            </thead>
                            <tbody>
                                @php $no = 1 @endphp
                                @php $ttlQty = 0 @endphp
                                @php $ttlPrice = 0 @endphp
                                @foreach ($purchaseItem as $item)
                                    <tr>
                                        <td>{{ $no }}.</td>
                                        <td>{{ $item->product->name }}</td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($item->price) }}</td>
                                        <td class="text-right">{{ $item->qty }} kg</td>
                                        <td class="text-right">{{ GeneralHelper::rupiah($item->total) }}</td>
                                    </tr>
                                @php $no += 1 @endphp
                                @php $ttlQty += $item->qty  @endphp
                                @php $ttlPrice += $item->total  @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th colspan="3" class="text-center">Total</th>
                                <th class="text-right">{{ $ttlQty }} kg</th>
                                <th class="text-right">{{ GeneralHelper::rupiah($ttlPrice) }}</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('rice.TrnsctPurchase.asset.js')
@endsection
