@extends('index')

@section('content')
<section class="content-header">
    <h1>Pembelian</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        List Data
                        <div class="card-tools">
                            <a href="{{ route('rice.purchase.form_add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Input Pembelian
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>Penanggung Jawab</th>
                                <th>Petani</th>
                                <th>Calo</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(route('rice.purchase.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'purchase_date', name: 'purchase_date', searchable: true },
            { data: 'pj', name: 'pj', searchable: true },
            { data: 'farmer_info', name: 'farmer_info', searchable: true },
            { data: 'agent_info', name: 'agent_info', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@include('rice.TrnsctPurchase.asset.js')
@endsection

