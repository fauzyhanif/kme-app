@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('rice.delivery') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pengiriman
            </a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('rice.delivery.form_edit', $deliveryId) }}">
                Kembali ke form awal
            </a>
        </li>
        <li class="breadcrumb-item active">Input Item Pengiriman</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <form name="delivery_item" action="{{ route('rice.delivery.add_new_item', $deliveryId) }}" method="POST">
                        @csrf
                        <input type="hidden" name="type" value="add">
                        <input type="hidden" name="delivery_id" value="{{ $deliveryId }}">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Produk  <span class="text-red">*</span></label>
                                        <select name="product_id" class="form-control" required>
                                            <option value="">** Pilih Produk</option>
                                            @foreach ($products as $product)
                                                <option value="{{ $product->product_id }}">{{ $product->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Berat (hitungan kilo gram) <span class="text-red">*</span></label>
                                        <input type="number" name="begin_qty" class="form-control item-qty" required value="0">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Harga (perkilo gram) <span class="text-red">*</span></label>
                                        <input type="text" name="price" class="form-control money item-price" required value="0">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Total <span class="text-red">*</span></label>
                                        <input type="text" name="total" class="form-control money item-total" required value="0" style="pointer-events: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                            <a href="{{ route('rice.delivery') }}" class="btn btn-primary">
                                Selesai
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Daftar Item Pembelian</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <th width="5%">No</th>
                                <th>Produk</th>
                                <th width="5%">Qty</th>
                                <th width="20%">Harga</th>
                                <th width="20%">Subtotal</th>
                                <th width="5%">Hapus</th>
                            </thead>
                            <tbody id="view-list-item">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('rice.TrnsctDelivery.modalDeleteItem')
@include('rice.TrnsctDelivery.asset.js')
@endsection
