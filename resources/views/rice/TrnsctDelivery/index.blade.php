@extends('index')

@section('content')
<section class="content-header">
    <h1>Pengiriman</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        List Data
                        <div class="card-tools">
                            <a href="{{ route('rice.delivery.form_add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Input Pengiriman
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>Tujuan</th>
                                <th>Supir</th>
                                <th>Helper</th>
                                <th>Mobil</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(route('rice.delivery.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'delivery_date', name: 'delivery_date', searchable: true },
            { data: 'destination', name: 'destination', searchable: true },
            { data: 'mainDriver.name', name: 'driver', searchable: true },
            { data: 'mainHelper.name', name: 'helper', searchable: true },
            { data: 'car.name', name: 'car', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@include('rice.TrnsctPurchase.asset.js')
@endsection

