<div class="form-group">
    <label>Tanggal Pengiriman <span class="text-red">*</span></label>
    <input
        type="date"
        name="delivery_date"
        class="form-control"
        required
        @isset($data)
            value="{{ $data->delivery_date }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Tujuan</label>
    <input type="text" name="destination" class="form-control" @isset($data) value="{{ $data->destination }}" @endisset>
</div>

<div class="form-group">
    <label>Mobil <span class="text-red">*</span></label>
    <select name="car_id" class="form-control" required>
        <option value="">** Pilih Mobil</option>
        @foreach ($cars as $car)
            <option value="{{ $car->car_id }}" {{ (isset($data) && $data->car_id == $car->car_id) ? 'selected' : '' }}>{{ $car->police_num }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Supir <span class="text-red">*</span></label>
    <select name="driver_id" class="form-control" required>
        <option value="">** Pilih Supir</option>
        @foreach ($drivers as $driver)
            <option value="{{ $driver->driver_id }}" {{ (isset($data) && $data->driver_id == $driver->driver_id) ? 'selected' : '' }}>{{ $driver->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Helper <span class="text-red">*</span></label>
    <select name="helper_id" class="form-control" required>
        <option value="">** Pilih Helper</option>
        @foreach ($helpers as $helper)
            <option value="{{ $helper->helper_id }}" {{ (isset($data) && $data->helper_id == $driver->helper_id) ? 'selected' : '' }}>{{ $helper->name }}</option>
        @endforeach
    </select>
</div>


