<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($helper)
            value="{{ $helper->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($helper)
            value="{{ $helper->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat <span class="text-red">*</span></label>
    <input
        type="text"
        name="address"
        class="form-control"
        required
        @isset($helper)
            value="{{ $helper->address }}"
        @endisset
    >
</div>

@isset($helper)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($helper)
                @if ($helper->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($helper)
                @if ($helper->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
