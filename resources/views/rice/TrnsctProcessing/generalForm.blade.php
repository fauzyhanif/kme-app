<div class="form-group">
    <label>Type Proses <span class="text-red">*</span></label>
    <select name="type" class="form-control" required>
        <option value="">** Pilih Proses</option>
        <option {{ (isset($data) && $data->type == 'OVEN') ? 'selected' : '' }}>OVEN</option>
        <option {{ (isset($data) && $data->type == 'GILING') ? 'selected' : '' }}>GILING</option>
    </select>
</div>

<div class="form-group">
    <label>Gudang <span class="text-red">*</span></label>
    <select name="warehouse_id" class="form-control" required>
        <option value="">** Pilih Gudang</option>
        @foreach ($warehouses as $warehouse)
            <option value="{{ $warehouse->warehouse_id }}" {{ (isset($data) && $data->warehouse_id == $warehouse->warehouse_id) ? 'selected' : '' }}>{{ $warehouse->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Tanggal <span class="text-red">*</span></label>
    <input
        type="date"
        name="date"
        class="form-control"
        required
        @if(isset($data))
            value="{{ $data->date }}"
        @else
            value="{{ date('Y-m-d') }}"
        @endif
    >
</div>

<div class="form-group">
    <label>Penanggung Jawab</label>
    <input type="text" name="pj" class="form-control" @isset($data) value="{{ $data->pj }}" @endisset>
</div>

<div class="form-group">
    <label>Produk <span class="text-red">*</span></label>
    <select name="product_id" class="form-control" required>
        <option value="">** Pilih Produk</option>
        @foreach ($products as $product)
        <option value="{{ $product->product_id }}"
            {{ (isset($data) && $data->product_id == $product->product_id) ? 'selected' : '' }}>{{ $product->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Berat (dalam hitungan kilogram)</label>
    <input type="number" name="qty" class="form-control" @isset($data) value="{{ $data->qty }}" @endisset>
</div>
