<script>
$(document).ready(function () {
    viewListItem();
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }

                if (res.status == 'success') {
                    toastr.success(res.text);
                    var baseUrl = {!! json_encode(url('/rice/processing/form_add_item')) !!};
                    var baseUrl = baseUrl + "/" + res.id;
                    window.location.href = baseUrl;
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="processing_item"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    form.trigger("reset");
                    viewListItem();
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="delete_purchase_item"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.status == 'success') {
                    toastr.success(res.text);
                    $('#modal-delete-item').modal('hide');
                    viewListItem();
                } else {
                    toastr.error(res.text)
                }

            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function viewListItem() {
    var purchaseId = $('input[name="process_id"]').val();
    var baseUrl = {!! json_encode(url('/rice/processing/view_list_item')) !!};
    baseUrl = baseUrl + '/' + purchaseId;
    $.ajax({
        type: 'GET',
        url: baseUrl,
        dataType:'html',
        success: function (res) {
            $('#view-list-item').html(res);
        }
    });
}

function confirmDeleteItem(id) {
    $('#modal-delete-item').modal('show');
    $('#modal-delete-item-id').val(id);
}
</script>
