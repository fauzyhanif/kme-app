@extends('index')

@section('content')
<section class="content-header">
    <h1>Proses Pengeringan & Giling</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        List Data
                        <div class="card-tools">
                            <a href="{{ route('rice.processing.form_add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Input Proses
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>Penanggung Jawab</th>
                                <th>Proses</th>
                                <th>Produk</th>
                                <th>Berat (kg)</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(function() {
    loadData();
});

function loadData() {
    var base = {!! json_encode(route('rice.processing.list_data')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'date', name: 'date', searchable: true },
            { data: 'type', name: 'type', searchable: true },
            { data: 'pj', name: 'pj', searchable: true },
            { data: 'product_id', name: 'product_id', searchable: true },
            { data: 'qty', name: 'qty', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@endsection

