<!-- The Modal -->
<div class="modal" id="modal-delete-item">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="delete_purchase_item" action="{{ route('rice.processing.delete_processing_item') }}" method="POST">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Hapus Item Hasil Oven & Penggilingan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <input type="hidden" name="procitem_id" id="modal-delete-item-id">
                    Anda yakin inginmenghapus item ini?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Ya, Hapus Sekarang</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
