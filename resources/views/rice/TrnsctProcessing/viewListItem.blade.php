@php $no = 1; @endphp
@php $total = 0; @endphp
@foreach ($items as $item)
    <tr>
        <td>{{ $no }}.</td>
        <td>{{ $item->product->name }}</td>
        <td>{{ $item->qty }} kg</td>
        <td>
            <button type="button" class="btn btn-danger btn-xs" onclick="confirmDeleteItem('{{ $item->procitem_id }}')">
                <i class="fas fa-trash"></i>
            </button>
        </td>
    </tr>
@php $no += 1; @endphp
@php $total += $item->qty; @endphp
@endforeach
<tr>
    <td colspan="2" class="font-weight-bold text-center">Total</td>
    <td class="font-weight-bold">{{ $total }} kg</td>
    <td></td>
</tr>
