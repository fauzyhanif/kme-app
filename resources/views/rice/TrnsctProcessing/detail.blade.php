@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('rice.purchase') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Proses Pengeringan & Giling
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Proses Pengeringan & Giling</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Info Proses Pengeringan & Giling</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <td width="35%">Type Proses</td>
                                <td class="font-weight-bold">{{ $processingInfo->type }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Tgl Proses</td>
                                <td class="font-weight-bold">{{ GeneralHelper::konversiTgl($processingInfo->purchase_date) }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Pabrik</td>
                                <td class="font-weight-bold">{{ $processingInfo->warehouse->name }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Penanggung Jawab</td>
                                <td class="font-weight-bold">{{ $processingInfo->pj }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Produk diproses</td>
                                <td class="font-weight-bold">{{ $processingInfo->product->name }}</td>
                            </tr>
                            <tr>
                                <td width="35%">Berat Produk diproses</td>
                                <td class="font-weight-bold">{{ $processingInfo->qty }} kg</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Item Hasil</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-sm">
                            <thead class="bg-info">
                                <th width="5%">No</th>
                                <th>Item</th>
                                <th class="text-right" width="15%">Qty</th>
                            </thead>
                            <tbody>
                                @php $no = 1 @endphp
                                @php $ttlQty = 0 @endphp
                                @foreach ($processingItem as $item)
                                    <tr>
                                        <td>{{ $no }}.</td>
                                        <td>{{ $item->product->name }}</td>
                                        <td class="text-right">{{ $item->qty }} kg</td>
                                    </tr>
                                @php $no += 1 @endphp
                                @php $ttlQty += $item->qty  @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th colspan="2" class="text-center">Total</th>
                                <th class="text-right">{{ $ttlQty }} kg</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('rice.TrnsctPurchase.asset.js')
@endsection
