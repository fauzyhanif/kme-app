<div class="form-group">
    <label>No Polisi <span class="text-red">*</span></label>
    <input
        type="text"
        name="police_num"
        class="form-control"
        required
        @isset($car)
            value="{{ $car->police_num }}"
        @endisset>
</div>

<div class="form-group">
    <label>Pemilik <span class="text-red">*</span></label>
    <input
        type="text"
        name="owner"
        class="form-control"
        required
        @isset($car)
            value="{{ $car->owner }}"
        @endisset>
</div>

<div class="form-group">
    <label>Merk</label>
    <input
        type="text"
        name="brand"
        class="form-control"
        @isset($car)
            value="{{ $car->brand }}"
        @endisset>
</div>

<div class="form-group">
    <label>Tipe</label>
    <input
        type="text"
        name="type"
        class="form-control"
        @isset($car)
            value="{{ $car->type }}"
        @endisset>
</div>

<div class="form-group">
    <label>Warna</label>
    <input
        type="text"
        name="color"
        class="form-control"
        @isset($car)
            value="{{ $car->color }}"
        @endisset>
</div>

<div class="form-group">
    <label>No Rangka</label>
    <input
        type="text"
        name="framework_num"
        class="form-control"
        @isset($car)
            value="{{ $car->framework_num }}"
        @endisset>
</div>

<div class="form-group">
    <label>No Mesin</label>
    <input
        type="text"
        name="machine_num"
        class="form-control"
        @isset($car)
            value="{{ $car->machine_num }}"
        @endisset>
</div>

<div class="form-group">
    <label>No BPKB</label>
    <input
        type="text"
        name="bpkb_num"
        class="form-control"
        @isset($car)
            value="{{ $car->bpkb_num }}"
        @endisset>
</div>

<div class="form-group">
    <label>Tanggal KIR</label>
    <input
        type="date"
        name="kir_date"
        class="form-control"
        @isset($car)
            value="{{ $car->kir_date }}"
        @endisset>
</div>

<div class="form-group">
    <label>Tanggal STNK</label>
    <input
        type="date"
        name="stnk_tax_date"
        class="form-control"
        @isset($car)
            value="{{ $car->stnk_tax_date }}"
        @endisset>
</div>

@isset($car)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($car)
                @if ($car->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($car)
                @if ($car->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
