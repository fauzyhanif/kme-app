<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($driver)
            value="{{ $driver->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No SIM</label>
    <input
        type="text"
        name="sim_num"
        class="form-control"
        @isset($driver)
            value="{{ $driver->sim_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($driver)
            value="{{ $driver->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat <span class="text-red">*</span></label>
    <input
        type="text"
        name="address"
        class="form-control"
        required
        @isset($driver)
            value="{{ $driver->address }}"
        @endisset
    >
</div>

@isset($driver)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($driver)
                @if ($driver->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($driver)
                @if ($driver->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
