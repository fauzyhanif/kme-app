@extends('index')

@section('content')
<section class="content-header">
    <h1>Manajemen Driver</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card shadow-none">
                    <div class="card-header">
                        List Data
                        <div class="card-tools">
                            <a href="{{ route('rice.driver.form_add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Driver
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th class="text-center" width="5%">No</th>
                                <th>Nama</th>
                                <th>No Handphone</th>
                                <th>Alamat</th>
                                <th class="text-center" width="10%">Status</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>
                                @php $no = 1 @endphp
                                @foreach ($drivers as $driver)
                                    <tr>
                                        <td class="text-center">{{ $no }}.</td>
                                        <td>{{ $driver->name }}</td>
                                        <td>{{ $driver->phone_num }}</td>
                                        <td>{{ $driver->address }}</td>
                                        <td class="text-center">{{ ($driver->active == 'Y') ? 'Aktif' : 'Nonaktif' }}</td>
                                        <td>
                                            <a
                                                href="{{ url('/rice/driver/form_edit', $driver->driver_id) }}"
                                                class="btn btn-sm btn-primary">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @php $no += 1 @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

