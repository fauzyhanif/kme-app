@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('rice.warehouse') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pabrik / Gudang
            </a>
        </li>
        <li class="breadcrumb-item active">Edit Pabrik / Gudang</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <form id="form-add" action="{{ route('rice.warehouse.edit', $warehouse->warehouse_id) }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="edit">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div id="response-alert"></div>

                            <div class="tab-content">
                                <div class="tab-pane container active" id="general">
                                    @include('rice.RefWarehouse.generalForm')
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('rice.RefWarehouse.asset.js')
@endsection
