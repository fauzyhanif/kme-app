<style>
    body {
        font-size: 11px;
    }

    .borderless {
        border-collapse: separate;
        border-spacing: 0 -15px;
        margin-top: 10px;
    }

    .borderless td,
    .borderless th {
        border: none;
    }

    .data-title {
        font-size: 16px;
    }

    tr td.except,
    tr th.except {
        padding-top: 6 !important;
        padding-bottom: 6 !important;
        margin: 0 !important;
    }

    tr th.small-head {
        padding-top: 2 !important;
        padding-bottom: 2 !important;
        margin: 0 !important;
    }

    tr td.small-body {
        padding: 2px 4px !important;
        margin: 0 !important;
    }
</style>
