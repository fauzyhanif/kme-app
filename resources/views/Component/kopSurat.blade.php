
<div class="row kop-surat">
    <div class="col-md-3">
        <img src="{{ url('public/img/logo_kecil.png') }}"
            style="max-width: 110px; margin: 30px 10px -180px 30px"
            alt="">
    </div>
    <div class="col-md-9 text-center">
        <h3 class="text-bold">PT KARYA MAS EMPAT</h3>
        <p>
            Jl. Raya Rancaudik KM. 5 Tambakdahan 41253 Subang - Jawa Barat
            <br>
            No. HP 0813 8888 7100 / 0813 8888 5445 Email: buskaryamasempat@gmail.com
        </p>
    </div>
</div>
