@extends('index')

@section('content')
<section class="content-header">
    <h1>Ubah Password</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('travel.agent.addnew') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="add">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Password Lama</label>
                                <input type="password" name="old_password" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Password Baru</label>
                                <input type="password" name="new_password" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Konfirmasi Password</label>
                                <input type="password" name="repeat_password" class="form-control">
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success btn-save">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.RefAgent.asset.js')
@endsection
