<style>
.card {
    box-shadow: 0px !important;
}

.table td, .table th {
    padding: 0.45rem 0.75rem !important;
}

.scroll-x {
    overflow-x: scroll;
}
</style>
