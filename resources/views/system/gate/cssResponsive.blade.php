<style>
@media (max-width: 768px) {
    .wrapp-module {
        margin-bottom: 15px;
    }

    .wrapp-button {
        float: left !important;
    }

    .module > a > .card > .card-body > .logo {
        margin: 0px 0px 12px 12px;
    }

    .module-name > p {
        margin-top: -10px;
    }

    .module-list {
        text-align: center;
    }

    .logo-module {
        margin-bottom: 10px
    }
}
</style>
