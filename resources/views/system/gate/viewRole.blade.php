<div class="main-title mb-3">
    <h4>Daftar Role</h4>
    <p class="text-muted">Silahkan pilih role dibawah ini</p>
</div>

<div class="navigation row">
    @php
        $arrRole = json_decode(Session::get('auth_role'))
    @endphp
    @foreach ($roles as $role)
        @if (in_array($role->id_role, $arrRole))
        <div class="col-md-12 mb-2 role">
            <a href="{{ route('gate.set.auth.role.and.module', [$role->id_role, $idModule]) }}">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $role->nama }}</h5>
                        <p class="card-text">Sistem Manajemen Travel</p>
                    </div>
                </div>
            </a>
        </div>
    @endif
    @endforeach
</div>
