@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.acl') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Acl
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Acl Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Acl Baru
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.acl.add.new') }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Module <span class="text-red">*</span></label> <br>
                                @foreach ($modules as $module)
                                <input type="checkbox" name="id_module[]" value="{{ $module->id_module }}"> {{ $module->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Role <span class="text-red">*</span></label> <br>
                                @foreach ($roles as $role)
                                <input type="checkbox" name="id_role[]" value="{{ $role->id_role }}"> {{ $role->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Group Menu <span class="text-red">*</span></label> <br>
                                @foreach ($groups as $group)
                                <input type="checkbox" name="id_group_menu[]" value="{{ $group->id_group_menu }}"> {{ $group->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Ditampilkan di menu sidebar?</label>
                                <p>
                                    <input type="radio" name="is_menu[]" value="N" checked> Tidak <br>
                                    <input type="radio" name="is_menu[]" value="Y"> Ya <br>
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Submenu?</label>
                                <p>
                                    <input type="radio" name="is_child[]" value="N" checked> Tidak <br>
                                    <input type="radio" name="is_child[]" value="Y"> Ya <br>
                                </p>
                            </div>

                            <div class="form-group">
                                <label>ID Parent</label>
                                <input type="number" name="parent" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Icon</label>
                                <input type="text" name="icon" class="form-control" placeholder="fa-loading">
                            </div>

                            <div class="form-group">
                                <label>Label Menu</label>
                                <input type="text" name="label" class="form-control" placeholder="Acl">
                            </div>

                            <div class="form-group">
                                <label>Method</label>
                                <p>
                                    <input type="checkbox" name="method[]" value="GET" checked> GET <br>
                                    <input type="checkbox" name="method[]" value="POST"> POST <br>
                                    <input type="checkbox" name="method[]" value="PUT"> PUT <br>
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Url</label>
                                <input type="text" name="url" class="form-control" placeholder="/system/acl">
                            </div>

                            <div class="form-group">
                                <label>Nama Route</label>
                                <input type="text" name="route" class="form-control" placeholder="system.acl">
                            </div>

                            <div class="form-group">
                                <label>Controller</label>
                                <input type="text" name="controller" class="form-control" placeholder="AclController">
                            </div>

                            <div class="form-group">
                                <label>Function</label>
                                <input type="text" name="function" class="form-control" placeholder="index">
                            </div>

                            <div class="form-group">
                                <label>Middleware</label>
                                <p>
                                    <input type="checkbox" name="middleware[]" value="auth" checked> auth <br>
                                    <input type="checkbox" name="middleware[]" value="gate"> gate
                                </p>
                            </div>

                            <div class="form-group">
                                <label>Urut</label>
                                <input type="number" name="urut" class="form-control">
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.groupMenu.asset.js')
@endsection
