@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.module') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Module
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Module Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Module Baru
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.module.add.new') }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>ID Module <span class="text-red">*</span></label>
                                <input type="number" name="id_module" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nama Module <span class="text-red">*</span></label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi <span class="text-red">*</span></label>
                                <textarea name="deskripsi" class="form-control" required></textarea>
                            </div>

                            <div class="form-group">
                                <label>Icon Module</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.module.asset.js')
@endsection
