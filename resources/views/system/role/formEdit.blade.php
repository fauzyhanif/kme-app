@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('system.role') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Role
            </a>
        </li>
        <li class="breadcrumb-item active">Edit Role</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Edit Role
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('system.role.edit', $role->id_role) }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>ID Role <span class="text-red">*</span></label>
                                <input type="number" name="id_role" value="{{ $role->id_role }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Nama Role <span class="text-red">*</span></label>
                                <input type="text" name="nama" value="{{ $role->nama }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Module <span class="text-red">*</span></label> <br>
                                @php
                                    $id_module = json_decode($role->id_module);
                                @endphp
                                @foreach ($modules as $module)
                                <input type="checkbox" name="id_module[]" value="{{ $module->id_module }}" @if (in_array($module->id_module, $id_module)) checked @endif> {{ $module->nama }} <br>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Aktif</label>
                                <p>
                                    <input type="radio" name="aktif[]" value="Y" @if ($role->aktif == "Y") checked @endif> Ya <br>
                                    <input type="radio" name="aktif[]" value="N" @if ($role->aktif == "N") checked @endif> Tidak
                                </p>
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('system.role.asset.js')
@endsection
