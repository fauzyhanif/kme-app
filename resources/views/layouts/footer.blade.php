        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2020</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 1.0-beta
            </div>
        </footer>
    </div>
    @include('layouts.assets.js')
</body>
</html>
