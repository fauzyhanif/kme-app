@extends('index')

@section('content')

<style>
.table-wrapper {
    overflow-x:scroll;
}

.except {
    position: sticky;
    min-width: 20em;
    left: 0;
    top: auto;
    border-top-width: 2px;
    margin-top: -1px;
    background-color: #007bff;
    color: #fff;
    border: 2px solid #fff !important;
}

th, td {
    min-width: 100px;
}

</style>
<section class="content-header">
    <h1>Kalender Booking</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-body table-wrapper">
                        <div class="row justify-content-center">
                            <div class="col-md-4 text-center">
                                <h3 class="font-weight-bold">
                                    {{ GeneralHelper::month(substr($thisMonth, -2)) . " " . substr($thisMonth, 0, 4) }}
                                </h3>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-4 text-center">
                                <form method="POST" action="">
                                    @csrf
                                    <input type="hidden" name="this_month" value="{{ $thisMonth }}">

                                    <button
                                        type="submit"
                                        class="btn btn-sm btn-primary"
                                        name="type"
                                        value="minus">
                                        <i class="fa fa-chevron-left"></i> Sebelumnya
                                    </button>

                                    <button
                                        type="submit"
                                        class="btn btn-sm btn-primary"
                                        name="type"
                                        value="plus">
                                        Selanjutnya <i class="fa fa-chevron-right"></i>
                                    </button>
                                </form>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <table class="table table-bordered" id="fixed">
                                <thead>
                                    <th class="except">Unit</th>
                                    <th class="bg-info">Jml Unit</th>
                                    @for ($i = 1; $i <= $numOfDays; $i++)
                                        <th class="text-center bg-info" width="20">{{ $i }}</th>
                                    @endfor
                                </thead>
                                <tbody>
                                    @foreach ($units as $unit)
                                        <tr>
                                            <td class="except">
                                                <b>{{ $unit->name }} ({{ $unit->seat }} seat)</b>
                                            </td>
                                            <td class="text-center">{{ $unit->ttl_ready }}</td>
                                            @for ($i = 1; $i <= $numOfDays; $i++)
                                                <td class="text-center">
                                                    @php
                                                        $thisDate = str_pad($i, 2, "0", STR_PAD_LEFT);
                                                        $thisDate = $a_month . '_' . $thisDate;
                                                    @endphp
                                                    @if (array_key_exists($unit->category_id."_".$thisDate, $arrBooked))
                                                        @php
                                                            $ttl_ready = $unit->ttl_ready - $arrBooked[$unit->category_id."_".$thisDate]
                                                        @endphp
                                                        @if ($ttl_ready < $unit->ttl_ready && $ttl_ready > 0)
                                                            <span class="text-orange font-weight-bold">{{ $ttl_ready }}</span>
                                                        @endif

                                                        @if ($ttl_ready == 0)
                                                            <span class="text-danger font-weight-bold">{{ $ttl_ready }}</span>
                                                        @endif
                                                    @else
                                                        <span class="font-weight-bold">{{ $unit->ttl_ready }}</span>
                                                    @endif
                                                </td>
                                            @endfor
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
