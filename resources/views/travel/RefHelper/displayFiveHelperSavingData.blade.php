@if (count($savings) > 0)
    @php
        $no = 1;
    @endphp
    @foreach ($savings as $saving)
    <tr>
        <td>{{ $no }}.</td>
        <td>{{ GeneralHelper::konversiTgl($saving->trnsct_date, 'slash') }}</td>
        <td>{{ $saving->information }}</td>
        <td>{{ GeneralHelper::rupiah($saving->in) }}</td>
        <td>{{ GeneralHelper::rupiah($saving->out) }}</td>
        <td>
            <button type="button" class="btn btn-xs btn-danger" onclick="showFormDeleteSaving($saving->id)"><i class="fas fa-trash"></i> Hapus</button>
        </td>
    </tr>
    @php
        $no += 1;
    @endphp
    @endforeach
@else
    <tr>
        <td class="text-center" colspan="5">Tidak ada data tabungan premi</td>
    </tr>
@endif

