<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Pengambilan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="saving_retreival" action="{{ route('travel.helper.savingretrieval') }}" method="POST">
                <input type="hidden" name="ttl_saving" value="{{ $helper->ttl_saving }}">
                <input type="hidden" name="type" value="HELPER">
                <input type="hidden" name="user_id" value="{{ $helper->helper_id }}">

                <!-- Modal body -->
                <div class="modal-body">
                    @if ($helper->ttl_saving <= 0) <div class="alert alert-warning alert-dismissible">
                        <h5><i class="icon fas fa-exclamation-triangle"></i> Uuppss!</h5>
                        Helper ini tidak memilik tabungan premi saat ini.
                </div>
                @endif

                <div class="alert alert-warning alert-dismissible alert-withdrawal-over" style="display: none">
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Uuppss!</h5>
                    Tabungan driver tidak cukup.
                </div>

                <div class="form-group">
                    <label>Tanggal Pengambilan</label>
                    <input type="date" name="trnsct_date" value="<?= date('Y-m-d') ?>" class="form-control" required
                        @if($helper->ttl_saving <= 0) readonly @endif>
                </div>

                <div class="form-group">
                    <label>Jumlah</label>
                    <input type="text" name="out" class="form-control money" required
                        @if ($helper->ttl_saving <= 0) readonly @endif>
                </div>

                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" name="information" class="form-control" required
                        @if ($helper->ttl_saving <= 0) readonly @endif>
                </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-saving"
                @if ($helper->ttl_saving <= 0) disabled @endif>
                    Simpan
            </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
    </div>
</div>
</div>
