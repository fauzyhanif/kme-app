@extends('index')

@section('content')
<section class="content-header">
    <div class="row">
        <div class="col-md-6">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('travel.helper') }}">
                        <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                        Daftar Helper
                    </a>
                </li>
                <li class="breadcrumb-item active">Detail Helper</li>
            </ol>
        </div>
        <div class="col-md-6">
            <div class="float-right">
                <a href="{{ url('/travel/helper/form-edit',  $helper->helper_id) }}" class="btn btn-primary btn-sm">
                    <div class="fas fa-edit"></div> Edit Data
                </a>
            </div>
        </div>
    </div>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="card shadow-none">
					<div class="card-header">
						<h3 class="card-title">
							Detail Helper
						</h3>
					</div>
					<div class="card-body">
                        <table class="table sm table-borderless">
                            <tbody>
                                <tr>
                                    <td class="text-secondary" width="30%">Nama</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $helper->name }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="30%">No Telp / Hp </td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $helper->phone_num }}</td>
                                </tr>
                                <tr>
                                    <td class="text-secondary" width="30%">Alamat</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $helper->address }}</td>
                                </tr>
                            </tbody>
                        </table>

					</div>
				</div>
			</div>

			<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ ($helper->ttl_trnsct > 0) ? $helper->ttl_trnsct : 0 }}</h3>

                                <p>Total Jalan</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-car"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-12">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>Rp. {{ GeneralHelper::rupiah($helper->ttl_saving) }}</h3>

                                <p>Total Tabungan Premi</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-wallet"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Daftar Mutasi Tabungan</h3>
                                <div class="card-tools">
                                    <a href="{{ route('travel.helper.alldriversaving', $helper->helper_id) }}"
                                        class="text-gray mr-3">
                                        <u>Lihat Selengkapnya</u>
                                    </a>
                                    <button class="btn btn-success btn-sm"  data-toggle="modal" data-target="#form-initial-saving">
										<i class="fas fa-wallet"></i> &nbsp;Tabungan Awal
									</button>
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                                        <i class="fas fa-wallet"></i> &nbsp;
                                        Ambil Tabungan
                                    </button>
                                </div>
                            </div>
                            <div class="card-body scroll-x">
                                <table class="table table-bordered">
                                    <thead class="bg-info">
                                        <th>No</th>
                                        <th>Tgl Transaksi</th>
                                        <th>Keterangan</th>
                                        <th>Masuk</th>
                                        <th>Keluar</th>
                                        <th width="10%">Hapus</th>
                                    </thead>
                                    <tbody id="display-helper-saving-data">
                                        <tr>
                                            <td class="text-center" colspan="5">Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="mt-4">
									<div class="alert alert-default-warning">
										Data yang bisa dihapus hanya data Tabungan Awal
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Daftar Perjalanan</h3>

                                <div class="card-tools">
                                    <a href="{{ route('travel.helper.all-trnsct-page', $helper->helper_id) }}"
                                        class="text-gray mr-3">
                                        <u>Lihat Selengkapnya</u>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body scroll-x">
                                <table class="table table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th>No</th>
                                        <th>Kode Booking</th>
                                        <th>Tujuan</th>
                                        <th>Tgl Berangkat</th>
                                    </thead>
                                    <tbody id="display-history-trnsct">
                                        <tr>
                                            <td colspan="4" class="text-center">Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
		</div>
	</div>
</section>

{{-- parameter data for load driver saving --}}
<input type="hidden" id="user-type" value="HELPER">
<input type="hidden" id="saving-column" value="user_id">
<input type="hidden" id="trnsct-column" value="helper_id">
<input type="hidden" id="value" value="{{ $helper->helper_id }}">

@include('travel.RefHelper.formDeleteSaving')
@include('travel.RefHelper.formInitialSaving')
@include('travel.RefHelper.savingsRetrievalForm')
@include('travel.RefHelper.asset.js')
@endsection
