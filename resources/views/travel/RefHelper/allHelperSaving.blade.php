@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.helper.detail', $helper->helper_id) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Detail Helper
            </a>
        </li>
        <li class="breadcrumb-item active">Data Tabungan Helper</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        Jumlah Tabungan <b>{{ $helper->name }}</b> : <b>Rp. {{ GeneralHelper::rupiah($helper->ttl_saving) }}</b>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Data Tabungan helper {{ $helper->name }}
                        </h3>

                        <div class="card-tools">
                            <button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#myModal">
                                <i class="fas fa-edit"></i> &nbsp;
                                Ambil Tabungan
                            </button>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered" id="datatable-helper-saving-page">
                            <thead class="bg-info">
                                <th>Tgl Transaksi</th>
                                <th>Keterangan</th>
                                <th>Masuk</th>
                                <th>Keluar</th>
                                <th width="15%">Hapus</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- parameter data for load helper saving --}}
<input type="hidden" id="helper-id-for-saving-page" value="{{ $helper->helper_id }}">

@include('travel.RefHelper.formDeleteSaving')
@include('travel.RefHelper.savingsRetrievalForm')
@include('travel.RefHelper.asset.js')
@endsection
