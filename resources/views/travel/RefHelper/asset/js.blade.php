<script>
$(function() {
    loadHelperData();
    loadFiveHelperSavingData();
    loadAllHelperSavingData();
    loadHistoryTrnsct();
    datatableHistoryPage();
    $('.money').keyup(function() {
        var amount = this.value;
        var ttlSaving = $('input[name="ttl_saving"]').val();
        amount = parseFloat(amount.replace(/\./g, ''));
        if (amount > ttlSaving) {
            $('.alert-withdrawal-over').css("display", "block");
            $('.btn-saving').attr("disabled", true);
        } else {
            $('.alert-withdrawal-over').css("display", "none");
            $('.btn-saving').attr("disabled", false);
        }
    });
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan");
            },
            success: function (data) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }

                toastr.success(data)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });

    $('form[name="saving_retreival"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }
                toastr.success(response);
                $('#myModal').modal('hide');

                // load car service data on detail page
                loadFiveHelperSavingData();
                $('#datatable-Helper-saving-page').ajax.reload();
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });

        document.documentElement.scrollTop = 0;
    });

    $('form[name="initial_saving"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan");
            },
            success: function (response) {
                if (response.status == 'success') {
                    toastr.success(response.message);
                    $('.modal').modal('hide');

                    // load car service data on detail page
                    location.reload();
                } else {
                    toastr.error(response.message);
                }
                
            },
            error: function (data) {
                var res = errorAlert(data);
                toastr.error(res);
            }
        });
    });

    $('form[name="delete_initial_saving"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Hapus");
            },
            success: function (response) {
                if (response.status == 'success') {
                    toastr.success(response.message);
                    $('.modal').modal('hide');

                    location.reload();    
                } else {
                    toastr.error(response.message);
                }
                
            },
            error: function (data) {
                var res = errorAlert(data);
                toastr.error(res);
            }
        });
    });
})();

function loadHelperData() {
    var base = {!! json_encode(route('travel.helper.jsondata')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'name', name: 'name', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'address', name: 'address', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function loadAllHelperSavingData() {
    var base = {!! json_encode(route('travel.helper.getallhelpersavingdata')) !!};
    var helperId =  $('#helper-id-for-saving-page').val();
    $('#datatable-helper-saving-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url": base,
            "type": "POST",
            "data": {
                "helper_id" : helperId,
            }
        },
        columns: [
            { data: 'trnsct_date', name: 'trnsct_date', searchable: true },
            { data: 'information', name: 'information', searchable: true },
            { data: 'in', name: 'in', searchable: false },
            { data: 'out', name: 'out', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function loadFiveHelperSavingData() {
    var userType = $('#user-type').val();
    var column = $('#saving-column').val();
    var value = $('#value').val();
    var url = {!! json_encode(route('travel.driver.getfivedriversavingdata')) !!};

    $.ajax({
        type: 'POST',
        url: url,
        data: column+'='+value+'&user_type='+userType,
        dataType:'html',
        success: function (response) {
            $('#display-helper-saving-data').html(response);
        }
    });
}

function loadHistoryTrnsct() {
    var column = $('#trnsct-column').val();
    var value = $('#value').val();
    var url = {!! json_encode(route('travel.kendaraan.get-five-car-history')) !!};
    $.ajax({
        type: 'POST',
        url: url,
        data: column+'='+value,
        dataType:'html',
        success: function (response) {
            $('#display-history-trnsct').html(response);
        }
    });
}

function datatableHistoryPage() {
    var baseUrl = {!! json_encode(route('travel.driver.get-all-driver-history-data-json')) !!};
    var driverId = $('#driver-id-for-history-page').val();
    $('#datatable-history-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url": baseUrl,
            "type": "POST",
            "data": {
                "driver_id" : driverId,
            }
        },
        columns: [
            { data: 'booking_id', name: 'booking_id', searchable: true },
            { data: 'booking.destination', name: 'booking.destination', searchable: true },
            { data: 'booking_date', name: 'booking_date', searchable: true },
        ]
    });
}

function showFormDeleteSaving(id) {
    var form = $('#form-delete-saving').modal('show');
    form.find('input[name="id"]').val(id)
}
</script>
