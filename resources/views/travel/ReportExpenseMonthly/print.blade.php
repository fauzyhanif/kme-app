<!DOCTYPE html>
<html>

    <head>
        <title>Laporan Pengeluaran Bulanan</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('Component.cssPrint')
    </head>

    <body>
        <div class="container-fluid">
            @include('Component.kopSurat')

            <hr style="margin-top: -10px">

            <div class="row text-center">
                <div class="col-md-12">
                    <p class="font-weight-bold" style="font-size: 13px">
                        LAPORAN PENGELUARAN <br>
                        BULAN {{ strtoupper(GeneralHelper::month($month) . ' ' . $year) }}
                    </p>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="font-weight-bold small-body text-center" width="5%">No</td>
                                <td class="font-weight-bold small-body" width="12%">No Bukti</td>
                                <td class="font-weight-bold small-body" width="15%">Tanggal</td>
                                <td class="font-weight-bold small-body">Keterangan</td>
                                <td class="font-weight-bold small-body text-right" width="20%">Jumlah</td>
                            </tr>
                            @php $no = 1 @endphp
                            @php $total = 0 @endphp
                            @php $ttl_upah_kerja = 0 @endphp
                            @foreach ($expenses as $expense)
                                @if ($expense->debit != '0')
                                    @if ($expense->account_id != '8-004')
                                        <tr>
                                            <td class="small-body text-center">{{ $no }}.</td>
                                            <td class="small-body">{{ substr($expense->proof_id,9) }}</td>
                                            <td class="small-body">{{ date("d-m-Y", strtotime($expense->trnsct_date)) }}</td>
                                            <td class="small-body">{{ $expense->description }}</td>
                                            <td class="small-body text-right">{{ GeneralHelper::rupiah($expense->debit) }}</td>
                                        </tr>
                                        @php $no += 1 @endphp
                                    @else
                                        @php $ttl_upah_kerja += $expense->debit @endphp
                                    @endif
                                @endif

                                @php $total += $expense->debit @endphp
                            @endforeach

                            @php
                                $time = strtotime("$year-$month-01");
                                $newformat = date('Y-m-d',$time);

                                $datestring = $newformat . ' first day of last month';
                                $dt = date_create($datestring);
                                $theMonth = $dt->format('Y-m');
                            @endphp

                            <tr>
                                <td class="small-body text-center">{{ $no }}.</td>
                                <td class="small-body">-</td>
                                <td class="small-body">{{ $month . '-' . $year }}</td>
                                <td class="small-body">Gaji Karyawan Bulan {{ GeneralHelper::konversiTgl($theMonth, 'my') }}</td>
                                <td class="small-body text-right">{{ GeneralHelper::rupiah($ttl_upah_kerja) }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="font-weight-bold text-center small-body">Total</td>
                                <td class="font-weight-bold small-body text-right">Rp. {{ GeneralHelper::rupiah($total) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                    Petugas,

                    <br>
                    <br>
                    <br>
                    <p>(Hj. Nani Inayah)</p>
                </div>
            </div>

        </div>
    </body>
</html>
