@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Pengeluaran Bulanan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header with-border">
                        <form action="{{ route('travel.report.expense_monthly') }}" method="GET">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <select name="year" class="form-control">
                                        @php
                                            $year_start = '2021';
                                            $year_end = date('Y');
                                        @endphp
                                        @for ($i = $year_end; $i >= $year_start; $i--)
                                            <option {{ ($year == $i) ? 'selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="month" class="form-control">
                                        <option value="01" {{ ($month == '01') ? 'selected' : '' }}>Januari</option>
                                        <option value="02" {{ ($month == '02') ? 'selected' : '' }}>Februari</option>
                                        <option value="03" {{ ($month == '03') ? 'selected' : '' }}>Maret</option>
                                        <option value="04" {{ ($month == '04') ? 'selected' : '' }}>April</option>
                                        <option value="05" {{ ($month == '05') ? 'selected' : '' }}>Mei</option>
                                        <option value="06" {{ ($month == '06') ? 'selected' : '' }}>Juni</option>
                                        <option value="07" {{ ($month == '07') ? 'selected' : '' }}>Juli</option>
                                        <option value="08" {{ ($month == '08') ? 'selected' : '' }}>Agustus</option>
                                        <option value="09" {{ ($month == '09') ? 'selected' : '' }}>September</option>
                                        <option value="10" {{ ($month == '10') ? 'selected' : '' }}>Oktober</option>
                                        <option value="11" {{ ($month == '11') ? 'selected' : '' }}>November</option>
                                        <option value="12" {{ ($month == '12') ? 'selected' : '' }}>Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">Tampilkan</button>
                                    <button type="button" class="btn btn-warning" onclick="print()">Cetak</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th width="5%" class="text-center">No</th>
                                <th width="15%">Tanggal</th>
                                <th width="15%">No Bukti</th>
                                <th>Uraian</th>
                                <th width="15%" class="text-right">Jumlah</th>
                            </thead>
                            <tbody>
                            @php $no = 1 @endphp
                            @php $ttl_all = 0 @endphp
                            @php $ttl_upah_kerja = 0 @endphp
                            @foreach ($expenses as $expense)
                                @if ($expense->debit != '0')
                                    @if ($expense->account_id != '8-004')
                                        <tr>
                                            <td class="text-center">{{ $no }}.</td>
                                            <td>{{ date("d-m-Y", strtotime($expense->trnsct_date)) }}</td>
                                            <td>{{ substr($expense->proof_id,9) }}</td>
                                            <td>{{ $expense->description }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($expense->debit) }}</td>
                                        </tr>

                                        @php $no += 1 @endphp
                                    @else
                                        @php $ttl_upah_kerja += $expense->debit @endphp
                                    @endif
                                @endif

                                @php $ttl_all += $expense->debit @endphp
                            @endforeach

                            @php
                                $time = strtotime("$year-$month-01");
                                $newformat = date('Y-m-d',$time);

                                $datestring = $newformat . ' first day of last month';
                                $dt = date_create($datestring);
                                $theMonth = $dt->format('Y-m');
                            @endphp

                                <tr>
                                    <td class="text-center">{{ $no }}</td>
                                    <td>{{ $month . '-' . $year }}</td>
                                    <td>-</td>
                                    <td>Gaji Karyawan Bulan {{ GeneralHelper::konversiTgl($theMonth, 'my') }}</td>
                                    <td class="text-right">{{ GeneralHelper::rupiah($ttl_upah_kerja) }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center" colspan="3">Total</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($ttl_all) }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.ReportExpenseMonthly.asset.js')
@endsection
