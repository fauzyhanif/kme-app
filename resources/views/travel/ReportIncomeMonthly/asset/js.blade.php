<script>
function print() {
    var year = $('select[name="year"]').val();
    var month = $('select[name="month"]').val();
    var url = "{{ url('travel/report/income_monthly/print?year=') }}" + year + '&month=' + month;
    window.open(url, '_blank');
}
</script>
