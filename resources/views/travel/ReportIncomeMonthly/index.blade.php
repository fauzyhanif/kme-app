@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Pemasukan Bulanan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header with-border">
                        <form action="{{ route('travel.report.income_monthly') }}" method="GET">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <select name="year" class="form-control">
                                        @php
                                            $year_start = '2021';
                                            $year_end = date('Y');
                                        @endphp
                                        @for ($i = $year_end; $i >= $year_start; $i--)
                                            <option {{ ($year == $i) ? 'selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="month" class="form-control">
                                        <option value="01" {{ ($month == '01') ? 'selected' : '' }}>Januari</option>
                                        <option value="02" {{ ($month == '02') ? 'selected' : '' }}>Februari</option>
                                        <option value="03" {{ ($month == '03') ? 'selected' : '' }}>Maret</option>
                                        <option value="04" {{ ($month == '04') ? 'selected' : '' }}>April</option>
                                        <option value="05" {{ ($month == '05') ? 'selected' : '' }}>Mei</option>
                                        <option value="06" {{ ($month == '06') ? 'selected' : '' }}>Juni</option>
                                        <option value="07" {{ ($month == '07') ? 'selected' : '' }}>Juli</option>
                                        <option value="08" {{ ($month == '08') ? 'selected' : '' }}>Agustus</option>
                                        <option value="09" {{ ($month == '09') ? 'selected' : '' }}>September</option>
                                        <option value="10" {{ ($month == '10') ? 'selected' : '' }}>Oktober</option>
                                        <option value="11" {{ ($month == '11') ? 'selected' : '' }}>November</option>
                                        <option value="12" {{ ($month == '12') ? 'selected' : '' }}>Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">Tampilkan</button>
                                    <button type="button" class="btn btn-warning" onclick="print()">Cetak</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th width="15%">Tanggal</th>
                                <th width="15%">No Bukti</th>
                                <th>Uraian</th>
                                <th width="15%" class="text-right">Jumlah</th>
                            </thead>
                            <tbody>
                            @php $no = 1 @endphp
                            @php $total = 0 @endphp
                            @foreach ($car_category_income as $income)
                                @if ($income['amount'] != '0')
                                <tr>
                                    <td>{{ $month.'/'.$year }}</td>
                                    <td>{{ $no }}</td>
                                    <td>Pendapatan {{ $income['name'] }}</td>
                                    <td class="text-right">{{ GeneralHelper::rupiah($income['amount']) }}</td>
                                </tr>
                                @php $no += 1 @endphp
                                @php $total += $income['amount'] @endphp
                                @endif
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center" colspan="3">Total</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($total) }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.ReportIncomeMonthly.asset.js')
@endsection
