<!DOCTYPE html>
<html>

    <head>
        <title>Laporan Pemasukan Bulanan</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('Component.cssPrint')
    </head>

    <body>
        <div class="container-fluid">
            @include('Component.kopSurat')

            <hr style="margin-top: -10px">

            <div class="row text-center">
                <div class="col-md-12">
                    <p class="font-weight-bold" style="font-size: 13px">
                        LAPORAN PEMASUKAN <br>
                        BULAN {{ strtoupper(GeneralHelper::month($month) . ' ' . $year) }}
                    </p>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="font-weight-bold small-body" width="15%">Tanggal</td>
                                <td class="font-weight-bold small-body" width="12%">No Bukti</td>
                                <td class="font-weight-bold small-body">Uraian</td>
                                <td class="font-weight-bold small-body text-right" width="20%">Jumlah</td>
                            </tr>
                            @php $no = 1 @endphp
                            @php $total = 0 @endphp
                            @foreach ($car_category_income as $income)
                                @if ($income['amount'] != '0')
                                <tr>
                                    <td class="small-body">{{ $month . '/' . $year }}</td>
                                    <td class="small-body">{{ $no }}</td>
                                    <td class="small-body">Pendapatan {{ $income['name'] }}</td>
                                    <td class="small-body text-right">{{ GeneralHelper::rupiah($income['amount']) }}</td>
                                </tr>
                                @endif
                            @php $no += 1 @endphp
                            @php $total += $income['amount'] @endphp
                            @endforeach
                            <tr>
                                <td colspan="3" class="font-weight-bold text-center small-body">Total</td>
                                <td class="font-weight-bold small-body text-right">Rp. {{ GeneralHelper::rupiah($total) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                    Petugas,

                    <br>
                    <br>
                    <br>
                    <p>(Hj. Nani Inayah)</p>
                </div>
            </div>

        </div>
    </body>
</html>
