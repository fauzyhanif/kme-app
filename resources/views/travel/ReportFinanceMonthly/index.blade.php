@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Keuangan Bulanan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header with-border">
                        <form action="{{ route('travel.report.finance_monthly') }}" method="GET">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <select name="year" class="form-control">
                                        @php
                                            $year_start = '2021';
                                            $year_end = date('Y');
                                        @endphp
                                        @for ($i = $year_end; $i >= $year_start; $i--)
                                            <option {{ ($year == $i) ? 'selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="month" class="form-control">
                                        <option value="01" {{ ($month == '01') ? 'selected' : '' }}>Januari</option>
                                        <option value="02" {{ ($month == '02') ? 'selected' : '' }}>Februari</option>
                                        <option value="03" {{ ($month == '03') ? 'selected' : '' }}>Maret</option>
                                        <option value="04" {{ ($month == '04') ? 'selected' : '' }}>April</option>
                                        <option value="05" {{ ($month == '05') ? 'selected' : '' }}>Mei</option>
                                        <option value="06" {{ ($month == '06') ? 'selected' : '' }}>Juni</option>
                                        <option value="07" {{ ($month == '07') ? 'selected' : '' }}>Juli</option>
                                        <option value="08" {{ ($month == '08') ? 'selected' : '' }}>Agustus</option>
                                        <option value="09" {{ ($month == '09') ? 'selected' : '' }}>September</option>
                                        <option value="10" {{ ($month == '10') ? 'selected' : '' }}>Oktober</option>
                                        <option value="11" {{ ($month == '11') ? 'selected' : '' }}>November</option>
                                        <option value="12" {{ ($month == '12') ? 'selected' : '' }}>Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">Tampilkan</button>
                                    <button type="button" class="btn btn-warning" onclick="print()">Cetak</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-12">
                                <h4 class="font-weight-bold">Rincian Keuangan</h4>
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <td width="5%" class="text-center font-weight-bold">A.</td>
                                            <td colspan="5" class="font-weight-bold">PENDAPATAN</td>
                                        </tr>

                                        @php $no = 1; @endphp
                                        @php $car_category_income_total = 0; @endphp
                                        @foreach ($car_category_income as $item)
                                            @if ($item['amount'] != '0')
                                                <tr>
                                                    <td width="5%" class="text-center">{{ $no }}.</td>
                                                    <td>{{ $item['name'] }}</td>
                                                    <td width="5%" class="text-right">Rp</td>
                                                    <td width="25%" class="text-right">{{ GeneralHelper::rupiah($item['amount']) }}</td>
                                                    <td width="5%"></td>
                                                    <td width="15%"></td>
                                                </tr>
                                            @endif
                                        @php $no += 1; @endphp
                                        @php $car_category_income_total += $item['amount']; @endphp
                                        @endforeach

                                        <tr>
                                            <td width="5%" class="text-center"></td>
                                            <td class="font-weight-bold">Jumlah</td>
                                            <td width="5%" class="text-right" style="border-top: 1px solid black">Rp</td>
                                            <td width="25%" class="text-right" style="border-top: 1px solid black">{{ GeneralHelper::rupiah($car_category_income_total) }}</td>
                                            <td width="5%"></td>
                                            <td width="15%"></td>
                                        </tr>

                                        <tr>
                                            <td width="5%" class="text-center font-weight-bold">B.</td>
                                            <td colspan="5" class="font-weight-bold">PENGELUARAN</td>
                                        </tr>
                                        <tr>
                                            <td width="5%" class="text-center">1.</td>
                                            <td>Nota Pengeluaran</td>
                                            <td width="5%" class="text-right">Rp</td>
                                            <td width="25%" class="text-right">{{ GeneralHelper::rupiah($expenses) }}</td>
                                            <td width="5%"></td>
                                            <td width="15%"></td>
                                        </tr>
                                        <tr>
                                            <td width="5%" class="text-center"></td>
                                            <td class="font-weight-bold">Jumlah Pengeluaran Bulan {{ GeneralHelper::month($month) }} Tahun {{ $year }}</td>
                                            <td width="5%" class="text-right" style="border-top: 1px solid black">Rp</td>
                                            <td width="25%" class="text-right" style="border-top: 1px solid black">{{ GeneralHelper::rupiah($expenses) }}</td>
                                            <td width="5%"></td>
                                            <td width="15%"></td>
                                        </tr>

                                        <tr>
                                            <td width="5%" class="text-center font-weight-bold">C.</td>
                                            <td colspan="3" class="font-weight-bold">JUMLAH PENDAPATAN BULAN {{ strtoupper(GeneralHelper::month($month)) }} TAHUN {{ $year }}</td>
                                            <td width="5%" class="text-right font-weight-bold">Rp</td>
                                            <td width="15%" class="text-right font-weight-bold">{{ GeneralHelper::rupiah($car_category_income_total - $expenses) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-lg-12">
                                <h4 class="font-weight-bold">Rincian Saldo</h4>

                                <table class="table table-sm table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th width="5%" class="text-center">No</th>
                                        <th width="20%">Nama Kas</th>
                                        <th width="25%" class="text-right">Saldo Bulan Lalu</th>
                                        <th width="25%" class="text-right">Uang Masuk Bulan {{ GeneralHelper::month($month) }}</th>
                                        <th width="25%" class="text-right">Jumlah</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td>Kas Kantor</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($saldo_kantor) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($incomes_kantor - $expenses_kantor) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($saldo_kantor + ($incomes_kantor - $expenses_kantor)) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">2</td>
                                            <td>Bank BRI</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($saldo_bri) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($incomes_bri - $expenses_bri) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($saldo_bri + ($incomes_bri - $expenses_bri)) }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <th class="text-right" colspan="2">Total</th>
                                        <th class="text-right">{{ GeneralHelper::rupiah($saldo_kantor + $saldo_bri) }}</th>
                                        <th class="text-right">{{ GeneralHelper::rupiah(($incomes_bri - $expenses_bri) + ($incomes_kantor - $expenses_kantor)) }}</th>
                                        <th class="text-right">{{ GeneralHelper::rupiah(($saldo_kantor + ($incomes_kantor - $expenses_kantor)) + ($saldo_bri + ($incomes_bri - $expenses_bri))) }}</th>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.ReportFinanceMonthly.asset.js')
@endsection
