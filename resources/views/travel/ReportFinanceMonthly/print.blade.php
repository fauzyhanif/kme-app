<!DOCTYPE html>
<html>

    <head>
        <title>Laporan Keuangan Bulanan</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('Component.cssPrint')
    </head>

    <body>
        <div class="container-fluid">
            @include('Component.kopSurat')

            <hr style="margin-top: -10px">

            <div class="row text-center">
                <div class="col-md-12">
                    <p class="font-weight-bold" style="font-size: 13px">
                        LAPORAN KEUANGAN <br>
                        BULAN {{ strtoupper(GeneralHelper::month($month) . ' ' . $year) }}
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <span class="font-weight-bold" style="font-size: 13px">RINGKASAN KEUANGAN</span>
                    <table class="table table-borderless table-sm">
                        <tbody>
                            <tr>
                                <td width="5%" class="text-center font-weight-bold">A.</td>
                                <td colspan="4" class="font-weight-bold">PENDAPATAN</td>
                            </tr>
                            @php $no = 1; @endphp
                            @php $car_category_income_total = 0; @endphp
                            @foreach ($car_category_income as $item)
                                @if ($item['amount'] != '0')
                                    <tr>
                                        <td></td>
                                        <td width="5%" class="text-center">{{ $no }}.</td>
                                        <td>{{ $item['name'] }}</td>
                                        <td width="5%" class="text-right">Rp</td>
                                        <td width="25%" class="text-right">{{ GeneralHelper::rupiah($item['amount']) }}</td>
                                    </tr>
                                @endif
                            @php $no += 1; @endphp
                            @php $car_category_income_total += $item['amount']; @endphp
                            @endforeach

                            <tr>
                                <td></td>
                                <td colspan="2" class="font-weight-bold">Jumlah</td>
                                <td width="5%" class="text-right" style="border-top: 1px solid black">Rp</td>
                                <td width="25%" class="text-right" style="border-top: 1px solid black">{{ GeneralHelper::rupiah($car_category_income_total) }}</td>
                            </tr>

                            <tr>
                                <td width="5%" class="text-center font-weight-bold">B.</td>
                                <td colspan="4" class="font-weight-bold">PENGELUARAN</td>
                            </tr>

                            <tr>
                                <td></td>
                                <td width="5%" class="text-center">1.</td>
                                <td>Nota Pengeluaran</td>
                                <td width="5%" class="text-right">Rp</td>
                                <td width="25%" class="text-right">{{ GeneralHelper::rupiah($expenses) }}</td>
                            </tr>
                            <tr>
                                <td width="5%" class="text-center"></td>
                                <td colspan="2" class="font-weight-bold">Jumlah Pengeluaran Bulan {{ GeneralHelper::month($month) }} Tahun {{ $year }}</td>
                                <td width="5%" class="text-right" style="border-top: 1px solid black">Rp</td>
                                <td width="25%" class="text-right" style="border-top: 1px solid black">{{ GeneralHelper::rupiah($expenses) }}</td>
                            </tr>

                            <tr>
                                <td width="5%" class="text-center font-weight-bold">C.</td>
                                <td colspan="2" class="font-weight-bold">JUMLAH PENDAPATAN BULAN {{ strtoupper(GeneralHelper::month($month)) }}TAHUN {{ $year }}</td>
                                <td width="5%" class="text-right font-weight-bold">Rp</td>
                                <td width="15%" class="text-right font-weight-bold">{{ GeneralHelper::rupiah($car_category_income_total - $expenses) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" style="margin-top: 50px !important;">
                <div class="col-lg-12">
                    <span class="font-weight-bold" style="font-size: 13px">RINCIAN SALDO</span>

                    <table class="table table-bordered table-sm">
                        <tbody>
                            <tr>
                                <th width="5%" class="text-center small-body">No</th>
                                <th width="20%" class="small-body">Nama Kas</th>
                                <th width="25%" class="text-right small-body">Saldo Bulan Lalu</th>
                                <th width="25%" class="text-right small-body">Uang Masuk Bulan {{ GeneralHelper::month($month) }}</th>
                                <th width="25%" class="text-right small-body">Jumlah</th>
                            </tr>
                            <tr>
                                <td class="text-center small-body">1</td>
                                <td class="small-body">Kas Kantor</td>
                                <td class="text-right small-body">{{ GeneralHelper::rupiah($saldo_kantor) }}</td>
                                <td class="text-right small-body">{{ GeneralHelper::rupiah($incomes_kantor - $expenses_kantor) }}</td>
                                <td class="text-right small-body">{{ GeneralHelper::rupiah($saldo_kantor + ($incomes_kantor - $expenses_kantor)) }}</td>
                            </tr>
                            <tr>
                                <td class="text-center small-body">2</td>
                                <td class="small-body">Bank BRI</td>
                                <td class="text-right small-body">{{ GeneralHelper::rupiah($saldo_bri) }}</td>
                                <td class="text-right small-body">{{ GeneralHelper::rupiah($incomes_bri - $expenses_bri) }}</td>
                                <td class="text-right small-body">{{ GeneralHelper::rupiah($saldo_bri + ($incomes_bri - $expenses_bri)) }}</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <th class="text-right small-body" colspan="2">Total</th>
                            <th class="text-right small-body">{{ GeneralHelper::rupiah($saldo_kantor + $saldo_bri) }}</th>
                            <th class="text-right small-body">
                                {{ GeneralHelper::rupiah(($incomes_bri - $expenses_bri) + ($incomes_kantor - $expenses_kantor)) }}
                            </th>
                            <th class="text-right small-body">
                                {{ GeneralHelper::rupiah(($saldo_kantor + ($incomes_kantor - $expenses_kantor)) + ($saldo_bri + ($incomes_bri - $expenses_bri))) }}
                            </th>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                    Petugas,

                    <br>
                    <br>
                    <br>
                    <p>(Hj. Nani Inayah)</p>
                </div>
            </div>

        </div>
    </body>
</html>
