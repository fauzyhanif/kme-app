<script>
$(document).ready(function () {
    var currentTime = new Date();
    // First Date Of the month
    var startDateFrom = '{{ $firstDay }}';
    // var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
    // Last Date Of the Month
    var startDateTo = '{{ $lastDay }}';
    // var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
    $('#reservation').daterangepicker({
        startDate: startDateFrom,
        endDate: startDateTo,
        locale : {
           format : 'YYYY-MM-DD'
        }
    })
});
</script>
