@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Laba Rugi</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card shadow-none">
                    <form action="{{ route('travel.laba-rugi.print') }}" method="POST" target="_blank">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label>Periode</label>
                                <input type="text" name="date" class="form-control" id="reservation">
                            <div>

                            <hr>

                            <div class="form-group">
                                <button type="submit" class="btn btn-sm btn-primary btn-block">
                                    Tampilkan Data
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.ReportLabaRugi.asset.js')
@endsection
