<!DOCTYPE html>
<html>

    <head>
        <title>Slip Pemasukan</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                font-size: 10px;
            }

            .borderless {
                border-collapse: separate;
                border-spacing: 0 -15px;
                margin-top: 10px;
            }

            .borderless td,
            .borderless th {
                border: none;
            }

            .data-title {
                font-size: 16px;
            }

            tr td.except,
            th.except {
                padding-top: 2 !important;
                padding-bottom: 2 !important;
                margin: 0 !important;
            }
        </style>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row kop-surat">
                <div class="col-md-12 text-center">
                    <h3 class="text-bold">PT KARYA MAS EMPAT</h3>
                    <p>
                        Jl. Raya Rancaudik KM. 5 Tambakdahan 41253 Subang - Jawa Barat
                        <br>
                        Telp. (0260) 540153 - 552244 e-mail : karyamasempat@yahoo.co.id
                    </p>
                </div>
            </div>

            <hr>

            <div class="row text-center">
                <div class="col-md-12">
                    <h5 class="font-weight-bold">LAPORAN LABA/RUGI</h5>
                    <p>
                        Periode {{ GeneralHelper::konversiTgl($startDate, 'ttd') }}
                        s/d {{ GeneralHelper::konversiTgl($endDate, 'ttd') }}
                    </p>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                            {{-- PENDAPATAN --}}
                            <tr>
                                <td colspan="2" class="except">
                                    <b>PENDAPATAN</b>
                                </td>
                            </tr>
                            @php $ttl_main_income = 0; @endphp
                            @foreach ($mainIncomes as $mainIncome)
                                <tr>
                                    <td class="except">&nbsp;&nbsp;&nbsp;{{ $mainIncome->name }}</td>
                                    <td class="except text-right">{{ GeneralHelper::rupiah($mainIncome->ttl_credit) }}</td>
                                </tr>
                            @php $ttl_main_income += $mainIncome->ttl_credit; @endphp
                            @endforeach
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;<b>Total Pendapatan</b></td>
                                <td class="except text-right"><b>{{ GeneralHelper::rupiah($ttl_main_income) }}</b></td>
                            </tr>
                            {{-- PENDAPATAN --}}

                            <tr>
                                <td colspan="2"></td>
                            </tr>

                            {{-- HPP --}}
                            <tr>
                                <td colspan="2" class="except">
                                    <b>HARGA POKOK PENJUALAN</b>
                                </td>
                            </tr>
                            @php $ttl_hpp = 0; @endphp
                            @foreach ($capitals as $capital)
                                <tr>
                                    <td class="except">&nbsp;&nbsp;&nbsp;{{ $capital->name }}</td>
                                    <td class="except text-right">{{ GeneralHelper::rupiah($capital->ttl_debit) }}</td>
                                </tr>
                            @php $ttl_hpp = $capital->ttl_debit; @endphp
                            @endforeach
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;<b>Total Harga Pokok Penjualan</b></td>
                                <td class="except text-right"><b>{{ GeneralHelper::rupiah($ttl_hpp) }}</b></td>
                            </tr>
                            {{-- HPP --}}

                            {{-- LABA KOTOR --}}
                            @php
                                $ttl_laba_kotor = $ttl_main_income - $ttl_hpp;
                            @endphp
                            <tr>
                                <td class="except"><b class="text-primary">Laba Kotor</b></td>
                                <td class="except text-right"><b class="text-primary">{{ GeneralHelper::rupiah($ttl_laba_kotor) }}</b></td>
                            </tr>
                            {{-- LABA KOTOR --}}

                            <tr>
                                <td colspan="2"></td>
                            </tr>

                            {{-- BIAYA --}}
                            <tr>
                                <td colspan="2" class="except">
                                    <b>BIAYA OPERASIONAL</b>
                                </td>
                            </tr>
                            @php $ttl_cost = 0; @endphp
                            @foreach ($costs as $cost)
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;{{ $cost->name }}</td>
                                <td class="except text-right">{{ GeneralHelper::rupiah($cost->ttl_debit) }}</td>
                            </tr>
                            @php $ttl_cost = $cost->ttl_debit; @endphp
                            @endforeach
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;<b>Total Biaya</b></td>
                                <td class="except text-right"><b>{{ GeneralHelper::rupiah($ttl_cost) }}</b></td>
                            </tr>
                            {{-- BIAYA --}}

                            {{-- PENDAPATAN BERSIH OPERASIONAL --}}
                            @php
                                $ttl_pendapatan_bersih_operasional = $ttl_laba_kotor - $ttl_cost;
                            @endphp
                            <tr>
                                <td class="except"><b class="text-primary">Pendapatan Bersih Operasional</b></td>
                                <td class="except text-right"><b class="text-primary">{{ GeneralHelper::rupiah($ttl_pendapatan_bersih_operasional) }}</b></td>
                            </tr>
                            {{-- PENDAPATAN BERSIH OPERASIONAL --}}

                            <tr>
                                <td colspan="2"></td>
                            </tr>

                            {{-- PENDAPATAN LAINNYA --}}
                            <tr>
                                <td colspan="2" class="except">
                                    <b>PENDAPATAN LAINNYA</b>
                                </td>
                            </tr>
                            @php $ttl_other_income = 0; @endphp
                            @foreach ($otherIncomes as $otherIncome)
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;{{ $otherIncome->name }}</td>
                                <td class="except text-right">{{ GeneralHelper::rupiah($otherIncome->ttl_credit) }}</td>
                            </tr>
                            @php $ttl_other_income = $otherIncome->ttl_credit; @endphp
                            @endforeach
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;<b>Total Pendapatan Lainnya</b></td>
                                <td class="except text-right"><b>{{ GeneralHelper::rupiah($ttl_other_income) }}</b></td>
                            </tr>
                            {{-- PENDAPATAN LAINNYA --}}

                            <tr>
                                <td colspan="2"></td>
                            </tr>

                            {{-- BIAYA LAINNYA --}}
                            <tr>
                                <td colspan="2" class="except">
                                    <b>BIAYA LAINNYA</b>
                                </td>
                            </tr>
                            @php $ttl_other_cost = 0; @endphp
                            @foreach ($otherCosts as $otherCost)
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;{{ $otherCost->name }}</td>
                                <td class="except text-right">{{ GeneralHelper::rupiah($otherCost->ttl_debit) }}</td>
                            </tr>
                            @php $ttl_other_cost = $otherCost->ttl_debit; @endphp
                            @endforeach
                            <tr>
                                <td class="except">&nbsp;&nbsp;&nbsp;<b>Total Biaya Lainnya</b></td>
                                <td class="except text-right"><b>{{ GeneralHelper::rupiah($ttl_other_cost) }}</b></td>
                            </tr>
                            {{-- BIAYA LAINNYA --}}

                            <tr>
                                <td colspan="2"></td>
                            </tr>

                            {{-- PENDAPATAN BERSIH  --}}
                            @php
                            $ttl_pendapatan_bersih = $ttl_pendapatan_bersih_operasional + $ttl_other_income;
                            $ttl_pendapatan_bersih = $ttl_pendapatan_bersih - $ttl_other_cost;
                            @endphp
                            <tr>
                                <td class="except"><b class="text-primary">Pendapatan Bersih</b></td>
                                <td class="except text-right"><b
                                        class="text-primary">{{ GeneralHelper::rupiah($ttl_pendapatan_bersih) }}</b></td>
                            </tr>
                            {{-- PENDAPATAN BERSIH  --}}


                        </tbody>

                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                    Petugas,

                    <br>
                    <br>
                    <br>
                    <p>(H. Yusuf)</p>
                </div>
            </div>

        </div>
    </body>
