<script>
$(function() {
    loadData();
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }

                toastr.success(data)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function loadData() {
    var base = {!! json_encode(route('travel.finance.account.jsondata')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'group.name', name: 'group.name', searchable: true },
            { data: 'account_id', name: 'account_id', searchable: true },
            { data: 'name', name: 'name', searchable: true },
            { data: 'post_report', name: 'post_report', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function showParentAccount(value) {
    var url = {!! json_encode(url('/travel/finance/account/get-parent')) !!}
    var base = url + '/' + value
    $.ajax({
        type: 'GET',
        url: base,
        dataType:'html',
        success: function (data) {
            $('.parent-form').html(data);
        }
    });
}

</script>
