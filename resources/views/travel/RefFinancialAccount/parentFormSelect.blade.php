<label>Akun Parent <span class="text-red">*</span></label>
<select name="parent" class="form-control">
    <option value="">-- Silahkan Pilih Akun Parent --</option>
    @foreach ($accounts as $account)
    <option value="{{ $account->account_id }}">
        {{ $account->account_id ." - ". $account->name }}
    </option>
    @endforeach
</select>
