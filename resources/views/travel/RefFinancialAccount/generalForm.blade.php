<div class="form-group">
    <label>Group Akun <span class="text-red">*</span></label>
    <select name="group_id" class="form-control" required onchange="showParentAccount(this.value)">
        <option value="">-- Silahkan Pilih Group Akun --</option>
        @foreach ($groups as $group)
            <option
                value="{{ $group->group_id }}"
                @isset($account)
                    {{ ($account->group_id == $group->group_id) ? 'selected' : '' }}
                @endisset
                >
                {{ $group->name }}
            </option>
        @endforeach
    </select>
</div>

<div class="form-group parent-form">
    <label>Akun Parent</label>
    @if (isset($account))
        <select name="parent" class="form-control">
            <option value="">-- Silahkan Pilih Akun Parent --</option>
            @foreach ($accounts as $account)
                <option
                    value="{{ $account->account_id }}"
                    {{ ($account->parent == $account->account_id) ? 'selected' : '' }}
                    >
                    {{ $account->account_id . " - " . $account->name }}
                </option>
            @endforeach
        </select>
    @else
        <select name="parent" class="form-control">
            <option value="">-- Silahkan Pilih Akun Parent --</option>
        </select>
    @endif
</div>

<div class="form-group">
    <label>Nomor Akun <span class="text-red">*</span></label>
    <input
        type="text"
        name="account_id"
        class="form-control"
        required
        @isset($account)
            readonly
            value="{{ $account->account_id }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Nama Akun <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($account)
            value="{{ $account->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Pos Laporan</label>
    <select name="post_report" class="form-control">
        <option
            @isset($account)
                @if ($account->post_report == 'NERACA')
                    selected
                @endif
            @endisset>
            NERACA
        </option>
        <option
            @isset($account)
                @if ($account->post_report == 'LABARUGI')
                    selected
                @endif
            @endisset>
            LABARUGI
        </option>
    </select>
</div>

<div class="form-group">
    <label>Sub Pos Laporan</label>
    <select name="sub_post_report" class="form-control">
        <option @isset($account) @if ($account->sub_post_report == 'KEWAJIBAN')
            selected
            @endif
            @endisset>
            KEWAJIBAN
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'MODAL')
            selected
            @endif
            @endisset>
            MODAL
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'PENDAPATAN')
            selected
            @endif
            @endisset>
            PENDAPATAN
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'HPP')
            selected
            @endif
            @endisset>
            HPP
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'BIAYA')
            selected
            @endif
            @endisset>
            BIAYA
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'PENDAPATAN_LAIN')
            selected
            @endif
            @endisset>
            PENDAPATAN_LAIN
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'BIAYA_LAIN')
            selected
            @endif
            @endisset>
            BIAYA_LAIN
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'AKTIVA_TETAP')
            selected
            @endif
            @endisset>
            AKTIVA_TETAP
        </option>
        <option @isset($account) @if ($account->sub_post_report == 'DEPRESIASI')
            selected
            @endif
            @endisset>
            DEPRESIASI
        </option>
    </select>
</div>

@isset($account)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($account)
                @if ($account->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($account)
                @if ($account->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
