@extends('index')

@section('content')
<section class="content-header">
    <h1>Akun</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Akun
                        <div class="card-tools">
                            <a href="{{ route('travel.finance.account.formadd') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Akun Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered datatable">
                            <thead>
                                <th width="15%">Group Akun</th>
                                <th width="15%">Nomor Akun</th>
                                <th>Nama Akun</th>
                                <th width="8%">Pos Laporan</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('travel.RefFinancialAccount.asset.js')
@endsection

