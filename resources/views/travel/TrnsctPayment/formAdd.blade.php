@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.payment') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pembayaran
            </a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ url('travel/booking/detail', $booking->booking_id) }}">
                Detail Booking
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Pembayaran Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Keterangan</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="30%">Kode Booking</td>
                                    <td>{{ $booking->booking_id }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Pelanggan</td>
                                    <td>{{ $booking->customer->name }}</td>
                                </tr>
                                <tr>
                                    <td>Tgl Booking</td>
                                    <td>
                                        {{ GeneralHelper::konversiTgl($booking->booking_start_date, 'slash') }}
                                        s/d
                                        {{ GeneralHelper::konversiTgl($booking->booking_end_date, 'slash') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tujuan</td>
                                    <td>{{ $booking->destination }}</td>
                                </tr>
                                <tr>
                                    <td>Jml Unit</td>
                                    <td>{{ $booking->ttl_unit }} Unit</td>
                                </tr>
                                <tr>
                                    <td>Jml Tagihan</td>
                                    <td>Rp. {{ GeneralHelper::rupiah($booking->ttl_trnsct) }}</td>
                                </tr>
                                <tr>
                                    <td>Telah Dibayar</td>
                                    <td>Rp. {{ GeneralHelper::rupiah($booking->ttl_trnsct_paid) }}</td>
                                </tr>
                                <tr>
                                    <td>Sisa Tagihan</td>
                                    <td>Rp. {{ GeneralHelper::rupiah($booking->ttl_trnsct - $booking->ttl_trnsct_paid) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            @if ($booking->ttl_trnsct == $booking->ttl_trnsct_paid)
                <div class="col-md-6">
                    <div class="border rounded my-2" style="background-color: #FFF9CA">
                        <div class="p-3">
                            <span class="font-weight-bold">Tagihan Sudah Lunas!</span>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-6">
                    <div class="card">
                        <form id="form-add" action="{{ route('travel.payment.addnew') }}" method="POST" data-remote>
                            @csrf
                            <input type="hidden" name="type" value="add">
                            <input type="hidden" name="booking_id" value="{{ $booking->booking_id }}">

                            <div class="card-header">
                                <h3 class="card-title">
                                    Form
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>
                                        Tanggal Bayar
                                        <span class="text-red">*</span>
                                    </label>
                                    <input
                                        type="date"
                                        name="payment_date"
                                        class="form-control"
                                        value="{{ date('Y-m-d') }}"
                                        required>
                                </div>

                                <div class="form-group">
                                    <label>
                                        Diterima Dari
                                        <span class="text-red">*</span>
                                    </label>
                                    <input
                                        type="text"
                                        name="received_from"
                                        class="form-control"
                                        value="{{ $booking->customer->name }}"
                                        required>
                                </div>

                                <div class="form-group">
                                    <label>
                                        Sebesar
                                        <span class="text-red">*</span>
                                    </label>
                                    <input
                                        type="text"
                                        name="amount"
                                        class="form-control money"
                                        value="{{ $booking->ttl_trnsct - $booking->ttl_trnsct_paid }}"
                                        required>
                                </div>

                                <div class="form-group">
                                    <label>
                                        Masuk Kas/Bank
                                        <span class="text-red">*</span>
                                    </label>
                                    <select name="payment_method" id="" class="form-control" required>
                                        <option value="">** Pilih Kas/Bank</option>
                                        @foreach ($cashAccount as $account)
                                            <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                                <button type="submit" class="btn btn-success">
                                    Simpan & Cetak
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                            <div class="card-body">
                                <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>

@include('travel.TrnsctPayment.asset.js')
@endsection
