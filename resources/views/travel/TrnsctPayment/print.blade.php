@php
    use Illuminate\Support\Facades\Session;
    $payment_information_array = explode(" ", $payment->information);
    $first_payment_information = (count($payment_information_array) > 0) ? $payment_information_array[0] : '';
    $last_payment_information = substr($payment->information, strpos($payment->information, 'Tgl'));

    $jenisUnit = "";
    foreach ($bookingDtl as $item) {
        $jenisUnit .= $item->carCategory->name . "($item->unit_qty), ";
    }
    
    $jenisUnit = rtrim($jenisUnit, ", ");
@endphp

<!DOCTYPE html>
<html>
    <head>
        <title>Kwitansi {{ $payment->payment_id }}</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('Component.cssPrint')
        <style>
            body {
                font-size: 12px
            }
        </style>
    </head>

    <body>
        <div class="container-fluid">
            @include('Component.kopSurat')

            <hr style="margin-top: -10px; border : 1px solid black">

            <div class="row text-center">
                <div class="col-md-12">
                    <h4 class="font-weight-bold">KWITANSI</h4>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-md-12">
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="25%" class="except">No Kwitansi</td>
                                <td width="75%" class="except">: <b>{{ $payment->payment_id }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Telah Diterima Dari</td>
                                <td width="75%" class="except">: <b>{{ $payment->booking->customer->name }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Sebesar</td>
                                <td width="75%" class="except">: <b>Rp. {{ GeneralHelper::rupiah($payment->amount) }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Terbilang</td>
                                <td width="75%" class="except">: <b class="font-italic">-</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Keterangan</td>
                                <td width="75%" class="except">: <b>{{ $first_payment_information . ' ' . $jenisUnit . ' ' . $last_payment_information }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Cara Pembayaran</td>
                                <td width="75%" class="except">: <b>{{ $payment->coa->name }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 pl-4">
                    <b class="pl-1">Untuk</b>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-12">
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="25%" class="except">Kode Booking</td>
                                <td width="75%" class="except">: <b>{{ $booking->booking_id }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Jumlah Unit</td>
                                <td width="75%" class="except">: <b>{{ $booking->ttl_unit }} Unit</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Jenis Unit</td>
                                <td width="75%" class="except">:
                                    <b>{{ $jenisUnit }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Tgl. Berangkat</td>
                                <td width="75%" class="except">: <b>{{ GeneralHelper::konversiTgl($booking->booking_start_date) }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Tujuan</td>
                                <td width="75%" class="except">: <b>{{ $booking->destination }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Lokasi Penjemputan</td>
                                <td width="75%" class="except">: <b>{{ $booking->pick_up_location }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%" class="except">Jam Penjemputan</td>
                                <td width="75%" class="except">: <b>{{ $booking->standby_time }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead class="bg-info text-white">
                            <tr>
                                <th class="small-head text-center" width="35%">Total Tagihan</th>
                                <th class="small-head text-center" width="33%">Sudah Dibayar</th>
                                <th class="small-head text-center" width="33%">Sisa Tagihan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="small-body">Rp. {{ GeneralHelper::rupiah($booking->ttl_trnsct) }}</td>
                                <td class="small-body">Rp. {{ GeneralHelper::rupiah($booking->ttl_trnsct_paid) }}</td>
                                <td class="small-body bg-warning">Rp.
                                    {{ GeneralHelper::rupiah($booking->ttl_trnsct - $booking->ttl_trnsct_paid) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead class="bg-info text-white">
                            <tr>
                                <th class="small-head text-center" width="5%">No</th>
                                <th class="small-head text-center" width="28%">No Kwitansi</th>
                                <th class="small-head text-center" width="33%">Tgl Pembayaran</th>
                                <th class="small-head text-center" width="33%">Nominal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($payments as $item)
                            <tr>
                                <td class="small-body">{{ $no++ }}.</td>
                                <td class="small-body">{{ $item->payment_id }}</td>
                                <td class="small-body">{{ GeneralHelper::konversiTgl($item->payment_date, 'slash') }}</td>
                                <td class="text-right small-body">Rp. {{ GeneralHelper::rupiah($item->amount) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" style="margin-top: -10px">
                <div class="col-md-12">
                    <ol>
                        <li>DP/Uang muka minimal 20% dari harga sewa bus.</li>
                        <li>Uang muka hangus bila pemesanan dibatalkan.</li>
                        <li>Pelunasan sewa maksimal H-3 dari tanggal keberangkatan.</li>
                        <li>Pembayaran melalui Bank BRI No. Rek. 0355-01-001297-56-6 a/n Cucup Yusuf.</li>
                        <li>Transfer ke <u>Selain</u> rekening tersebut diluar tanggung jawab kami.</li>
                        <li>Bukti kwitansi jangan sampai hilang.</li>
                    </ol>
                </div>
            </div>

            <div class="row" style="margin-top: -10px !important;">
                <div class="col-md-6 text-center" style="margin-left: -400px;">
                    <br>
                    Penyetor,

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <p>({{ $payment->received_from }})</p>
                </div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl($payment->payment_date, 'ttd') }} <br>
                    Petugas,

                    <br>
                    <img src="{{ url('public/img/cap.png') }}" style="max-width: 100px; padding-bottom: -20px !important; padding-left: -10px !important" alt="">
                    <img src="{{ url('public/img/ttd-yusuf.png') }}" style="max-width: 70px; padding-bottom: -20px !important; padding-left: -50px !important" alt="">
                    <br>
                    <p>({{ Session::get('auth_nama') }})</p>
                </div>
            </div>
        </div>
    </body>
<html>
