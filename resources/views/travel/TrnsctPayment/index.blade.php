@extends('index')

@section('content')
<section class="content-header">
    <h1>Daftar Pembayaran</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Pembayaran
                    </div>
                    <div class="card-body">
                        <div class="border rounded my-2" style="background-color: #FFF9CA">
                            <div class="p-3">
                                <span class="font-weight-bold">Panduan!</span> <br>
                                <span>
                                    Untuk input Pembayaran silahkan buka menu Daftar Booking > klik tombol Bayar di sebelah kanan data yang
                                    dituju
                                </span>
                            </div>
                        </div>

                        <table class="table table-bordered table-hover datatable-index-page">
                            <thead class="bg-info">
                                <th>Tgl Bayar</th>
                                <th>No Kwitansi</th>
                                <th>Dari</th>
                                <th>Nominal</th>
                                <th>Via</th>
                                <th width="10%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctPayment.asset.js')
@endsection
