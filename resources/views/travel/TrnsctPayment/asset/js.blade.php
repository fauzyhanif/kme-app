<script>
$(function() {
    datatableIndexPage();
});

function datatableIndexPage() {
    var baseIndex = {!! json_encode(route('travel.payment.jsondata')) !!};
    $('.datatable-index-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: baseIndex,
        columns: [
            { data: 'payment_date', name: 'payment_date', searchable: true },
            { data: 'payment_id', name: 'payment_id', searchable: true },
            { data: 'received_from', name: 'received_from', searchable: true },
            { data: 'amount', name: 'amount', searchable: true },
            { data: 'payment_method', name: 'payment_method', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }

                toastr.success(response.text)

                // redirect to detail
                var detailUrl = {!! json_encode(url('/travel/booking/detail')) !!};
                var detailUrl = detailUrl + "/" + response.booking_id;
                document.location.href = detailUrl;

                //this will redirect us in same window
                var base = {!! json_encode(url('/travel/payment/print')) !!};
                var urlReload = base + "/" + response.payment_id;
                window.open(urlReload, '_blank');
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
        document.documentElement.scrollTop = 0;
    });
})();

function searchBookingID(params) {

}
</script>
