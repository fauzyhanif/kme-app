@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Pendapatan Sewa Bus</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header with-border">
                        <form action="" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" name="date" class="form-control" id="reservation">
                                </div>
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                    <a
                                        href="{{ route('travel.report.pendapatan-sewa-bus.print') }}"
                                        class="btn btn-primary"
                                        target="_blank">
                                        Cetak
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th width="5%">No</th>
                                <th>Unit</th>
                                <th width="30%">Jumlah</th>
                            </thead>
                            <tbody>
                                @php $no = 1 @endphp
                                @php $total = 0 @endphp
                                @foreach ($reports as $report)
                                    <tr>
                                        <td>{{ $no }}.</td>
                                        <td>{{ $report->name }}</td>
                                        <td>{{ GeneralHelper::rupiah($report->total) }}</td>
                                    </tr>
                                @php $no += 1 @endphp
                                @php $total += $report->total @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th colspan="2" class="text-center">Total</th>
                                <th>{{ GeneralHelper::rupiah($total) }}</th>
                            </tfoot>
                        </table>

                        <div class="border rounded mt-3" style="background-color: #FFF9CA">
                            <div class="p-3">
                                <span class="font-weight-bold">Informasi!</span>
                                <span>Pendapatan sewa diatas belum termasuk potongan diskon booking dan kas jalan.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.ReportAgendaUnit.asset.js')
@endsection
