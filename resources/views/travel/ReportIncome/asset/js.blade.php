<script>
$(document).ready(function () {
    var currentTime = new Date();
    // First Date Of the month
    var startDateFrom = '{{ $firstDay }}';
    // Last Date Of the Month
    var startDateTo = '{{ $lastDay }}';
    $('#reservation').daterangepicker({
        startDate: startDateFrom,
        endDate: startDateTo,
        locale : {
           format : 'YYYY-MM-DD'
        }
    })
});
</script>
