<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Laporan Agenda Armada</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap 4 -->

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/dist/css/adminlte.min.css') }}">

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

        <style>
            .kop-surat{
                border-bottom: 2px solid black;
            }
        </style>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row kop-surat">
                <div class="col-md-12 text-center">
                    <h1 class="text-bold">PT KARYA MAS EMPAT</h1>
                    <p>
                        Jl. Raya Rancaudik KM. 5 Tambakdahan 41253 Subang - Jawa Barat
                        <br>
                        Telp. (0260) 540153 - 552244 e-mail : karyamasempat@yahoo.co.id
                    </p>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <h3 class="text-bold">Pendapatan Sewa Bus</h3>
                </div>
            </div>

            <div class="row mt-1">
                <div class="col-md-12">
                    <p class="text-bold">
                        Periode : {{ GeneralHelper::konversiTgl($firstDay) }} s/d {{ GeneralHelper::konversiTgl($lastDay) }}
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                            <th width="5%">No</th>
                            <th>Unit</th>
                            <th width="30%">Jumlah</th>
                        </thead>
                        <tbody>
                            @php $no = 1 @endphp
                            @php $total = 0 @endphp
                            @foreach ($reports as $report)
                            <tr>
                                <td>{{ $no }}.</td>
                                <td>{{ $report->name }}</td>
                                <td>{{ GeneralHelper::rupiah($report->total) }}</td>
                            </tr>
                            @php $no += 1 @endphp
                            @php $total += $report->total @endphp
                            @endforeach
                        </tbody>
                        <tfoot>
                            <th colspan="2" class="text-center">Total</th>
                            <th>{{ GeneralHelper::rupiah($total) }}</th>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center">Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }}</div>

                <div class="col-md-6 text-center"></div>
                <div class="col-md-6 text-center">Bag. Keuangan</div>
            </div>
            <br>
            <br>
            <div class="row mt-4">
                <div class="col-md-6 text-center"></div>
                <div class="col-md-6 text-center">YUSUP TAKHYUDIN, SE</div>
            </div>
        </div>
    </body>

</html>
