<div id="modal-pembatalan-angsuran-{{ $payment->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ url('travel/kasbon/pembatalan_pembayaran') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $payment->id }}">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Pembatalan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Yakin ingin batalkan angsuran kasbon ini?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Ya, Batalkan Sekarang!</button>
                </div>
            </form>

        </div>
    </div>
</div>
