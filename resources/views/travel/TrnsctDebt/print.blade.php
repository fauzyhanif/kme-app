<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Daftar Kasbon Karyawan</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('cssCustom.css_print')
        <style>
            @page {
                size: A4 potrait
            }

            body {
                letter-spacing: 1px;
            }
        </style>
    </head>
    <body class="A4 potrait" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                    <div class="row text-center">
                        <div class="col-lg-12">
                            <h5 class="font-weight-bold">BON PINAJAMAN KARYAWAN</h5>
                            <h3 class="font-weight-bold">PT. KARYAMAS EMPAT</h3>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-12">
                            <table class="table table-bordered table-sm" border="1">
                                <thead>
                                    <th width="5%" class="text-center">NO</th>
                                    <th width="20%" class="text-center">TANGGAL</th>
                                    <th class="text-center">NAMA</th>
                                    <th width="20%" class="text-center">NOMINAL</th>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                        $total = 0;
                                    @endphp
                                    @foreach ($debts as $debt)
                                        <tr>
                                            <td>{{ $no }}.</td>
                                            <td>{{ $debt->date }}</td>
                                            <td>{{ $debt->sdm->name }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($debt->amount) }}</td>
                                        </tr>
                                    @php
                                        $no += 1;
                                        $total += $debt->amount;
                                    @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th colspan="3" class="text-center">JUMLAH</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($total) }}</th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-center">
                            Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                            Bendahara,

                            <br>
                            <br>
                            <br>
                            <p>(Hj. Nani Inayah, SE)</p>
                        </div>
                    </div>

            </div>
        </section>
    </body>
