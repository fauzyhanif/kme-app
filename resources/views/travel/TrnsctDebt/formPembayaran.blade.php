@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('travel/kasbon') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Kasbon
            </a>
        </li>
        <li class="breadcrumb-item active">Form Pembayaran Kasbon</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Informasi Pinjaman</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td width="25%" class="text-secondary">Nama & Jabatan</td>
                                    <td width="5%" class="text-right">:</td>
                                    <td width="70%">{{ $debt->sdm->name . ' - ' . $debt->sdm->position->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Tgl Pinjaman</td>
                                    <td width="5%" class="text-right">:</td>
                                    <td width="70%">{{ GeneralHelper::konversiTgl($debt->date) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Jml Pinjaman</td>
                                    <td width="5%" class="text-right">:</td>
                                    <td width="70%">{{ GeneralHelper::rupiah($debt->amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Jml Bayar</td>
                                    <td width="5%" class="text-right">:</td>
                                    <td width="70%">{{ GeneralHelper::rupiah($debt->amount_paid) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Sisa Pinjaman</td>
                                    <td width="5%" class="text-right">:</td>
                                    <td width="70%">{{ GeneralHelper::rupiah($debt->amount - $debt->amount_paid) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Keterangan</td>
                                    <td width="5%" class="text-right">:</td>
                                    <td width="70%">{{ $debt->desc }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Sumber Kas</td>
                                    <td width="5%" class="text-right">:</td>
                                    <td width="70%">{{ $debt->account_id .' '. $debt->cashAccount->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if ($debt->payment->count() > 0)
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title m-0">Daftar Angsuran Pinjaman</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-sm table-bordered">
                                <thead class="bg-info">
                                    <th width="5%">No</th>
                                    <th width="20%">Tanggal Bayar</th>
                                    <th width="20%">Masuk Kas</th>
                                    <th width="20%">Nominal</th>
                                    <th>keterangan</th>
                                    <th width="15%">Batalkan</th>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($debt->payment as $payment)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ generalHelper::konversiTgl($payment->payment_date) }}</td>
                                            <td>{{ $payment->cashAccount->name }}</td>
                                            <td>{{ generalHelper::rupiah($payment->amount) }}</td>
                                            <td>
                                                {{ ($payment->payrol_payment_id != '') ? 'Angsuran dari potongan gaji ' . GeneralHelper::konversiTgl($payment->payrolPayment->payrol->year.'-'.$payment->payrolPayment->payrol->month.'-01', 'my') : '' }}
                                            </td>
                                            <td>
                                                @if ($payment->payrol_payment_id == '')
                                                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal"
                                                        data-target="#modal-pembatalan-angsuran-{{ $payment->id }}">
                                                        <i class="fas fa-trash"></i> Batalkan
                                                    </button>
                                                    @include('travel.TrnsctDebt.formPembatalanAngsruan')
                                                @endif
                                            </td>
                                        </tr>
                                    @php
                                        $no += 1;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="mt-3">
                                <div class="border rounded" style="background-color: #FFF9CA">
                                    <div class="p-3">
                                        <span class="font-weight-bold">Perhatian!</span>
                                        <span>Angsuran yang di ambil dari bulanan hanya bisa dibatalkan di menu Penggajian!</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if ($debt->amount > $debt->amount_paid)
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title m-0">Form Pembayaran Kasbon</h5>
                        </div>
                        <div class="card-body">
                            <form name="form_pembayaran" action="{{ url('travel/kasbon/pembayaran') }}" method="POST">
                                @csrf
                                <input type="hidden" name="debt_id" value="{{ $debt->id }}">
                                <input type="hidden" name="related_person" value="{{ $debt->sdm->name }}">

                                <div class="form-group">
                                    <label for="">Tanggal <span class="text-red">*</span></label>
                                    <input type="date" name="payment_date" value="{{ date('Y-m-d') }}" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label for="">Jumlah Bayar <span class="text-red">*</span></label>
                                    <input type="text" name="kasbon_amount" value="0" class="form-control money" required>
                                </div>

                                <div class="form-group">
                                    <label>Masuk Kas <span class="text-red">*</span></label>
                                    <select name="cash_account" class="form-control" required>
                                        <option value="">-- Pilih Sumber Kas --</option>
                                        @foreach ($cashAccounts as $account)
                                        <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Akun Kasbon <span class="text-red">*</span></label>
                                    <select name="kasbon_account" class="form-control" required>
                                        <option value="">-- Pilih Akun --</option>
                                        @foreach ($cashAccounts as $account)
                                        <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <hr>

                                <button type="submit" class="btn btn-success btn-save">
                                    SIMPAN
                                </button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>

@include('travel.TrnsctDebt.asset.js')
@endsection
