@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('travel/kasbon') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Kasbon
            </a>
        </li>
        <li class="breadcrumb-item active">Form Input Kasbon</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h5 class="card-title m-0">Form Input Kasbon</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('travel/kasbon/add_new') }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>Sdm <span class="text-red">*</span></label>
                                <select name="sdm_id" class="form-control" required>
                                    <option value="">-- Pilih SDM --</option>
                                    @foreach ($employes as $employe)
                                        <option value="{{ $employe->sdm_id.'-'.$employe->name }}">{{ $employe->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Tanggal <span class="text-red">*</span></label>
                                <input type="date" name="date" value="{{ date('Y-m-d') }}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="">Nominal Pinjaman <span class="text-red">*</span></label>
                                <input type="text" name="amount" value="0" class="form-control money" required>
                            </div>

                            <div class="form-group">
                                <label>Sumber Kas <span class="text-red">*</span></label>
                                <select name="account_id" class="form-control" required>
                                    <option value="">-- Pilih Sumber Kas --</option>
                                    @foreach ($cashAccounts as $account)
                                    <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Akun Kasbon <span class="text-red">*</span></label>
                                <select name="kasbon_account" class="form-control" required>
                                    <option value="">-- Pilih Akun --</option>
                                    @foreach ($cashAccounts as $account)
                                    <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Keterangan <span class="text-red">*</span></label>
                                <input type="text" name="desc" class="form-control" required>
                            </div>


                            <hr>

                            <button type="submit" class="btn btn-success btn-save">
                                SIMPAN
                            </button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctDebt.asset.js')
@endsection
