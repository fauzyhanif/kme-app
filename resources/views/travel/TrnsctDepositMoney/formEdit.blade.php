@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.deposit_money') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Kirim Uang
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Transaksi</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <form
                        id="form-add"
                        action="{{ route('travel.deposit_money.edit', $cashAccountOld->proof_id) }}"
                        method="POST"
                        data-remote>

                        @csrf
                        <input type="hidden" name="type" value="add">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tanggal <span class="text-red">*</span></label>
                                        <input
                                            type="date"
                                            name="trnsct_date"
                                            class="form-control"
                                            value="{{ $cashAccountOld->trnsct_date }}"
                                            required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Penerima <span class="text-red">*</span></label>
                                        <input
                                            type="text"
                                            name="related_person"
                                            class="form-control"
                                            value="{{ $cashAccountOld->related_person }}"
                                            required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sumber Kas <span class="text-red">*</span></label>
                                        <select name="cash_account" class="form-control" required>
                                            <option value="">-- Pilih kas --</option>
                                            @foreach ($cashAccount as $account)
                                            <option
                                                value="{{ $account->account_id }}"
                                                {{
                                                    ($cashAccountOld->account_id == $account->account_id)
                                                        ? 'selected'
                                                        : ''
                                                }}>
                                                {{ $account->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Deskripsi <span class="text-red">*</span></label>
                                        <textarea
                                            name="cash_description"
                                            class="form-control"
                                            required>{{ $cashAccountOld->description }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <table class="table">
                                    <thead class="bg-info">
                                        <th>Kirim Uang Untuk</th>
                                        <th width="20%">Jumlah</th>
                                        <th width="5%"></th>
                                    </thead>
                                    <tbody class="form-wrapper">
                                        @foreach ($incomeAccountOld as $oldItem)
                                            <tr class="form-content">

                                                <input
                                                    type="hidden"
                                                    name="jurnal_id[]"
                                                    value="{{ $oldItem->jurnal_id }}">

                                                <td>
                                                    <select
                                                        name="income_account[{{ $oldItem->jurnal_id }}]"
                                                        class="form-control income-account"
                                                        required>
                                                        <option value="">-- Pilih Akun --</option>
                                                        @foreach ($incomeAccount as $item)
                                                        <option
                                                            value="{{ $item->account_id."*".$item->post_report }}"
                                                            {{
                                                                ($oldItem->account_id == $item->account_id)
                                                                    ? 'selected'
                                                                    : ''
                                                            }}
                                                            >
                                                            {{ $item->account_id }} {{ $item->name }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <input
                                                        type="text"
                                                        name="amount[{{ $oldItem->jurnal_id }}]"
                                                        class="form-control amount"
                                                        value="{{ $oldItem->debit }}"
                                                        required>
                                                </td>
                                                <td>
                                                    <button
                                                        type="button"
                                                        class="btn btn-xs btn-danger btn-remove-is-data">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-sm btn-primary btn-clone-edit-page">
                                        <i class="fas fa-plus"></i> Tambah Data
                                    </button>
                                </div>
                                <div class="col-md-6 text-right">
                                    <input
                                        type="hidden"
                                        class="value-total-amount"
                                        name="total-amount"
                                        value="{{ $cashAccountOld->credit }}">
                                    <h4 class="text-bold mr-4" id="text-total-amount">
                                        Total Rp. {{ GeneralHelper::rupiah($cashAccountOld->credit) }}
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <a
                                href="{{ route('travel.deposit_money.print', $cashAccountOld->proof_id) }}"
                                class="btn btn-warning"
                                target="_blank">
                                <i class="fas fa-print"></i> Cetak
                            </a>
                            <button class="btn btn-secondary float-right">
                                Batal
                            </button>
                            <button type="submit" class="btn btn-success btn-save float-right mr-1">
                                Simpan
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctDepositMoney.asset.js')
@endsection
