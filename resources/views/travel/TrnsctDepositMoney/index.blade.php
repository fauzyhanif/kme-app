@extends('index')

@section('content')
<section class="content-header">
    <h1>Kirim Uang</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Transaksi
                        <div class="card-tools">
                            <a href="{{ route('travel.deposit_money.form_add') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Transaksi Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable-index-page">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>No Kwitansi</th>
                                <th>Deskripsi</th>
                                <th>Penerima</th>
                                <th>Nominal</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctDepositMoney.asset.js')
@endsection
