<script>
$(function() {
    datatableIndexPage();

    /* For add action */
    // clone table row
    $('.btn-clone').on('click', function () {
        var form = $(".form-content:last").clone(true).appendTo(".form-wrapper");
        form.find('select[name="spending_account[]"]').val("");
        form.find('input[name="description[]"]').val("");
        form.find('input[name="amount[]"]').val("");

        // add attr button remove row
        form.find('.btn-danger').addClass("btn-remove");
    });

    // remove table row
    $('table').on('click', '.btn-remove',function () {
        $(this).closest(".form-content").remove();
        sumAllAmount();
    });

    /* For edit action */
    // clone table row
    $('.btn-clone-edit-page').on('click', function () {
        var form = $(".form-content:last").clone(true).appendTo(".form-wrapper");
        form.find('.spending-account').val("");
        form.find('.spending-account').attr("name", "spending_account[x]");

        form.find('.description').val("");
        form.find('.description').attr("name", "description[x]");

        form.find('.amount').val("");
        form.find('.amount').attr("name", "amount[x]");

        // add attr button remove row
        form.find('.btn-danger').removeClass("btn-remove-is-data");
        form.find('.btn-danger').addClass("btn-remove");
    });

    // remove table row
    $('table').on('click', '.btn-remove-is-data',function () {
        var row = $(this).closest(".form-content");
        var jurnalId = row.find('input[name="jurnal_id[]"]').val();
        var modal = $('#myModal').modal('show');
        modal.find('#btn-delete-item-jurnal').attr('onclick', "removeJurnalItem('" + jurnalId + "')");
    });

    $('.amount').keyup(function() {
        sumAllAmount();
    });
});

function sumAllAmount() {
    var sum = 0;
    $('.amount').each(function(){
        sum += parseFloat(this.value.replace(/\./g, ''));
    });

    sum = (sum == "") ? 0 : sum;

    $('input[name="total-amount"]').val(sum);
    $('#text-total-amount').text("Rp. " + formatRupiah(sum));
}

function removeJurnalItem(jurnalId) {
    $('#myModal').modal('hide');
    $("#data-" + jurnalId).remove();
    sumAllAmount();

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: "{{ route('travel.spending.remove-jurnal-item') }}",
        type: "post",
        dataType: 'json',
        data: "_token="+CSRF_TOKEN+"&jurnal_id="+jurnalId,
        success: function (data) {
            toastr.success(data.text);
        }
    });
}

function datatableIndexPage() {
    var baseIndex = {!! json_encode(route('travel.spending.jsondata')) !!};
    $('.datatable-index-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: baseIndex,
        columns: [
            { data: 'trnsct_date', name: 'trnsct_date', searchable: true },
            { data: 'proof_id', name: 'proof_id', searchable: true },
            { data: 'description', name: 'description', searchable: false },
            { data: 'related_person', name: 'related_person', searchable: false },
            { data: 'credit', name: 'credit', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan");
            },
            success: function (data) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                    $('#text-total-amount').text('Total Rp. 0');
                }

                toastr.success(data.text)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
        document.documentElement.scrollTop = 0;
    });
})();

function searchBookingID(params) {

}
</script>
