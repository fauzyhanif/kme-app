@extends('index')

@section('content')
<section class="content-header">
    <h1>Pengeluaran</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Pengeluaran
                        <div class="card-tools">
                            <a href="{{ route('travel.spending.formadd') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Pengeluaran Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable-index-page">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>No Kwitansi</th>
                                <th>Deskripsi</th>
                                <th>Diberikan Kepada</th>
                                <th>Nominal</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctSpending.asset.js')
@endsection
