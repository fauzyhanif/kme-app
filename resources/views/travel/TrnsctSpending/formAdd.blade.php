@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.spending') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pengeluaran
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Pengeluaran Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <form id="form-add" action="{{ route('travel.spending.addnew') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="type" value="add">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tanggal <span class="text-red">*</span></label>
                                        <input type="date" name="trnsct_date" class="form-control" value="{{ date('Y-m-d') }}" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Diberikan kepada <span class="text-red">*</span></label>
                                        <input type="text" name="related_person" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sumber kas <span class="text-red">*</span></label>
                                        <select name="cash_account" class="form-control" required>
                                            <option value="">-- Pilih Akun --</option>
                                            @foreach ($cashAccount as $account)
                                            <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Deskripsi <span class="text-red">*</span></label>
                                        <textarea name="cash_description" class="form-control" required></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <table class="table">
                                    <thead class="bg-info">
                                        <th>Item Pengeluaran</th>
                                        <th width="20%">Jumlah</th>
                                        <th width="5%"></th>
                                    </thead>
                                    <tbody class="form-wrapper">
                                        <tr class="form-content">
                                            <td>
                                                <select name="spending_account[]" class="form-control" required>
                                                    <option value="">-- Pilih Pengeluaran --</option>
                                                    @foreach ($spendingAccount as $item)
                                                    <option value="{{ $item->account_id."*".$item->post_report }}">{{ $item->account_id }} {{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="amount[]" class="form-control amount" required>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-xs btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" class="btn btn-sm btn-primary btn-clone">
                                        <i class="fas fa-plus"></i> Tambah Data
                                    </button>
                                </div>
                                <div class="col-md-6 text-right">
                                    <input type="hidden" class="value-total-amount" name="total-amount" value="0">
                                    <h4 class="text-bold mr-4" id="text-total-amount">Total Rp. 0</h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-right" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success btn-save">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctSpending.asset.js')
@endsection
