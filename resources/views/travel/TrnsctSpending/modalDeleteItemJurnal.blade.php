<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Hapus Item Jurnal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Yakin ingin mengahpus item ini?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btn-delete-item-jurnal" data-dismiss="modal">Ya, Hapus sekarang</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>

        </div>
    </div>
</div>
