@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Neraca</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Data Neraca per {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }}
                        </h3>
                        <div class="card-tools">
                            <a href="{{ route('travel.neraca.print') }}" target="_blank" class="btn btn-primary btn-sm">
                                <i class="fas fa-print"></i> &nbsp;
                                Cetak
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="text-bold">AKTIVA</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;&nbsp;Aktiva Lancar</td>
                                </tr>
                                @php $ttl_aktiva_lancar = 0 @endphp
                                @foreach ($datas as $data)
                                @if ($data->post_sub_report == 'AKTIVA_LANCAR')
                                    <tr>
                                        <td>
                                            <a href="{{ route('travel.neraca.rincian', $data->account_id) }}">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                {{ $data->account_id . ' - ' . $data->name }}
                                            </a>
                                        </td>
                                        <td class="text-primary text-right">
                                            {{ GeneralHelper::rupiah($data->debit - $data->credit) }}
                                        </td>
                                    </tr>
                                @php $ttl_aktiva_lancar += ($data->debit - $data->credit)  @endphp
                                @endif
                                @endforeach
                                <tr>
                                    <td class="text-bold">&nbsp;&nbsp;Total Aktiva Lancar</td>
                                    <td class="text-right text-bold">{{ GeneralHelper::rupiah($ttl_aktiva_lancar) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>

                                <tr>
                                    <td colspan="2">&nbsp;&nbsp;Aktiva Tetap</td>
                                </tr>
                                @php $ttl_aktiva_tetap = 0 @endphp
                                @foreach ($datas as $data)
                                @if ($data->post_sub_report == 'AKTIVA_TETAP')
                                    <tr>
                                        <td>
                                            <a href="{{ route('travel.neraca.rincian', $data->account_id) }}">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                {{ $data->account_id . ' - ' . $data->name }}
                                            </a>
                                        </td>
                                        <td class="text-primary text-right">
                                            {{ GeneralHelper::rupiah($data->debit - $data->credit) }}
                                        </td>
                                    </tr>
                                @php $ttl_aktiva_tetap += ($data->debit - $data->credit) @endphp
                                @endif
                                @endforeach
                                <tr>
                                    <td class="text-bold">&nbsp;&nbsp;Total Aktiva Tetap</td>
                                    <td class="text-right text-bold">{{ GeneralHelper::rupiah($ttl_aktiva_tetap) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>

                                <tr>
                                    <td colspan="2">&nbsp;&nbsp;Depresiasi & Amortasi</td>
                                </tr>
                                @php $ttl_depresiasi = 0 @endphp
                                @foreach ($datas as $data)
                                @if ($data->post_sub_report == 'DEPRESIASI')
                                    <tr>
                                        <td>
                                            <a href="{{ route('travel.neraca.rincian', $data->account_id) }}">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                {{ $data->account_id . ' - ' . $data->name }}
                                            </a>
                                        </td>
                                        <td class="text-primary text-right">
                                            {{ GeneralHelper::rupiah($data->credit - $data->debit) }}
                                        </td>
                                    </tr>
                                @php $ttl_depresiasi += ($data->credit - $data->debit) @endphp
                                @endif
                                @endforeach
                                <tr>
                                    <td class="text-bold">&nbsp;&nbsp;Total Depresiasi & Amortasi</td>
                                    <td class="text-right text-bold">{{ GeneralHelper::rupiah($ttl_depresiasi) }}</td>
                                </tr>

                                @php
                                    $ttl_aktiva = $ttl_aktiva_lancar + $ttl_aktiva_tetap;
                                    $ttl_aktiva = $ttl_aktiva - $ttl_depresiasi;
                                @endphp
                                <tr>
                                    <td class="text-bold">Total Aktiva</td>
                                    <td class="text-right text-bold">{{ GeneralHelper::rupiah($ttl_aktiva) }}</td>
                                </tr>

                                <tr>
                                    <td colspan="2"></td>
                                </tr>

                                <tr>
                                    <td colspan="2"  class="text-bold">PASIVA (Kewajiban & Modal)</td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;&nbsp;Kewajiban Lancar</td>
                                </tr>
                                @php $ttl_kewajiban_lancar = 0 @endphp
                                @foreach ($datas as $data)
                                @if ($data->post_sub_report == 'KEWAJIBAN')
                                    <tr>
                                        <td>
                                            <a href="{{ route('travel.neraca.rincian', $data->account_id) }}">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                {{ $data->account_id . ' - ' . $data->name }}
                                            </a>
                                        </td>
                                        <td class="text-primary text-right">
                                            {{ GeneralHelper::rupiah($data->credit - $data->debit) }}
                                        </td>
                                    </tr>
                                @php $ttl_kewajiban_lancar += ($data->credit - $data->debit) @endphp
                                @endif
                                @endforeach
                                <tr>
                                    <td class="text-bold">&nbsp;&nbsp;Total Kewajiban Lancar</td>
                                    <td class="text-right text-bold">{{ GeneralHelper::rupiah($ttl_kewajiban_lancar) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                </tr>

                                <tr>
                                    <td colspan="2">&nbsp;&nbsp;Modal</td>
                                </tr>
                                @php $ttl_modal = 0 @endphp
                                @foreach ($datas as $data)
                                @if ($data->post_sub_report == 'MODAL')
                                    <tr>
                                        <td>
                                            <a href="{{ route('travel.neraca.rincian', $data->account_id) }}">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                {{ $data->account_id . ' - ' . $data->name }}
                                            </a>
                                        </td>
                                        <td class="text-primary text-right">
                                            {{ GeneralHelper::rupiah($data->credit - $data->debit) }}
                                        </td>
                                    </tr>
                                @php $ttl_modal += ($data->credit - $data->debit) @endphp
                                @endif
                                @endforeach
                                <tr>
                                    <td class="text-bold">&nbsp;&nbsp;Total Modal</td>
                                    <td class="text-right text-bold">{{ GeneralHelper::rupiah($ttl_modal) }}</td>
                                </tr>

                                @php
                                    $ttl_kewajiban = $ttl_kewajiban_lancar + $ttl_modal;
                                @endphp
                                <tr>
                                    <td class="text-bold">Total Kewajiban & Modal</td>
                                    <td class="text-right text-bold">{{ GeneralHelper::rupiah($ttl_kewajiban) }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
