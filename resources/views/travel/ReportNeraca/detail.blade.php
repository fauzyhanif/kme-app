@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.neraca') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Neraca
            </a>
        </li>
        <li class="breadcrumb-item active">Rincian Transaksi</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Rincian Transaksi <b>{{ $account->name }} ({{ $account->account_id }})</b>
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead class="bg-info">
                                <th width="15%">Tanggal</th>
                                <th>Keterangan</th>
                                <th width="15%">Kredit</th>
                                <th width="15%">Debet</th>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data)
                                    <tr>
                                        <td>{{ GeneralHelper::konversitgl($data->trnsct_date, 'slash') }}</td>
                                        <td>{{ $data->description }}</td>
                                        <td>{{ GeneralHelper::rupiah($data->credit) }}</td>
                                        <td>{{ GeneralHelper::rupiah($data->debit) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        {{ $datas->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
