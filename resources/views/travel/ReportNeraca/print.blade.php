<!DOCTYPE html>
<html>

    <head>
        <title>Slip Pemasukan</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                font-size: 10px;
            }

            .borderless {
                border-collapse: separate;
                border-spacing: 0 -15px;
                margin-top: 10px;
            }

            .borderless td,
            .borderless th {
                border: none;
            }

            .data-title {
                font-size: 16px;
            }

            tr td.except,
            th.except {
                padding-top: 2 !important;
                padding-bottom: 2 !important;
                margin: 0 !important;
            }
        </style>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row kop-surat">
                <div class="col-md-12 text-center">
                    <h3 class="text-bold">PT KARYA MAS EMPAT</h3>
                    <p>
                        Jl. Raya Rancaudik KM. 5 Tambakdahan 41253 Subang - Jawa Barat
                        <br>
                        Telp. (0260) 540153 - 552244 e-mail : karyamasempat@yahoo.co.id
                    </p>
                </div>
            </div>

            <hr>

            <div class="row text-center">
                <div class="col-md-12">
                    <h5 class="font-weight-bold">Neraca</h5>
                    <p>
                        Data per {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }}
                    </p>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="except" colspan="2">
                                    <b>AKTIVA</b>
                                </td>
                            </tr>
                            <tr>
                                <td class="except" colspan="2">&nbsp;&nbsp;Aktiva Lancar</td>
                            </tr>
                            @php $ttl_aktiva_lancar = 0 @endphp
                            @foreach ($datas as $data)
                            @if ($data->post_sub_report == 'AKTIVA_LANCAR')
                            <tr>
                                <td class="except" class="text-primary">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    {{ $data->account_id . ' - ' . $data->name }}
                                </td>
                                <td class="text-primary text-right except">
                                    {{ GeneralHelper::rupiah($data->debit - $data->credit) }}
                                </td>
                            </tr>
                            @php $ttl_aktiva_lancar += ($data->debit - $data->credit) @endphp
                            @endif
                            @endforeach
                            <tr>
                                <td class="except"><b>&nbsp;&nbsp;Total Aktiva Lancar</b></td>
                                <td class="text-right except"><b>{{ GeneralHelper::rupiah($ttl_aktiva_lancar) }}</b></td>
                            </tr>
                            <tr>
                                <td class="except" colspan="2"></td>
                            </tr>

                            <tr>
                                <td class="except" colspan="2">&nbsp;&nbsp;Aktiva Tetap</td>
                            </tr>
                            @php $ttl_aktiva_tetap = 0 @endphp
                            @foreach ($datas as $data)
                            @if ($data->post_sub_report == 'AKTIVA_TETAP')
                            <tr>
                                <td class="text-primary except">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    {{ $data->account_id . ' - ' . $data->name }}
                                </td>
                                <td class="text-primary text-right except">
                                    {{ GeneralHelper::rupiah($data->debit - $data->credit) }}
                                </td>
                            </tr>
                            @php $ttl_aktiva_tetap += ($data->debit - $data->credit) @endphp
                            @endif
                            @endforeach
                            <tr>
                                <td class="except"><b>&nbsp;&nbsp;Total Aktiva Tetap</b></td>
                                <td class="text-right except"><b>{{ GeneralHelper::rupiah($ttl_aktiva_tetap) }}</b></td>
                            </tr>
                            <tr>
                                <td class="except" colspan="2"></td>
                            </tr>

                            <tr>
                                <td class="except" colspan="2">&nbsp;&nbsp;Depresiasi & Amortasi</td>
                            </tr>
                            @php $ttl_depresiasi = 0 @endphp
                            @foreach ($datas as $data)
                            @if ($data->post_sub_report == 'DEPRESIASI')
                            <tr>
                                <td class="text-primary except">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    {{ $data->account_id . ' - ' . $data->name }}
                                </td>
                                <td class="text-primary text-right except">
                                    {{ GeneralHelper::rupiah($data->credit - $data->debit) }}
                                </td>
                            </tr>
                            @php $ttl_depresiasi += ($data->credit - $data->debit) @endphp
                            @endif
                            @endforeach
                            <tr>
                                <td class="except"><b>&nbsp;&nbsp;Total Depresiasi & Amortasi</b></td>
                                <td class="text-right except"><b>{{ GeneralHelper::rupiah($ttl_depresiasi) }}</b></td>
                            </tr>

                            @php
                            $ttl_aktiva = $ttl_aktiva_lancar + $ttl_aktiva_tetap;
                            $ttl_aktiva = $ttl_aktiva - $ttl_depresiasi;
                            @endphp
                            <tr>
                                <td class="except"><b>Total Aktiva</b></td>
                                <td class="text-right except"><b>{{ GeneralHelper::rupiah($ttl_aktiva) }}</b></td>
                            </tr>

                            <tr>
                                <td class="except" colspan="2"></td>
                            </tr>

                            <tr>
                                <td class="except text-bold" colspan="2">
                                    <b>PASIVA (Kewajiban & Modal)</b>
                                </td>
                            </tr>
                            <tr>
                                <td class="except" colspan="2">&nbsp;&nbsp;Kewajiban Lancar</td>
                            </tr>
                            @php $ttl_kewajiban_lancar = 0 @endphp
                            @foreach ($datas as $data)
                            @if ($data->post_sub_report == 'KEWAJIBAN')
                            <tr>
                                <td class="text-primary except">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    {{ $data->account_id . ' - ' . $data->name }}
                                </td>
                                <td class="text-primary text-right except">
                                    {{ GeneralHelper::rupiah($data->credit - $data->debit) }}
                                </td>
                            </tr>
                            @php $ttl_kewajiban_lancar += ($data->credit - $data->debit) @endphp
                            @endif
                            @endforeach
                            <tr>
                                <td class="except"><b>&nbsp;&nbsp;Total Kewajiban Lancar</b></td>
                                <td class="text-right except"><b>{{ GeneralHelper::rupiah($ttl_kewajiban_lancar) }}</b></td>
                            </tr>
                            <tr>
                                <td class="except" colspan="2"></td>
                            </tr>

                            <tr>
                                <td class="except" colspan="2">&nbsp;&nbsp;Modal</td>
                            </tr>
                            @php $ttl_modal = 0 @endphp
                            @foreach ($datas as $data)
                            @if ($data->post_sub_report == 'MODAL')
                            <tr>
                                <td class="text-primary except">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    {{ $data->account_id . ' - ' . $data->name }}
                                </td>
                                <td class="text-primary text-right except">
                                    {{ GeneralHelper::rupiah($data->credit - $data->debit) }}
                                </td>
                            </tr>
                            @php $ttl_modal += ($data->credit - $data->debit) @endphp
                            @endif
                            @endforeach
                            <tr>
                                <td class="except"><b>&nbsp;&nbsp;Total Modal</b></td>
                                <td class="text-right except"><b>{{ GeneralHelper::rupiah($ttl_modal) }}</b></td>
                            </tr>

                            @php
                            $ttl_kewajiban = $ttl_kewajiban_lancar + $ttl_modal;
                            @endphp
                            <tr>
                                <td class="except"><b>Total Kewajiban & Modal</b></td>
                                <td class="text-right except"><b>{{ GeneralHelper::rupiah($ttl_kewajiban) }}</b></td>
                            </tr>

                        </tbody>

                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                    Petugas,

                    <br>
                    <br>
                    <br>
                    <p>(H. Yusuf)</p>
                </div>
            </div>

        </div>
    </body>
