@extends('index')

@section('content')


<input type="hidden" name="status_get_data" value="{{ $status }}">

<section class="content-header">
    <h1>Daftar Booking</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-md-12">
                <a href="{{ url('travel/booking?status=ALL') }}" class="btn {{ ($status == 'ALL') ? 'btn-primary' : 'btn-outline-primary' }}  mr-2">
                    Semua Data Booking
                </a>
                <a href="{{ url('travel/booking?status=NOT_YET') }}" class="btn {{ ($status == 'NOT_YET') ? 'btn-primary' : 'btn-outline-primary' }}  mr-2">
                    Data Belum Berangkat
                </a>
                <a href="{{ url('travel/booking?status=DONE') }}" class="btn {{ ($status == 'DONE') ? 'btn-primary' : 'btn-outline-primary' }}  mr-2">
                    Data Sudah Berangkat
                </a>
                <a href="{{ url('travel/booking?status=CANCEL') }}" class="btn {{ ($status == 'CANCEL') ? 'btn-primary' : 'btn-outline-primary' }} ">
                    Data Cancel
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Booking
                        <div class="card-tools">
                            <a href="{{ route('travel.booking.search.cars.page') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Booking Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <div class="border rounded my-2" style="background-color: #FFF9CA">
                            <div class="p-3">
                                <span class="font-weight-bold">Panduan!</span>
                                <span>Untuk mempercepat pencarian data silahkan ketik di kolom pencarian sebelah kanan. </span>
                            </div>
                        </div>

                        <table class="table table-bordered table-hover datatable-index-page">
                            <thead class="bg-info">
                                <th>Kode Booking</th>
                                <th>Customer</th>
                                <th>Tujuan</th>
                                <th>Tgl Booking</th>
                                <th>Jml Unit</th>
                                <th>Pembayaran</th>
                                <th>Status</th>
                                <th width="20%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctBooking.list.modalCancel')
@include('travel.TrnsctBooking.list.modalCancelSecond')
@include('travel.TrnsctBooking.list.modalDeleteBooking')
@include('travel.TrnsctBooking.asset.js')

<script>
$(function() {
    datatableIndexPage();
});

function datatableIndexPage() {
    var status = $('input[name="status_get_data"]').val();

    var baseIndex = {!! json_encode(url('travel/booking/json-data')) !!} + '?status=' + status;
    $('.datatable-index-page').DataTable({
        pageLength: 50,
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: baseIndex,
        columns: [
            { data: 'booking_id', name: 'booking_id', searchable: true },
            { data: 'customer.name', name: 'customer.name', searchable: true },
            { data: 'destination', name: 'destination', searchable: true },
            { data: 'booking_date', name: 'booking_date', searchable: false },
            { data: 'ttl_unit', name: 'ttl_unit', searchable: false },
            { data: 'status_pembayaran', name: 'status_pembayaran', searchable: false },
            { data: 'status', name: 'status', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}
</script>
@endsection
