@extends('index')

@section('content')

<section class="content-header">
    <h1>Daftar Booking Berangkat Minggu Ini</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Booking
                        <div class="card-tools">
                            <a href="{{ route('travel.booking.search.cars.page') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Booking Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-hover">
                            <thead class="bg-info">
                                <th>Kode Booking</th>
                                <th>Customer</th>
                                <th>Tujuan</th>
                                <th>Tgl Booking</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th>Pembayaran</th>
                                <th width="20%">Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($bookings as $booking)
                                    <tr>
                                        <td>{{ $booking->booking_id }}</td>
                                        <td>{{ $booking->cust_name }}</td>
                                        <td>{{ $booking->destination }}</td>
                                        <td>
                                            {{
                                                date_format(date_create($booking->booking_start_date), 'd/m/Y') . ' sd ' .
                                                date_format(date_create($booking->booking_end_date), 'd/m/Y')
                                            }}
                                        </td>
                                        <td>
                                            @foreach ($booking->cars as $car)
                                                - {{ $car->carCategory->name }} [{{ $car->unit_qty }}] <br>
                                            @endforeach
                                        </td>
                                        <td>{{ $booking->status }}</td>
                                        <td>
                                            @if ($booking->ttl_trnsct <= $booking->ttl_trnsct_paid)
                                                <span class='text-green'>Sudah Lunas</span>
                                            @else
                                                <span class='text-red'>Belum Lunas</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a
                                                href="{{ route('travel.booking.detail', $booking->booking_id) }}"
                                                class='btn btn-info btn-xs'>
                                                <i class='fas fa-eye'></i> Detail
                                            </a>
                                            <a
                                                href="{{ route('travel.payment.formadd', $booking->booking_id) }}"
                                                class='btn btn-success btn-xs'>
                                                <i class='fas fa-money-bill-wave'></i> Bayar
                                            </a>
                                            @php
                                                $statusCancel = ($booking->status == 'BATAL') ? 'HAS_BEEN_CANCELED' : 'NOT_CANCELED';
                                            @endphp
                                            @if ($booking->status != 'SELESAI')
                                                <a
                                                    href="{{ route('travel.booking.formedit', $booking->booking_id) }}"
                                                    class='btn btn-primary btn-xs'>
                                                    <i class='fas fa-edit' style='padding: 1px'></i> Edit
                                                </a>

                                                <button
                                                    class="btn btn-danger btn-xs"
                                                    onclick="modalCancel(
                                                        '{{ $booking->booking_id }}',
                                                        '{{ $booking->ttl_trnsct_paid }}',
                                                        '{{ $booking->ttl_trnsct }}',
                                                        '{{ $statusCancel }}',
                                                    )">
                                                    <i class='fas fa-times' style='padding: 3px'></i> Batal
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('travel.TrnsctBooking.list.modalCancel')
@include('travel.TrnsctBooking.list.modalCancelSecond')
@include('travel.TrnsctBooking.asset.js')
@endsection
