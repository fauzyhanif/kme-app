<div class="modal" id="modal-cancel">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Pembatalan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="form_pembatalan" action="{{ route('travel.booking.cancel_booking') }}" method="POST">

                <input type="hidden" name="booking_id" value="">
                <input type="hidden" name="related_person" value="">
                <input type="hidden" name="trnsct_paid" value="">
                <input type="hidden" name="status_cancel" value="">

                @csrf
                <!-- Modal body -->
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead class="bg-info">
                            <th>Total Transaksi</th>
                            <th>Total Uang Muka</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td id="cancel-ttl-trnsct"></td>
                                <td id="cancel-ttl-paid"></td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="form-group">
                        <label>Tanggal Pembatalan</label>
                        <input type="date" name="cancel_date" class="form-control" value="{{ date('Y-m-d') }}">
                    </div>

                    <div class="form-group">
                        <label>Dibatalkan Oleh</label>
                        <input type="text" name="cancel_by" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Rencana Uang Dikembalikan Kepada Pelanggan</label>
                        <input type="text" name="money_refundable_plan" class="form-control money" required>
                    </div>

                    <div class="form-group">
                        <label>Potongan Untuk Perusahaan</label>
                        <input type="text" name="refund_cut" class="form-control money" readonly required>
                    </div>

                    <div class="form-group">
                        <label>Uang yang akan dikembalikan sekarang</label>
                        <input type="text" name="refund_now" class="form-control money" required>
                    </div>

                    <div class="form-group">
                        <label>Uang Diambil Dari</label>
                        <select name="money_get_from" class="form-control" required>
                            <option value="">-- Pilih Akun --</option>
                            @foreach ($cashAccount as $account)
                            <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-save">Batalkan Sekarang</button>
                </div>
            </form>
        </div>
    </div>
</div>
