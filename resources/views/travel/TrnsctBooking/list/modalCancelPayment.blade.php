<div class="modal" id="modal-cancel-payment-{{ $item->payment_id }}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Pembatalan Pembayaran</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="{{ route('travel.payment.cancel') }}" method="POST">
                @csrf
                <input type="hidden" name="payment_id" value="{{ $item->payment_id }}">
                <input type="hidden" name="booking_id" value="{{ $booking->booking_id }}">

                <!-- Modal body -->
                <div class="modal-body">
                    yakin ingin membatalkan pembayaran dengan nomor kwitansi <b>{{ $item->payment_id }}</b> ?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-save">Batalkan Sekarang</button>
                </div>
            </form>
        </div>
    </div>
</div>
