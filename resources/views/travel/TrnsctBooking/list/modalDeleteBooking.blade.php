<div class="modal" id="modal-delete-booking">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Hapus Booking</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="form_delete_booking" action="{{ route('travel.booking.delete_booking') }}" method="POST">

                <input type="hidden" name="booking_id" value="">

                @csrf
                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Yakin ingin menghapus booking ini?</h4>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-save">Hapus Sekarang</button>
                </div>
            </form>
        </div>
    </div>
</div>
