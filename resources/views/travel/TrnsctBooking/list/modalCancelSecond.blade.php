<div class="modal" id="modal-cancel-second">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Pembatalan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="form_pembatalan" action="{{ route('travel.booking.cancel_booking_second') }}" method="POST">
                <input
                    type="hidden"
                    name="booking_id"
                    value="">

                <input
                    type="hidden"
                    name="related_person"
                    value="">

                <input
                    type="hidden"
                    name="status_cancel"
                    value="">

                @csrf
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="alert alert-danger" role="alert">
                        <h5>Transaksi ini sudah dibatalkan sebelumnya oleh <span id="cancel-by"></span>.</h5>
                        Silahkan serahkan sisa uang refund.
                    </div>
                    <table class="table table-bordered">
                        <thead class="bg-info">
                            <th>Total Refund</th>
                            <th>Sudah Diserahkan</th>
                            <th>Sisa Refund</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td id="cancel-ttl-refund"></td>
                                <td id="cancel-ttl-refund-paid"></td>
                                <td id="cancel-ttl-rest"></td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="form-group">
                        <label>Tanggal refund</label>
                        <input type="date" name="refund_date" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Uang yang akan dikembalikan sekarang</label>
                        <input type="text" name="refund_now" class="form-control money" required>
                    </div>

                    <div class="form-group">
                        <label>Uang Diambil Dari</label>
                        <select name="money_get_from" class="form-control" required>
                            <option value="">-- Pilih Akun --</option>
                            @foreach ($cashAccount as $account)
                            <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-save">Batalkan Sekarang</button>
                </div>
            </form>
        </div>
    </div>
</div>
