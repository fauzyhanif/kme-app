@extends('index')

@section('content')
<section class="content-header">
    <div class="row">
        <div class="col-md-6">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('travel.booking') }}">
                        <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                        Daftar Booking
                    </a>
                </li>
                <li class="breadcrumb-item active">Detail Booking</li>
            </ol>
        </div>
        <div class="col-md-6">
            <div class="float-right">
                @if ($booking->booking_from != 'UMUM')
                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-pencairan">
                        <i class="fas fa-edit"></i> Pencairan Komisi
                    </button>

                    @include('travel.TrnsctBooking.list.modalDisbursement')
                @endif

                @if ($booking->status != 'BATAL' && $booking->status != 'SELESAI')
                    @php
                        $statusCancel = ($booking->status == 'BATAL') ? 'HAS_BEEN_CANCELED' : 'NOT_CANCELED';
                    @endphp
                    <a href="{{ route('travel.booking.formedit',  $booking->booking_id) }}" class="btn btn-primary btn-sm">
                        <div class="fas fa-edit"></div> Update
                    </a>
                    <button type="button" class="btn btn-danger btn-sm" onclick="modalCancel('{{ $booking->booking_id }}', '{{ $booking->ttl_trnsct_paid }}', '{{ $booking->ttl_trnsct }}','{{ $statusCancel }}')">
                        <div class="fas fa-times"></div> Pembatalan
                    </a>
                @endif
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        @if ($booking->status == 'BATAL')
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i> Bookingan ini sudah dibatalkan!</h5>
                    Bookingan ini dibatalkan oleh {{ $booking->cancel_by }}
                    pada tanggal {{ GeneralHelper::konversiTgl($booking->cancel_date,'ttd') }}
                </div>
            </div>
        </div>
        @endif

        <div class="my-3">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Informasi Booking
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 text-secondary">Kode Booking</div>
                            <div class="col-md-8">: {{ $booking->booking_id }}</div>

                            <div class="col-md-4 text-secondary">Status</div>
                            <div class="col-md-8">: {{ $booking->status }}</div>

                            <div class="col-md-4 text-secondary">Tgl Transaksi</div>
                            <div class="col-md-8">: @php echo date_format(date_create($booking->trnsct_date), 'd/m/Y H:i:s'); @endphp</div>

                            <div class="col-md-4 text-secondary">Pelanggan</div>
                            <div class="col-md-8">: {{ $booking->customer->name }}</div>

                            <div class="col-md-4 text-secondary">Nomor Telpon</div>
                            <div class="col-md-8">: {{ $booking->customer->phone_num }}</div>

                            <div class="col-md-4 text-secondary">Alamat</div>
                            <div class="col-md-8">: {{ $booking->customer->address }}</div>

                            <div class="col-md-4 text-secondary">Tgl Booking</div>
                            <div class="col-md-8">:
                                @php
                                echo date_format(date_create($booking->booking_start_date), 'd/m/Y');
                                echo " s/d " . date_format(date_create($booking->booking_end_date), 'd/m/Y');
                                @endphp
                            </div>

                            <div class="col-md-4 text-secondary">Tujuan</div>
                            <div class="col-md-8">: {{ $booking->destination }}</div>

                            <div class="col-md-4 text-secondary">Stand By</div>
                            <div class="col-md-8">: {{ $booking->standby_time }}</div>

                            <div class="col-md-4 text-secondary">Penjemputan</div>
                            <div class="col-md-8">: {{ $booking->pick_up_location }}</div>

                            <div class="col-md-4 text-secondary">Keterangan</div>
                            <div class="col-md-8">: {{ $booking->information }}</div>

                            <div class="col-md-4 text-secondary">Booking Dari</div>
                            <div class="col-md-8">: {{ $booking->booking_from }}</div>

                            @if ($booking->booking_from != 'UMUM')
                                <div class="col-md-4 text-secondary">Nama Agen / Biro</div>
                                <div class="col-md-8">: {{ ($booking->agent_id != '') ? $booking->agent->name : '' }}</div>

                                <div class="col-md-4 text-secondary">Komisi Agen / Biro</div>
                                <div class="col-md-8">:
                                    {{ GeneralHelper::rupiah($booking->agent_commission) }}
                                </div>

                                <div class="col-md-4 text-secondary">Komisi sudah Dicairkan</div>
                                <div class="col-md-8">: {{ GeneralHelper::rupiah($booking->agent_commission_paid) }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">

                @if ($booking->status == 'BATAL')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card shadow-none">
                                <div class="card-header">
                                    <h3 class="card-title">Ringkasan Pembatalan</h3>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered table-hover mb-4">
                                        <thead class="bg-info">
                                            <th width="33%">Total Refund</th>
                                            <th width="33%">Sudah Diserahkan</th>
                                            <th width="33%">Sisa Refund</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ GeneralHelper::rupiah($booking->money_refundable_plan) }}</td>
                                                <td>{{ GeneralHelper::rupiah($booking->money_return) }}</td>
                                                <td>{{ GeneralHelper::rupiah($booking->money_refundable_plan - $booking->money_return) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-bordered">
                                        <thead class="bg-info">
                                            <th>Tgl Refund</th>
                                            <th>Penerima</th>
                                            <th>Sebesar</th>
                                            <th>Cetak</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($moneyHasBeenRefund as $item)
                                                <tr>
                                                    <td>{{ GeneralHelper::konversiTgl($item->trnsct_date) }}</td>
                                                    <td>{{ $item->related_person }}</td>
                                                    <td>{{ GeneralHelper::rupiah($item->credit) }}</td>
                                                    <td>
                                                        <a
                                                            href="{{ url('/travel/booking/print_cancel', $item->proof_id) }}"
                                                            target="_blank"
                                                            class="btn btn-primary btn-sm">
                                                            Cetak
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Daftar Bookingan</h3>
                            </div>
                            <div class="card-body scroll-x">
                                <table class="table table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th width="5%">No</th>
                                        <th>Kendaraan</th>
                                        <th>Qty</th>
                                        <th>Harga</th>
                                        <th>Jumlah</th>
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                            $ttl_unit = 0;
                                            $spj_created = 0;
                                        @endphp
                                        @foreach ($bookingItems as $item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>
                                                {{ $item->carCategory->name }} <br>
                                                <small class="text-muted">{{ $item->carCategory->seat }} seat</small>
                                            </td>
                                            <td>{{ $item->unit_qty }} unit</td>
                                            <td>Rp. {{ GeneralHelper::rupiah($item->price) }}</td>
                                            <td>Rp. {{ GeneralHelper::rupiah($item->total) }}</td>
                                            <td>
                                                @if ($booking->status != 'BATAL' && $booking->status != 'SELESAI')
                                                    @if ($item->unit_qty > $item->unit_qty_spj)
                                                        <a href="{{ url('travel/spj/form-add/'.$booking->booking_id.'/'.$item->category_id) }}" class="btn btn-success btn-sm">
                                                            Buat SPJ
                                                        </a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @php
                                            $no += 1;
                                            $ttl_unit += $item->unit_qty;
                                            $spj_created += $item->unit_qty_spj;
                                        @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Surat Perintah Jalan</h3>
                            </div>
                            <div class="card-body">
                                @if ($spj_created < $ttl_unit)
                                    <div class="border rounded mb-2" style="background-color: #FFF9CA">
                                        <div class="p-2">
                                            <span>{{ $ttl_unit - $spj_created }} unit belum dibuatkan SPJ</span>
                                        </div>
                                    </div>
                                @endif
                                <table class="table table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th>Unit</th>
                                        <th>No Polisi</th>
                                        <th>Driver</th>
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($spj as $item)
                                            <tr>
                                                <td>{{ $item->carCategory->name }}</td>
                                                <td>{{ $item->police_num }}</td>
                                                <td>{{ $item->mainDriver->name }}</td>
                                                <td>
                                                    <a
                                                        href="{{ route('travel.spj.formedit', $item->id) }}"
                                                        class="btn btn-primary btn-sm">
                                                        <i class="fas fa-edit"></i> Edit
                                                    </a>

                                                    <a
                                                        href="{{ route('travel.spj.print', $item->id) }}"
                                                        class="btn btn-warning btn-sm"
                                                        target="_blank">
                                                        <i class="fas fa-print"></i> Cetak
                                                    </a>

                                                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#form-delete-spj-{{ $item->id }}">
                                                        <i class="fas fa-trash"></i> Hapus
                                                    </button>

                                                    @include('travel.TrnsctBooking.form.formDeleteSpj')
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Ringkasan Pembayaran</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        Total Tagihan <br>
                                        <b>Rp. {{ GeneralHelper::rupiah($booking->ttl_trnsct) }}</b>
                                    </div>
                                    <div class="col-md-4">
                                        Sudah Dibayar <br>
                                        <b>Rp. {{ GeneralHelper::rupiah($booking->ttl_trnsct_paid) }}</b>
                                    </div>
                                    <div class="col-md-4">
                                        Sisa Pembayaran <br>
                                        <b>Rp.
                                            {{ GeneralHelper::rupiah($booking->ttl_trnsct - $booking->ttl_trnsct_paid) }}</b>
                                    </div>
                                </div>

                                @if ($booking->discount != '0')
                                    <br>
                                    <span class="text-blue pt-3">*Total tagihan sudah termasuk diskon sebesar {{ Generalhelper::rupiah($booking->discount) }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Daftar Pembayaran</h3>

                                <div class="card-tools">
                                    <a href="{{ url('travel/payment/form-add', $booking->booking_id) }}" target="_blank" class="btn btn-primary btn-sm">
                                        <i class="fas fa-edit"></i> Input Pembayaran
                                    </a>
                                </div>
                            </div>
                            <div class="card-body scroll-x">
                                <table class="table table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th>No Kwitansi</th>
                                        <th>Tanggal Bayar</th>
                                        <th class="text-right">Jumlah</th>
                                        <th>Via</th>
                                        <th>Cetak</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($payments as $item)
                                        <tr>
                                            <td>{{ $item->payment_id }}</td>
                                            <td>{{ GeneralHelper::konversiTgl($item->payment_date, 'slash') }}</td>
                                            <td class="text-right">Rp. {{ GeneralHelper::rupiah($item->amount) }}</td>
                                            <td>Cash</td>
                                            <td>
                                                <a href="{{ route('travel.payment.print', $item->payment_id) }}"
                                                    target="_blank" class="btn btn-warning btn-sm">
                                                    <i class="fas fa-print"></i> Cetak
                                                </a>

                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-cancel-payment-{{ $item->payment_id }}">
                                                    <i class="fas fa-times"></i> Batal
                                                </button>

                                                @include('travel.TrnsctBooking.list.modalCancelPayment')
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctBooking.list.modalCancel')
@include('travel.TrnsctBooking.list.modalCancelSecond')
@include('travel.TrnsctBooking.list.modalCancelDisbursement')


@include('travel.TrnsctBooking.asset.js')
@endsection
