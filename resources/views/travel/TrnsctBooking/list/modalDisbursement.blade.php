<div class="modal" id="modal-pencairan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Pencairan Komisi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="form_pencairan" action="{{ route('travel.booking.pencairan_komisi') }}" method="POST">
                <input
                    type="hidden"
                    name="booking_id"
                    value="{{ $booking->booking_id }}">
                <input
                    type="hidden"
                    name="related_person"
                    value="{{ ($booking->agent_id != '') ? $booking->agent->name : '' }}">
                @csrf
                <!-- Modal body -->
                <div class="modal-body">

                    <table class="table table-bordered">
                        <thead class="bg-info">
                            <th width="5%">#</th>
                            <th>Tgl Pencairan</th>
                            <th>Jml Pencairan</th>
                            <th>Melalui</th>
                            <th width="15%" class="text-center">Aksi</th>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                                $total = 0;
                            @endphp
                            @foreach ($listDisbursementCommission as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ date_format(date_create($item->trnsct_date), 'd/m/Y') }}</td>
                                    <td>{{ GeneralHelper::rupiah($item->credit) }}</td>
                                    <td>{{ $item->coa->name }}</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-sm btn-danger" onclick="showModalCancelDisbursement('{{ $item->booking_id }}', '{{ $item->proof_id }}')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @php
                                $total += $item->credit;
                            @endphp
                            @endforeach

                            <tr class="bg-purple">
                                <td colspan="5" class="text-center">
                                    Total Sudah Dicairkan Rp. {{ GeneralHelper::rupiah($total) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    @if ($booking->agent_commission > $booking->agent_commission_paid)
                        <div class="form-group">
                            <label>Tanggal Pencairan</label>
                            <input type="date" name="trnsct_date" class="form-control" value="{{ date('Y-m-d') }}">
                        </div>

                        <div class="form-group">
                            <label>Akun Kas & Bank</label>
                            <select name="payment_method" class="form-control" required>
                                <option value="">-- Pilih Akun --</option>
                                @foreach ($cashAccount as $account)
                                <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Sebesar</label>
                            <input
                                type="text"
                                name="amount"
                                class="form-control money"
                                required
                                value="{{ $booking->agent_commission - $booking->agent_commission_paid }}">
                        </div>
                    @endif
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    @if ($booking->agent_commission > $booking->agent_commission_paid)
                        <button type="submit" class="btn btn-success btn-save">Cairkan Sekarang</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
