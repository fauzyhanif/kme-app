<div class="modal" id="modal-cancel-disbursement">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Pembatalan Pencairan Komisi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="cancel_disbursement" action="{{ url('/travel/booking/cancel_pencairan_komisi') }}" method="POST">
                @csrf
                <input type="hidden" name="booking_id" value="">
                <input type="hidden" name="proof_id" value="">

                <!-- Modal body -->
                <div class="modal-body">
                    Yakin ingin membatalkan pencairan komisi?
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-save">Batalkan Sekarang</button>
                </div>
            </form>
        </div>
    </div>
</div>
