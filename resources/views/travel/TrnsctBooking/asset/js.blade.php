<script>


    $(document).ready(function () {
        var currentTime = new Date();
        // today
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;
        // First Date Of the month
        // var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
        // Last Date Of the Month
        var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
        $('#reservation').daterangepicker({
            startDate: today,
            endDate: startDateTo,
            locale : {
                format : 'YYYY-MM-DD'
            }
        })

        var startDateFormAdd = $('input[name="start_date_hidden"]').val();
        var endDateFormAdd = $('input[name="end_date_hidden"]').val();
        $('#reservation-form-add').daterangepicker({
            startDate: startDateFormAdd,
            endDate: endDateFormAdd,
            locale : {
                format : 'YYYY-MM-DD'
            }
        })

        $('#reservation-form-edit').daterangepicker({
            startDate: startDateFormAdd,
            endDate: endDateFormAdd,
            locale : {
                format : 'YYYY-MM-DD'
            }
        })

        $('.unit-price').keyup(function() {
            var unitPrice = this.value.replace(/\./g, '');
            var row = $(this).closest("tr");
            var qtyBooking = row.find('.qty-booking').val();
            var amountPrice = unitPrice * qtyBooking;
            row.find('.amount-price').val(formatRupiah(amountPrice));

            var sum = 0;
            $('.amount-price').each(function(){
                sum += parseFloat(this.value.replace(/\./g, ''));
            });

            $('input[name="ttl_trnsct"]').val(formatRupiah(sum))
        });

        $('.qty-booking').keyup(function() {
            var qtyBooking = this.value.replace(/\./g, '');
            var row = $(this).closest("tr");
            var unitPrice = row.find('.unit-price').val().replace(/\./g, '');
            var amountPrice = unitPrice * qtyBooking;
            row.find('.amount-price').val(formatRupiah(amountPrice));

            var sum = 0;
            $('.amount-price').each(function(){
                sum += parseFloat(this.value.replace(/\./g, ''));
            });

            $('input[name="ttl_trnsct"]').val(formatRupiah(sum))
        });

        /* Cancel trnsct */
        $('input[name="money_refundable_plan"]').keyup(function() {
            var moneyRefundablePlan = this.value.replace(/\./g, '');
            var trnsctPaid = $('input[name="trnsct_paid"]').val();
            var refundCut = 0;

            refundCut = trnsctPaid - moneyRefundablePlan;

            $('input[name="refund_cut"]').val(formatRupiah(refundCut))
        });

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $("#select-customer").select2({
            theme: 'bootstrap4',
            ajax: {
            url: "{{route('travel.booking.get-old-customer')}}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                _token: CSRF_TOKEN,
                search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });

        $("#select-agent").select2({
            theme: 'bootstrap4',
            ajax: {
            url: "{{route('travel.booking.get-agent')}}",
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                _token: CSRF_TOKEN,
                search: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
            }
        });
    });

    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('form[name="search_cars"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses Pencarian...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Cari Kendaraan");
            },
            success: function (response) {
                $('#display-result-from-search-cars').html( response );
            }
            });
        });

        $('form[name="add_booking"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    document.getElementById("btn-save").disabled = true;
                    $(".btn-save").text("Proses...");
                },
                complete: function() {
                    document.getElementById("btn-save").disabled = false;
                    $(".btn-save").text("Simpan Booking");
                },
                success: function (response) {
                    var urlDetailBooking = {!! json_encode(url('/travel/booking/detail/')) !!};
                    if (response.type == "success") {
                        toastr.success(response.text);
                        if (response.page == 'store') {

                            // redirect to detail
                            var detailUrl = {!! json_encode(url('/travel/booking/detail')) !!};
                            var detailUrl = detailUrl + "/" + response.booking_id;
                            document.location.href = detailUrl;

                            // print payment
                            var basePrintPayment = {!! json_encode(url('/travel/payment/print')) !!};
                            var urlPrintPayment = basePrintPayment + "/" + response.payment_id;
                            window.open(urlPrintPayment, '_blank');

                        }
                    } else if (response.type == 'success_with_no_payment') {
                        // redirect to detail
                        var detailUrl = {!! json_encode(url('/travel/booking/detail')) !!};
                        var detailUrl = detailUrl + "/" + response.booking_id;
                        document.location.href = detailUrl;
                    } else {
                        toastr.error(response.text)
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            });
        });

        $('form[name="form_pencairan"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-save").addClass("disabled");
                    $(".btn-save").text("Proses...");
                },
                complete: function() {
                    $(".btn-save").removeClass("disabled");
                    $(".btn-save").text("Cairkan Sekarang");
                },
                success: function (response) {
                    if (response.type == "success") {
                        toastr.success(response.text);
                        $('#modal-pencairan').modal('hide');

                        // redirect to detail
                        var detailUrl = {!! json_encode(url('/travel/booking/detail')) !!};
                        var detailUrl = detailUrl + "/" + response.booking_id;
                        document.location.href = detailUrl;
                    } else {
                        toastr.error(response.text)
                    }
                }
            });
        });

        $('form[name="form_pembatalan"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-save").addClass("disabled");
                    $(".btn-save").text("Proses...");
                },
                complete: function() {
                    $(".btn-save").removeClass("disabled");
                    $(".btn-save").text("Batalkan Sekarang");
                },
                success: function (response) {
                    if (response.type == "success") {
                        toastr.success(response.text);
                        $('#modal-cancel').modal('hide');
                        $('.datatable-index-page').DataTable().ajax.reload(null, true);

                        // print payment
                        var basePrintPayment = {!! json_encode(url('/travel/booking/print_cancel')) !!};
                        var urlPrintPayment = basePrintPayment + "/" + response.payment_id;
                        window.open(urlPrintPayment, '_blank');

                    } else {
                        toastr.error(response.text)
                    }
                }
            });
        });

        $('form[name="form_delete_booking"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    if (response.type == "success") {
                        toastr.success(response.text);
                        $('#modal-delete-booking').modal('hide');
                        $('.datatable-index-page').DataTable().ajax.reload(null, true);
                    } else {
                        toastr.error(response.text)
                    }
                }
            });
        });

        $('form[name="delete_spj"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    if (response.status == "success") {
                        toastr.success(response.message);
                        location.reload();
                    } else {
                        toastr.error(response.message)
                    }
                }
            });
        });

        $('form[name="cancel_disbursement"]').on('submit', function(e) {
            e.preventDefault();
            var form    = $(this);
            var url     = form.prop('action');

            $.ajax({
                type: 'POST',
                url: url,
                dataType:'html',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".btn-save").addClass("disabled");
                    $(".btn-save").text("Proses Pembatalan...");
                },
                complete: function() {
                    $(".btn-save").removeClass("disabled");
                    $(".btn-save").text("Batalkan Sekarang");
                },
                success: function (response) {
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // Handle error
                    console.log('Error:', textStatus, errorThrown);
                    alert('An error occurred: ' + textStatus);
                }
            });
        });

    })();

    $('#reservation-form-add').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate.format('YYYY-MM-DD');
        var endDate = picker.endDate.format('YYYY-MM-DD');
        var url = "{{ route('travel.booking.search.cars') }}";
        var data = "page=FORM_ADD&date="+startDate + " - " + endDate

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: data,
            beforeSend: function() {
                $('.btn-saving').html("Loading...");
            },
            success: function (response) {
                $('#display-result-from-search-cars').html( response );
            }
        });
    });

    $('#reservation-form-edit').on('apply.daterangepicker', function(ev, picker) {
        var startDate = picker.startDate.format('YYYY-MM-DD');
        var endDate = picker.endDate.format('YYYY-MM-DD');
        var bookingId = $('input[name="booking_id"]').val();
        var url = "{{ route('travel.booking.search.cars') }}";
        var data = "page=FORM_ADD&date="+startDate + " - " + endDate+'&booking_id='+bookingId;

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'html',
            data: data,
            beforeSend: function() {
                $('.btn-saving').html("Loading...");
            },
            success: function (response) {
                $('#display-result-from-search-cars').html( response );
            }
        });
    });

    function checkCustomer(value) {
        if (value == '0') {
            $('.new-customer').css("display", "block");
            $('.old-customer').css("display", "none");
        } else if(value == '1') {
            $('.new-customer').css("display", "none");
            $('.old-customer').css("display", "block");
        }
    }

    function bookingForm(value) {
        console.log(value)
        if (value == 'UMUM') {
            $('.agent-form').css('display', 'none');
        } else {
            $('.agent-form').css('display', 'block');
        }
    }

    function modalCancel(bookingId, paid, totalTrnsct, statusCancel) {
        $('input[name="status_cancel"]').val(statusCancel);
        $('input[name="booking_id"]').val(bookingId);

        if (statusCancel == 'NOT_CANCELED') {
            if (paid > 0) {
                $('#modal-cancel').modal('show');
                $('input[name="trnsct_paid"]').val(paid);
                $('#cancel-ttl-trnsct').text(formatRupiah(totalTrnsct));
                $('#cancel-ttl-paid').text(formatRupiah(paid));
            } else {
                $('#modal-delete-booking').modal('show');
            }
        } else {
            var url = "{{ route('travel.booking.get_detail_cancel') }}";
            var data = "bookingId="+bookingId;
            $.ajax({
                type: 'POST',
                url: url,
                dataType:'json',
                data: data,
                success: function (res) {
                    $('#modal-cancel-second').modal('show');
                    $('#cancel-by').text(res.cancel_by);
                    $('#cancel-ttl-refund').text(formatRupiah(res.money_refundable_plan));
                    $('#cancel-ttl-refund-paid').text(formatRupiah(res.money_return));
                    $('#cancel-ttl-rest').text(formatRupiah(res.money_refundable_plan - res.money_return));
                    $('input[name="refund_now"]').val(formatRupiah(res.money_refundable_plan - res.money_return));
                    $('input[name="status_cancel"]').val(statusCancel);
                    $('input[name="related_person"]').val(res.cancel_by);
                }
            });
        }
    }

    function showModalCancelDisbursement(bookingId, proofId) {
        var form = $('#modal-cancel-disbursement').modal('show');
        form.find('input[name="booking_id"]').val(bookingId);
        form.find('input[name="proof_id"]').val(proofId);
    }
</script>
