@php
    use Illuminate\Support\Facades\Session;
@endphp

<!DOCTYPE html>
<html>
    <head>
        <title>Tanda Terima Refund {{ $booking->booking_id }}</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('Component.cssPrint')
    </head>

    <body>
        <div class="container-fluid">
            @include('Component.kopSurat')

            <hr style="margin-top: -10px">

            <div class="row text-center">
                <div class="col-md-12">
                    <h4 class="font-weight-bold">TANDA TERIMA REFUND</h4>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-md-12">
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="25%">No Kwitansi</td>
                                <td width="75%">: <b>{{ $payment->proof_id }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Tgl. Pembayaran</td>
                                <td width="75%">:
                                    <b>
                                        @php
                                        echo date_format(date_create($payment->trnsct_date), 'd/m/Y H:i:s');
                                        @endphp
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">Telah Diserahkan Kepada</td>
                                <td width="75%">: <b>{{ $payment->related_person }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Sebesar</td>
                                <td width="75%">: <b>Rp. {{ GeneralHelper::rupiah($payment->credit) }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Terbilang</td>
                                <td width="75%">: <b class="font-italic">-</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Keterangan</td>
                                <td width="75%">: <b>{{ $payment->description }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Cara Pembayaran</td>
                                <td width="75%">: <b>{{ $payment->coa->name }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 pl-4">
                    <b class="pl-1">Untuk</b>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-12">
                    <table class="table borderless">
                        <tbody>
                            <tr>
                                <td width="25%">Kode Booking</td>
                                <td width="75%">: <b>{{ $booking->booking_id }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Jumlah Unit</td>
                                <td width="75%">: <b>{{ $booking->ttl_unit }} Unit</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Jenis Unit</td>
                                <td width="75%">:
                                    @php $jenisUnit = "" @endphp
                                    @foreach ($bookingDtl as $item)
                                        @php
                                            $jenisUnit .= $item->carCategory->name . "($item->unit_qty), ";
                                        @endphp
                                    @endforeach
                                    @php $jenisUnit = rtrim($jenisUnit, ", ") @endphp
                                    <b>{{ $jenisUnit }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">Tgl. Berangkat</td>
                                <td width="75%">:
                                    <b>
                                        {{ GeneralHelper::konversiTgl($booking->booking_start_date) }}
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">Tujuan</td>
                                <td width="75%">: <b>{{ $booking->destination }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Lokasi Penjemputan</td>
                                <td width="75%">: <b>{{ $booking->pick_up_location }}</b></td>
                            </tr>
                            <tr>
                                <td width="25%">Jam Penjemputan</td>
                                <td width="75%">: <b>{{ $booking->standby_time }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" style="margin-top: -10px">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead class="bg-info text-white">
                            <tr>
                                <th class="except">Total Refund</th>
                                <th class="except">Sudah Diserahkan</th>
                                <th class="except">Sisa Refund</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="except">Rp. {{ GeneralHelper::rupiah($booking->money_refundable_plan) }}</td>
                                <td class="except">Rp. {{ GeneralHelper::rupiah($booking->money_return) }}</td>
                                <td class="except bg-warning">Rp.
                                    {{ GeneralHelper::rupiah($booking->money_refundable_plan - $booking->money_return) }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead class="bg-info text-white">
                            <tr>
                                <th class="except" width="5%">No</th>
                                <th class="except" width="20%">No Kwitansi</th>
                                <th class="except" width="20%">Tgl Pembayaran</th>
                                <th class="except">Keterangan</th>
                                <th class="except">Nominal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($payments as $item)
                            <tr>
                                <td class="except">{{ $no++ }}</td>
                                <td class="except">{{ $item->proof_id }}</td>
                                <td class="except">{{ GeneralHelper::konversiTgl($item->trnsct_date, 'slash') }}</td>
                                <td class="except">{{ $item->description }}</td>
                                <td class="text-right except">Rp. {{ GeneralHelper::rupiah($item->credit) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" style="margin-top: -10px">
                <div class="col-md-12">
                    <ol>
                        <li>DP/Uang muka minimal 20% dari harga sewa bus.</li>
                        <li>Uang muka hangus bila pemesanan dibatalkan.</li>
                        <li>Pelunasan sewa maksimal H-3 dari tanggal keberangkatan.</li>
                        <li>Pembayaran melalui Bank BRI No. Rek. 0355-01-001297-56-6 a/n Cucup Yusuf.</li>
                        <li>Transfer ke <u>Selain</u> rekening tersebut diluar tanggung jawab kami.</li>
                        <li>Bukti kwitansi jangan sampai hilang.</li>
                    </ol>
                </div>
            </div>

            <div class="row" style="margin-top: -10px">
                <div class="col-md-6 text-center" style="margin-left: -400px;">
                    <br>
                    Pemerima,

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <p>({{ $payment->related_person }})</p>
                </div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl($payment->payment_date, 'ttd') }} <br>
                    Petugas,

                    <br>
                    <img src="{{ url('public/img/cap.png') }}"
                        style="max-width: 100px; padding-bottom: -20px !important; padding-left: -10px !important" alt="">
                    <img src="{{ url('public/img/ttd-yusuf.png') }}"
                        style="max-width: 70px; padding-bottom: -20px !important; padding-left: -50px !important" alt="">
                    <br>
                    <p>({{ Session::get('auth_nama') }})</p>
                </div>
            </div>
        </div>
    </body>
<html>
