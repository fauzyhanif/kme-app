<div class="form-group new-customer">
    <label>Nama Pelanggan <span class="text-red">*</span></label>
    <input
        type="text"
        class="form-control"
        name="cust_name"
        value="{{ $booking->cust_name }}">
</div>

<div class="form-group new-customer">
    <label>Nomor Telepon Pelanggan <span class="text-red">*</span></label>
    <input
        type="text"
        class="form-control"
        name="cust_phone_num"
        value="{{ $booking->cust_phone_num }}">
</div>

<div class="form-group new-customer">
    <label>Alamat Pelanggan <span class="text-red">*</span></label>
    <input
        type="text"
        class="form-control"
        name="cust_address"
        value="{{ $booking->cust_address }}">
</div>

<div class="form-group">
    <label>Tanggal Booking</label>
    <input
        type="text"
        name="date"
        class="form-control"
        id="reservation-form-edit"
        placeholder="Silahkan isi tanggal yang diinginkan">

    <input type="hidden" name="start_date_hidden" value="{{ $booking->booking_start_date }}">
    <input type="hidden" name="end_date_hidden" value="{{ $booking->booking_end_date }}">
</div>

<div class="form-group">
    <label>Tanggal Tidak Dijual</label>
    <input type="date" name="date_not_for_sale" class="form-control" value="{{ $booking->date_not_for_sale }}" required>
</div>

<div class="alert alert-danger alert-dismissible" id="alert-over-qty" style="display: none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5><i class="icon fas fa-ban"></i> Ooopss!</h5>
    Mohon maaf jumlah unit yang dipesan melebihi unit ready.
    Silahkan kurangi jumlah unit yang dipesan.
</div>

<div class="form-group" id="display-result-from-search-cars" style="overflow-x: scroll">
    <table class="table table-striped">
        <thead>
            <th>Kendaraan</th>
            <th>Unit Ready</th>
            <th width="20%">Unit Booking</th>
            <th width="20%">Harga</th>
            <th width="20%">Jumlah</th>
        </thead>
        <tbody>
            @foreach ($carReady as $car)
            @php
            $ttl_ready = 0;
            if(array_key_exists($car->category_id, $arrBooked)) {
                if(array_key_exists($car->category_id, $bookingItems)) {
                    $ttl_ready = ($car->ttl_ready - $arrBooked[$car->category_id]) + $bookingItems[$car->category_id]["category_id_$car->category_id"];
                } else {
                    $ttl_ready = $car->ttl_ready - $arrBooked[$car->category_id];
                }
            } else {
                $ttl_ready = $car->ttl_ready;
            }
            @endphp
            <tr>
                <td>
                    {{ $car->name }} <br>
                    <small class="text-muted">{{ $car->seat }} seat</small>
                </td>
                <td>{{ $ttl_ready }} Unit</td>
                <td>
                    <input type="hidden" name="ttl_ready" value="{{ $ttl_ready }}">
                    <input
                        type="number"
                        name="item_booking[{{ $car->category_id }}]"
                        class="form-control qty-booking"
                        @if(array_key_exists($car->category_id, $bookingItems))
                            value="{{ $bookingItems[$car->category_id]["category_id_$car->category_id"] }}"
                        @else
                            value="0"
                        @endif>
                </td>
                <td>
                    <input
                        type="text"
                        name="price[{{ $car->category_id }}]"
                        class="form-control unit-price rupiah money"
                        @if (array_key_exists($car->category_id, $bookingItems))
                            value="{{ $bookingItems[$car->category_id]["price_$car->category_id"] }}"
                        @else
                            value="0"
                        @endif>
                </td>
                <td>
                    <input
                        type="text"
                        name="total[{{ $car->category_id }}]"
                        class="form-control amount-price rupiah money"
                        @if (array_key_exists($car->category_id, $bookingItems))
                            value="{{ $bookingItems[$car->category_id]["total_$car->category_id"] }}"
                        @else
                            value="0"
                        @endif
                        readonly>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="form-group">
    <label>Tujuan <span class="text-red">*</span></label>
    <input
        type="text"
        name="destination"
        class="form-control"
        @isset($booking)
            value="{{ $booking->destination }}"
        @endisset>
</div>

<div class="form-group">
    <label>Lokasi Penjemputan <span class="text-red">*</span></label>
    <input
        type="text"
        name="pick_up_location"
        class="form-control"
        @isset($booking)
            value="{{ $booking->pick_up_location }}"
        @endisset>
</div>

<div class="form-group">
    <label>Stand By <span class="text-red">*</span></label>
    <input
        type="text"
        name="standby_time"
        class="form-control"
        @isset($booking)
            value="{{ $booking->standby_time }}"
        @endisset>
</div>

<div class="form-group">
    <label>Booking Dari <span class="text-red">*</span></label>
    <select name="booking_from" class="form-control" onchange="bookingForm(this.value)">
        <option @isset($booking) @if($booking->booking_from == 'UMUM') selected @endif @endisset>UMUM</option>
        <option @isset($booking) @if($booking->booking_from == 'AGENT') selected @endif @endisset>AGENT</option>
        <option @isset($booking) @if($booking->booking_from == 'BIRO') selected @endif @endisset>BIRO</option>
    </select>
</div>

<div class="form-group agent-form">
    <label>Nama Agen/Biro</label>
    <select name="agent_id" class="form-control" id="select-agent">
        @if ($booking->booking_from != 'UMUM' && $booking->agent_id != '')
            <option value="{{ $booking->agent_id }}">{{ $booking->agent->name }}</option>
        @else
            <option value="">-- Pilih Agen / Biro --</option>
        @endif
    </select>

    <span class="text-blue">
        *Jika Agen/Biro tidak ditemukan silahkan
        <a href="{{ route('travel.agent.formadd') }}" target="_blank">
            <u>KLIK DISINI</u>
        </a>
        untuk menambah agen/biro yang baru
    </span>
</div>

<div class="form-group agent-form" @if ($booking->booking_from != 'UMUM') style="display: block" @endif>
    <label>Komisi Agen / Biro</label>
    <input
        type="text"
        name="agent_commission"
        class="form-control money"
        value="{{ $booking->agent_commission }}"
        >
</div>

<div class="form-group">
    <label>Total Transaksi</label>
    <input
        type="text"
        name="ttl_trnsct"
        class="form-control money"
        @isset($booking)
            value="{{ $booking->ttl_trnsct }}"
        @endisset>
</div>

<div class="form-group">
    <label>Potongan</label>
    <input
        type="text"
        name="discount"
        class="form-control money"
        @isset($booking)
            value="{{ $booking->discount }}"
        @endisset>
</div>

<div class="form-group">
    <label>Keterangan</label>
    <textarea name="information" class="form-control">@isset($booking){{ $booking->information }}@endisset</textarea>
</div>
