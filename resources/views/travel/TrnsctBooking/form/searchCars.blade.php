@extends('index')

@section('content')
<section class="content-header">
    <h1>Input Booking</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-none">
                    <div class="card-body">
                        <form name="search_cars" action="{{ route('travel.booking.search.cars') }}" method="POST">
                            <div class="form-group">
                                <input type="text" name="date" class="form-control" id="reservation" placeholder="Silahkan isi tanggal yang diinginkan">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block btn-save">
                                Cari Kendaraan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="display-result-from-search-cars">
        </div>
    </div>
</section>

@include('travel.TrnsctBooking.asset.js')
@endsection
