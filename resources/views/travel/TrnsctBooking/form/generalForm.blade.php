<div class="form-group">
    <label>Tanggal Booking</label>
    <input type="text" name="date" class="form-control" id="reservation-form-add"
        placeholder="Silahkan isi tanggal yang diinginkan">

    <input type="hidden" name="start_date_hidden" value="{{ $startDate }}">
    <input type="hidden" name="end_date_hidden" value="{{ $endDate }}">
</div>

<div class="form-group">
    <label>Tanggal Tidak Dijual</label>
    <input type="date" name="date_not_for_sale" class="form-control" value="{{ $dateNotForSale }}" required>
</div>

<div class="alert alert-danger alert-dismissible" id="alert-over-qty" style="display: none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5><i class="icon fas fa-ban"></i> Ooopss!</h5>
    Mohon maaf jumlah unit yang dipesan melebihi unit ready.
    Silahkan kurangi jumlah unit yang dipesan.
</div>

<div class="form-group" id="display-result-from-search-cars" style="overflow-x: scroll">
    <table class="table table-striped">
        <thead>
        <th>Kendaraan</th>
        <th>Unit Ready</th>
        <th width="20%">Unit Booking</th>
        <th width="20%">Harga</th>
        <th width="20%">Jumlah</th>
        </thead>
        <tbody>
            @foreach ($carReady as $car)
            @php
                $ttl_ready = 0;
                if(array_key_exists($car->category_id, $arrBooked)) {
                    $ttl_ready = $car->ttl_ready - $arrBooked[$car->category_id];
                } else {
                    $ttl_ready = $car->ttl_ready;
                }
            @endphp
            <tr
                @if (array_key_exists($car->category_id, $bookingItems))
                    @if ($bookingItems[$car->category_id] != '0')
                        style='background-color: #a9f1df'
                    @endif
                @endif
            >
                <td>
                    {{ $car->name }} <br>
                    <small class="text-muted">{{ $car->seat }} seat</small>
                </td>
                <td>{{ $ttl_ready }} Unit</td>
                <td>
                    <input type="hidden" name="ttl_ready" value="{{ $ttl_ready }}">
                    <input
                        type="number"
                        name="item_booking[{{ $car->category_id }}]"
                        class="form-control qty-booking"
                        @if (array_key_exists($car->category_id, $bookingItems))
                            value="{{ $bookingItems[$car->category_id] }}"
                        @else
                            value="0"
                        @endif>
                </td>
                <td>
                    <input
                        type="text"
                        name="price[{{ $car->category_id }}]"
                        class="form-control unit-price rupiah money"
                        value="0">
                </td>
                <td>
                    <input
                        type="text"
                        name="total[{{ $car->category_id }}]"
                        class="form-control amount-price rupiah money"
                        value="0"
                        readonly>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="form-group">
    <label>Tujuan <span class="text-red">*</span></label>
    <input type="text" name="destination" class="form-control">
</div>

<div class="form-group">
    <label>Lokasi Penjemputan <span class="text-red">*</span></label>
    <input type="text" name="pick_up_location" class="form-control">
</div>

<div class="form-group">
    <label>Jam Penjemputan <span class="text-red">*</span></label>
    <input type="text" name="standby_time" class="form-control">
</div>

<div class="form-group">
    <label>Keterangan</label>
    <textarea name="information" class="form-control"></textarea>
</div>
