<div class="col-md-8">
    <div class="card shadow-none">
        <form action="{{ route('travel.booking.formadd') }}" method="POST">

        {{-- hidden data --}}
        @csrf
        <input type="hidden" name="booking_start_date" value="{{ $startDate }}">
        <input type="hidden" name="booking_end_date" value="{{ $endDate }}">

            <div class="card-header text-center">
                <h3 class="card-title">
                Daftar kendaraan siap pakai tanggal <b>{{ GeneralHelper::konversiTgl($startDate, 'slash') }} -
                    {{ GeneralHelper::konversiTgl($endDate, 'slash') }}</b>
                </h3>
            </div>
            <div class="card-body">
                <div class="alert alert-danger alert-dismissible" id="alert-over-qty" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-ban"></i> Ooopss!</h5>
                Mohon maaf jumlah unit yang dipesan melebihi unit ready.
                Silahkan kurangi jumlah unit yang dipesan.
                </div>

                <table class="table table-hover table-bordered">
                    <thead class="bg-info">
                        <th>Jenis Kendaraan</th>
                        <th>Jumlah Unit Ready</th>
                        <th width="15%">Jml Booking</th>
                    </thead>
                    <tbody>
                        @foreach ($carReady as $car)
                        @php
                            $ttl_ready = 0;
                            if(array_key_exists($car->category_id, $arrBooked)) {
                                $ttl_ready = $car->ttl_ready - $arrBooked[$car->category_id];
                            } else {
                                $ttl_ready = $car->ttl_ready;
                            }
                        @endphp
                        <tr>
                            <td>
                                {{ $car->name }} <br>
                                <small class="text-muted">{{ $car->seat }} seat</small>
                            </td>
                            <td>
                                {{ $ttl_ready }} Unit
                            </td>
                            <td>
                                <input type="hidden" name="ttl_ready" value="{{ $ttl_ready }}">
                                <input
                                    type="number"
                                    name="booking_item[{{ $car->category_id }}]"
                                    class="form-control qty-booking"
                                    @if ($ttl_ready <=0) disabled @endif
                                    value="0">
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                @if ($isPublic == 'N')
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-success btn-lg btn-block" id="btn-save">
                            Lanjutkan Booking &nbsp;
                            <i class="fas fa-long-arrow-alt-right"></i>
                        </button>
                    </div>
                @endif
            </div>
        </form>
    </div>
</div>

<script>
    $(function() {
        $('.qty-booking').keyup(function() {
            var qtyBooking = this.value;
            var row = $(this).closest("tr");
            var qtyReady = row.find('input[name="ttl_ready"]').val();
            if (qtyBooking >= qtyReady + 1) {
                console.log("lebih")
                $('#btn-save').attr('disabled', true);
                $('#alert-over-qty').css('display', 'block');
            } else {
                console.log("kurang")
                $('#btn-save').attr('disabled', false);
                $('#alert-over-qty').css('display', 'none');
            }
        });
    });
</script>
