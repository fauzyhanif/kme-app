<div class="modal" id="form-delete-spj-{{ $item->id }}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Tabungan Awal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="delete_spj" action="{{ route('travel.spj.delete') }}" method="POST">
                @csrf
                <input type="hidden" name="spj_id" value="{{ $item->spj_id }}">

                <!-- Modal body -->
                <div class="modal-body">
                    <h5>Yakin ingin menghapus SPJ ini?</h5>
                    <br>
                    Jika ya maka pencatatan SPJ yang ada di operasional dan tabungan premi baik driver atau co-driver akan terhapus.
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Hapus</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
