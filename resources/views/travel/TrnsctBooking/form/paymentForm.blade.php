<style>
    .agent-form {
        display: none;
    }
</style>

<div class="form-group">
    <label>Tanggal Pembayaran</label>
    <input type="date" name="payment_date" class="form-control" value="{{ date('Y-m-d') }}">
</div>

<div class="form-group">
    <label>Booking Dari <span class="text-red">*</span></label>
    <select name="booking_from" class="form-control" onchange="bookingForm(this.value)">
        <option>UMUM</option>
        <option>AGENT</option>
        <option>BIRO</option>
    </select>
</div>

<div class="form-group agent-form">
    <label>Silahkan cari Agen/Biro</label>
    <select name="agent_id" class="form-control" id="select-agent">

    </select>

    <span class="text-blue">
        *Jika Agen/Biro tidak ditemukan silahkan
        <a href="{{ route('travel.agent.formadd') }}" target="_blank">
            <u>KLIK DISINI</u>
        </a>
        untuk menambah agen/biro yang baru
    </span>
</div>


<div class="form-group agent-form">
    <label>Komisi Agen / Biro</label>
    <input type="text" name="agent_commission" class="form-control money" value="0">
</div>

<div class="form-group">
    <label>Total Tagihan</label>
    <input type="text" name="ttl_trnsct" class="form-control money" value="0" readonly>
</div>

<div class="form-group">
    <label>Potongan</label>
    <input type="text" name="discount" class="form-control money" value="0">
</div>

<div class="form-group">
    <label>Jumlah Uang Muka <span class="text-red">*</span></label>
    <input type="text" name="ttl_trnsct_paid" class="form-control money" value="0">
</div>

<div class="form-group">
    <label>Pembayaran Masuk Ke <span class="text-red">*</span></label>
    <select name="payment_method" class="form-control">
        @foreach ($cashAccount as $account)
        <option value="{{ $account->account_id }}">{{ $account->name }}</option>
        @endforeach
    </select>
</div>
