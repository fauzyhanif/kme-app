@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.booking') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Booking
            </a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ url('travel/booking/detail', $booking->booking_id) }}">
                Detail Booking
            </a>
        </li>
        <li class="breadcrumb-item active">Update Booking</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form name="add_booking" action="{{ route('travel.booking.edit') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="booking_id" value="{{ $booking->booking_id }}">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('travel.TrnsctBooking.form.generalUpdateForm')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success" id="btn-save">
                                Simpan Perubahan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctBooking.asset.js')
@endsection
