@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.booking.search.cars.page') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Cari Kendaraan
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Booking Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <form name="add_booking" action="{{ route('travel.booking.addnew') }}" method="POST" data-remote>
            @csrf
            <div class="row justify-content-center mb-2">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title m-0">Data Pelanggan</h5>
                        </div>
                        <div class="card-body">
                            @include('travel.TrnsctBooking.form.customerForm')
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title m-0">Data Booking</h5>
                        </div>
                        <div class="card-body">
                            @include('travel.TrnsctBooking.form.generalForm')
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title m-0">Data Pembayaran</h5>
                        </div>
                        <div class="card-body">
                            @include('travel.TrnsctBooking.form.paymentForm')
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 mb-4">
                    <button type="submit" class="btn btn-success btn-block btn-save" id="btn-save">
                        Simpan Booking
                    </button>
                    <a href="{{ url('/travel/booking/search-cars-page') }}" class="btn btn-secondary btn-back btn-block" style="display: none">
                        Kembali
                    </a>
                </div>
            </div>
        </form>

    </div>
</section>

@include('travel.TrnsctBooking.asset.js')
@endsection
