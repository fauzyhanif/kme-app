<table class="table table-striped">
    <thead>
        <th>Kendaraan</th>
        <th>Unit Ready</th>
        <th width="20%">Unit Booking</th>
        <th width="20%">Harga</th>
        <th width="20%">Jumlah</th>
    </thead>
    <tbody>
        @foreach ($carReady as $car)
        @php
            $ttl_ready = 0;
            if(array_key_exists($car->category_id, $arrBooked)) {
                $ttl_ready = $car->ttl_ready - $arrBooked[$car->category_id];
            } else {
                $ttl_ready = $car->ttl_ready;
            }
        @endphp

        <tr>
            <td>
                {{ $car->name }} <br>
                <small class="text-muted">{{ $car->seat }} seat</small>
            </td>
            <td>{{ $ttl_ready }} Unit</td>
            <td>
                <input type="hidden" name="ttl_ready" value="{{ $ttl_ready }}">
                <input
                    type="number"
                    name="item_booking[{{ $car->category_id }}]"
                    class="form-control qty-booking"
                    value="0">
            </td>
            <td>
                <input type="text" name="price[{{ $car->category_id }}]" class="form-control unit-price money" value="0">
            </td>
            <td>
                <input type="text" name="total[{{ $car->category_id }}]" class="form-control amount-price money" value="0" readonly>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
$(function() {
    $('.money').mask('000.000.000.000.000', {reverse: true});

    $('.qty-booking').keyup(function() {
        var qtyBooking = this.value.replace(/\./g, '');
        var row = $(this).closest("tr");
        var unitPrice = row.find('.unit-price').val().replace(/\./g, '');
        var amountPrice = unitPrice * qtyBooking;
        row.find('.amount-price').val(formatRupiah(amountPrice));

        var sum = 0;
        $('.amount-price').each(function(){
            sum += parseFloat(this.value.replace(/\./g, ''));
        });

        $('input[name="ttl_trnsct"]').val(formatRupiah(sum))
    });

    $('.unit-price').keyup(function() {
        var unitPrice = this.value.replace(/\./g, '');
        var row = $(this).closest("tr");
        var qtyBooking = row.find('.qty-booking').val();
        var amountPrice = unitPrice * qtyBooking;
        row.find('.amount-price').val(formatRupiah(amountPrice));

        var sum = 0;
        $('.amount-price').each(function(){
            sum += parseFloat(this.value.replace(/\./g, ''));
        });

        $('input[name="ttl_trnsct"]').val(formatRupiah(sum))
    });
});

</script>
