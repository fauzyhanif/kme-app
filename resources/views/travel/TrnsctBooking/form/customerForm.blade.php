<div class="form-group">
    <label>Sudah pernah kesini?</label>
    <br>
    <input type="radio" name="sudah_pernah" value="0" checked onclick="checkCustomer('0')"> Belum
    <br>
    <input type="radio" name="sudah_pernah" value="1" onclick="checkCustomer('1')"> Sudah
</div>

<div class="form-group old-customer" style="width: 100%; display: none;">
    <label>Silahkan cari customer</label>
    <select name="cust_id" class="form-control" id="select-customer" onchange="testtt(this.value)">

    </select>
</div>

<div class="form-group new-customer">
    <label>Nama Pelanggan <span class="text-red">*</span></label>
    <input
        type="text"
        class="form-control"
        name="cust_name"
        @isset($booking)
            value="{{ $booking->cust_name }}"
            readonly
        @endisset>
</div>

<div class="form-group new-customer">
    <label>Nomor Telepon <span class="text-red">*</span></label>
    <input
        type="text"
        class="form-control"
        name="cust_phone_num"
        @isset($booking)
            value="{{ $booking->cust_phone_num }}"
            readonly
        @endisset>
</div>

<div class="form-group new-customer">
    <label>Alamat <span class="text-red">*</span></label>
    <input
        type="text"
        class="form-control"
        name="cust_address"
        @isset($booking)
            value="{{ $booking->cust_address }}"
            readonly
        @endisset>
</div>
