<div class="modal" id="form-initial-saving">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Form Tabungan Awal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form name="initial_saving" action="{{ route('travel.driver.initial-saving') }}" method="POST">
                <input type="hidden" name="driver_id" value="{{ $driver->driver_id }}">

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Jumlah Tabungan Awal</label>
                        <input type="text" name="in" required class="form-control money" value="0">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
