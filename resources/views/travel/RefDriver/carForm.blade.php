<div class="form-group">
    <label>Keterangan <span class="text-red">*</span></label>
    <select name="driver_type" class="form-control">
        <option
            @isset($driver)
                @if ($driver->driver_type == 'UTAMA')
                    selected
                @endif
            @endisset
        >
            UTAMA
        </option>
        <option
            @isset($driver)
                @if ($driver->driver_type == 'CADANGAN')
                    selected
                @endif
            @endisset
        >
            CADANGAN
        </option>
    </select>
</div>

<div class="form-group">
    <label>Kendaraan yang dipegang <span class="text-red">*</span></label>
    <select name="police_num" class="form-control">
        <option value="">** Silahkan pilih mobil</option>
        @foreach ($cars as $car)
            <option
                value="{{ $car->police_num }}"
                @isset($driver)
                    @if ($driver->police_num == $car->police_num)
                        selected
                    @endif
                @endisset
            >
                {{ $car->police_num }} ({{ $car->carCategory->name }})
            </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Diberikan izin membawa kendaraan <span class="text-red">*</span></label> <br>
    @isset($driver)
    @php
        $arrCar = json_decode($driver->car_permit);
    @endphp
    @endisset
    @foreach ($categories as $category)
        <input
            type="checkbox"
            name="car_permit[]"
            value="{{ $category->category_id }}"
            @isset($driver)
                @if (in_array($category->category_id, $arrCar)) checked @endif
            @endisset
        >
        {{ $category->name }} <br>
    @endforeach
</div>
