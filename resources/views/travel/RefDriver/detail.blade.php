@extends('index')

@section('content')
<section class="content-header">
	<div class="row">
		<div class="col-md-6">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="{{ route('travel.driver') }}">
						<i class="fas fa-long-arrow-alt-left"></i> &nbsp;
						Daftar Driver
					</a>
				</li>
				<li class="breadcrumb-item active">Detail Driver</li>
			</ol>
		</div>
		<div class="col-md-6">
			<div class="float-right">
				<a href="{{ url('/travel/driver/form-edit',  $driver->driver_id) }}" class="btn btn-primary btn-sm">
					<div class="fas fa-edit"></div> Edit Data
				</a>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
				<div class="card shadow-none">
					<div class="card-header">
						<h3 class="card-title">
							Detail Driver
						</h3>
					</div>
					<div class="card-body">
						<table class="table table-sm table-borderless">
							<tr>
								<td class="text-secondary" width="30%">Nama Driver</td>
								<td class="text-right" width="3%">:</td>
								<td width="67%">{{ $driver->name }}</td>
							</tr>
							<tr>
								<td class="text-secondary" width="30%">No Telp / Hp </td>
								<td class="text-right" width="3%">:</td>
								<td width="67%">{{ $driver->phone_num }}</td>
							</tr>
							<tr>
								<td class="text-secondary" width="30%">No SIM </td>
								<td class="text-right" width="3%">:</td>
								<td width="67%">{{ $driver->sim_num }}</td>
							</tr>
							<tr>
								<td class="text-secondary" width="30%">Alamat</td>
								<td class="text-right" width="3%">:</td>
								<td width="67%">{{ $driver->address }}</td>
							</tr>
							<tr>
								<td class="text-secondary" width="30%">Pegang Kendaraan</td>
								<td class="text-right" width="3%">:</td>
								<td width="67%">{{ $driver->police_num }} ({{ $driver->car->carCategory->name }})</b></td>
							</tr>
							<tr>
								<td class="text-secondary" width="30%">Status</td>
								<td class="text-right" width="3%">:</td>
								<td width="67%">{{ $driver->driver_type }}</td>
							</tr>
							<tr>
								<td class="text-secondary" width="30%">Izin membawa kendaraan</td>
								<td class="text-right" width="3%">:</td>
								<td width="67%">
									@php $arrCategory = json_decode($driver->car_permit) @endphp
									@foreach ($categories as $category)
										@if (in_array($category->category_id, $arrCategory))
											<label class="badge badge-default">{{ $category->name }}</label>
										@endif
									@endforeach
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-6 col-12">
						<!-- small box -->
						<div class="small-box bg-info">
							<div class="inner">
								<h3>{{ $driver->ttl_trnsct }}x</h3>

								<p>Total Jalan</p>
							</div>
							<div class="icon">
								<i class="fas fa-car"></i>
							</div>
						</div>
					</div>

					<div class="col-lg-6 col-12">
						<!-- small box -->
						<div class="small-box bg-warning">
							<div class="inner">
								<h3>Rp. {{ GeneralHelper::rupiah($driver->ttl_saving) }}</h3>

								<p>Total Tabungan Premi</p>
							</div>
							<div class="icon">
								<i class="fas fa-wallet"></i>
							</div>
						</div>
					</div>
				</div>

                <div class="row">
					<div class="col-md-12">
						<div class="card shadow-none">
							<div class="card-header">
								<h3 class="card-title">Daftar Tabungan Premi</h3>
								<div class="card-tools">
									<a href="{{ route('travel.driver.alldriversaving', $driver->driver_id) }}" class="text-gray mr-3">
										<u>Lihat Selengkapnya</u>
									</a>
									<button class="btn btn-success btn-sm"  data-toggle="modal" data-target="#form-initial-saving">
										<i class="fas fa-wallet"></i> &nbsp;
										Tabungan Awal
									</button>
									<button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#myModal">
										<i class="fas fa-wallet"></i> &nbsp;
										Ambil Tabungan
									</button>
								</div>
							</div>
							<div class="card-body scroll-x">
								<table class="table table-bordered">
									<thead class="bg-info">
										<th>No</th>
										<th>Tgl Transaksi</th>
										<th>Keterangan</th>
										<th>Masuk</th>
										<th>Keluar</th>
										<th width="10%">Hapus</th>
									</thead>
									<tbody id="display-driver-saving-data">
										<tr>
											<td class="text-center" colspan="5">Loading...</td>
										</tr>
									</tbody>
								</table>

								<div class="mt-4">
									<div class="alert alert-default-warning">
										Data yang bisa dihapus hanya data Tabungan Awal
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">Daftar Perjalan</h3>

								<div class="card-tools">
									<a href="{{ route('travel.driver.all-trnsct-page', $driver->driver_id) }}" class="text-gray mr-3">
										<u>Lihat Selengkapnya</u>
									</a>
								</div>
							</div>
							<div class="card-body scroll-x">
								<table class="table table-bordered">
									<thead class="bg-info">
										<th>No</th>
										<th>Kode Booking</th>
										<th>Tujuan</th>
										<th>Tgl Berangkat</th>
									</thead>
									<tbody id="display-history-trnsct">
										<tr>
											<td colspan="4" class="text-center">Loading...</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

{{-- parameter data for load driver saving --}}
<input type="hidden" id="user-type" value="DRIVER">
<input type="hidden" id="saving-column" value="user_id">
<input type="hidden" id="trnsct-column" value="main_driver">
<input type="hidden" id="value" value="{{ $driver->driver_id }}">

@include('travel.RefDriver.formDeleteSaving')
@include('travel.RefDriver.savingsRetrievalForm')
@include('travel.RefDriver.formInitialSaving')
@include('travel.RefDriver.asset.js')
@endsection
