@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.driver.detail', $driver->driver_id) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Detail Driver
            </a>
        </li>
        <li class="breadcrumb-item active">Data Tabungan Driver</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        Jumlah Tabungan <b>{{ $driver->name }}</b> : <b>Rp. {{ GeneralHelper::rupiah($driver->ttl_saving) }}</b>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Mutasi Tabungan Driver {{ $driver->name }}
                        </h3>

                        <div class="card-tools">
                            <button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#myModal">
                                <i class="fas fa-edit"></i> &nbsp;
                                Ambil Tabungan
                            </button>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered" id="datatable-driver-saving-page">
                            <thead class="bg-info">
                                <th>Tgl Transaksi</th>
                                <th>Keterangan</th>
                                <th>Masuk</th>
                                <th>Keluar</th>
                                <th width="10%">Hapus</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- parameter data for load driver saving --}}
<input type="hidden" id="driver-id-for-saving-page" value="{{ $driver->driver_id }}">

@include('travel.RefDriver.formDeleteSaving')
@include('travel.RefDriver.savingsRetrievalForm')
@include('travel.RefDriver.asset.js')
@endsection
