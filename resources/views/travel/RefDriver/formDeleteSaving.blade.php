<div class="modal" id="form-delete-saving">
  <div class="modal-dialog">
    <div class="modal-content">
      <form name="delete_initial_saving" action="{{ url('/travel/driver/delete-initial-saving') }}" method="POST">
        @csrf
        <input type="hidden" name="id">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Hapus Data</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <span>Yakin ingin menghapus data ini?</span>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
        </div>

      </form>
    </div>
  </div>
</div>
