<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($driver)
            value="{{ $driver->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>SIM (Surat Izin Mengemudi)</label>
    <input
        type="text"
        name="sim_num"
        class="form-control"
        @isset($driver)
            value="{{ $driver->sim_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($driver)
            value="{{ $driver->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat</label>
    <textarea name="address" class="form-control">@isset($driver){{ $driver->address }}@endisset
    </textarea>
</div>
