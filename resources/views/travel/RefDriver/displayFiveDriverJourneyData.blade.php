@if (count($journeys) > 0)
    @php
        $no = 1;
    @endphp
    @foreach ($journeys as $journey)
    <tr>
        <td>{{ $no }}.</td>
        <td>{{ $journey->booking_id }}</td>
        <td>{{ $journey->booking->destination }}</td>
        <td>{{ GeneralHelper::konversiTgl($journey->booking->booking_start_date) }}</td>
    </tr>
    @php
        $no += 1;
    @endphp
    @endforeach
@else
    <tr>
        <td class="text-center" colspan="4">Tidak ada data perjalanan</td>
    </tr>
@endif

