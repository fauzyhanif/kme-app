@extends('index')

@section('content')
<section class="content-header">
    <h1>Driver</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Driver
                        <div class="card-tools">
                            <a href="{{ route('travel.driver.formadd') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Driver Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th width="20%">Nama</th>
                                <th>No HP</th>
                                <th>Alamat</th>
                                <th width="15%">Tipe Driver</th>
                                <th width="17%">Kendaraan</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('travel.RefDriver.asset.js')
@endsection

