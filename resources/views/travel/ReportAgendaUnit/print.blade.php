<!DOCTYPE html>
<html>

    <head>
        <title>Laporan Pemasukan Bulanan</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        @include('Component.cssPrint')
    </head>

    <body>
        <div class="container-fluid">
            @include('Component.kopSurat')

            <hr style="margin-top: -10px">

            <div class="row text-center">
                <div class="col-md-12">
                    <p class="font-weight-bold" style="font-size: 13px">
                        AGENDA ARMADA <br>
                        BULAN {{ strtoupper(GeneralHelper::month($month) . ' ' . $year) }} <br>
                        {{ strtoupper($categoryName) }}
                    </p>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="font-weight-bold small-body">Tanggal</td>
                                <td class="font-weight-bold small-body" width="9%">No Unit</td>
                                <td class="font-weight-bold small-body">Driver</td>
                                <td class="font-weight-bold small-body">Dari</td>
                                <td class="font-weight-bold small-body">Jam</td>
                                <td class="font-weight-bold small-body">Tujuan</td>
                                <td class="font-weight-bold small-body">Konsumen</td>
                                <td class="font-weight-bold small-body">No. Handphone</td>
                                <td class="font-weight-bold small-body text-right">Harga</td>
                                <td class="font-weight-bold small-body text-right">Kas Jalan</td>
                                <td class="font-weight-bold small-body text-right">Kas Kantor</td>
                            </tr>
                            @php $no = 1 @endphp
                            @php $total_harga = 0 @endphp
                            @php $total_kas_jalan = 0 @endphp
                            @php $total_kas_kantor = 0 @endphp

                            @foreach ($reports as $report)
                                <tr>
                                    <td class="small-body">{{ GeneralHelper::konversiTgl($report->booking_start_date, 'slash') }}</td>
                                    <td class="small-body">{{ $report->police_num }}</td>
                                    <td class="small-body">{{ $report->driver_nm }}</td>
                                    <td class="small-body">{{ $report->pick_up_location }}</td>
                                    <td class="small-body">{{ $report->standby_time }}</td>
                                    <td class="small-body">{{ $report->destination }}</td>
                                    <td class="small-body">{{ $report->cust_name }}</td>
                                    <td class="small-body">{{ $report->cust_phone_num }}</td>
                                    <td class="small-body text-right">{{ GeneralHelper::rupiah($report->harga) }}</td>
                                    <td class="small-body text-right">{{ GeneralHelper::rupiah($report->kas_jalan) }}</td>
                                    <td class="small-body text-right">{{ GeneralHelper::rupiah($report->harga - $report->kas_jalan) }}</td>
                                </tr>

                                @php $total_harga += $report->harga @endphp
                                @php $total_kas_jalan += $report->kas_jalan @endphp
                                @php $total_kas_kantor += $report->harga - $report->kas_jalan @endphp
                            @endforeach

                            <tr>
                                <td class="small-body text-right font-weight-bold" colspan="8">Total</td>
                                <td class="small-body text-right font-weight-bold">{{ GeneralHelper::rupiah($total_harga) }}</td>
                                <td class="small-body text-right font-weight-bold">{{ GeneralHelper::rupiah($total_kas_jalan) }}</td>
                                <td class="small-body text-right font-weight-bold">{{ GeneralHelper::rupiah($total_kas_kantor) }}</td>
                            </tr>


                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center" style="margin-left: 400px;">
                    Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                    Petugas,

                    <br>
                    <br>
                    <br>
                    <p>(Hj. Nani Inayah)</p>
                </div>
            </div>

        </div>
    </body>
</html>
