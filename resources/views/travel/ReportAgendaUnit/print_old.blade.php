<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Laporan Agenda Armada</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap 4 -->

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ url('public/admin-lte/dist/css/adminlte.min.css') }}">

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

        {{-- @include('Component.cssPrint') --}}
        <style>
            .kop-surat{
                border-bottom: 2px solid black;
            }

            @media print{@page {size: landscape}}
        </style>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row kop-surat">
                <div class="col-md-12 text-center">
                    <h1 class="text-bold">PT KARYA MAS EMPAT</h1>
                    <p>
                        Jl. Raya Rancaudik KM. 5 Tambakdahan 41253 Subang - Jawa Barat
                        <br>
                        Telp. (0260) 540153 - 552244 e-mail : karyamasempat@yahoo.co.id
                    </p>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <h3 class="text-bold">AGENDA ARMADA</h3>
                </div>
            </div>

            <div class="row mt-1">
                <div class="col-md-12">
                    <p class="text-bold">
                        BULAN {{ strtoupper(GeneralHelper::month($month) . ' ' . $year) }}
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                            <th>Tanggal</th>
                            <th>No Unit</th>
                            <th>Driver</th>
                            <th>Dari</th>
                            <th>Jam</th>
                            <th>Tujuan</th>
                            <th>Customer</th>
                            <th>No. HP</th>
                            <th>Harga</th>
                            <th>Kas Jalan</th>
                            <th>Kas Kantor</th>
                        </thead>
                        <tbody>
                            @foreach ($reports as $report)
                            <tr>
                                <td>{{ GeneralHelper::konversiTgl($report->booking_start_date, 'slash') }}</td>
                                <td>{{ $report->police_num }}</td>
                                <td>{{ $report->driver_nm }}</td>
                                <td>{{ $report->pick_up_location }}</td>
                                <td>{{ $report->standby_time }}</td>
                                <td>{{ $report->destination }}</td>
                                <td>{{ $report->cust_name }}</td>
                                <td>{{ $report->cust_phone_num }}</td>
                                <td>{{ GeneralHelper::rupiah($report->harga) }}</td>
                                <td>{{ GeneralHelper::rupiah($report->kas_jalan) }}</td>
                                <td>{{ GeneralHelper::rupiah($report->harga - $report->kas_jalan) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 text-center">Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }}</div>

                <div class="col-md-6 text-center">Mengetahui,</div>
                <div class="col-md-6 text-center"></div>

                <div class="col-md-6 text-center">Direktur PT. Karya Mas Empat,</div>
                <div class="col-md-6 text-center">Manager Oprasional,</div>
            </div>
            <br>
            <br>
            <div class="row mt-4">
                <div class="col-md-6 text-center">H. ADE NURCAYA</div>
                <div class="col-md-6 text-center">H. M. YUSUF</div>
            </div>
        </div>
    </body>

</html>
