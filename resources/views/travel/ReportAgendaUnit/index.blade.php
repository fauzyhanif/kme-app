@extends('index')

@section('content')
<section class="content-header">
    <h1>Laporan Agenda Armada</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header with-border">
                        <form action="" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <select name="year" class="form-control">
                                        @php
                                        $year_start = '2021';
                                        $year_end = date('Y');
                                        @endphp
                                        @for ($i = $year_end; $i >= $year_start; $i--)
                                        <option {{ ($year==$i) ? 'selected' : '' }}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="month" class="form-control">
                                        <option value="01" {{ ($month=='01' ) ? 'selected' : '' }}>Januari</option>
                                        <option value="02" {{ ($month=='02' ) ? 'selected' : '' }}>Februari</option>
                                        <option value="03" {{ ($month=='03' ) ? 'selected' : '' }}>Maret</option>
                                        <option value="04" {{ ($month=='04' ) ? 'selected' : '' }}>April</option>
                                        <option value="05" {{ ($month=='05' ) ? 'selected' : '' }}>Mei</option>
                                        <option value="06" {{ ($month=='06' ) ? 'selected' : '' }}>Juni</option>
                                        <option value="07" {{ ($month=='07' ) ? 'selected' : '' }}>Juli</option>
                                        <option value="08" {{ ($month=='08' ) ? 'selected' : '' }}>Agustus</option>
                                        <option value="09" {{ ($month=='09' ) ? 'selected' : '' }}>September</option>
                                        <option value="10" {{ ($month=='10' ) ? 'selected' : '' }}>Oktober</option>
                                        <option value="11" {{ ($month=='11' ) ? 'selected' : '' }}>November</option>
                                        <option value="12" {{ ($month=='12' ) ? 'selected' : '' }}>Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-print">Cetak</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <th>Tanggal</th>
                                <th>Unit</th>
                                <th>No Unit</th>
                                <th>Driver</th>
                                <th>Dari</th>
                                <th>Jam</th>
                                <th>Tujuan</th>
                                <th>Customer</th>
                                <th>No. HP</th>
                                <th>Harga</th>
                                <th>Kas Jalan</th>
                                <th>Kas Kantor</th>
                            </thead>
                            <tbody>
                                @foreach ($reports as $report)
                                <tr>
                                    <td>{{ GeneralHelper::konversiTgl($report->booking_start_date, 'slash') }}</td>
                                    <td>{{ $report->category_name }}</td>
                                    <td>{{ $report->police_num }}</td>
                                    <td>{{ $report->driver_nm }}</td>
                                    <td>{{ $report->pick_up_location }}</td>
                                    <td>{{ $report->standby_time }}</td>
                                    <td>{{ $report->destination }}</td>
                                    <td>{{ $report->cust_name }}</td>
                                    <td>{{ $report->cust_phone_num }}</td>
                                    <td>{{ GeneralHelper::rupiah($report->harga) }}</td>
                                    <td>{{ GeneralHelper::rupiah($report->kas_jalan) }}</td>
                                    <td>{{ GeneralHelper::rupiah($report->harga - $report->kas_jalan) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-print">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Cetak</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="{{ url('travel/agenda-armada') }}" method="POST" target="_blank">
                @csrf
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Bulan</label>
                        <select name="month" class="form-control">
                            <option value="01" {{ ($month=='01' ) ? 'selected' : '' }}>Januari</option>
                            <option value="02" {{ ($month=='02' ) ? 'selected' : '' }}>Februari</option>
                            <option value="03" {{ ($month=='03' ) ? 'selected' : '' }}>Maret</option>
                            <option value="04" {{ ($month=='04' ) ? 'selected' : '' }}>April</option>
                            <option value="05" {{ ($month=='05' ) ? 'selected' : '' }}>Mei</option>
                            <option value="06" {{ ($month=='06' ) ? 'selected' : '' }}>Juni</option>
                            <option value="07" {{ ($month=='07' ) ? 'selected' : '' }}>Juli</option>
                            <option value="08" {{ ($month=='08' ) ? 'selected' : '' }}>Agustus</option>
                            <option value="09" {{ ($month=='09' ) ? 'selected' : '' }}>September</option>
                            <option value="10" {{ ($month=='10' ) ? 'selected' : '' }}>Oktober</option>
                            <option value="11" {{ ($month=='11' ) ? 'selected' : '' }}>November</option>
                            <option value="12" {{ ($month=='12' ) ? 'selected' : '' }}>Desember</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Tahun</label>
                        <select name="year" class="form-control">
                            @php
                                $year_start = '2021';
                                $year_end = date('Y');
                            @endphp
                            @for ($i = $year_end; $i >= $year_start; $i--)
                                <option {{ ($year==$i) ? 'selected' : '' }}>{{ $i }}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Unit</label>
                        <select name="category_id" class="form-control">
                            <option value="">-- Pilih Unit --</option>
                            @foreach ($carCategories as $carCategory)
                                <option value="{{ $carCategory->category_id }}|{{ $carCategory->name }}">{{ $carCategory->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Cetak</button>
                </div>
            </form>

        </div>
    </div>
</div>

@include('travel.ReportAgendaUnit.asset.js')
@endsection
