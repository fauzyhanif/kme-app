<!DOCTYPE html>
<html>

<head>
    <title>Slip Pemasukan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @include('Component.cssPrint')
</head>

<body>
    <div class="container-fluid">
        @include('Component.kopSurat')

        <hr style="margin-top: -20px">

        <div class="row text-center">
            <div class="col-md-12">
                <h5 class="font-weight-bold">TERIMA UANG</h5>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-md-12">
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="17%">No Transaksi</td>
                            <td width="33%">:
                                <b>{{ $cashAccount->proof_id }}</b>
                            </td>
                            <td width="17%">Uang masuk ke</td>
                            <td width="33%">:
                                <b>{{ $cashAccount->coa->name }}</b>
                            </td>
                        </tr>
                        <tr>
                            <td width="17%">Tgl. Transaksi</td>
                            <td width="33%">:
                                <b>{{ GeneralHelper::konversiTgl($cashAccount->trnsct_date) }}</b>
                            </td>
                            <td width="17%">Terima dari</td>
                            <td width="33%">:
                                <b>{{ $cashAccount->related_person }}</b>
                            </td>
                        </tr>
                        <tr>
                            <td width="17%">Deskripsi</td>
                            <td colspan="3">:
                                <b>{{ $cashAccount->description }}</b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead class="bg-info">
                        <tr>
                            <th width="20%" class="except">Kode Penyetor</th>
                            <th class="except">Ket. Penyetor</th>
                            <th width="15%" class="except text-right">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $total = 0 @endphp
                        @foreach ($incomeAccount as $account)
                            <tr>
                                <td class="except">{{ $account->account_id }}</td>
                                <td class="except">{{ $account->coa->name }}</td>
                                <td class="except text-right">Rp. {{ GeneralHelper::rupiah($account->credit) }}</td>
                            </tr>
                        @php $total += $account->credit @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" class="except text-center text-bold">Total</td>
                            <td class="except text-right">Rp. {{ GeneralHelper::rupiah($total) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-center" style="margin-left: 400px;">
                Subang, {{ GeneralHelper::konversiTgl($cashAccount->trnsct_date, 'ttd') }} <br>
                Petugas,

                <br>
                <br>
                <br>
                <p>({{ Session::get('auth_nama') }})</p>
            </div>
        </div>

    </div>
</body>
