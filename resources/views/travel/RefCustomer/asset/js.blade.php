<script>
$(function() {
    loadData();
    loadDataDetailPage();
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }

                toastr.success(data)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function loadData() {
    var base = {!! json_encode(route('travel.customer.jsondata')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'name', name: 'name', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'address', name: 'address', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function loadDataDetailPage() {
    var custId = $('input[name="cust_id"]').val();
    var base = {!! json_encode(route('travel.customer.datatable-detail-page')) !!};
    $('.datatable-detail-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url": base,
            "type": "POST",
            "data": {
                "cust_id" : custId,
            }
        },
        columns: [
            { data: 'booking_id', name: 'booking_id', searchable: true },
            { data: 'destination', name: 'destination', searchable: true },
            { data: 'booking_date', name: 'booking_date', searchable: false },
        ]
    });
}

</script>
