@extends('index')

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('travel.customer') }}">
				<i class="fas fa-long-arrow-alt-left"></i> &nbsp;
				Daftar Customer
			</a>
		</li>
		<li class="breadcrumb-item active">Detail Customer</li>
	</ol>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">
							Detail customer
						</h3>
					</div>
					<div class="card-body">
						<div class="row mb-2">
							<div class="col-md-4">Nama</div>
							<div class="col-md-8">: <b>{{ $customer->name }}</b></div>
						</div>
						<div class="row mb-2">
							<div class="col-md-4">No Telp / Hp </div>
							<div class="col-md-8">: <b>{{ $customer->phone_num }}</b></div>
						</div>
						<div class="row mb-2">
							<div class="col-md-4">Alamat</div>
							<div class="col-md-8">: <b>{{ $customer->address }}</b></div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="row justify-content-center">
			<div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sejarah Transaksi</h3>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-hover datatable-detail-page">
                            <thead class="bg-info">
                                <th width="20%">Kode Booking</th>
                                <th>Tujuan</th>
                                <th>Tgl Berangkat</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>

{{-- for get data datatable --}}
<input type="hidden" name="cust_id" value="{{ $customer->cust_id }}">

@include('travel.RefCustomer.asset.js')
@endsection
