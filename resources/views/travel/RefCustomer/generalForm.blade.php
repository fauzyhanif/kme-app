<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($customer)
            value="{{ $customer->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($customer)
            value="{{ $customer->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat</label>
    <textarea name="address" class="form-control">@isset($customer){{ $customer->address }}@endisset
    </textarea>
</div>
