@if (count($services) > 0)
    @php
        $no = 1;
    @endphp
    @foreach ($services as $service)
    <tr>
        <td>{{ $no }}.</td>
        <td>{{ GeneralHelper::konversiTgl($service->repair_date, 'slash') }}</td>
        <td>{{ $service->repair_place }}</td>
        <td>{{ $service->information }}</td>
    </tr>
    @php
        $no += 1;
    @endphp
    @endforeach
@else
    <tr>
        <td class="text-center" colspan="4">Tidak ada data perbaikan</td>
    </tr>
@endif

