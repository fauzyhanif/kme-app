<div class="form-group">
    <label>Kategori Kendaraan <span class="text-red">*</span></label>
    <select name="category_id" class="form-control" required>
        <option value="">-- Pilih Kategori Kendaraan --</option>
        @foreach ($categories as $category)
        <option
            value="{{ $category->category_id }}"
            @isset($car)
                @if ($car->category_id == $category->category_id)
                    selected
                @endif
            @endisset
        >
            {{ $category->name . ' (' . $category->seat . ' seat)' }}
        </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>No Polisi <span class="text-red">*</span></label>
    <input
        type="text"
        name="police_num"
        class="form-control"
        required
        @isset($car)
            value="{{ $car->police_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Kondisi Kendaraan <span class="text-red">*</span></label>
    <select name="condition_id" class="form-control" required>
        <option value="">-- Pilih Kondisi --</option>
        @foreach ($conditions as $condition)
        <option
            value="{{ $condition->id }}"
            @isset($car)
                @if ($car->condition_id == $condition->id)
                    selected
                @endif
            @endisset
        >
            {{ $condition->name }}
        </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Pemilik <span class="text-red">*</span></label>
    <input
        type="text"
        name="owner"
        class="form-control"
        required
        @isset($car)
            value="{{ $car->owner }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat Pemilik</label>
    <textarea name="owners_addres" class="form-control">
        @isset($car)
            {{ $car->owners_addres }}
        @endisset
    </textarea>
</div>

<div class="form-group">
    <label>No BPKB <span class="text-red">*</span></label>
    <input
        type="text"
        name="bpkb_num"
        class="form-control"
        required
        @isset($car)
            value="{{ $car->bpkb_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Nominal Pajak <span class="text-red">*</span></label>
    <input
        type="text"
        name="nominal_tax"
        class="form-control money"
        required
        @isset($car)
            value="{{ $car->nominal_tax }}"
        @endisset
    >
</div>
