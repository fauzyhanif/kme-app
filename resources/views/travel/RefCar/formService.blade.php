<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form Tambah Data Perbaikan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <form name="add_car_service" action="{{ route('travel.kendaraan.service') }}" method="POST">

        <input type="hidden" name="police_num" value="{{ $car->police_num }}">

        <!-- Modal body -->
        <div class="modal-body">
          <div class="form-group">
            <label>Tanggal Perbaikan</label>
            <input type="date" name="repair_date" class="form-control">
          </div>

          <div class="form-group">
            <label>Tempat Perbaikan</label>
            <input type="text" name="repair_place" class="form-control">
          </div>

          <div class="form-group">
            <label>Keterangan</label>
            <input type="text" name="information" class="form-control">
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-save">Simpan Data Perbaikan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
