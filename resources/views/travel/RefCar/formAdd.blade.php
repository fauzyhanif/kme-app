@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.kendaraan') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Kendaraan
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Kendaraan Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow-none">
                    <div id="response-alert"></div>
                    <form id="form-add" action="{{ route('travel.kendaraan.addnew') }}" method="POST" data-remote>
                        @csrf

                        <div class="card-header">
                            <h3 class="card-title">
                                Form
                            </h3>
                        </div>
                        <div class="card-body">
                            @include('travel.RefCar.generalForm')

                            <div class="form-group bg-info text-center">
                                <p class="font-weight-bold p-2">BODY KENDARAAN</p>
                            </div>
                            @include('travel.RefCar.bodyForm')

                            <div class="form-group bg-info text-center">
                                <p class="font-weight-bold p-2">TANGGAL PENTING</p>
                            </div>
                            @include('travel.RefCar.importantDateForm')
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success btn-save">
                                Simpan
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.RefCar.asset.js')
@endsection
