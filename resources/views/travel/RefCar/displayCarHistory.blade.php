@if (count($histories) > 0)
    @php
        $no = 1;
    @endphp
    @foreach ($histories as $history)
    <tr>
        <td>{{ $no }}.</td>
        <td>{{ $history->booking_id }}</td>
        <td>{{ $history->booking->destination }}</td>
        <td>{{ GeneralHelper::konversiTgl($history->booking->booking_start_date, 'slash') }}</td>
    </tr>
    @php
        $no += 1;
    @endphp
    @endforeach
@else
    <tr>
        <td class="text-center" colspan="4">Tidak ada data service</td>
    </tr>
@endif

