<div class="form-group">
    <label>Tgl. Kir</label>
    <input
        type="date"
        name="kir_date"
        class="form-control"
        @isset($car)
            value="{{ $car->kir_date }}"
        @endisset
        >
</div>

<div class="form-group">
    <label>Tgl. Izin Kps</label>
    <input
        type="date"
        name="kps_permit_date"
        class="form-control"
        @isset($car)
            value="{{ $car->kps_permit_date }}"
        @endisset
        >
</div>

<div class="form-group">
    <label>Tgl. Asuransi</label>
    <input
        type="date"
        name="insurance_date"
        class="form-control"
        @isset($car)
            value="{{ $car->insurance_date }}"
        @endisset
        >
</div>

<div class="form-group">
    <label>Tgl. Pajak STNK <span class="text-red">*</span></label>
    <input
        type="date"
        name="stnk_tax_date"
        class="form-control"
        @isset($car)
            value="{{ $car->stnk_tax_date }}"
        @endisset
        required>
</div>
