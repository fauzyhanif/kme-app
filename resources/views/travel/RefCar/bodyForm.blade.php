<div class="form-group">
    <label>Merk</label>
    <input
        type="text"
        name="brand"
        class="form-control"
        @isset($car)
            value="{{ $car->brand }}"
        @endisset>
</div>

<div class="form-group">
    <label>Type</label>
    <input
        type="text"
        name="type"
        class="form-control"
        @isset($car)
            value="{{ $car->type }}"
        @endisset>
</div>

<div class="form-group">
    <label>Warna <span class="text-red">*</span></label>
    <input
        type="text"
        name="color"
        class="form-control"
        required
        @isset($car)
            value="{{ $car->color }}"
        @endisset>
</div>

<div class="form-group">
    <label>Tahun Rakit</label>
    <input
        type="text"
        name="raft_year"
        class="form-control"
        @isset($car)
            value="{{ $car->raft_year }}"
        @endisset>
</div>

<div class="form-group">
    <label>Tahun Beli</label>
    <input
        type="text"
        name="purchase_year"
        class="form-control"
        @isset($car)
            value="{{ $car->purchase_year }}"
        @endisset>
</div>

<div class="form-group">
    <label>CC Mesin</label>
    <input
        type="text"
        name="machine_cc"
        class="form-control"
        @isset($car)
            value="{{ $car->machine_cc }}"
        @endisset>
</div>

<div class="form-group">
    <label>No Rangka</label>
    <input
        type="text"
        name="framework_num"
        class="form-control"
        @isset($car)
            value="{{ $car->framework_num }}"
        @endisset>
</div>

<div class="form-group">
    <label>No Mesin</label>
    <input
        type="text"
        name="machine_num"
        class="form-control"
        @isset($car)
            value="{{ $car->machine_num }}"
        @endisset>
</div>
