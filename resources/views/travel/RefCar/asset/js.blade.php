<script>
$(function() {
    loadAlltrnsct();
    loadCarServiceData();
    loadCarHistoryData();
    datatableIndexPage();
    datatableServicePage();
    datatableHistoryPage();
});

function datatableIndexPage() {
    var baseIndex = {!! json_encode(route('travel.kendaraan.jsondata')) !!};
    $('.datatable').DataTable({
        pageLength: 25,
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: baseIndex,
        columns: [
            { data: 'car_category.name', name: 'car_category.name', searchable: true },
            { data: 'police_num', name: 'police_num', searchable: true },
            { data: 'car_category.seat', name: 'car_category.seat', searchable: true },
            { data: 'stnk_tax_date', name: 'stnk_tax_date', searchable: false },
            { data: 'car_condition.name', name: 'car_condition.name', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function datatableServicePage() {
    var baseService = {!! json_encode(route('travel.kendaraan.getallcarservicedatajson')) !!};
    var policeNumForServicePage = $('#police-num-for-service-page').val();
    $('#datatable-service-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url": baseService,
            "type": "POST",
            "data": {
                "police_num" : policeNumForServicePage,
            }
        },
        columns: [
            { data: 'repair_date', name: 'repair_date', searchable: true },
            { data: 'repair_place', name: 'repair_place', searchable: true },
            { data: 'information', name: 'information', searchable: true },
        ]
    });
}

function loadCarServiceData() {
    var policeNum = $('#police-num').val();
    var url = {!! json_encode(route('travel.kendaraan.getfivecarservicedata')) !!};
    $.ajax({
        type: 'POST',
        url: url,
        data: 'police_num=' + policeNum,
        dataType:'html',
        success: function (response) {
            $('#display-car-service-data').html(response);
        }
    });
}

function loadCarHistoryData() {
    var column = $('#column').val();
    var value = $('#police-num').val();
    var url = {!! json_encode(route('travel.kendaraan.get-five-car-history')) !!};
    $.ajax({
        type: 'POST',
        url: url,
        data: column+'='+value,
        dataType:'html',
        success: function (response) {
            $('#display-car-history-data').html(response);
        }
    });
}

function loadAlltrnsct() {
    var policeNum = $('#police-num').val();
    var url = {!! json_encode(route('travel.kendaraan.get-all-trnsct')) !!};
    $.ajax({
        type: 'POST',
        url: url,
        data: 'police_num='+policeNum,
        dataType:'html',
        success: function (response) {
            console.log(response)
            $('#all-trnsct').html(response);
        }
    });
}

function datatableHistoryPage() {
    var baseUrl = {!! json_encode(route('travel.kendaraan.get-all-car-history-data-json')) !!};
    var policeNum = $('#police-num-for-history-page').val();
    $('#datatable-history-page').DataTable({
        pageLength: 50,
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url": baseUrl,
            "type": "POST",
            "data": {
                "police_num" : policeNum,
            }
        },
        columns: [
            { data: 'booking_id', name: 'booking_id', searchable: true },
            { data: 'booking.destination', name: 'booking.destination', searchable: true },
            { data: 'booking_date', name: 'booking_date', searchable: true },
        ]
    });
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan");
            },
            success: function (data) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }

                toastr.success("Data berhasil disimpan.")
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });

        document.documentElement.scrollTop = 0;
    });

    $('form[name="add_car_service"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan");
            },
            success: function (response) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }
                toastr.success(response);
                $('#myModal').modal('hide');

                // load car service data on detail page
                loadCarServiceData();
                $('#datatable-service-page').ajax.reload();
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });

        document.documentElement.scrollTop = 0;
    });
})();

</script>
