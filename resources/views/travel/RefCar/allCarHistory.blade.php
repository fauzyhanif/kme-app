@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.kendaraan.detail', $car->id) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Detail Kendaraan
            </a>
        </li>
        <li class="breadcrumb-item active">Data Sejarah Jalan Kendaraan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Data Sejarah Jalan Kendaraan {{ $car->police_num . " (" . $car->carCategory->name . ")" }}
                        </h3>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-hover" id="datatable-history-page">
                            <thead class="bg-info">
                                <th>Kode Booking</th>
                                <th>Tujuan</th>
                                <th>Tgl. Berangkat</th>
                            </thead>
                            <tbody id="display-data-history-page">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- parameter for load car service data --}}
<input type="hidden" id="police-num-for-history-page" value="{{ $car->police_num }}">

@include('travel.RefCar.asset.js')
@endsection
