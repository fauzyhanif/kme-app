@extends('index')

@section('content')
<section class="content-header">
    <div class="row">
        <div class="col-md-6">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('travel.kendaraan') }}">
                        <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                        Daftar Kendaraan
                    </a>
                </li>
                <li class="breadcrumb-item active">Detail Kendaraan</li>
            </ol>
        </div>
        <div class="col-md-6">
            <div class="float-right">
                <a href="{{ url('/travel/kendaraan/form-edit',  $car->id) }}" class="btn btn-primary btn-sm">
                    <div class="fas fa-edit"></div> Edit Data
                </a>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">
                            Informasi Kendaraan
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td width="30%" class="text-secondary">No Polisi</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->police_num }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Kategori Kendaraan</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->carCategory->name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Kondisi</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->carCondition->name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Pemilik</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->owner }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Alamat Pemilik</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->owner_address }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Nomonal Pajak</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">Rp @currency($car->nominal_tax)/Tahun</td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" colspan="3">Bodi Kendaraan</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Merk</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->brand }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Type</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->type }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Warna</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->color }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Tahun Rakit</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->raft_year }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Tahun Beli</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->purchase_year }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">CC Mesin</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->machine_cc }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Tahun Beli</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->purchase_year }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Nomor Rangka</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->framework_num }}</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Nomor Mesin</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->machine_num }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" colspan="3">Tanggal Penting</td>
                                </tr>
                                <tr>
                                    <td width="30%" class="text-secondary">Tgl. Kir</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->kir_date }}</td>
                                </div>
                                <tr>
                                    <td width="30%" class="text-secondary">Tgl. Izin KPS</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->kps_permit_date }}</td>
                                </div>
                                <tr>
                                    <td width="30%" class="text-secondary">Tgl. Asuransi</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->insurance_date }}</td>
                                </div>
                                <tr>
                                    <td width="30%" class="text-secondary">Tgl. Pajak STNK</td>
                                    <td width="3%" class="text-right">:</td>
                                    <td width="67%">{{ $car->stnk_tax_date }}</td>
                                </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3 id="all-trnsct">0</h3>

                                <p>Total Jalan</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-car"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card scroll-x shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Daftar Perjalanan</h3>
                                <div class="card-tools">
                                    <a href="{{ route('travel.kendaraan.all-car-history', $car->id) }}" class="text-gray mr-3">
                                        <u>Lihat Selengkapnya</u>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th>No</th>
                                        <th>Kode Booking</th>
                                        <th>Tujuan</th>
                                        <th>Tgl Berangkat</th>
                                    </thead>
                                    <tbody id="display-car-history-data">
                                        <tr>
                                            <td class="text-center" colspan="4">Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow-none">
                            <div class="card-header">
                                <h3 class="card-title">Daftar Perbaikan</h3>
                                <div class="card-tools">
                                    <a href="{{ route('travel.kendaraan.allcarservice', $car->id) }}" class="text-gray mr-3">
                                        <u>Lihat Selengkapnya</u>
                                    </a>
                                    <button class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#myModal">
                                        <i class="fas fa-edit"></i> &nbsp;
                                        Tambah Data Perbaikan
                                    </button>
                                </div>
                            </div>
                            <div class="card-body scroll-x">
                                <table class="table table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th>No</th>
                                        <th>Tgl Perbaikan</th>
                                        <th>Keterangan</th>
                                        <th>Tempat Perbaikan</th>
                                    </thead>
                                    <tbody id="display-car-service-data">
                                        <tr>
                                            <td class="text-center" colspan="4">Loading...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- parameter for load car service data --}}
<input type="hidden" id="column" value="police_num">
<input type="hidden" id="police-num" value="{{ $car->police_num }}">

@include('travel.RefCar.formService')
@include('travel.RefCar.asset.js')
@endsection
