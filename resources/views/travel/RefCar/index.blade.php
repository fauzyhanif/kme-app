@extends('index')

@section('content')
<section class="content-header">
    <h1>Kendaraan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Kendaraan
                        <div class="card-tools">
                            <a href="{{ route('travel.kendaraan.formadd') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Kendaraan Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th>Kategori</th>
                                <th>No Polisi</th>
                                <th>Seat</th>
                                <th>Tgl. Pajak</th>
                                <th width="12%">Kondisi</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.RefCar.asset.js')
@endsection
