<script>
$(document).ready(function () {
    $('.form-item-kasbon').keyup(function() {

        var ttlKasbon = 0;
        $('.form-item-kasbon').each(function(){
            ttlKasbon += parseFloat(this.value.replace(/\./g, ''));
        });

        $('#text-total-kasbon-bayar').text('Rp ' + formatRupiah(ttlKasbon))
        $('#form-total-kasbon-bayar').val(ttlKasbon)

        var ttlGaji = $('#form-total-gaji').val();
        var sisaGaji = ttlGaji - ttlKasbon;
        $('#text-total-gaji').text('Rp ' + formatRupiah(sisaGaji))

        if (sisaGaji < 0) {
            $('.alert-tidak-cukup').css('display', 'block');
            $('.btn-save').css('display', 'none');
        } else {
            $('.alert-tidak-cukup').css('display', 'none');
            $('.btn-save').css('display', 'block');
        }

    });
});
</script>
