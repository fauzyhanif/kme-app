@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('travel/penggajian?month='.$payrol->month.'&year='.$payrol->year) }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Gaji
            </a>
        </li>
        <li class="breadcrumb-item active">Form Pencairan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            @if ($payrol->total_paid == $payrol->total)
                <div class="col-md-10 mb-4">
                    <div class="border rounded" style="background-color: #FFF9CA">
                        <div class="p-3">
                            <span class="font-weight-bold">Informasi!</span>
                            <span>Gaji sudah dicairkan semua.</span>
                        </div>
                    </div>
                </div>
            @endif

            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Informasi Staff
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td width="25%" class="text-secondary">Nama Lengkap</td>
                                    <td width="2%">:</td>
                                    <td width="70%">{{ $payrol->sdm->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Jabatan</td>
                                    <td width="2%">:</td>
                                    <td width="70%">{{ $payrol->sdm->position->name }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Periode</td>
                                    <td width="2%">:</td>
                                    <td width="70%">{{ $payrol->month .'/' . $payrol->year }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Jumlah Gaji</td>
                                    <td width="2%">:</td>
                                    <td width="70%">Rp {{ GeneralHelper::rupiah($payrol->upah_bulanan) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Jumlah Potongan Kasbon</td>
                                    <td width="2%">:</td>
                                    <td width="70%">Rp {{ GeneralHelper::rupiah($payrol->potongan_kasbon) }}</td>
                                </tr>
                                <tr>
                                    <td width="25%" class="text-secondary">Sudah Dicairkan</td>
                                    <td width="2%">:</td>
                                    <td width="70%">Rp {{ GeneralHelper::rupiah($payrol->total_paid) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if ($payrol->total_paid < $payrol->total)
            <form name="form_pencairan" action="{{ url('travel/penggajian/pencairan') }}" method="POST">
                <div class="row justify-content-center">
                    @csrf

                    @php
                        $ttl_debts = 0;
                    @endphp
                    @if ($debts->count() > 0)
                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Daftar Kasbon Belum Lunas
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered table-sm">
                                        <thead class="bg-info">
                                            <th width="15%">Tanggal</th>
                                            <th>Keterangan</th>
                                            <th width="15%">Jumlah Kasbon</th>
                                            <th width="15%">Sisa</th>
                                            <th width="20%">Bayar</th>
                                        </thead>
                                        <tbody>

                                            @foreach ($debts as $debt)
                                                <tr>
                                                    <td>{{ date_format(date_create($debt->date), 'd/m/Y') }}</td>
                                                    <td>{{ $debt->desc }}</td>
                                                    <td>Rp {{ GeneralHelper::rupiah($debt->amount) }}</td>
                                                    <td>Rp {{ GeneralHelper::rupiah($debt->amount - $debt->amount_paid) }}</td>
                                                    <td>
                                                        <input type="text" name="item_kasbon[{{ $debt->id }}]" class="form-control form-control-sm form-item-kasbon money" value="0">
                                                    </td>
                                                </tr>
                                                @php
                                                    $ttl_debts += $debt->amount - $debt->amount_paid;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="col-md-10">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Form Pencairan
                                </h3>
                            </div>
                            <div class="card-body">

                                <input type="hidden" name="payrol_id" value="{{ $payrol->id }}">
                                <input type="hidden" name="related_person" value="{{ $payrol->sdm->name }}">
                                <input type="hidden" name="total_kasbon_bayar" id="form-total-kasbon-bayar" value="{{ $ttl_debts }}">
                                <input type="hidden" name="total_gaji" id="form-total-gaji" value="{{ $payrol->total - $payrol->total_paid }}">

                                <div class="row mb-2 alert-tidak-cukup" style="display: none">
                                    <div class="col-md-12">
                                        <div class="alert alert-danger">
                                            <h3>Gaji tidak cukup bayar kasbon!</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-sm">
                                            <thead class="bg-info">
                                                <th width="33%">Gaji</th>
                                                <th width="33%">Jumlah Bayar Kasbon</th>
                                                <th width="33%">Gaji Dicairkan</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Rp {{ GeneralHelper::rupiah($payrol->total - $payrol->total_paid) }}</td>
                                                    <td id="text-total-kasbon-bayar">Rp 0</td>
                                                    <td id="text-total-gaji">Rp {{ GeneralHelper::rupiah($payrol->total - $payrol->total_paid) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tanggal Pencairan <span class="text-red">*</span></label>
                                            <input type="date" name="payment_date" value="{{ date('Y-m-d') }}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sumber Kas <span class="text-red">*</span></label>
                                            <select name="cash_account"  class="form-control" required>
                                                <option value="">-- Pilih Sumber Kas --</option>
                                                @foreach ($cashAccounts as $account)
                                                    <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    @if ($debts->count() > 0)
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Akun Kasbon @if($debts->count() > 0) <span class="text-red">*</span> @endif</label>
                                                <select name="kasbon_account" class="form-control" @if($debts->count() > 0) required @endif>
                                                    <option value="">-- Pilih Akun Kasbon --</option>
                                                    @foreach ($cashAccounts as $account)
                                                        <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Akun Operasional <span class="text-red">*</span></label>
                                            <select name="operasional_account" id="" class="form-control" required>
                                                <option value="">-- Pilih Akun Operasional --</option>
                                                @foreach ($operasionalAccounts as $operasional)
                                                    <option value="{{ $operasional->account_id }}">{{ $operasional->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Keterangan di jurnal <span class="text-red">*</span></label>
                                            <textarea name="desc" id="" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success btn-block btn-save">CAIRKAN SEKARANG</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @else

                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title m-0">Daftar Pencairan Gaji </h5>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-bordered table-hover">
                                    <thead class="bg-info">
                                        <th class="text-center" width="5%">No</th>
                                        <th>Tanggal</th>
                                        <th>Sumber Kas</th>
                                        <th>Jumlah</th>
                                        <th width="20%">Aksi</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($payrolPayments as $payment)
                                            <tr>
                                                <td class="text-center">{{ $no }}</td>
                                                <td>{{ date_format(date_create($payment->payment_date), 'd/m/Y') }}</td>
                                                <td>{{ $payment->account->name }}</td>
                                                <td>Rp {{ GeneralHelper::rupiah($payment->amount) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-pembatalan-{{ $payment->id }}">
                                                        <i class="fas fa-trash"></i> Batalkan
                                                    </button>
                                                    <a href="{{ url('travel/penggajian/print_slip_gaji', $payment->id) }}" target="_blank" class="btn btn-xs btn-warning">
                                                        <i class="fas fa-print"></i> Cetak Slip Gaji
                                                    </a>
                                                    @include('travel.TrnsctPayrol.formPembatalan')
                                                </td>
                                            </tr>
                                            @if ($payment->debtPayment)
                                                <tr>
                                                    <td></td>
                                                    <td colspan="4">
                                                        *Dipotong {{ GeneralHelper::rupiah($payment->debtPayment->amount) }} untuk angsuran kasbon tanggal {{ GeneralHelper::konversiTgl($payment->debtPayment->debt->date, 'slash') }},
                                                        sisa kasbon ini = {{ GeneralHelper::rupiah($payment->debtPayment->debt->amount - $payment->debtPayment->debt->amount_paid) }}
                                                    </td>
                                                </tr>
                                            @endif
                                        @php
                                            $no += 1;
                                        @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>

@include('travel.TrnsctPayrol.asset.js')
@endsection
