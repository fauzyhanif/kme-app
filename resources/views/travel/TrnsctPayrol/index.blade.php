@extends('index')

@section('content')
<section class="content-header">
    <h1>Penggajian</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <form action="" method="GET">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <select name="month" class="form-control">
                            <option value="01" {{ ($this_month == "01") ? 'selected' : '' }}>Januari</option>
                            <option value="02" {{ ($this_month == "02") ? 'selected' : '' }}>Februari</option>
                            <option value="03" {{ ($this_month == "03") ? 'selected' : '' }}>Maret</option>
                            <option value="04" {{ ($this_month == "04") ? 'selected' : '' }}>April</option>
                            <option value="05" {{ ($this_month == "05") ? 'selected' : '' }}>Mei</option>
                            <option value="06" {{ ($this_month == "06") ? 'selected' : '' }}>Juni</option>
                            <option value="07" {{ ($this_month == "07") ? 'selected' : '' }}>Juli</option>
                            <option value="08" {{ ($this_month == "08") ? 'selected' : '' }}>Agustus</option>
                            <option value="09" {{ ($this_month == "09") ? 'selected' : '' }}>September</option>
                            <option value="10" {{ ($this_month == "10") ? 'selected' : '' }}>Oktober</option>
                            <option value="11" {{ ($this_month == "11") ? 'selected' : '' }}>November</option>
                            <option value="12" {{ ($this_month == "12") ? 'selected' : '' }}>Desember</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <select name="year" class="form-control">
                        @foreach ($years as $year)
                            <option {{ ($year == $this_year) ? 'selected' : '' }}>{{ $year }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-search"></i> TAMPILKAN
                    </button>
                    <a href="{{ url('travel/penggajian/print?month='.$this_month.'&year='.$this_year) }}" class="btn btn-default" target="_blank">
                        <i class="fas fa-print"></i> CETAK
                    </a>
                </div>
            </div>
        </form>

        <div class="row mb-3">
            <div class="col-md-12">
                <div class="border rounded" style="background-color: #FFF9CA">
                    <div class="p-3">
                        <span class="font-weight-bold">Perhatian!</span>
                        <span>Bon & angsuran akan terlihat datanya ketika proses pencairan gaji.</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Gajian

                        @if ($payrols->count() > 0)
                            <div class="card-tools">
                                <form action="{{ url('travel/penggajian/generate') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="month" value="{{ $this_month }}">
                                    <input type="hidden" name="year" value="{{ $this_year }}">

                                    <button type="submit" class="btn bg-purple">
                                        GENERATE ULANG GAJI PERIODE {{ $this_month . '/' . $this_year }}
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                    <div class="card-body" style="overflow-x: scroll">
                        <table class="table table-bordered table-striped datatable-index-page mb-4">
                            <thead class="bg-info">
                                <th width="25%">Nama & Jabatan</th>
                                <th width="13%">Gaji</th>
                                <th width="13%">Bon & Angsuran</th>
                                <th width="13%">Jumlah</th>
                                <th width="13%">Jumlah Diterima</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>
                                @if ($payrols->count() > 0)
                                    @foreach ($payrols as $payrol)
                                        <tr>
                                            <td>
                                                @if ($payrol->sdm_type == 'SDM')
                                                    <span class="font-weight-bold">{{ $payrol->sdm_name }}</span> <br>
                                                    <span class="text-secondary">{{ $payrol->position_name }}</span>
                                                @elseif($payrol->sdm_type == 'DRIVER')
                                                    <span class="font-weight-bold">{{ $payrol->driver_name }}</span> <br>
                                                    <span class="text-secondary">DRIVER</span>
                                                @elseif($payrol->sdm_type == 'HELPER')
                                                    <span class="font-weight-bold">{{ $payrol->helper_name }}</span> <br>
                                                    <span class="text-secondary">HELPER</span>
                                                @endif
                                            </td>
                                            <td>{{ GeneralHelper::rupiah($payrol->upah_bulanan) }}</td>
                                            <td>{{ GeneralHelper::rupiah($payrol->potongan_kasbon) }}</td>
                                            <td>{{ GeneralHelper::rupiah($payrol->total) }}</td>
                                            <td>{{ GeneralHelper::rupiah($payrol->total_paid) }}</td>
                                            <td>
                                                <a href="{{ url('travel/penggajian/form_pencairan', $payrol->id) }}" class="btn btn-success btn-xs"><i class="fas fa-edit"></i> Cairkan</a>
                                                <a href="" class="btn btn-info btn-xs"><i class="fas fa-eye"></i> Detail</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="text-center" colspan="7">
                                            <div class="py-5">
                                                <h1 class="font-weight-bold">Data Penggajian belum tersedia!</h1>
                                                <h4>Silahkan klik tombol dibawah ini untuk Generate Gaji secara Otomatis</h4>

                                                <form action="{{ url('travel/penggajian/generate') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="month" value="{{ $this_month }}">
                                                    <input type="hidden" name="year" value="{{ $this_year }}">

                                                    <button type="submit" class="btn bg-purple">
                                                        GENERATE GAJI PERIODE {{ $this_month . '/' . $this_year }}
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
