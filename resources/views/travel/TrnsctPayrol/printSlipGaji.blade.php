<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Slip Gaji Gaji Karyawan</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('cssCustom.css_print')
        <style>
            @page {
                size: F4;
            }

            body {
                letter-spacing: 1px;
            }
        </style>
    </head>
    <body class="F4 landscape" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                    <div class="row text-center mb-4">
                        <div class="col-lg-12">
                            <h5 class="font-weight-bold">SLIP GAJI KARYAWAN</h5>
                            <h3 class="font-weight-bold">PT. KARYAMAS EMPAT</h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                    <tr>
                                        <td width="25%" class="text-secondary">Nama Lengkap</td>
                                        <td width="2%">:</td>
                                        <td width="70%">{{ $payrol->payrol->sdm->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" class="text-secondary">Jabatan</td>
                                        <td width="2%">:</td>
                                        <td width="70%">{{ $payrol->payrol->sdm->position->name }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" class="text-secondary">Periode</td>
                                        <td width="2%">:</td>
                                        <td width="70%">{{ GeneralHelper::konversiTgl($payrol->payrol->year.'-'.$payrol->payrol->month.'-01', 'my') }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" class="text-secondary">Jumlah Gaji</td>
                                        <td width="2%">:</td>
                                        <td width="70%">Rp {{ GeneralHelper::rupiah($payrol->payrol->upah_bulanan) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" class="text-secondary">Jumlah Potongan Kasbon</td>
                                        <td width="2%">:</td>
                                        <td width="70%">Rp {{ GeneralHelper::rupiah($payrol->payrol->potongan_kasbon) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="25%" class="text-secondary">Sudah Dicairkan</td>
                                        <td width="2%">:</td>
                                        <td width="70%">Rp {{ GeneralHelper::rupiah($payrol->payrol->total_paid) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-center">
                            Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                            Bendahara,

                            <br>
                            <br>
                            <br>
                            <p>(Hj. Nani Inayah, SE)</p>
                        </div>
                    </div>

            </div>
        </section>
    </body>
