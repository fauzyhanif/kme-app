<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Daftar Penerimaan Gaji Karyawan</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{  url('public/admin-lte/dist/css/adminlte.min.css') }}">

        @include('cssCustom.css_print')
        <style>
            @page {
                size: A4 landscape
            }

            body {
                letter-spacing: 1px;
            }

            table, th, td {
                border: 2px solid black;
            }
        </style>
    </head>
    <body class="A4 landscape" onload="window.print()">
        <section class="sheet padding-10mm content">
            <div class="container-fluid">
                    <div class="row text-center mb-4">
                        <div class="col-lg-12">
                            <h5 class="font-weight-bold">DAFTAR PENERIMAAN GAJI KARYAWAN</h5>
                            <h3 class="font-weight-bold">PT. KARYAMAS EMPAT</h3>
                            <h5 class="font-weight-bold">PERIODE {{ GeneralHelper::konversiTgl("$this_year-$this_month-01", 'my') }}</h5>
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-12">
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <th width="5%" class="text-center">NO</th>
                                    <th class="text-center" width="16%">NAMA</th>
                                    <th class="text-center" width="17%">JABATAN</th>
                                    <th class="text-center" width="13%">GAJI</th>
                                    <th class="text-center" width="13%">ANGSURAN</th>
                                    <th class="text-center" width="13%">JML GAJI</th>
                                    <th class="text-center" width="13%">JML DITERIMA</th>
                                    <th class="text-center" colspan="2" width="10%">TTD</th>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                        $ttl_gaji = 0;
                                        $ttl_kasbon = 0;
                                        $ttl_sisa = 0;
                                        $ttl_diterima = 0;
                                    @endphp
                                    @foreach ($payrols as $payrol)
                                        <tr>
                                            <td>{{ $no }}.</td>
                                            <td>{{ $payrol->sdm_name }}</td>
                                            <td>{{ $payrol->position_name }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($payrol->upah_bulanan) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($payrol->potongan_kasbon) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($payrol->total) }}</td>
                                            <td class="text-right">{{ GeneralHelper::rupiah($payrol->total_paid) }}</td>
                                            <td>@if($loop->iteration % 2 != 0) {{ $no }}. @endif</td>
                                            <td>@if($loop->iteration % 2 == 0) {{ $no }}. @endif</td>
                                        </tr>
                                    @php
                                        $no += 1;
                                        $ttl_gaji += $payrol->upah_bulanan;
                                        $ttl_kasbon += $payrol->potongan_kasbon;
                                        $ttl_sisa += $payrol->total;
                                        $ttl_diterima += $payrol->total_paid;
                                    @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <th colspan="3" class="text-center">JUMLAH</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($ttl_gaji) }}</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($ttl_kasbon) }}</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($ttl_sisa) }}</th>
                                    <th class="text-right">{{ GeneralHelper::rupiah($ttl_diterima) }}</th>
                                    <th></th>
                                    <th></th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-center">
                            Subang, {{ GeneralHelper::konversiTgl(date('Y-m-d'), 'ttd') }} <br>
                            Bendahara,

                            <br>
                            <br>
                            <br>
                            <p>(Hj. Nani Inayah, SE)</p>
                        </div>
                    </div>

            </div>
        </section>
    </body>
