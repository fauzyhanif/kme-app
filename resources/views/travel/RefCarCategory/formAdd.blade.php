@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.kategorikendaraan') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Kategori Kendaraan
            </a>
        </li>
        <li class="breadcrumb-item active">Tambah Kategori Kendaraan Baru</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Form Kategori Kendaraan Baru
                    </div>
                    <div class="card-body">
                        <div id="response-alert"></div>
                        <form id="form-add" action="{{ route('travel.kategorikendaraan.addnew') }}" method="POST" data-remote>
                            <div class="form-group">
                                <label>ID <span class="text-red">*</span></label>
                                <input type="text" name="category_id" class="form-control" placeholder="Contoh: 1">
                            </div>

                            <div class="form-group">
                                <label>Nama <span class="text-red">*</span></label>
                                <input type="text" name="name" class="form-control" placeholder="Big Bus">
                            </div>

                            <div class="form-group">
                                <label>Seat <span class="text-red">*</span></label>
                                <input type="text" name="seat" class="form-control" required placeholder="47/49">
                            </div>

                            <hr>

                            <div class="wrapp-button">
                                <button type="submit" class="btn btn-success btn-save">
                                    Simpan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.RefCarCategory.asset.js')
@endsection
