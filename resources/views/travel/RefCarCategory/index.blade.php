@extends('index')

@section('content')
<section class="content-header">
    <h1>Kategori Kendaraan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Data
                        <div class="card-tools">
                            <a href="{{ route('travel.kategorikendaraan.formadd') }}" class="btn btn-success btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Data Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-striped datatable">
                            <thead class="bg-info">
                                <th width="5%">ID</th>
                                <th>Nama Kategori</th>
                                <th>Seat</th>
                                <th width="15%">Aktif</th>
                                <th width="15%">Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.RefCarCategory.asset.js')
@endsection
