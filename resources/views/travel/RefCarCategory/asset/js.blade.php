<script>
(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan");
            },
            success: function (data) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }
                toastr.success("Data berhasil disimpan.")
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

$(function() {
    var base = {!! json_encode(route('travel.kategorikendaraan.jsondata')) !!};

    $('.datatable').DataTable({
        "pageLength": 25,
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,
        columns: [
            { data: 'category_id', name: 'category_id', searchable: true },
            { data: 'name', name: 'name', searchable: true },
            { data: 'seat', name: 'seat', searchable: true },
            { data: 'active', name: 'active', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
});
</script>
