@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.income') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar Pemasukan
            </a>
        </li>
        <li class="breadcrumb-item active">Detail Pemasukan</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            Detail Pemasukan
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="30%">No Kwitansi</td>
                                    <td>{{ $income->payment_id }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Tgl. Transaksi</td>
                                    <td>{{ GeneralHelper::konversiTgl($income->payment_date) }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Diterima Dari</td>
                                    <td>{{ $income->received_from }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Keterangan</td>
                                    <td>{{ $income->information }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Sebesar</td>
                                    <td>Rp. {{ GeneralHelper::rupiah($income->amount) }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Masuk Kas/Bank</td>
                                    <td>{{ $income->paymentMethod->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
