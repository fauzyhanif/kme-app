@extends('index')

@section('content')
<section class="content-header">
    <h1>Pemasukan</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        Daftar Pemasukan
                        <div class="card-tools">
                            <a href="{{ route('travel.income.formadd') }}" class="btn btn-primary btn-sm">
                                <i class="fas fa-plus-circle"></i> &nbsp;
                                Tambah Pemasukan Baru
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped datatable-index-page">
                            <thead class="bg-info">
                                <th>Tanggal</th>
                                <th>No Kwitansi</th>
                                <th>Deskripsi</th>
                                <th>Terima Dari</th>
                                <th>Nominal</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctIncome.asset.js')
@endsection
