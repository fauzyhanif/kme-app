<!DOCTYPE html>
<html>
<head>
	<title>Surat Perintah Jalan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @include('Component.cssPrint')
    <style>
        body {
            font-size: 12px
        }
    </style>
</head>
<body>
	<div class="container-fluid">
        @include('Component.kopSurat')


        <hr style="margin-top: -10px; border : 1px solid black">

        <div class="row text-center">
            <div class="col-md-12">
                <h4 class="font-weight-bold"><u>Surat Perintah Jalan</u></h4>
                No Surat : {{ $spj->spj_id }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                Dengan hormat, <br>
                Bersama ini saya memerintahkan kepada :
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-md-12">
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="25%" class="except">Nama Driver</td>
                            <td width="75%" class="except">:
                                <b>
                                    {{ $spj->mainDriver->name }}
                                    @if ($spj->second_driver != "")
                                        / {{ $spj->secondDriver->name }}
                                    @endif
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">No SIM</td>
                            <td width="75%" class="except">:
                                <b>
                                    {{ $spj->mainDriver->sim_num }}
                                    @if ($spj->second_driver != "")
                                        / {{ $spj->secondDriver->sim_num }}
                                    @endif
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">Jenis Kendaraan</td>
                            <td width="75%" class="except">: <b>{{ $spj->carCategory->name }} ({{ $spj->carCategory->seat }} seat)</b></td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">No Polisi</td>
                            <td width="75%" class="except">: <b>{{ $spj->police_num }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                Untuk keperluan sebagai berikut :
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="25%" class="except">Nama Customer</td>
                            <td width="75%" class="except">: <b>{{ $booking->customer->name }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">No Telp / HP</td>
                            <td width="75%" class="except">: <b>{{ $booking->customer->phone_num }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">Alamat Jemput</td>
                            <td width="75%" class="except">: <b>{{ $booking->customer->address }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">Jam Stand By</td>
                            <td width="75%" class="except">: <b>{{ $booking->standby_time }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">Tujuan / Rute</td>
                            <td width="75%" class="except">: <b>{{ $booking->destination }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">Keterangan</td>
                            <td width="75%" class="except">: <b>{{ $booking->information }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%" class="except">Tgl. Berangkat</td>
                            <td width="75%" class="except">:
                                <b>{{ GeneralHelper::konversiTgl($booking->booking_start_date, 'slash') }}</b>
                                -
                                <b>{{ GeneralHelper::konversiTgl($booking->booking_end_date, 'slash') }}</b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead class="bg-info text-white">
                        <tr>
                            <th class="small-head">Alokasi</th>
                            <th class="small-head">Jumlah</th>
                            <th class="small-head text-right">Biaya</th>
                            <th class="small-head">Durasi</th>
                            <th class="small-head text-right">Total Biaya</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $total = 0; @endphp
                        @foreach ($spjBudget as $account)
                            <tr>
                                <td class="small-body">{{ $account->budgetAccount->name }}</td>
                                <td class="small-body">{{ $account->qty }}</td>
                                <td class="small-body text-right">{{ GeneralHelper::rupiah($account->amount) }}</td>
                                <td class="small-body">{{ $account->duration }}</td>
                                <td class="small-body text-right">{{ GeneralHelper::rupiah($account->ttl_amount) }}</td>
                            </tr>
                        @php $total += $account->ttl_amount; @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right small-body"><b>Total</b></td>
                            <td class="small-body text-right"><b>{{ GeneralHelper::rupiah($total) }}</b></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 text-center" style="margin-left: -400px;">
                <br>
                Penerima,

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <p>({{ $spj->mainDriver->name }})</p>
            </div>
            <div class="col-md-6 text-center" style="margin-left: 400px;">
                Subang, {{ GeneralHelper::konversiTgl($booking->booking_start_date, 'ttd') }} <br>
                Petugas,
                <br>
                <img src="{{ url('public/img/cap.png') }}" style="max-width: 100px; padding-bottom: -20px !important; padding-left: -10px !important" alt="">
                <img src="{{ url('public/img/ttd-aziz.png') }}" style="max-width: 70px; padding-bottom: -20px !important; padding-left: -50px !important" alt="">
                <br>
                <p>({{ Session::get('auth_nama') }})</p>
            </div>
        </div>

    </div>
</body>
