@extends('index')

@section('content')
<section class="content-header">
    <h1>Daftar SPJ</h1>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">Daftar SPJ</h3>
                    </div>
                    <div class="card-body scroll-x">
                        <div class="border rounded my-2" style="background-color: #FFF9CA">
                            <div class="p-3">
                                <span class="font-weight-bold">Panduan!</span> <br>
                                <span>
                                    Untuk input SPJ baru silahkan buka menu Daftar Booking > klik tombol Detail di sebelah kanan data yang dituju > klik tombol Buat SPJ
                                    > isi formulir dengan lengkap
                                </span>
                            </div>
                        </div>

                        <table class="table table-bordered table-striped datatable-index-page">
                            <thead class="bg-info">
                                <th>No. Surat</th>
                                <th>No. Polisi</th>
                                <th>Driver</th>
                                <th>Tujuan</th>
                                <th>Tgl Berangkat</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctSpj.popupCancel')
@include('travel.TrnsctSpj.asset.js')
@endsection
