<div class="form-group">
    <label>Nama Pelanggan</label>
    <p>Hanif</p>
</div>

<div class="form-group">
    <label>No. Telp / HP.</label>
    <p>082325494207</p>
</div>

<div class="form-group">
    <label>Alamat Jemput</label>
    <p>Legonkulon</p>
</div>

<div class="form-group">
    <label>Jam Stand By</label>
    <p>7 pagi</p>
</div>

<div class="form-group">
    <label>Rute</label>
    <p>Gunungkidul, Yogyakarta</p>
</div>

<div class="form-group">
    <label>Tanggal Berangkat - Kembali</label>
    <p>01/01/2021 s/d 05/01/2021</p>
</div>
