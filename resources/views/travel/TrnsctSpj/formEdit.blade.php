@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.spj.index') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar SPJ
            </a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ url('travel/booking/detail', $booking->booking_id) }}">
                Detail Booking
            </a>
        </li>
        <li class="breadcrumb-item active">Form Edit SPJ</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Keterangan</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="30%">No. SPJ</td>
                                    <td width="70%">{{ $letter->spj_id }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Kode Booking</td>
                                    <td width="70%">{{ $booking->booking_id }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Nama Pelanggan</td>
                                    <td width="70%">{{ $booking->cust_name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">No. Handphone</td>
                                    <td width="70%">{{ $booking->cust_phone_num }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Tgl Booking</td>
                                    <td width="70%">
                                        {{ GeneralHelper::konversiTgl($booking->booking_start_date, 'slash') }}
                                        -
                                        {{ GeneralHelper::konversiTgl($booking->booking_end_date, 'slash') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%">Tujuan</td>
                                    <td width="70%">{{ $booking->destination }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Penjemputan</td>
                                    <td width="70%">{{ $booking->pick_up_location }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Stand By</td>
                                    <td width="70%">{{ $booking->standby_time }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Keterangan</td>
                                    <td width="70%">{{ $booking->information }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('travel.spj.edit') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="spj_id" value="{{ $letter->spj_id }}">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form SPJ untuk {{ $category->name }} ({{ $category->seat }} seat)
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Mobil</label>
                                <select name="police_num" class="form-control" required>
                                    <option value="">-- Pilih Mobil --</option>
                                    <option selected>{{ $letter->police_num }}</option>
                                    @foreach ($cars as $car)
                                        <option>{{ $car }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Driver Utama</label>
                                <select name="main_driver" class="form-control" required>
                                    <option value="">-- Pilih Driver --</option>
                                    <option value="{{ $letter->main_driver }}" selected>{{ $letter->main_driver_name }}</option>
                                    @foreach ($drivers as $driver)
                                        <option value="{{ $driver->driver_id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Premi Driver Utama</label>
                                <input
                                    type="text"
                                    name="premi_main_driver"
                                    class="form-control money"
                                    value="{{ $letter->premi_main_driver }}">
                            </div>

                            <div class="form-group">
                                <label>Driver Kedua</label>
                                <select name="second_driver" class="form-control">
                                    <option value="">-- Pilih Driver --</option>
                                    @if ($letter->second_driver_name != "")
                                        <option value="{{ $letter->second_driver }}" selected>{{ $letter->second_driver_name }}</option>
                                    @endif
                                    @foreach ($drivers as $driver)
                                        <option value="{{ $driver->driver_id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Premi Driver Kedua</label>
                                <input
                                    type="text"
                                    name="premi_second_driver"
                                    class="form-control money"
                                    value="{{ $letter->premi_second_driver }}">
                            </div>

                            <div class="form-group">
                                <label>Helper</label>
                                <select name="helper_id" class="form-control">
                                    <option value="">-- Pilih Helper --</option>
                                    @if ($letter->helper_name != "")
                                        <option value="{{ $letter->helper_id }}" selected>{{ $letter->helper_name }}</option>
                                    @endif
                                    @foreach ($helpers as $helper)
                                        <option value="{{ $helper->helper_id }}">{{ $helper->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Premi Helper</label>
                                <input
                                    type="text"
                                    name="premi_helper"
                                    class="form-control money"
                                    value="{{ $letter->premi_helper }}">
                            </div>

                            <hr>

                            <table class="table table-bordered">
                                <thead>
                                    <th width="20%">Alokasi</th>
                                    <th>Jumlah</th>
                                    <th>Durasi</th>
                                    <th>Biaya</th>
                                    <th>Total Biaya</th>
                                </thead>
                                <tbody>
                                    @php $ttl_budget = 0 @endphp
                                    @foreach ($budgets as $account)
                                        <tr>

                                            <input type="hidden" name="account_id[]" value="{{ $account->account_id }}">

                                            <td>{{ $account->budgetAccount->name }}</td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="qty[{{ $account->account_id }}]"
                                                    value="{{ $account->qty }}"
                                                    class="form-control form-add-qty">
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="duration[{{ $account->account_id }}]"
                                                    value="{{ $account->duration }}"
                                                    class="form-control form-add-duration">
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="amount[{{ $account->account_id }}]"
                                                    value="{{ $account->amount }}"
                                                    class="form-control form-add-amount money">
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="ttl_amount[{{ $account->account_id }}]"
                                                    value="{{ $account->ttl_amount }}"
                                                    class="form-control form-add-ttl-amount money">
                                            </td>
                                        </tr>
                                    @php $ttl_budget += $account->ttl_amount @endphp
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" class="text-center bg-info">
                                            <input type="hidden" name="grand_ttl_amount" value="0">
                                            <h4>
                                                Total : <b id="ttl-amount">Rp. {{ GeneralHelper::rupiah($ttl_budget) }}</b>
                                            </h4>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        @if ($letter->status != "BATAL" && $letter->status != "SELESAI")
                            <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                                <button type="submit" class="btn btn-success">
                                    Simpan Perubahan
                                </button>
                                <button class="btn btn-secondary">
                                    Batal
                                </button>
                            </div>
                            <div class="card-body">
                                <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctSpj.asset.js')
@endsection
