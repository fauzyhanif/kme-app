<!DOCTYPE html>
<html>
<head>
	<title>Surat Perintah Jalan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @include('Component.cssPrint')
</head>
<body>
	<div class="container-fluid">
        @include('Component.kopSurat')

        <hr style="margin-top: -20px">

        <div class="row text-center">
            <div class="col-md-12">
                <h4 class="font-weight-bold"><u>Surat Perintah Jalan</u></h4>
                No Surat : {{ $spj->spj_id }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                Dengan hormat, <br>
                Bersama ini saya memerintahkan kepada :
            </div>
        </div>

        <div class="row mb-2">
            <div class="col-md-12">
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="25%">Nama Driver</td>
                            <td width="75%">:
                                <b>
                                    {{ $spj->mainDriver->name }}
                                    @if ($spj->second_driver != "")
                                        / {{ $spj->secondDriver->name }}
                                    @endif
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">No SIM</td>
                            <td width="75%">:
                                <b>
                                    {{ $spj->mainDriver->sim_num }}
                                    @if ($spj->second_driver != "")
                                        / {{ $spj->secondDriver->sim_num }}
                                    @endif
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">Jenis Kendaraan</td>
                            <td width="75%">: <b>{{ $spj->carCategory->name }} ({{ $spj->carCategory->seat }} seat)</b></td>
                        </tr>
                        <tr>
                            <td width="25%">No Polisi</td>
                            <td width="75%">: <b>{{ $spj->police_num }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                Untuk keperluan sebagai berikut :
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table borderless">
                    <tbody>
                        <tr>
                            <td width="25%">Nama Customer</td>
                            <td width="75%">: <b>{{ $booking->cust_name }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%">No Telp / HP</td>
                            <td width="75%">: <b>{{ $booking->cust_phone_num }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%">Alamat Jemput</td>
                            <td width="75%">: <b>{{ $booking->cust_address }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%">Jam Stand By</td>
                            <td width="75%">: <b>{{ $booking->standby_time }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%">Tujuan / Rute</td>
                            <td width="75%">: <b>{{ $booking->destination }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%">Keterangan</td>
                            <td width="75%">: <b>{{ $booking->information }}</b></td>
                        </tr>
                        <tr>
                            <td width="25%">Tgl. Berangkat</td>
                            <td width="75%">:
                                <b>{{ GeneralHelper::konversiTgl($booking->booking_start_date, 'slash') }}</b>
                                -
                                <b>{{ GeneralHelper::konversiTgl($booking->booking_end_date, 'slash') }}</b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="except">Alokasi</th>
                            <th class="except">Jumlah</th>
                            <th class="except text-right">Biaya</th>
                            <th class="except">Durasi</th>
                            <th class="except text-right">Total Biaya</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $total = 0; @endphp
                        @foreach ($spjBudget as $account)
                            <tr>
                                <td class="except">{{ $account->budgetAccount->name }}</td>
                                <td class="except">{{ $account->qty }}</td>
                                <td class="except text-right">{{ GeneralHelper::rupiah($account->amount) }}</td>
                                <td class="except">{{ $account->duration }}</td>
                                <td class="except text-right">{{ GeneralHelper::rupiah($account->ttl_amount) }}</td>
                            </tr>
                        @php $total += $account->ttl_amount; @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="text-right except"><b>Total</b></td>
                            <td class="except text-right"><b>{{ GeneralHelper::rupiah($total) }}</b></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 text-center" style="margin-left: -400px;">
                <br>
                Penerima,

                <br>
                <br>
                <br>
                <br>
                <p>({{ $spj->mainDriver->name }})</p>
            </div>
            <div class="col-md-6 text-center" style="margin-left: 400px;">
                Subang, {{ GeneralHelper::konversiTgl($booking->booking_start_date, 'ttd') }} <br>
                Petugas,

                <br>
                <br>
                <br>
                <br>
                <p>({{ Session::get('auth_nama') }})</p>
            </div>
        </div>

    </div>
</body>
