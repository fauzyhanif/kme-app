<script>
$(document).ready(function () {
    datatableIndexPage();

    $('.form-add-qty').keyup(function() {
        var qty = this.value;
        var row = $(this).closest("tr");
        var amount = row.find('.form-add-amount').val();
        var duration = row.find('.form-add-duration').val();
        var result = (qty * duration) * amount.replace(/\./g, '');
        row.find('.form-add-ttl-amount').val(formatRupiah(result));

        sumTtlAmount();
    });

    $('.form-add-duration').keyup(function() {
        var duration = this.value;
        var row = $(this).closest("tr");
        var amount = row.find('.form-add-amount').val();
        var qty = row.find('.form-add-qty').val();
        var result = (qty * duration) * amount.replace(/\./g, '');
        row.find('.form-add-ttl-amount').val(formatRupiah(result));

        sumTtlAmount();
    });

    $('.form-add-amount').keyup(function() {
        var amount = this.value.replace(/\./g, '');
        var row = $(this).closest("tr");
        var qty = row.find('.form-add-qty').val();
        var duration = row.find('.form-add-duration').val();
        var result = (qty * duration) * amount;
        row.find('.form-add-ttl-amount').val(formatRupiah(result));

        sumTtlAmount();
    });
});

function datatableIndexPage() {
    var baseIndex = {!! json_encode(route('travel.spj.jsondata')) !!};
    $('.datatable-index-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: baseIndex,
        columns: [
            { data: 'spj_id', name: 'spj_id', searchable: true },
            { data: 'police_num', name: 'police_num', searchable: true },
            { data: 'main_driver', name: 'main_driver', searchable: true },
            { data: 'destination', name: 'destination', searchable: true },
            { data: 'booking_date', name: 'booking_date', searchable: true },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function sumTtlAmount() {
    var sum = 0;
    $('.form-add-ttl-amount').each(function(){
        sum += parseFloat(this.value.replace(/\./g, ''));
    });

    $('input[name="ttl_amount"]').val(sum);
    $('#ttl-amount').text("Rp. " + formatRupiah(sum))
}

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan SPJ");
            },
            success: function (response) {
                if (response.type == "success") {
                    toastr.success(response.text);

                    // redirect to detail
                    var detailUrl = {!! json_encode(url('/travel/booking/detail')) !!};
                    var detailUrl = detailUrl + "/" + response.booking_id;
                    document.location.href = detailUrl;

                    //this will redirect us in same window
                    var base = {!! json_encode(url('/travel/spj/print')) !!};
                    var urlReload = base + "/" + response.spj_id;
                    window.open(urlReload, '_blank');

                } else {
                    toastr.error(response.text)
                }
            },
            error: function (response) {
                toastr.error(response.text)
            }
        });
        document.documentElement.scrollTop = 0;
    });

    $('form[name="status_spj"]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $('#myModal').modal('hide');
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Ya, Batalkan Sekarang");
            },
            success: function (response) {
                if (response.type == "success") {
                    $('.datatable-index-page').DataTable().ajax.reload(null, true);
                    toastr.success(response.text)
                } else {
                    toastr.error(response.text)
                }
            },
            error: function (response) {
                toastr.error(response.text)
            }
        });
        document.documentElement.scrollTop = 0;
    });
})();

function popupStatus(id, status) {
    $('#myModal').modal('show');
    $('#spj-id').val(id);
    $('select[name="status"]').val(status);
}
</script>
