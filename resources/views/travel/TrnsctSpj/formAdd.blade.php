@extends('index')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('travel.spj.index') }}">
                <i class="fas fa-long-arrow-alt-left"></i> &nbsp;
                Daftar SPJ
            </a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ url('travel/booking/detail', $booking->booking_id) }}">
                Detail Booking
            </a>
        </li>
        <li class="breadcrumb-item active">Form SPJ</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Keterangan</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td width="30%">Kode Booking</td>
                                    <td width="70%">{{ $booking->booking_id }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Nama Pelanggan</td>
                                    <td width="70%">{{ $booking->cust_name }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">No. Handphone</td>
                                    <td width="70%">{{ $booking->cust_phone_num }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Tgl Booking</td>
                                    <td width="70%">
                                        {{ GeneralHelper::konversiTgl($booking->booking_start_date, 'slash') }}
                                        -
                                        {{ GeneralHelper::konversiTgl($booking->booking_end_date, 'slash') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="30%">Tujuan</td>
                                    <td width="70%">{{ $booking->destination }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Penjemputan</td>
                                    <td width="70%">{{ $booking->pick_up_location }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Stand By</td>
                                    <td width="70%">{{ $booking->standby_time }}</td>
                                </tr>
                                <tr>
                                    <td width="30%">Keterangan</td>
                                    <td width="70%">{{ $booking->information }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <form id="form-add" action="{{ route('travel.spj.addnew') }}" method="POST" data-remote>
                        @csrf
                        <input type="hidden" name="booking_id" value="{{ $booking->booking_id }}">
                        <input type="hidden" name="category_id" value="{{ $category->category_id }}">

                        <div class="card-header">
                            <h3 class="card-title">
                                Form SPJ untuk {{ $category->name }} ({{ $category->seat }} seat)
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Mobil</label>
                                <select name="police_num" class="form-control" required>
                                    <option value="">-- Pilih Mobil --</option>
                                    @foreach ($cars as $car)
                                        <option>{{ $car }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Driver Utama</label>
                                <select name="main_driver" class="form-control" required>
                                    <option value="">-- Pilih Driver --</option>
                                    @foreach ($drivers as $driver)
                                    <option value="{{ $driver->driver_id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Premi Driver Utama</label>
                                <input type="text" name="premi_main_driver" class="form-control money" value="0">
                            </div>

                            <div class="form-group">
                                <label>Driver Kedua</label>
                                <select name="second_driver" class="form-control">
                                    <option value="">-- Pilih Driver --</option>
                                    @foreach ($drivers as $driver)
                                    <option value="{{ $driver->driver_id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Premi Driver Kedua</label>
                                <input type="text" name="premi_second_driver" class="form-control money" value="0">
                            </div>

                            <div class="form-group">
                                <label>Helper</label>
                                <select name="helper_id" class="form-control">
                                    <option value="">-- Pilih Helper --</option>
                                    @foreach ($helpers as $helper)
                                    <option value="{{ $helper->helper_id }}">{{ $helper->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Premi Helper</label>
                                <input type="text" name="premi_helper" class="form-control money" value="0">
                            </div>

                            <div class="form-group">
                                <label>Uang diambil dari</label>
                                <select name="payment_method" class="form-control" required>
                                    <option value="">-- Pilih Kas/Bank --</option>
                                    @foreach ($cashAccounts as $account)
                                    <option value="{{ $account->account_id }}">{{ $account->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <hr>

                            <table class="table table-bordered">
                                <thead>
                                    <th width="20%">Alokasi</th>
                                    <th>Jumlah</th>
                                    <th>Durasi</th>
                                    <th>Biaya</th>
                                    <th>Jumlah Biaya</th>
                                </thead>
                                <tbody>
                                    @foreach ($budgetAccounts as $account)
                                        <tr>

                                            <input type="hidden" name="account_id[]" value="{{ $account->account_id }}">

                                            <td>{{ $account->name }}</td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="qty[{{ $account->account_id }}]"
                                                    value="0"
                                                    class="form-control form-add-qty">
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="duration[{{ $account->account_id }}]"
                                                    value="0"
                                                    class="form-control form-add-duration">
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="amount[{{ $account->account_id }}]"
                                                    value="0"
                                                    class="form-control form-add-amount money">
                                            </td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name="ttl_amount[{{ $account->account_id }}]"
                                                    value="0"
                                                    class="form-control form-add-ttl-amount money"
                                                    readonly>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" class="text-center bg-info">
                                            <input type="hidden" name="grand_ttl_amount" value="0">
                                            <h4>
                                                Total : <b id="ttl-amount">Rp. 0</b>
                                            </h4>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="card-body" style="border-top: 1px solid rgba(0,0,0,.125)">
                            <button type="submit" class="btn btn-success btn-save">
                                Simpan & Cetak
                            </button>
                            <button class="btn btn-secondary">
                                Batal
                            </button>
                        </div>
                        <div class="card-body">
                            <p class="font-italic"><span class="text-red">*</span>) Wajib diisi.</p>

                            <div class="border rounded" style="background-color: #FFF9CA">
                                <div class="p-3">
                                    <span class="font-weight-bold">Informasi!</span>
                                    <span>Jumlah biaya = jumlah x durasi x biaya.</span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@include('travel.TrnsctSpj.asset.js')
@endsection
