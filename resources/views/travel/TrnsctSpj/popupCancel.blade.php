<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">
                    Konfirmasi Pembatalan SPJ
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form name="status_spj" action="{{ route('travel.spj.change_status') }}" method="POST">
                @csrf
                <input type="hidden" name="id" id="spj-id">
                <input type="hidden" name="status" value="BATAL">
                <div class="modal-body">
                    Yakin ingin membatalkan SPJ ini?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-save">Ya, Batalkan Sekarang</button>
                </div>
            </form>
        </div>
    </div>
</div>
