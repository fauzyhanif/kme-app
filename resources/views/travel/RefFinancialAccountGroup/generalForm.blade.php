<div class="form-group">
    <label>Nomor Akun <span class="text-red">*</span></label>
    <input
        type="text"
        name="group_id"
        class="form-control"
        required
        @isset($group)
            readonly
            value="{{ $group->group_id }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Nama Akun <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($group)
            value="{{ $group->name }}"
        @endisset
    >
</div>

@isset($group)
    <label>Aktif?</label>
    <select name="active" class="form-control">
        <option
            value="Y"
            @isset($group)
                @if ($group->active == 'Y')
                selected
                @endif
            @endisset>
            Aktif
        </option>
        <option
            value="N"
            @isset($group)
                @if ($group->active == 'N')
                selected
                @endif
            @endisset>
            Nonaktif
        </option>
    </select>
@endisset
