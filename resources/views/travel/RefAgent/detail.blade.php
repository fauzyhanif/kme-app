@extends('index')

@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('travel.agent') }}">
				<i class="fas fa-long-arrow-alt-left"></i> &nbsp;
				Daftar Agent
			</a>
		</li>
		<li class="breadcrumb-item active">Detail Agent</li>
	</ol>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
				<div class="card shadow-none">
					<div class="card-header">
						<h3 class="card-title">
							Detail agent
						</h3>
					</div>
					<div class="card-body">
                        <table class="table table-sm table-borderless">
                            <tbody>
                                <tr>
                                    <td class="text-secondary" width="30%">Agent / Biro</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $agent->agent_type }}</td>
                                </div>
                                <tr>
                                    <td class="text-secondary" width="30%">Nama</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $agent->name }}</td>
                                </div>
                                <tr>
                                    <td class="text-secondary" width="30%">No Telp / Hp </td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $agent->phone_num }}</td>
                                </div>
                                <tr>
                                    <td class="text-secondary" width="30%">Alamat</td>
                                    <td class="text-right" width="3%">:</td>
                                    <td>{{ $agent->address }}</td>
                                </div>
                            </tbody>
                        </table>

					</div>
				</div>
			</div>
		</div>

        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="card-title">Sejarah Transaksi</h3>
                    </div>
                    <div class="card-body scroll-x">
                        <table class="table table-bordered table-hover datatable-detail-page">
                            <thead class="bg-info">
                                <th width="20%">Kode Booking</th>
                                <th>Tujuan</th>
                                <th>Tgl Berangkat</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>

{{-- for get data datatable --}}
<input type="hidden" name="agent_id" value="{{ $agent->agent_id }}">

@include('travel.RefAgent.asset.js')
@endsection
