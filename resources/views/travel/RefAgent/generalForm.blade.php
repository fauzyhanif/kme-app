<div class="form-group">
    <label>Type <span class="text-red">*</span></label>
    <select name="agent_type" class="form-control">
        <option value="AGENT"
            @isset($agent)
                @if ($agent->agent_type == 'AGENT')
                    selected
                @endif
            @endisset
        >
            AGENT
        </option>
        <option value="BIRO"
            @isset($agent)
                @if ($agent->agent_type == 'BIRO')
                    selected
                @endif
            @endisset
        >
            BIRO
        </option>
    </select>
</div>

<div class="form-group">
    <label>Nama <span class="text-red">*</span></label>
    <input
        type="text"
        name="name"
        class="form-control"
        required
        @isset($agent)
            value="{{ $agent->name }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>No. Handphone <span class="text-red">*</span></label>
    <input
        type="text"
        name="phone_num"
        class="form-control"
        required
        @isset($agent)
            value="{{ $agent->phone_num }}"
        @endisset
    >
</div>

<div class="form-group">
    <label>Alamat</label>
    <textarea name="address" class="form-control">@isset($agent){{ $agent->address }}@endisset</textarea>
</div>
