<script>
$(function() {
    loadData();
    loadDataDetailPage();
});

(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();
        var form    = $(this);
        var url     = form.prop('action');

        $.ajax({
            type: 'POST',
            url: url,
            dataType:'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".btn-save").addClass("disabled");
                $(".btn-save").text("Proses...");
            },
            complete: function() {
                $(".btn-save").removeClass("disabled");
                $(".btn-save").text("Simpan");
            },
            success: function (data) {
                if ($('input[name="type"]').val() != 'edit') {
                    form.trigger("reset");
                }

                toastr.success(data)
            },
            error: function (data) {
                var res = errorAlert(data);
                $('#response-alert').append(res);
            }
        });
    });
})();

function loadData() {
    var base = {!! json_encode(route('travel.agent.jsondata')) !!};
    $('.datatable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: base,

        columns: [
            { data: 'agent_type', name: 'agent_type', searchable: true },
            { data: 'name', name: 'name', searchable: true },
            { data: 'phone_num', name: 'phone_num', searchable: true },
            { data: 'address', name: 'address', searchable: false },
            { data: 'actions_link', name: 'actions_link', searchable: false },
        ]
    });
}

function loadDataDetailPage() {
    var agentId = $('input[name="agent_id"]').val();
    var base = {!! json_encode(route('travel.agent.datatable-detail-page')) !!};
    $('.datatable-detail-page').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: {
            "url": base,
            "type": "POST",
            "data": {
                "agent_id" : agentId,
            }
        },
        columns: [
            { data: 'booking_id', name: 'booking_id', searchable: true },
            { data: 'destination', name: 'destination', searchable: true },
            { data: 'booking_date', name: 'booking_date', searchable: false },
        ]
    });
}

</script>
