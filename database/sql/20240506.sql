-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 06 Bulan Mei 2024 pada 11.15
-- Versi server: 10.6.17-MariaDB-cll-lve
-- Versi PHP: 8.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kary2842_app`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_car`
--

CREATE TABLE `rice_ref_car` (
  `car_id` int(11) NOT NULL,
  `police_num` varchar(12) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `framework_num` varchar(45) DEFAULT NULL,
  `machine_num` varchar(45) DEFAULT NULL,
  `bpkb_num` varchar(45) DEFAULT NULL,
  `kir_date` date DEFAULT NULL,
  `stnk_tax_date` date DEFAULT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_ref_car`
--

INSERT INTO `rice_ref_car` (`car_id`, `police_num`, `owner`, `brand`, `type`, `color`, `framework_num`, `machine_num`, `bpkb_num`, `kir_date`, `stnk_tax_date`, `active`, `created_at`, `updated_at`) VALUES
(1, 'T1234BW', 'Haji Ade', '1', '2', '3', '4', '5', '6', NULL, NULL, 'Y', '2021-06-20 16:23:54', '2021-06-20 16:27:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_delivery_status`
--

CREATE TABLE `rice_ref_delivery_status` (
  `status_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_driver`
--

CREATE TABLE `rice_ref_driver` (
  `driver_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `sim_num` varchar(45) DEFAULT NULL,
  `phone_num` varchar(45) NOT NULL,
  `address` varchar(125) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_ref_driver`
--

INSERT INTO `rice_ref_driver` (`driver_id`, `name`, `sim_num`, `phone_num`, `address`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Hanifs', '1234', '082325494207', 'Legonkulon, subang', 'Y', '2021-06-22 17:48:00', '2021-06-22 17:49:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_helper`
--

CREATE TABLE `rice_ref_helper` (
  `helper_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_num` varchar(45) NOT NULL,
  `address` varchar(125) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_ref_helper`
--

INSERT INTO `rice_ref_helper` (`helper_id`, `name`, `phone_num`, `address`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Adzka', '089669', 'subang', 'Y', '2021-06-22 17:55:42', '2021-06-22 17:55:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_process`
--

CREATE TABLE `rice_ref_process` (
  `process_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_At` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_product`
--

CREATE TABLE `rice_ref_product` (
  `product_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_ref_product`
--

INSERT INTO `rice_ref_product` (`product_id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Padi 42', 'Y', '2021-06-23 19:03:25', '2021-06-23 19:03:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_product_stock`
--

CREATE TABLE `rice_ref_product_stock` (
  `stok_id` int(11) NOT NULL,
  `warehouse_id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `wet_stock` int(15) NOT NULL,
  `dry_stock` int(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_ref_warehouse`
--

CREATE TABLE `rice_ref_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL,
  `address` varchar(125) NOT NULL,
  `active` enum('Y','N') DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_ref_warehouse`
--

INSERT INTO `rice_ref_warehouse` (`warehouse_id`, `name`, `address`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Pabrik 1', 'depan jalan', 'Y', '2021-06-18 16:42:48', '2021-06-18 16:49:38'),
(2, 'Pabrik 2', 'Belakang', 'Y', '2021-06-18 16:42:57', '2021-06-18 16:42:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_trnsct_delivery`
--

CREATE TABLE `rice_trnsct_delivery` (
  `delivery_id` bigint(20) NOT NULL,
  `delivery_date` date NOT NULL,
  `back_date` date DEFAULT NULL,
  `car_id` int(5) NOT NULL,
  `car_sub_id` int(5) DEFAULT NULL,
  `driver_id` int(5) NOT NULL,
  `driver_sub_id` int(5) DEFAULT NULL,
  `helper_id` int(5) DEFAULT NULL,
  `helper_sub_id` int(5) DEFAULT NULL,
  `driver_commission` int(25) NOT NULL DEFAULT 0,
  `helper_commission` int(25) NOT NULL DEFAULT 0,
  `destination` varchar(65) NOT NULL,
  `status_id` int(5) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_trnsct_delivery`
--

INSERT INTO `rice_trnsct_delivery` (`delivery_id`, `delivery_date`, `back_date`, `car_id`, `car_sub_id`, `driver_id`, `driver_sub_id`, `helper_id`, `helper_sub_id`, `driver_commission`, `helper_commission`, `destination`, `status_id`, `created_at`, `updated_at`) VALUES
(2, '2021-09-01', NULL, 1, NULL, 1, NULL, 1, NULL, 0, 0, 'Pasar Induk Jakarta', 1, '2021-09-21 00:48:19', '2021-09-21 00:48:19'),
(3, '2022-10-01', NULL, 1, NULL, 1, NULL, 1, NULL, 0, 0, 'asdsad', 1, '2022-10-25 20:50:23', '2022-10-25 20:50:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_trnsct_delivery_item`
--

CREATE TABLE `rice_trnsct_delivery_item` (
  `id` int(11) NOT NULL,
  `delivery_id` int(15) NOT NULL,
  `product_id` int(5) NOT NULL,
  `price` int(15) NOT NULL,
  `begin_qty` float(10,2) NOT NULL,
  `sales_end` float(10,2) DEFAULT NULL,
  `sales_price` int(15) DEFAULT NULL,
  `sales_total` int(25) DEFAULT NULL,
  `end_qty` float(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_trnsct_delivery_item`
--

INSERT INTO `rice_trnsct_delivery_item` (`id`, `delivery_id`, `product_id`, `price`, `begin_qty`, `sales_end`, `sales_price`, `sales_total`, `end_qty`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 9500, 1000.00, NULL, NULL, NULL, NULL, '2021-09-21 00:51:15', '2021-09-21 00:51:15'),
(2, 2, 1, 9500, 5.00, NULL, NULL, NULL, NULL, '2021-09-21 00:53:26', '2021-09-21 00:53:26'),
(3, 3, 1, 9200, 10000.00, NULL, NULL, NULL, NULL, '2022-10-25 20:50:45', '2022-10-25 20:50:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_trnsct_processing`
--

CREATE TABLE `rice_trnsct_processing` (
  `process_id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `type` enum('OVEN','GILING') NOT NULL,
  `warehouse_id` int(5) NOT NULL,
  `pj` varchar(35) NOT NULL,
  `product_id` int(10) NOT NULL,
  `qty` float(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_trnsct_processing`
--

INSERT INTO `rice_trnsct_processing` (`process_id`, `date`, `type`, `warehouse_id`, `pj`, `product_id`, `qty`, `created_at`, `updated_at`) VALUES
(2, '2021-09-16', 'OVEN', 1, 'hanif', 1, 1000.00, '2021-09-16 00:51:51', '2021-09-16 00:51:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_trnsct_processing_item`
--

CREATE TABLE `rice_trnsct_processing_item` (
  `procitem_id` bigint(20) NOT NULL,
  `process_id` bigint(20) NOT NULL,
  `product_id` int(10) NOT NULL,
  `qty` float(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_trnsct_processing_item`
--

INSERT INTO `rice_trnsct_processing_item` (`procitem_id`, `process_id`, `product_id`, `qty`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 500.00, '2021-09-16 00:34:07', '2021-09-16 00:34:07'),
(3, 2, 1, 800.00, '2021-09-16 00:51:59', '2021-09-16 00:51:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_trnsct_purchase`
--

CREATE TABLE `rice_trnsct_purchase` (
  `purchase_id` bigint(20) NOT NULL,
  `purchase_date` date NOT NULL,
  `warehouse_id` int(5) NOT NULL,
  `pj` varchar(65) DEFAULT NULL,
  `farmer_name` varchar(65) DEFAULT NULL,
  `farmer_address` varchar(125) DEFAULT NULL,
  `farmer_phone` varchar(65) DEFAULT NULL,
  `agent_name` varchar(65) DEFAULT NULL,
  `agent_phone` varchar(65) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_trnsct_purchase`
--

INSERT INTO `rice_trnsct_purchase` (`purchase_id`, `purchase_date`, `warehouse_id`, `pj`, `farmer_name`, `farmer_address`, `farmer_phone`, `agent_name`, `agent_phone`, `created_at`, `updated_at`) VALUES
(1, '2021-09-01', 1, 'hanif', 'azizah', 'legonkulon', '089660500095', 'arin', '0821323132', '2021-09-01 01:14:55', '2021-09-01 01:14:55'),
(2, '2021-09-01', 0, 'Hanif', 'Azizah', '1313213', '089898989', 'arin', '1331232', '2021-08-31 18:34:37', '2021-08-31 18:34:37'),
(3, '2021-09-02', 2, 'Hanif', 'Rifki', 'Yogyakarta', '0891231312', 'Niky', '0891231323', '2021-08-31 19:24:38', '2021-08-31 19:24:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rice_trnsct_purchase_item`
--

CREATE TABLE `rice_trnsct_purchase_item` (
  `purcitem_id` bigint(20) NOT NULL,
  `purchase_id` bigint(20) NOT NULL,
  `product_id` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` int(20) NOT NULL,
  `total` int(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `rice_trnsct_purchase_item`
--

INSERT INTO `rice_trnsct_purchase_item` (`purcitem_id`, `purchase_id`, `product_id`, `qty`, `price`, `total`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 2000, 5200, 10400000, '2021-09-01 19:23:34', '2021-09-01 19:23:34'),
(2, 1, 1, 10000, 6000, 60000000, '2021-09-01 19:28:14', '2021-09-01 19:28:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_acl`
--

CREATE TABLE `sys_ref_acl` (
  `id` int(11) NOT NULL,
  `id_module` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `id_role` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `id_group_menu` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `is_menu` varchar(1) NOT NULL DEFAULT 'Y',
  `icon` varchar(45) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `is_child` varchar(1) NOT NULL DEFAULT 'N',
  `parent` int(5) DEFAULT NULL,
  `method` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `url` varchar(45) NOT NULL,
  `route` varchar(45) NOT NULL,
  `controller` varchar(45) NOT NULL,
  `function` varchar(45) NOT NULL,
  `middleware` varchar(45) DEFAULT NULL,
  `urut` int(5) DEFAULT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `sys_ref_acl`
--

INSERT INTO `sys_ref_acl` (`id`, `id_module`, `id_role`, `id_group_menu`, `is_menu`, `icon`, `label`, `is_child`, `parent`, `method`, `url`, `route`, `controller`, `function`, `middleware`, `urut`, `aktif`, `created_at`, `updated_at`) VALUES
(1, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-th-large', 'Module', 'N', NULL, '[\"get\"]', '/system/module', 'system.module', 'ModuleController', 'index', '[\"auth\"]', 1, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(6, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-users', 'Role', 'N', NULL, '[\"GET\"]', '/system/role', 'system.role', 'RoleController', 'index', '[\"auth\"]', 2, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(9, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'N', NULL, NULL, 'N', NULL, '[\"GET\"]', '/system/role/form-add', 'system.role.form.add', 'RoleController', 'formAdd', '[\"auth\"]', NULL, 'Y', '2020-12-28 02:41:58', '2020-12-28 02:41:58'),
(11, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-user', 'User', 'N', NULL, '[\"GET\"]', '/system/user', 'system.user', 'UserController', 'index', '[\"auth\"]', 3, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(16, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-stream', 'Group Menu', 'N', NULL, '[\"GET\"]', '/system/group-menu', 'system.groupmenu', 'GroupMenuController', 'index', '[\"auth\"]', 4, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(20, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'N', NULL, NULL, 'N', NULL, '[\"POST\"]', '/system/group-menu/add-new', 'system.groupmenu.add.new', 'GroupMenuController', 'addNew', '[\"auth\"]', NULL, 'Y', '2020-12-28 02:41:58', '2020-12-28 02:41:58'),
(21, '[\"1\"]', '[\"1\"]', '[\"3\"]', 'Y', 'fa-check-circle', 'ACL', 'N', NULL, '[\"GET\"]', '/system/acl', 'system.groupmenu', 'AclController', 'index', '[\"auth\"]', 4, 'Y', '2020-12-28 02:40:23', '2020-12-28 02:40:23'),
(27, '[\"2\"]', '[\"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-car', 'Kategori Kendaraan', 'N', NULL, '[\"GET\"]', '/travel/kategori-kendaraan', 'travel.kategorikendaraan', 'RefKategoriKendaraanController', 'index', 'null', 1, 'Y', '2021-01-03 19:37:44', '2021-01-03 19:39:33'),
(30, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-car-side', 'Kendaraan', 'N', NULL, '[\"GET\"]', '/travel/kendaraan', 'travel.kendaraan', 'Travel\\RefKendaraanController', 'index', '[\"auth\"]', 2, 'Y', '2021-01-04 01:03:52', '2021-01-04 01:03:52'),
(31, '[\"2\"]', '[\"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-id-card', 'Driver', 'N', NULL, '[\"GET\"]', '/travel/driver', 'travel.driver', 'Travel\\RefDriverController', 'index', 'null', 3, 'Y', '2021-01-03 19:37:44', '2021-01-03 19:39:33'),
(32, '[\"2\"]', '[\"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-id-card-alt', 'Helper', 'N', NULL, '[\"GET\"]', '/travel/helper', 'travel.driver', 'Travel\\RefHelperController', 'index', 'null', 4, 'Y', '2021-01-03 19:37:44', '2021-01-03 19:39:33'),
(33, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-users', 'Customer', 'N', NULL, '[\"GET\"]', '/travel/customer', 'travel.customer', 'Travel\\RefPelangganController', 'index', '[\"auth\"]', 5, 'Y', '2021-01-18 19:03:37', '2021-01-18 19:03:37'),
(34, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-handshake', 'Input Booking', 'N', NULL, '[\"GET\"]', '/travel/booking/search-cars-page', 'travel.booking.search.cars.page', 'Travel\\TrxBookingController', 'searchCarsPage', 'null', 1, 'Y', '2021-01-18 19:17:08', '2021-01-18 19:19:09'),
(35, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-book', 'Daftar Booking', 'N', NULL, '[\"GET\"]', '/travel/booking', 'travel.booking', 'Travel/TrxBookingController', 'index', '[\"auth\"]', 2, 'Y', '2021-01-18 20:57:44', '2021-01-18 20:57:44'),
(36, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-file-alt', 'Cetak SPJ', 'N', NULL, '[\"GET\"]', '/travel/spj', 'travel.spj.index', 'SpjController', 'index', 'null', 3, 'Y', '2021-01-28 18:46:11', '2021-01-28 18:46:41'),
(37, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-file-invoice', 'Pembayaran', 'N', NULL, '[\"GET\"]', '/travel/payment', 'travel.payment', 'Travel\\TrnsctPaymentController', 'index', 'null', 4, 'Y', '2021-01-29 05:07:26', '2021-01-29 05:08:06'),
(38, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"1\"]', 'Y', 'fa-user-tag', 'Agen', 'N', NULL, '[\"GET\"]', '/travel/agen', 'travel.agen', 'Travel/RefAgenController', 'index', 'null', 6, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(40, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-calendar-check', 'Kalender Booking', 'N', NULL, '[\"GET\"]', '/travel/kalender-booking', 'travel.kalender.booking', 'Travel/TrxCalendarBookingController', 'index', 'null', 5, 'Y', '2021-02-02 05:58:57', '2021-02-02 06:07:15'),
(41, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-stream', 'Group Akun', 'N', NULL, '[\"GET\"]', '/travel/finance/account-group', 'travel.finance.account-group', 'PaymentAccountController', 'index', '[\"auth\"]', 1, 'Y', '2021-03-30 23:08:16', '2021-03-30 23:08:16'),
(42, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Pemasukan', 'N', NULL, '[\"GET\"]', '/travel/income', 'travel.income', 'Travel/TrnsctIncomeController', 'index', '[\"auth\"]', 3, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(43, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Pengeluaran', 'N', NULL, '[\"GET\"]', '/travel/spending', 'travel.income', 'Travel/TrnsctSpendingController', 'index', '[\"auth\"]', 4, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(44, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"6\"]', 'Y', 'fa-clipboard-list', 'Agenda Armada', 'N', NULL, '[\"GET\"]', '/travel/laporan/agenda-armada', 'travel.laporan.agenda-armada', 'ReportAgendaUnit', 'index', '[\"auth\"]', 1, 'Y', '2021-04-02 18:30:53', '2021-04-02 18:30:53'),
(45, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"6\"]', 'Y', 'fa-clipboard-list', 'Pendapatan Sewa Bus', 'N', NULL, '[\"GET\"]', '/travel/laporan/pendapatan-sewa-bus', 'travel.laporan.pendapatan-sewa-bus', 'ReportIncome', 'index', '[\"auth\"]', 1, 'Y', '2021-04-02 18:30:53', '2021-04-02 18:30:53'),
(46, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"7\"]', 'Y', 'fa-user-tag', 'Jabatan', 'N', NULL, '[\"GET\"]', '/system/jabatan', 'system.jabatan', 'System/RefSdmPositionController', 'index', '[\"auth\"]', 7, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(47, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"7\"]', 'Y', 'fa-user-tag', 'SDM', 'N', NULL, '[\"GET\"]', '/system/sdm', 'system.sdm', 'System/RefSdmPositionController', 'index', '[\"auth\"]', 8, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(48, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-stream', 'Akun', 'N', NULL, '[\"GET\"]', '/travel/finance/account', 'travel.finance.account', 'PaymentAccountController', 'index', '[\"auth\"]', 2, 'Y', '2021-03-30 23:08:16', '2021-03-30 23:08:16'),
(49, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-clipboard-list', 'Laba Rugi', 'N', NULL, '[\"GET\"]', '/travel/laba-rugi', 'travel.laba-rugi', 'Travel/ReportLabaRugiController', 'index', '[\"auth\"]', 5, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(50, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Terima Uang', 'N', NULL, '[\"GET\"]', '/travel/receive_money', 'travel.receive_money', 'Travel/TrnsctSpendingController', 'index', '[\"auth\"]', 4, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(51, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-donate', 'Kirim Uang', 'N', NULL, '[\"GET\"]', '/travel/deposit_money', 'travel.deposit_money', 'Travel/TrnsctSpendingController', 'index', '[\"auth\"]', 4, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(52, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-clipboard-list', 'Neraca', 'N', NULL, '[\"GET\"]', '/travel/neraca', 'travel.neraca', 'Travel/ReportNeracaController', 'index', '[\"auth\"]', 6, 'Y', '2021-03-31 18:19:52', '2021-03-31 18:19:52'),
(53, '[\"2\"]', '[\"2\", \"3\"]', '[\"4\"]', 'Y', 'fa-play', 'Berangkat Minggu Ini', 'N', NULL, '[\"GET\"]', '/travel/booking/leaving_this_week', 'travel.booking.leaving_this_week', 'Travel\\TrnsctBookingController', 'leavingThisWeek', '[\"auth\"]', 2, 'Y', '2021-05-31 19:01:40', '2021-05-31 19:01:40'),
(54, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-file-alt', 'Laporan Pengeluaran', 'N', NULL, '[\"GET\"]', '/travel/report/expense_monthly', '/travel/report/expense_monthly', 'ReportExpenseMonthlyController', 'index', '[\"auth\"]', 8, 'Y', '2021-06-13 04:56:08', '2021-06-13 04:56:08'),
(55, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"2\"]', 'Y', 'fa-folder', 'Pabrik / Gudang', 'N', NULL, '[\"GET\"]', '/rice/warehouse', 'beras.pabrik', 'Rice/RefWarehouseController', 'index', '[\"auth\"]', 1, 'Y', '2021-06-18 16:05:55', '2021-06-18 16:05:55'),
(56, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"2\"]', 'Y', 'fa-car', 'Transportasi', 'N', NULL, '[\"GET\"]', '/rice/car', 'beras.pabrik', 'Rice/RefCarController', 'index', '[\"auth\"]', 2, 'Y', '2021-06-18 16:05:55', '2021-06-18 16:05:55'),
(57, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"2\"]', 'Y', 'fa-users', 'Driver', 'N', NULL, '[\"GET\"]', '/rice/driver', 'rice.driver', 'Rice/RefDriverController', 'index', '[\"auth\"]', 3, 'Y', '2021-06-22 17:36:05', '2021-06-22 17:36:05'),
(58, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"2\"]', 'Y', 'fa-users', 'Helper', 'N', NULL, '[\"GET\"]', '/rice/helper', 'rice.helper', 'Rice/RefHelperController', 'index', '[\"auth\"]', 4, 'Y', '2021-06-22 17:36:05', '2021-06-22 17:36:05'),
(59, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"2\"]', 'Y', 'fa-cubes', 'Produk', 'N', NULL, '[\"GET\"]', '/rice/product', 'rice.product', 'Rice/RefProductController', 'index', '[\"auth\"]', 5, 'Y', '2021-06-22 17:36:05', '2021-06-22 17:36:05'),
(60, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"8\"]', 'Y', 'fa-dolly', 'Pembelian', 'N', NULL, '[\"GET\"]', '/rice/purchase', 'rice.purchase', '/Rice/TrnsctPurchaseController', 'index', '[\"auth\"]', 1, 'Y', '2021-08-10 04:41:19', '2021-08-10 04:41:19'),
(61, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"8\"]', 'Y', 'fa-dharmachakra', 'Oven & Giling', 'N', NULL, '[\"GET\"]', '/rice/processing', 'rice.purchase', '/Rice/TrnsctProcessingController', 'index', '[\"auth\"]', 2, 'Y', '2021-08-10 04:41:19', '2021-08-10 04:41:19'),
(62, '[\"3\"]', '[\"1\", \"2\", \"4\"]', '[\"8\"]', 'Y', 'fa-truck', 'Pengiriman', 'N', NULL, '[\"GET\"]', '/rice/delivery', 'rice.delivery', '/Rice/TrnsctDeliveryController', 'index', '[\"auth\"]', 3, 'Y', '2021-08-10 04:41:19', '2021-08-10 04:41:19'),
(63, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"7\"]', 'Y', 'fa-file-invoice', 'Penggajian', 'N', NULL, '[\"GET\"]', '/travel/penggajian', 'travel.penggajian', 'Travel/TrnsctPayrolController', 'index', '[\"auth\"]', 9, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(64, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"7\"]', 'Y', 'fa-file-invoice', 'Kasbon', 'N', NULL, '[\"GET\"]', '/travel/kasbon', 'travel.kasbon', 'Travel/TrnsctDebtController', 'index', '[\"auth\"]', 10, 'Y', '2021-01-29 20:37:18', '2021-01-31 17:50:36'),
(65, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-file-alt', 'Laporan Pemasukan', 'N', NULL, '[\"GET\"]', '/travel/report/income_monthly', '/travel/report/income_monthly', 'ReportIncomeMonthlyController', 'index', '[\"auth\"]', 8, 'Y', '2021-06-13 04:56:08', '2021-06-13 04:56:08'),
(66, '[\"2\"]', '[\"1\", \"2\", \"3\"]', '[\"5\"]', 'Y', 'fa-file-alt', 'Laporan Keuangan', 'N', NULL, '[\"GET\"]', '/travel/report/finance_monthly', '/travel/report/finance_monthly', 'ReportFinanceMonthlyController', 'index', '[\"auth\"]', 8, 'Y', '2021-06-13 04:56:08', '2021-06-13 04:56:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_group_menu`
--

CREATE TABLE `sys_ref_group_menu` (
  `id_group_menu` int(11) NOT NULL,
  `id_module` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `id_role` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `nama` varchar(35) NOT NULL,
  `urut` int(2) DEFAULT NULL,
  `aktif` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `sys_ref_group_menu`
--

INSERT INTO `sys_ref_group_menu` (`id_group_menu`, `id_module`, `id_role`, `nama`, `urut`, `aktif`, `created_at`, `updated_at`) VALUES
(1, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'DATA MASTER TRAVEL', 1, 'Y', '2020-12-27 02:37:10', '2020-12-28 02:37:13'),
(2, '[\"3\"]', '[\"1\", \"2\", \"4\"]', 'DATA MASTER BERAS', 1, 'Y', '2020-12-27 02:37:33', '2020-12-28 02:37:26'),
(3, '[\"1\"]', '[\"1\", \"2\"]', 'DATA SYSTEM', 1, 'Y', '2020-12-28 02:36:54', '2020-12-28 02:36:54'),
(4, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'MENU UTAMA', 2, 'Y', '2021-01-18 19:12:07', '2021-01-18 19:12:07'),
(5, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'KEUANGAN', 4, 'Y', '2021-03-30 23:00:55', '2021-03-30 23:01:26'),
(6, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'LAPORAN', 5, 'Y', '2021-04-02 18:24:55', '2021-04-02 18:24:55'),
(7, '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'KEPEGAWAIAN', 3, 'Y', '2021-03-30 23:00:55', '2021-03-30 23:01:26'),
(8, '[\"3\"]', '[\"1\", \"2\", \"4\"]', 'TRANSAKSI', 1, 'Y', '2020-12-27 02:37:33', '2020-12-28 02:37:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_module`
--

CREATE TABLE `sys_ref_module` (
  `id` int(11) NOT NULL,
  `id_module` int(5) NOT NULL,
  `image` varchar(45) DEFAULT NULL,
  `nama` varchar(45) NOT NULL,
  `deskripsi` text NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `sys_ref_module`
--

INSERT INTO `sys_ref_module` (`id`, `id_module`, `image`, `nama`, `deskripsi`, `aktif`, `created_at`, `updated_at`) VALUES
(14, 1, '20201229013241.png', 'System Developer', 'System Developer', 'Y', '2020-12-21 21:39:23', '2020-12-28 18:32:41'),
(15, 2, '20201228091521.png', 'SIM Travel', 'Sistem Informasi Manajemen Travel', 'Y', '2020-12-21 21:39:49', '2020-12-28 02:15:21'),
(16, 3, '20201228091601.png', 'SIM Beras', 'Sistem Informasi Manajemen Beras', 'Y', '2020-12-28 02:16:01', '2020-12-28 02:16:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_role`
--

CREATE TABLE `sys_ref_role` (
  `id` int(11) NOT NULL,
  `id_role` int(5) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `id_module` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `sys_ref_role`
--

INSERT INTO `sys_ref_role` (`id`, `id_role`, `nama`, `id_module`, `aktif`, `created_at`, `updated_at`) VALUES
(5, 1, 'Admin Developer', '[\"1\", \"2\", \"3\"]', 'Y', '2020-12-26 20:56:37', '2020-12-28 02:32:28'),
(6, 2, 'Superadmin', '[\"2\", \"3\"]', 'Y', '2020-12-26 20:56:45', '2020-12-28 02:32:39'),
(7, 3, 'Admin Travel', '[\"2\"]', 'Y', '2020-12-26 20:56:52', '2020-12-28 02:32:55'),
(8, 4, 'Admin Beras', '[\"3\"]', 'Y', '2020-12-26 20:56:59', '2020-12-28 02:33:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_salary`
--

CREATE TABLE `sys_ref_salary` (
  `salary_id` int(11) NOT NULL,
  `module_id` varchar(5) NOT NULL,
  `month` varchar(10) NOT NULL,
  `position_id` varchar(5) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `salary` int(20) NOT NULL,
  `salary_paid` int(20) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_sdm`
--

CREATE TABLE `sys_ref_sdm` (
  `sdm_id` int(11) NOT NULL,
  `module_id` varchar(5) NOT NULL,
  `position_id` varchar(5) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_num` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `sys_ref_sdm`
--

INSERT INTO `sys_ref_sdm` (`sdm_id`, `module_id`, `position_id`, `nip`, `name`, `phone_num`, `address`, `active`, `created_at`, `updated_at`) VALUES
(2, '2', '01', '01', 'Irfan Insani', '08xxx', 'Rancasari', 'Y', '2022-07-26 23:53:21', '2022-07-26 23:53:21'),
(3, '2', '02', '02', 'H.M. Yusuf', '08xxx', 'Kertajaya', 'Y', '2022-07-26 23:54:02', '2022-07-26 23:54:02'),
(4, '2', '03', '03', 'Hj. Nani Inayah, SE', '08xxx', 'Kertajaya', 'Y', '2022-07-26 23:55:01', '2022-07-26 23:55:01'),
(5, '2', '04', '04', 'Aziz Marfu Hakim', '08xxx', 'Rancasari', 'Y', '2022-07-26 23:57:08', '2022-07-26 23:57:08'),
(6, '2', '04', '05', 'Yusup Takhyudin', '08xxx', 'Bongas', 'Y', '2022-07-26 23:57:51', '2022-07-26 23:57:51'),
(7, '2', '05', '06', 'Heri Herdianto', '08xxx', 'Rancasari', 'Y', '2022-07-27 00:06:58', '2022-07-27 00:06:58'),
(8, '2', '05', '07', 'Ujang K', '08XXX', 'Pamanukan', 'Y', '2022-07-27 00:07:29', '2022-07-27 00:07:29'),
(9, '2', '06', '08', 'Wawan', '08xxx', 'Rancaudik', 'Y', '2022-07-27 00:08:06', '2022-07-27 00:08:06'),
(10, '2', '07', '09', 'Iyan', '08xxx', 'Rancaudik', 'Y', '2022-07-27 00:10:01', '2022-07-27 00:10:01'),
(12, '2', '08', '10', 'Dede Mulyadi', '08XXX', 'Rancasari', 'Y', '2022-07-27 00:11:40', '2022-07-27 00:11:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_sdm_position`
--

CREATE TABLE `sys_ref_sdm_position` (
  `module_id` varchar(10) NOT NULL,
  `position_id` varchar(5) NOT NULL,
  `name` varchar(45) NOT NULL,
  `wages` int(15) NOT NULL DEFAULT 0,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `sys_ref_sdm_position`
--

INSERT INTO `sys_ref_sdm_position` (`module_id`, `position_id`, `name`, `wages`, `active`, `created_at`, `updated_at`) VALUES
('2', '01', 'Direktur', 5000000, 'Y', '2021-04-03 20:00:59', '2022-07-26 23:45:13'),
('2', '02', 'Manajer Oprasional', 5000000, 'Y', '2021-04-03 20:18:42', '2022-07-26 23:46:22'),
('2', '03', 'Bendahara', 1500000, 'Y', '2021-04-03 20:18:51', '2022-07-26 23:47:07'),
('2', '04', 'Karyawan', 2500000, 'Y', '2021-04-03 20:19:10', '2022-07-26 23:48:00'),
('2', '05', 'Security', 1750000, 'Y', '2021-04-03 20:19:17', '2022-07-26 23:48:20'),
('2', '06', 'Montir 1', 5000000, 'Y', '2021-04-03 20:19:26', '2022-07-26 23:50:04'),
('2', '07', 'Montir 2', 1000000, 'Y', '2022-07-26 23:50:38', '2022-07-26 23:51:53'),
('2', '08', 'Office Boy', 1750000, 'Y', '2021-04-03 20:19:43', '2022-07-26 23:52:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_ref_user`
--

CREATE TABLE `sys_ref_user` (
  `id` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `id_module` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `id_role` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `aktif` enum('Y','N') DEFAULT 'Y',
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sys_ref_user`
--

INSERT INTO `sys_ref_user` (`id`, `nama`, `email`, `email_verified_at`, `password`, `no_telp`, `alamat`, `id_module`, `id_role`, `aktif`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Yusup', 'admin@admin.com', NULL, '$2y$10$zP4ZwKmd.CRYGuIiH6xupuzF.xSc70bhhmRymEmdYgxcDd0YdN3za', '082325494207', 'Subang', '[\"1\", \"2\", \"3\"]', '[\"1\", \"2\", \"3\", \"4\"]', 'Y', 'WD7BGSZz4cStSNbeRFQoOZ1wdX6a3dhijPQldDtPlzLehi9tHGrwPOmS0jp3', '2020-12-16 21:06:19', '2020-12-28 02:35:41'),
(2, 'Admin Beras', 'beras@admin.com', NULL, '$2y$10$gj1rXzJ1/sU7Q2Yy5PAlr.Y45GMhxggPI9DFDnG6iEJAP44vVHQ4q', '08231241412', 'Subang', '[\"3\"]', '[\"4\"]', 'Y', NULL, '2020-12-26 21:15:20', '2020-12-28 02:34:46'),
(3, 'Admin Travel', 'travel@addmin.com', NULL, '$2y$10$jqkQN53vpn01kDSPxjAyzOdFFoEpCV/sH0p8CtyucCQhVb1dRZhhW', '082325421424', 'Subang', '[\"2\"]', '[\"3\"]', 'N', NULL, '2020-12-26 21:50:04', '2020-12-28 02:35:16'),
(4, 'Yusup', 'yusuf@admin.com', NULL, '$2y$10$3hezDcbXgZZqNZwaQ0erAuthWKtIJlk8BZodln4sJiupj2bujgfqq', '08500000000', 'Subang', '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'Y', NULL, '2021-05-03 06:08:07', '2021-05-03 06:08:07'),
(5, 'Irfan', 'irfan@admin.com', NULL, '$2y$10$5leVWgh7oVyMFL7NQjxUc.ljgzAF68U09dYuO6XETou0NG3fAg4yq', '085000000000', 'Subang', '[\"2\"]', '[\"1\", \"2\", \"3\"]', 'Y', NULL, '2021-05-03 06:08:33', '2021-05-03 06:08:33'),
(6, 'Aziza', 'aziz@admin.com', NULL, '$2y$10$.1wKtTd9.Su8NvFKAEuIAOFlrBD/naJu1fyks1rFkGC7VSYOHBKyW', '085000000000', 'Subang', '[\"2\"]', '[\"2\", \"3\"]', 'Y', NULL, '2021-05-03 06:09:00', '2021-05-03 06:09:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_agent`
--

CREATE TABLE `trvl_ref_agent` (
  `agent_id` int(11) NOT NULL,
  `agent_type` enum('AGENT','BIRO') NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_num` varchar(25) NOT NULL,
  `address` text DEFAULT NULL,
  `ttl_trnsct` int(5) DEFAULT NULL,
  `ttl_commission` int(25) DEFAULT NULL,
  `active` enum('Y','N') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_agent`
--

INSERT INTO `trvl_ref_agent` (`agent_id`, `agent_type`, `name`, `phone_num`, `address`, `ttl_trnsct`, `ttl_commission`, `active`, `created_at`, `updated_at`) VALUES
(1, 'AGENT', 'AGEN PAMANUKAN', '0', 'PAMANUKAN', NULL, NULL, 'Y', '2021-07-03 01:41:30', '2021-07-03 01:41:30'),
(4, 'AGENT', 'Bp. Samir', '085664134487', 'Compreng', NULL, NULL, 'Y', '2022-10-24 02:41:07', '2022-10-24 02:41:07'),
(5, 'AGENT', 'Ibu Hj. Eli', '085691548890', 'Pamanukan', NULL, NULL, 'Y', '2022-10-24 03:11:35', '2022-10-24 03:11:35'),
(6, 'AGENT', 'Ibu Hj. Erni', '08xxx', 'Pamanukan', NULL, NULL, 'Y', '2022-10-25 21:12:08', '2022-10-25 21:12:08'),
(7, 'AGENT', 'Java Tour Karawang', '081215555128', 'Karawang', NULL, NULL, 'Y', '2022-11-03 03:00:55', '2022-11-03 03:00:55'),
(8, 'AGENT', 'Bp. Tama', '081215555128', 'Purwadadi', NULL, NULL, 'Y', '2022-11-08 20:10:34', '2022-11-08 20:10:34'),
(9, 'AGENT', 'Bp. Hasan Sanuri', '081215555128', 'Patrol', NULL, NULL, 'Y', '2022-11-08 22:29:21', '2022-11-08 22:29:21'),
(10, 'BIRO', 'Bp. Hasan Sanuri', '081215555128', 'Patrol', NULL, NULL, 'Y', '2022-12-10 05:47:04', '2022-12-10 05:47:04'),
(11, 'AGENT', 'Bp. Adi', '081215555128', 'As-syifa jalan cagak', NULL, NULL, 'Y', '2022-12-13 04:46:43', '2022-12-13 04:46:43'),
(12, 'BIRO', 'Parman CV Ma\'arif Tour & Trevel', '082129946179', 'Indramayu', NULL, NULL, 'Y', '2022-12-22 03:41:11', '2022-12-22 03:41:11'),
(13, 'BIRO', 'Bp. Parman CV Ma\'Arif Tour & Trevel', '082129946179', 'indramayu', NULL, NULL, 'Y', '2022-12-22 03:45:33', '2022-12-22 03:45:33'),
(14, 'AGENT', 'Bp. Hasan Sanuri', '081325567220', 'Patrol', NULL, NULL, 'Y', '2023-01-20 22:12:34', '2023-01-20 22:12:34'),
(15, 'AGENT', 'DWP KAB. SUBANG', '082319621973', 'Subang', NULL, NULL, 'Y', '2023-01-25 19:36:12', '2023-01-25 19:36:12'),
(16, 'AGENT', 'Bp. H. Tatang', '082129946179', 'Tanjung Salep', NULL, NULL, 'Y', '2023-02-16 23:20:14', '2023-02-16 23:20:14'),
(17, 'AGENT', 'anida', '0', NULL, NULL, NULL, 'Y', '2023-05-20 04:30:21', '2023-05-20 04:30:21'),
(18, 'AGENT', 'anida', '081215555128', NULL, NULL, NULL, 'Y', '2023-05-20 04:30:34', '2023-05-20 04:30:34'),
(19, 'AGENT', 'java tour', '081215555128', 'karawang', NULL, NULL, 'Y', '2023-05-24 05:53:09', '2023-05-24 05:53:09'),
(20, 'AGENT', 'ibu nani', '081215555128', 'jalan cagak', NULL, NULL, 'Y', '2023-07-02 05:10:04', '2023-07-02 05:10:04'),
(21, 'AGENT', 'Bp. Uke', '081215555128', 'Pagaden', NULL, NULL, 'Y', '2023-08-04 00:19:39', '2023-08-04 00:19:39'),
(22, 'AGENT', 'Mamah Dimas', '081617000745', 'Subang', NULL, NULL, 'Y', '2023-09-04 05:57:21', '2023-09-04 05:57:21'),
(23, 'AGENT', 'Mamah Dimas', '081617000745', 'subang', NULL, NULL, 'Y', '2023-09-04 06:04:45', '2023-09-04 06:04:45'),
(24, 'AGENT', 'Bp. Udin', '085352983954', 'Rancasari STIE', NULL, NULL, 'Y', '2023-09-30 06:05:32', '2023-09-30 06:05:32'),
(25, 'AGENT', 'Bp. Asep Astahanas', '082120357400', 'Jungklang', NULL, NULL, 'Y', '2024-02-22 01:26:04', '2024-02-22 01:26:04'),
(26, 'BIRO', 'Hasan Sanuri / Harum Tours', '081325567220', 'Patrol', NULL, NULL, 'Y', '2024-04-30 20:18:28', '2024-04-30 20:18:28'),
(27, 'AGENT', 'Bp. Deden', '081328196867', 'Ciasem', NULL, NULL, 'Y', '2024-05-02 20:12:28', '2024-05-02 20:12:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_booking_id`
--

CREATE TABLE `trvl_ref_booking_id` (
  `id` int(11) NOT NULL,
  `month` varchar(7) NOT NULL,
  `serial_num` int(4) UNSIGNED ZEROFILL NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_booking_id`
--

INSERT INTO `trvl_ref_booking_id` (`id`, `month`, `serial_num`, `created_at`, `updated_at`) VALUES
(1, '2023-09', 0003, '2023-09-23 08:36:22', '2023-09-30 13:06:48'),
(2, '2023-10', 0003, '2023-10-07 10:34:32', '2023-10-14 02:03:34'),
(3, '2023-11', 0002, '2023-11-10 10:22:48', '2023-11-10 10:22:48'),
(4, '2023-12', 0002, '2023-12-26 11:00:07', '2023-12-26 11:00:07'),
(5, '2024-01', 0004, '2024-01-04 02:50:11', '2024-01-30 12:43:44'),
(6, '2024-02', 0010, '2024-02-01 10:51:31', '2024-02-27 05:32:02'),
(7, '2024-03', 0004, '2024-03-04 06:22:47', '2024-03-14 08:49:36'),
(8, '2024-04', 0003, '2024-04-25 07:38:14', '2024-04-29 06:06:18'),
(9, '2024-05', 0004, '2024-05-01 03:32:31', '2024-05-03 02:34:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car`
--

CREATE TABLE `trvl_ref_car` (
  `id` int(11) NOT NULL,
  `category_id` varchar(4) NOT NULL,
  `condition_id` int(5) DEFAULT 1,
  `police_num` varchar(10) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `owners_address` text DEFAULT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `raft_year` varchar(4) DEFAULT NULL,
  `purchase_year` varchar(4) DEFAULT NULL,
  `machine_cc` varchar(10) DEFAULT NULL,
  `framework_num` varchar(45) DEFAULT NULL,
  `machine_num` varchar(45) DEFAULT NULL,
  `bpkb_num` varchar(45) DEFAULT NULL,
  `nominal_tax` int(22) DEFAULT NULL,
  `kir_date` varchar(12) DEFAULT NULL,
  `kps_permit_date` varchar(12) DEFAULT NULL,
  `insurance_date` varchar(12) DEFAULT NULL,
  `stnk_tax_date` varchar(12) DEFAULT NULL,
  `active` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_car`
--

INSERT INTO `trvl_ref_car` (`id`, `category_id`, `condition_id`, `police_num`, `owner`, `owners_address`, `brand`, `type`, `color`, `raft_year`, `purchase_year`, `machine_cc`, `framework_num`, `machine_num`, `bpkb_num`, `nominal_tax`, `kir_date`, `kps_permit_date`, `insurance_date`, `stnk_tax_date`, `active`, `created_at`, `updated_at`) VALUES
(5, '1', 1, 'T 7564 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19121', 'J08EUFJ77145', 'M13674652', 1234900, NULL, NULL, NULL, '2021-08-21', 'Y', NULL, '2021-07-09 03:55:58'),
(6, '1', 1, 'T 7566 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19124', 'J08EUFJ77148', 'M13674654', 1234900, NULL, NULL, NULL, '2021-08-21', 'Y', NULL, '2021-07-09 03:56:46'),
(7, '1', 1, 'T 7573 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2017', '2017', '7684', 'MJERK8JSKHJN20120', 'J08EUFJ85512', 'N06047673', 1238100, '2021-12-08', '2022-04-09', NULL, '2021-11-21', 'Y', NULL, '2021-07-09 04:00:12'),
(8, '1', 1, 'T 7574 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2017', '2017', '7684', 'MJERK8JSKHJN20112', 'J08EUFJ85390', 'N06047674', 1238100, '2021-12-08', '2022-04-09', NULL, '2021-11-21', 'Y', NULL, '2021-07-09 04:10:55'),
(9, '1', 1, 'T 7576 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2017', '2017', '7684', 'MJERK8JSKHJN20113', 'J08EUFJ85391', 'N06047675', 1238100, '2021-12-08', '2022-04-09', NULL, '2021-11-21', 'Y', NULL, '2021-07-09 04:13:38'),
(10, '2', 1, 'T 7545 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ / R260', 'ORANGE KOMBINASI', '2015', '2015', '7684', 'MJERK8JSKFJN18110', 'J08EUFJ73302', 'M01174561', 1800100, '2021-09-29', '2022-12-03', NULL, '2021-09-20', 'Y', NULL, '2021-07-09 04:17:38'),
(11, '2', 1, 'T 7546 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ / R260', 'ORANGE KOMBINASI', '2015', '2015', '7684', 'MJERK8JSKFJN18106', 'J08EUFJ73298', 'M01174562', 1800100, '2021-09-29', '2022-12-03', NULL, '2021-09-20', 'Y', NULL, '2021-07-09 04:19:36'),
(12, '2', 1, 'T 7547 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ / R260', 'ORANGE KOMBINASI', '2015', '2015', '7684', 'MJERK8JSKFJN18107', 'J08EUFJ73299', 'MO1174563', 1800100, '2021-04-01', '2022-12-03', NULL, '2021-09-20', 'Y', NULL, '2021-07-09 04:25:09'),
(13, '2', 1, 'T 7562 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19130', 'EUFJ77154', 'M13674650', 1234900, '2021-09-12', '2022-12-03', NULL, '2021-08-21', 'Y', NULL, '2021-07-09 04:27:55'),
(14, '2', 1, 'T 7565 TC', 'PT KARYA MAS EMPAT', NULL, 'HINO', 'RK8JSKA-NHJ', 'ORANGE KOMBINASI', '2016', '2016', '7684', 'MJERK8JSKGJN19127', 'J08EUFJ77151', 'M13674653', 1234900, '2021-08-12', '2022-12-03', NULL, '2021-08-21', 'Y', NULL, '2021-07-09 04:29:17'),
(15, '3', 1, 'T 7532 TC', 'PT KARYA MAS EMPAT', NULL, 'MITSUBISHI', 'COLT DIESEL FE 73 HD (4X2) M/T', 'ORANGE KOMBINASI', '2014', '2014', '3908', 'MHMFE84PBEJ006769', '4D34TKX6961', 'L10058381', 992200, NULL, NULL, NULL, '2022-03-04', 'Y', NULL, '2021-08-11 00:56:46'),
(16, '3', 1, 'T 7533 TC', 'PT KARYA MAS EMPAT', NULL, 'MITSUBISHI', 'COLT DIESEL FE 73 HD (4X2) M/T', 'ORANGE KOMBINASI', '2014', '2014', '3908', 'MHMFE84PBEJ006770', '4D34TKX6962', 'L10058380', 992200, NULL, NULL, NULL, '2022-03-04', 'Y', NULL, '2021-08-11 00:57:41'),
(17, '3', 1, 'T 7590 TC', 'PT KARYA MAS EMPAT', NULL, 'MITSUBISHI', 'COLT DIESEL FE 84G BC (4X2) M/T', 'ORANGE KOMBINASI', '2018', '2018', '3908', 'MHMFE84PBJJ009852', '4D34TS81796', '008011753', 906700, NULL, NULL, NULL, '2021-12-18', 'Y', NULL, '2021-08-11 00:58:44'),
(18, '3', 1, 'T 7591 TC', 'PT KARYA MAS EMPAT', 'Jln. Raya Rancaudik KM 5 Tambakdahan Subang', 'MITSUBISHI', 'COLT DIESEL FE 84G BC (4X2) M/T', 'ORANGE KOMBINASI', '2018', '2018', '3908', 'MHMFE84PBJJ009837', '4D34TSB1804', '', 906700, NULL, NULL, NULL, NULL, 'Y', NULL, NULL),
(22, '5', 1, 'T 7571 TC', 'PT KARYA MAS EMPAT', NULL, 'ISUZU', 'NKR 55 CO E2-1 LWB', 'ORANGE KOMBINASI', '2017', '2017', '2771', 'MHCNKR55HHJ074759', 'M074759', 'N05812677', 603600, NULL, NULL, NULL, '2021-10-02', 'Y', NULL, '2021-08-11 01:07:55'),
(23, '5', 1, 'T 7572 TC', 'PT KARYA MAS EMPAT', NULL, 'ISUZU', 'NKR 55 CO E2-1 LWB', 'ORANGE KOMBINASI', '2017', '2017', '2771', 'MHCNKR55HHJ074757', 'M074757', 'N05812678', 603600, NULL, NULL, NULL, '2021-10-02', 'Y', NULL, '2021-08-11 01:08:54'),
(24, '6', 1, 'T 7584 TC', 'PT KARYA MAS EMPAT', NULL, 'TOYOTA', 'HIACE COMMUTER MT', 'PUTIH', '2018', '2018', '2494', 'JTFSS22P4J0178236', '2KDA977542', '005070896', 1238400, NULL, NULL, NULL, '2021-10-04', 'Y', NULL, '2021-08-11 01:12:27'),
(25, '6', 1, 'T 7585 TC', 'PT KARYA MAS EMPAT', NULL, 'TOYOTA', 'HIACE COMMUTER MT', 'PUTIH', '2018', '2018', '2494', 'JTFSS22P3J0178390', '2KDA977980', '005070897', 1238400, NULL, NULL, NULL, '2021-10-04', 'Y', NULL, '2021-08-11 01:13:18'),
(26, '6', 1, 'T 7586 TC', 'PT KARYA MAS EMPAT', NULL, 'TOYOTA', 'HIACE COMMUTER MT', 'PUTIH', '2018', '2018', '2494', 'JTFSS22P8J0178269', '2KDA977643', '005070898', 1238400, NULL, NULL, NULL, '2021-10-04', 'Y', NULL, '2021-08-11 01:13:41'),
(28, '1', 1, 'T 7563', 'PT Karyamasempat', NULL, NULL, NULL, '-', NULL, NULL, NULL, NULL, NULL, '-', 0, NULL, NULL, NULL, '2021-11-19', 'Y', '2021-11-18 18:03:35', '2021-11-18 18:03:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car_category`
--

CREATE TABLE `trvl_ref_car_category` (
  `id` int(11) NOT NULL,
  `category_id` varchar(4) NOT NULL,
  `name` varchar(45) NOT NULL,
  `seat` varchar(20) NOT NULL,
  `active` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_car_category`
--

INSERT INTO `trvl_ref_car_category` (`id`, `category_id`, `name`, `seat`, `active`, `created_at`, `updated_at`) VALUES
(1, '1', 'Big Bus SHD', '47/59', 'Y', '2021-03-07 20:13:49', '2021-03-07 20:13:49'),
(2, '2', 'Big Bus Setra', '47/59', 'Y', '2021-03-07 20:14:03', '2021-03-07 20:14:03'),
(3, '3', 'Medium Bus', '29/31', 'Y', '2021-03-07 20:14:17', '2021-03-07 20:14:17'),
(4, '4', 'Elf Short', '15', 'N', '2021-03-07 20:14:29', '2022-03-27 20:09:03'),
(5, '5', 'Elf Long', '19', 'Y', '2021-03-07 20:14:47', '2021-03-07 20:14:47'),
(8, '6', 'Hiace', '15', 'Y', '2021-03-07 20:14:47', '2021-03-07 20:14:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car_condition`
--

CREATE TABLE `trvl_ref_car_condition` (
  `id` int(5) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_car_condition`
--

INSERT INTO `trvl_ref_car_condition` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Baik', NULL, '2021-02-22 02:54:03'),
(2, 'Sedang Service', NULL, '2021-02-22 02:54:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_car_service`
--

CREATE TABLE `trvl_ref_car_service` (
  `id` int(11) NOT NULL,
  `police_num` varchar(15) NOT NULL,
  `repair_date` varchar(10) DEFAULT NULL,
  `repair_place` varchar(45) DEFAULT NULL,
  `information` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_customer`
--

CREATE TABLE `trvl_ref_customer` (
  `cust_id` bigint(20) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_num` varchar(25) NOT NULL,
  `address` text DEFAULT NULL,
  `ttl_trnsct` int(5) DEFAULT NULL,
  `ttl_payment` int(25) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_customer`
--

INSERT INTO `trvl_ref_customer` (`cust_id`, `name`, `phone_num`, `address`, `ttl_trnsct`, `ttl_payment`, `created_at`, `updated_at`) VALUES
(1, 'TEST', '082325494207', 'TEST', 0, 0, '2021-07-26 00:02:29', '2021-07-26 00:02:29'),
(2, 'Hendra', '082315647182', 'Bongas', 0, 0, '2021-07-26 00:06:18', '2021-07-26 00:06:18'),
(3, 'Bp. Karyim', '085318965047', 'Kengkeng', 0, 0, '2021-08-11 08:31:30', '2022-03-30 11:29:59'),
(4, 'Ibu Wiwi', '085649064482', 'Subang', 0, 0, '2021-08-14 04:00:57', '2021-08-14 04:00:57'),
(5, 'Bp. Rosad', '082216445003', 'Lengkongjaya', 0, 0, '2021-08-14 08:09:47', '2021-08-14 08:09:47'),
(6, 'Bp. Mulyadi', '082216445003', 'Karawang', 0, 0, '2021-08-28 04:54:26', '2021-08-28 04:54:26'),
(7, 'Bp. Udin', '085649064482', 'Jungklang', 0, 0, '2021-08-28 05:01:27', '2021-08-28 05:01:27'),
(8, 'Bp. Abi', '0811346872249', 'Subang', 0, 0, '2021-08-28 05:24:14', '2021-08-28 05:24:14'),
(9, 'Bp. Abi', '085614784460', 'Subang', 0, 0, '2021-08-28 05:34:51', '2021-08-28 05:34:51'),
(10, 'Bp. Gilang AHM', '082216445003', 'Dawuan Cikampek', 0, 0, '2021-09-20 05:10:29', '2021-09-20 05:10:29'),
(11, 'Ibu Inevino', '081395081123', 'Kantor kec. pusakajaya', 0, 0, '2022-03-28 04:13:28', '2022-03-28 04:13:28'),
(12, 'Bp. Alimin', '085294465892', 'Klari', 0, 0, '2022-03-28 05:08:58', '2022-03-28 05:08:58'),
(13, 'Bp. Anwar', '081342722016', 'Ponpes al-Rasyid Pagaden', 0, 0, '2022-03-29 02:42:57', '2022-03-29 02:43:36'),
(14, 'Bp. Guru Entis', '081223055633', 'Batang', 0, 0, '2022-03-30 11:13:40', '2022-03-30 11:13:40'),
(15, 'SMK Teknovo Rancasari', '081223164816', 'Rancasari', 0, 0, '2022-03-30 11:21:30', '2022-03-30 11:21:30'),
(16, 'H. Asep Pahruroji', '085223046348', 'Rancasari', 0, 0, '2022-04-09 08:40:38', '2022-04-09 08:40:38'),
(17, 'Bp. Abas', '081917258329', 'Cilamaya', 0, 0, '2022-04-09 09:17:37', '2022-04-09 09:17:37'),
(18, 'Cahya Pesona Tou', '081772865431', 'Cikampek', 0, 0, '2022-04-11 02:49:29', '2022-04-11 02:49:29'),
(19, 'Bp. Adi Sopandi', '081342722016', 'Jln Cagak', 0, 0, '2022-04-15 03:52:01', '2022-04-15 03:52:01'),
(20, 'Bp. Darwin', '081772865431', 'Gardu', 0, 0, '2022-04-15 09:36:36', '2022-04-15 09:36:36'),
(21, 'Bp. Jaenudin', '08xxx', 'km4', 0, 0, '2022-07-27 08:24:07', '2022-07-27 08:24:07'),
(22, 'PT BAYER INDONESIA', '\'085223007577', 'Compreng', 0, 0, '2022-08-02 05:45:23', '2022-08-02 05:45:23'),
(23, 'Bp. Yedi', '081224517201', 'Bapas Subang Jln. Veteran, sebrang alfamart Sukamelang', 0, 0, '2022-09-30 03:23:14', '2022-09-30 03:23:14'),
(24, 'Bp. Rico', '085782472223', 'Karasak Cilamaya', 0, 0, '2022-10-05 02:05:45', '2022-10-05 02:05:45'),
(25, 'Ibu Heni', '085782472223', 'MTS Negri 4 Pusaka', 0, 0, '2022-10-10 03:49:34', '2022-10-10 03:49:34'),
(26, 'Bp. Revo', '085782472223', 'Bank BNI Patrol', 0, 0, '2022-10-13 10:17:04', '2022-10-13 10:17:04'),
(27, 'Astahanas Binong', '085782472223', 'Astahanas Binong', 0, 0, '2022-10-13 10:38:18', '2022-10-13 10:44:18'),
(28, 'Bp. Sandro', '085782472223', 'Pelabuhan Patimban', 0, 0, '2022-10-18 03:15:59', '2022-10-18 03:15:59'),
(29, 'hendro', '085782472223', 'rancasari', 0, 0, '2022-10-19 03:20:08', '2022-10-19 03:20:08'),
(30, 'Bp. Abdul', '085782472223', 'rancaudik', 0, 0, '2022-10-24 03:16:11', '2022-10-24 03:16:11'),
(31, 'Bp. Samir', '085782472223', 'Compreng', 0, 0, '2022-10-24 09:49:00', '2022-10-24 09:49:00'),
(32, 'Bp. Suryana', '\'085223007577', 'Pangungseng', 0, 0, '2022-10-24 09:57:19', '2022-10-24 09:57:19'),
(33, 'Bp. Samir', '\'085223007577', 'Compreng', 0, 0, '2022-10-24 10:00:36', '2022-10-24 10:00:36'),
(34, 'Ibu Tuti', '\'085223007577', 'Pamanukan', 0, 0, '2022-10-24 10:14:24', '2022-10-24 10:14:24'),
(35, 'ibu Hj. Erni', '085782472223', 'Pamanukan', 0, 0, '2022-10-26 04:13:50', '2022-10-26 04:13:50'),
(36, 'Alan', '085782472223', 'Polsek Cilamaya', 0, 0, '2022-11-01 13:06:53', '2022-11-01 13:06:53'),
(37, 'Bp. Tama', '085782472223', 'Ciasem, Purwadadi', 0, 0, '2022-11-03 04:23:04', '2022-11-03 04:23:04'),
(38, 'Java Tour Karawang', '085782472223', 'Mesjid Agung Rengas Dengklok', 0, 0, '2022-11-03 10:02:08', '2022-11-03 10:02:08'),
(39, 'Bp. Karyim', '085782472223', 'subang', 0, 0, '2022-11-09 02:22:53', '2022-11-09 02:22:53'),
(40, 'TES', '089XX', '-', 0, 0, '2022-11-09 02:25:43', '2022-11-09 02:25:43'),
(41, 'Bp. anwar', '085782472223', 'sukamanah', 0, 0, '2022-11-09 03:31:30', '2022-11-09 03:31:30'),
(42, 'Bp. Hasan Sanuri', '085782472223', 'Patrol', 0, 0, '2022-11-09 05:30:43', '2022-11-09 05:30:43'),
(43, 'Bp. Aji', '085782472223', 'SD Rabani Subang', 0, 0, '2022-11-11 12:39:47', '2022-11-11 12:39:47'),
(44, 'Bp. Chevi', '085782472223', 'karawang', 0, 0, '2022-11-16 04:03:43', '2022-11-16 04:03:43'),
(45, 'Bp. Sopandi', '085782472223', 'Dsn. Marjim Ds. Ciasem tengah', 0, 0, '2022-11-22 06:13:13', '2022-11-22 06:13:13'),
(46, 'PT Bayer', '085782472223', 'Jatimulya', 0, 0, '2022-11-23 03:53:30', '2022-11-23 03:53:30'),
(47, 'Bp. Adi', '085782472223', 'as-syifa jalan cagak', 0, 0, '2022-11-24 02:32:23', '2022-11-24 02:32:23'),
(48, 'Bp. Subhan', '085782472223', 'Toyota KIC Karawang', 0, 0, '2022-11-25 06:55:14', '2022-11-25 06:55:14'),
(49, 'Bp. Mulyadi', '085782472223', 'Cilamaya', 0, 0, '2022-11-26 05:25:40', '2022-11-26 05:25:40'),
(50, 'Bp. Nanda', '085782472223', 'SMP 2 Subang', 0, 0, '2022-11-29 09:54:06', '2022-12-05 04:47:39'),
(51, 'Paud Dahlia', '085782472223', 'Blanakan', 0, 0, '2022-11-30 03:32:04', '2022-11-30 03:32:04'),
(52, 'Bp. Hasan', '085782472223', 'Patrol', 0, 0, '2022-12-10 12:47:52', '2022-12-10 12:47:52'),
(53, 'Bp. Adi', '085782472223', 'As-syifa jalan cagak', 0, 0, '2022-12-14 01:13:17', '2022-12-14 01:13:17'),
(54, 'Puskesmas Blanakan', '085782472223', 'Blanakan', 0, 0, '2022-12-19 06:42:44', '2022-12-23 01:34:58'),
(55, 'Puskesmas Blanakan', '085782472223', 'Blanakan', 0, 0, '2022-12-19 06:43:07', '2022-12-23 01:35:56'),
(56, 'Puskesmas Belanakan', '085782472223', 'Blanakan', 0, 0, '2022-12-19 06:43:25', '2022-12-19 06:43:25'),
(57, 'Puskesmas Belanakan', '085782472223', 'Blanakan', 0, 0, '2022-12-19 06:43:28', '2022-12-19 06:43:28'),
(58, 'Puskesmas Blanakan', '085782472223', 'Blanakan', 0, 0, '2022-12-19 06:43:46', '2022-12-23 01:36:26'),
(59, 'Bp. Faisal', '085782472223', 'Indramayu', 0, 0, '2022-12-20 11:00:10', '2022-12-20 11:00:10'),
(60, 'Puskesmas Tes', '089XXX', '-', 0, 0, '2022-12-21 02:16:52', '2022-12-21 02:16:52'),
(61, 'Ibu Indri', '085721014597', 'Purwakarta', 0, 0, '2022-12-21 02:53:42', '2022-12-21 00:28:22'),
(62, 'Bp. Parman CV Ma\'arif Tor & Trevel', '082129946179', 'Kantor Kec. Widasari Indramayu', 0, 0, '2022-12-22 10:41:46', '2022-12-22 10:41:46'),
(63, 'Bp. Parman CV Ma\'arif Tor & Trevel', '082129946179', 'Kantor Kec. Widasari Indramayu', 0, 0, '2022-12-22 10:42:04', '2022-12-22 10:42:04'),
(64, 'Bp. Parman CV Ma\'arif Tor & Trevel', '082129946179', 'Kantor Kec. Widasari Indramayu', 0, 0, '2022-12-22 10:45:56', '2022-12-22 10:45:56'),
(65, 'Bp. Asep Sahel', '081320728095', 'Mesjid Al-Ilyas Cicadas', 0, 0, '2022-12-24 02:50:46', '2022-12-24 02:50:46'),
(66, 'PARMAN CV. Ma\'arif Tour Travel', '082129946179', 'UPTD Puskesmas Kandang Haur Indramayu', 0, 0, '2022-12-24 12:34:28', '2022-12-24 12:34:28'),
(67, 'PARMAN CV. Ma\'arif Tour Travel', '082129946179', 'UPTD Puskesmas Kandang Haur Indramayu', 0, 0, '2022-12-24 12:34:42', '2022-12-24 12:34:42'),
(68, 'PARMAN CV. Ma\'arif Tour Travel', '082129946179', 'UPTD Puskesmas Kandang Haur Indramayu', 0, 0, '2022-12-24 12:37:21', '2022-12-24 12:37:21'),
(69, 'Ibu Ratna', '082116114851', 'Kalijati', 0, 0, '2022-12-24 13:09:48', '2022-12-24 13:09:48'),
(70, 'Bp. Dali', '082116251966', 'Kantor Pabrik Gula Pasir Bungur purwadadi', 0, 0, '2023-01-04 08:28:03', '2023-01-04 08:28:03'),
(71, 'Bp. edu', '082129946179', 'bongas', 0, 0, '2023-01-18 02:49:06', '2023-01-18 02:49:06'),
(72, 'Bp. edu', '082129946179', 'bongas', 0, 0, '2023-01-18 02:50:04', '2023-01-18 02:50:04'),
(73, 'Bp. Hasan Sanuri', '081325567220', 'Pasar Patrol', 0, 0, '2023-01-21 05:13:11', '2023-01-21 05:13:11'),
(74, 'Bp. Abdul Rois', '085782472223', 'pom bensin warung nangka ciasem', 0, 0, '2023-01-21 10:44:18', '2023-01-21 10:44:18'),
(75, 'Bp. Hasan Sanuri', '085782472223', 'Pasar Patrol', 0, 0, '2023-01-24 09:55:38', '2023-01-24 09:55:38'),
(76, 'DWP KAB. SUBANG', '082319621973', 'SMKN 1 Subang', 0, 0, '2023-01-26 02:37:45', '2023-01-26 02:37:45'),
(77, 'Ibu Intan Kuswati', '085782472223', 'SD Nusa Indah Subang', 0, 0, '2023-01-27 09:54:41', '2023-01-27 09:54:41'),
(78, 'Bp. Anggoro / SD Bunda Maria', '085782472223', 'Bunda Maria Pamanukan', 0, 0, '2023-02-01 03:53:50', '2023-02-01 03:53:50'),
(79, 'Bp. Anggoro / SD Bunda Maria', '085782472223', 'Pamanukan', 0, 0, '2023-02-01 03:57:44', '2023-02-01 03:57:44'),
(80, 'Bp. Hasan Sanuri', '085782472223', 'Pasar Patrol', 0, 0, '2023-02-01 13:53:26', '2023-02-01 13:53:26'),
(81, 'Panatagama', '085782472223', 'Subang', 0, 0, '2023-02-01 14:18:54', '2023-02-01 14:18:54'),
(82, 'Panatagama', '085782472223', 'Subang', 0, 0, '2023-02-01 14:19:54', '2023-02-01 14:19:54'),
(83, 'Reyza/Ahsan Tour', '085782472223', 'Denong Dawuan', 0, 0, '2023-02-02 02:30:24', '2023-02-02 02:30:24'),
(84, 'Reyza/Ahsan Tour', '085782472223', 'Denong Dawuan', 0, 0, '2023-02-02 02:30:33', '2023-02-02 02:30:33'),
(85, 'Reyza/Ahsan Tour', '085782472223', 'Denong Dawuan', 0, 0, '2023-02-02 02:30:40', '2023-02-02 02:30:40'),
(86, 'Reyza/Ahsan Tour', '085782472223', 'Denong Dawuan', 0, 0, '2023-02-02 02:33:01', '2023-02-02 02:33:01'),
(87, 'Bp. Jamaludin', '085782472223', 'Mesjid Agung Subang', 0, 0, '2023-02-02 06:47:41', '2023-02-02 06:47:41'),
(88, 'Bp. Asep Ahmad Muchlisin', '085322020471', 'Ciasem', 0, 0, '2023-02-06 10:36:54', '2023-02-06 10:36:54'),
(89, 'Bp. Cevi', '085318823822', 'jalan cagak', 0, 0, '2023-02-07 01:36:42', '2023-02-07 01:36:42'),
(90, 'Bp. Cevi', '085318823822', 'jalan cagak', 0, 0, '2023-02-07 01:38:49', '2023-02-07 01:38:49'),
(91, 'PT MeiloonTechnologi Indonesia', '083821056313', 'Jl. Raya sembung pagaden', 0, 0, '2023-02-10 08:18:21', '2023-02-10 08:18:21'),
(92, 'Harum Tour', '081325567220/089525720389', 'Indramayu', 0, 0, '2023-02-13 10:50:47', '2023-02-13 10:50:47'),
(93, 'Harum Tour', '081325567220/089525720389', 'Indramayu', 0, 0, '2023-02-13 10:52:13', '2023-02-13 10:52:13'),
(94, 'Harum Tours', '085782472223', 'Patrol', 0, 0, '2023-02-16 08:27:55', '2023-02-16 08:27:55'),
(95, 'Harum Tours', '085782472223', 'Patrol', 0, 0, '2023-02-16 08:29:48', '2023-02-16 08:29:48'),
(96, 'Harum Tours', '085782472223', 'Patrol', 0, 0, '2023-02-16 08:40:22', '2023-02-16 08:40:22'),
(97, 'Bp. H. Tatang', '085782472223', 'Tanjung Salep', 0, 0, '2023-02-17 06:20:54', '2023-02-17 06:20:54'),
(98, 'Ibu Eha', '085782472223', 'Rancasari', 0, 0, '2023-03-06 10:32:14', '2023-03-06 10:32:14'),
(99, 'Ibu Eha', '085782472223', 'Rancasari', 0, 0, '2023-03-06 10:34:26', '2023-03-06 10:34:26'),
(100, 'Ibu Irma', '085782472223', 'Blanakan', 0, 0, '2023-03-10 12:19:24', '2023-03-10 12:19:24'),
(101, 'Bp. Syarifudin', '085782472223', 'Subang, Cikampek', 0, 0, '2023-03-11 04:42:52', '2023-03-11 04:42:52'),
(102, 'Bp. Syarifudin', '085782472223', 'Subang', 0, 0, '2023-03-11 04:48:39', '2023-03-11 04:48:39'),
(103, 'Anida', '085524651225', 'Rumah Makan Nikmat Sukamandi', 0, 0, '2023-05-20 11:30:54', '2023-05-20 11:30:54'),
(104, 'Anida', '085524651225', 'Rumah Makan Nikmat Sukamandi', 0, 0, '2023-05-20 11:30:59', '2023-05-20 11:30:59'),
(105, 'Anida', '08', NULL, 0, 0, '2023-05-20 11:32:43', '2023-05-20 11:32:43'),
(106, 'Anida', '085782472223', 'sukamandi', 0, 0, '2023-05-20 11:33:51', '2023-05-20 11:33:51'),
(107, 'Java Tour', '085782472223', 'Karawang', 0, 0, '2023-05-24 12:54:47', '2023-05-24 12:54:47'),
(108, 'Bp. Muhidin', '085782472223', 'Bojong Gayam', 0, 0, '2023-06-01 03:31:12', '2023-06-01 03:31:12'),
(109, 'Ibu Catharina', '085782472223', 'Pamanukan', 0, 0, '2023-06-19 03:06:02', '2023-06-19 03:06:02'),
(110, 'Bp. Bagus Laksono', '085782472223', 'Subang', 0, 0, '2023-06-19 09:10:46', '2023-06-19 09:10:46'),
(111, 'Bp. Andri', '085782472223', 'Ciasem', 0, 0, '2023-06-20 03:19:30', '2023-06-20 03:19:30'),
(112, 'Bp. Andri', '085782472223', 'Ciasem', 0, 0, '2023-06-20 03:20:14', '2023-06-20 03:20:14'),
(113, 'Bp. Andri', '085782472223', 'Ciasem', 0, 0, '2023-06-26 02:40:24', '2023-06-26 02:40:24'),
(114, 'Ibu Nani', '085782472223', 'Terminal Jalan Cagak', 0, 0, '2023-07-02 12:10:57', '2023-07-02 12:10:57'),
(115, 'Bp. Shandy Yuda', '085719477291', 'PT Murotech indonesia kawasan suryacipta Jln. Surya lestari Kapling I-2H Ciampel Karawang', 0, 0, '2023-07-14 06:13:27', '2023-07-14 06:13:27'),
(116, 'Harum Tours', '085782472223', 'ptrol, karang ampel kaplongan indramayu', 0, 0, '2023-07-28 03:57:17', '2023-07-28 03:57:17'),
(117, 'Bp. Uke', '085782472223', 'Pagaden', 0, 0, '2023-08-04 07:20:41', '2023-08-04 07:20:41'),
(118, 'Bp. Chevi', '085782472223', 'Karawang', 0, 0, '2023-08-11 02:45:07', '2023-08-11 02:45:07'),
(119, 'Bp. Chevi', '085782472223', 'Karawang', 0, 0, '2023-08-11 02:48:50', '2023-08-11 02:48:50'),
(120, 'PT. MEILOON/Bp. Hendri', '085782472223', 'Subang', 0, 0, '2023-08-12 05:24:08', '2023-09-03 20:24:16'),
(121, 'PT Meiloon Technology Indonesia', '085782472223', 'Subang', 0, 0, '2023-08-12 08:06:00', '2023-08-12 08:06:00'),
(122, 'Ustd. Ajat', '085782472223', 'Lebak Siuh', 0, 0, '2023-08-16 12:31:25', '2023-08-16 12:31:25'),
(123, 'As-Syifa Jalan Cagak', '085782472223', 'Jalan Cagak', 0, 0, '2023-09-04 06:51:27', '2023-09-04 06:51:27'),
(124, 'As-Syifa Jalan Cagak', '085782472223', 'Jalan Cagak', 0, 0, '2023-09-04 06:54:40', '2023-09-04 06:54:40'),
(125, 'As-Syifa Jalan Cagak', '085782472223', 'Jalan Cagak', 0, 0, '2023-09-04 08:59:16', '2023-09-04 08:59:16'),
(126, 'Mamah Dimas', '085782472223', 'Subang', 0, 0, '2023-09-04 12:58:16', '2023-09-04 12:58:16'),
(127, 'Mamah Dimas', '085782472223', 'Subang', 0, 0, '2023-09-04 12:59:15', '2023-09-04 12:59:15'),
(128, 'Mamah Dimas', '085782472223', 'Subang', 0, 0, '2023-09-04 12:59:58', '2023-09-04 12:59:58'),
(129, 'Mamah Dimas', '085782472223', 'Subang', 0, 0, '2023-09-04 13:05:30', '2023-09-04 13:05:30'),
(130, 'Panatagama', '085782472223', 'Subang', 0, 0, '2023-09-09 03:28:39', '2023-09-09 03:28:39'),
(131, 'Panatagama', '085782472223', 'Subang', 0, 0, '2023-09-09 03:33:16', '2023-09-09 03:33:16'),
(132, 'PT Meiloon Technology Indonesia', '082130748197', 'PT Meiloon Technology Indonesi Pagaden', 1, 3700000, '2023-09-23 08:36:22', '2023-09-23 08:36:22'),
(133, 'BEM STIE Miftahul Huda', '085352983954', 'Rancasari', 1, 4300000, '2023-09-30 13:06:48', '2023-09-30 13:06:48'),
(134, 'Bp. Syafiq', '081319924140', 'Purwakarta', 1, 4500000, '2023-10-07 10:34:32', '2023-10-07 10:34:32'),
(135, 'Fahrul', '083195550003', 'Jungklang', 1, 7900000, '2023-10-14 02:03:34', '2023-10-14 02:03:34'),
(136, 'Ibu Yeni Nuraeni', '081386356052/081320423030', 'Kantor Dinas Tenaga kerja transmigrasi subang', 1, 10500000, '2023-11-10 10:22:48', '2023-11-10 10:22:48'),
(137, 'Bp. Wirta', '085782472223', 'Sukra', 1, 4000000, '2023-12-26 11:00:07', '2023-12-26 11:00:07'),
(138, 'Bp. H. Yaya Ruhmaya', '085782472223', 'Pamanukan', 1, 8000000, '2024-01-04 02:50:11', '2024-01-04 02:50:11'),
(139, 'Sekolah Bunda Maria', '085782472223', 'Pamanukan', 1, 1000000, '2024-01-22 11:35:11', '2024-01-22 11:35:11'),
(140, 'Ahmad Arbah', '08219283572', 'Cidahu', 1, 2500000, '2024-01-30 12:43:44', '2024-01-30 12:43:44'),
(141, 'Fave Hotel Pamanukan', '085724427540', 'Pamanukan', 1, 1700000, '2024-02-01 10:51:31', '2024-02-01 10:51:31'),
(142, 'Fave Hotel Pamanukan', '085724427540', 'Pamanukan', 1, 1700000, '2024-02-01 10:51:51', '2024-02-01 10:51:51'),
(143, 'Bp. Sugandi', '082127163192', 'Sukadana', 1, 4700000, '2024-02-01 10:57:01', '2024-02-01 10:57:01'),
(144, 'Bp. Uhe', '085716455986', 'cikande cilebar karawang', 1, 11250000, '2024-02-13 03:02:53', '2024-02-13 03:02:53'),
(145, 'AKBAR', '083816387789', 'Ciasem', 1, 4300000, '2024-02-17 11:14:38', '2024-02-17 11:14:38'),
(146, 'Astahanas', '082120357400', 'Binong', 1, 12000000, '2024-02-22 08:27:15', '2024-02-22 08:27:15'),
(147, 'Fave Hotel Pamanukan', '085724427540', 'Pamanukan', 1, 1700000, '2024-02-22 11:00:43', '2024-02-22 11:00:43'),
(148, 'Bp. Jafar', '085782472223', 'Patrol', 1, 6600000, '2024-02-24 06:12:19', '2024-02-24 06:12:19'),
(149, 'SD Yusodarso', '08156119464', 'Subang', 1, 4200000, '2024-02-27 05:32:02', '2024-02-27 05:32:02'),
(150, 'Bp. Warsono', '085782472223', 'Karawang', 1, 3800000, '2024-03-04 06:22:47', '2024-03-04 06:22:47'),
(151, 'Gereja Katolik GKSP Subang Sie Pendidikan', '08156119464', 'Subang', 1, 4200000, '2024-03-12 04:10:57', '2024-03-12 04:10:57'),
(152, 'Gereja Katolik GKSP Subang Sie Pendidikan', '085782472223', 'Subang', 1, 2700000, '2024-03-14 08:49:36', '2024-03-14 08:49:36'),
(153, 'SD YOSUDARSO', '08156119464', 'Subang', 1, 2700000, '2024-04-25 07:38:14', '2024-04-25 07:38:14'),
(154, 'NCTZEN Subang', '085179522318', 'Subang', 1, 5300000, '2024-04-29 06:06:18', '2024-04-29 06:06:18'),
(155, 'Harum Tour', '081325567220', 'Patrol', 1, 7600000, '2024-05-01 03:32:31', '2024-05-01 03:32:31'),
(156, 'Harum Tour', '081325567220', 'Patrol', 1, 4200000, '2024-05-01 09:04:44', '2024-05-01 09:04:44'),
(157, 'SMAN 1 Ciasem', '081328196867', 'Ciasem', 1, 67500000, '2024-05-03 02:34:59', '2024-05-03 02:34:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_driver`
--

CREATE TABLE `trvl_ref_driver` (
  `driver_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `sim_num` varchar(40) DEFAULT NULL,
  `phone_num` varchar(45) NOT NULL,
  `address` text DEFAULT NULL,
  `driver_type` varchar(15) NOT NULL,
  `police_num` varchar(45) DEFAULT NULL,
  `car_permit` text DEFAULT NULL,
  `ttl_trnsct` int(5) DEFAULT 0,
  `ttl_saving` int(25) DEFAULT 0,
  `active` varchar(1) DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_driver`
--

INSERT INTO `trvl_ref_driver` (`driver_id`, `name`, `sim_num`, `phone_num`, `address`, `driver_type`, `police_num`, `car_permit`, `ttl_trnsct`, `ttl_saving`, `active`, `created_at`, `updated_at`) VALUES
(1, 'WIRTA', NULL, '082216445003', 'Dsn. Pangadangan RT. 18/05 Desa Rancasari Kec. Pamanukan Kab. Subang', 'UTAMA', 'T 7590 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-07-26 19:22:21'),
(2, 'ALIMIN', NULL, '085294465892', 'Dsn. Kertajaya RT. 02/13 Desa Mulyasari kec, Pamanukan Kab. Subang', 'UTAMA', 'T 7533 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2023-09-04 23:02:43'),
(3, 'DARWIN', NULL, '081324504132', 'Dsn Gardu II RT. 005/002 Ds. Gardu Mukti Kec. Tambakdahan Kab. Subang', 'UTAMA', 'T 7584 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-03-30 11:36:51'),
(4, 'KARYIM MULYADI', NULL, '085318965047', 'Dsn. Pangadangan RT 18/05 Desa Rancasari Kec. Pamanukan Kab. Subang', 'UTAMA', 'T 7586 TC', '[\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-11-09 02:42:33'),
(5, 'UDIN FAHRUDIN', NULL, '081224642887', 'Dsn. Jatibaru RT.003/001 Ds. Mulyasari Kec. Binong Kab. Subang', 'UTAMA', 'T 7571 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-10-25 11:06:03'),
(6, 'H. AMIN MUBAROK', NULL, '081310253230', 'Dsn. Sukawera RT. 019/005 Ds. Mekarjaya Kec. Compreng Kab. Subang', 'UTAMA', 'T 7532 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2023-01-24 11:18:09'),
(7, 'CEPI KURNIAWAN', NULL, '081290667082', 'Dsn. Sukatani RT. 02/01 Ds. Sukatani Kec. Compreng Kab. Subang', 'UTAMA', 'T 7591 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-08-14 07:20:07'),
(8, 'SOLIHIN', NULL, '081395492506', 'Kp. Pojok Tengah RT. 03/05 Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7562 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-08-14 07:14:46'),
(9, 'ASEP KUSMANA', NULL, '081395443443', 'Kp. Sukasari RT. 02/05 Desa Cikole Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7573 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-07-26 19:17:57'),
(10, 'RODI', NULL, '082128598694', 'Dsn. Rawasari RT. 17/08 Desa Sukamandi Kec. Ciasem Kab. Subang', 'UTAMA', 'T 7565 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-07-26 19:21:23'),
(11, 'EMAN', NULL, '081210910193', 'Kp. Karang Tengah RT. 002/015 Ds. Cikole Kec. Lembang Kab. Bandung', 'UTAMA', 'T 7545 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-12-24 12:25:42'),
(12, 'MOCH MUKTI D', NULL, '082110717919', 'Jl. Angkasa GG Motor No. 17 RT. 13/06 Gunung Sahari Jakarta Pusat', 'UTAMA', 'T 7566 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-07-26 19:20:47'),
(13, 'JAENUDIN', NULL, '085328864444', 'Kp. Sukamukti RT. 003/003 Ds. Situsaeur Kec. Karangpawitan Kab. Garut', 'UTAMA', 'T 7574 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-07-27 08:35:59'),
(14, 'ROHIM KUSTIAWAN', NULL, '081214934767', 'Kp. Pamecelan RT. 02/06 Desa Sukajaya Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7547 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-07-26 19:21:37'),
(15, 'AGUS SUROSO', NULL, '081295609511', 'Kp. Cinangka RT. 001/009 Ds. Margalakasana Kec. Cipeundeuy Kab. Bandung Barat', 'UTAMA', 'T 7563', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-10-25 04:11:39'),
(16, 'MAHFUD', NULL, '082218946238', 'Dsn. Priangan RT. 008/003 Ds. Binagun Kec. Pataruman Banjar', 'UTAMA', 'T 7563', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2023-02-08 11:52:11'),
(17, 'AGUS SANTOSA', NULL, '081315801322', 'Kp. Haurpanggung RT. 002/004 Ds. Haurpanggung Kec. Tarogong Kidul Kab. Garut', 'CADANGAN', 'T 7564 TC', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2023-09-04 22:02:27'),
(18, 'ENGKUS KUSNADI', NULL, '081394317401', 'Bunisari Kulon RT. 001/006 Ds. Gadobangkong Kec. Ngamprah Kab. Bandung Barat', 'CADANGAN', 'T 7566 TC', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2023-09-04 22:02:48'),
(19, 'NANA', NULL, '081215555128', 'Dsn. Karangmalang RT. 009/003 Ds. Bobos Kec. Legonkulon Kab. Subang', 'UTAMA', 'T 7584 TC', '[\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-10-25 04:13:09'),
(20, 'NANA', NULL, '082216825855', 'Dsn. Gardu II RT. 005/002 Ds. Gardu Mukti Kec. Tambakdahan Kab. Subang', 'UTAMA', NULL, '[\"5\",\"6\"]', 0, 0, 'N', NULL, '2022-11-15 20:51:56'),
(21, 'AGUS SUUD', NULL, '081324993605', 'Kp. Batureok RT.01/08 Kec. Lembang Kab. Bandung Barat', 'UTAMA', 'T 7546 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2023-02-07 02:29:14'),
(22, 'NANANG BIN OCEP', NULL, '081291449589', 'Jl. Mayjen Sutiyoso No. 8 Ke. Kota Baru Bandar Lampung', 'UTAMA', 'T 7564 TC', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"]', 0, 0, 'Y', NULL, '2021-07-26 19:21:13'),
(23, 'SUHENDI', NULL, '083837106331', 'Dsn. Bongas Rt 002/001 Kec. Pamanukan Kab. Subang', 'UTAMA', 'T 7572 TC', '[\"5\",\"6\"]', 0, 0, 'Y', NULL, '2022-11-11 12:48:05'),
(24, 'Bandi', NULL, '085320365246', 'Subang', 'UTAMA', 'T 7574 TC', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 0, 0, 'Y', '2022-04-15 00:09:48', '2022-07-27 08:35:59'),
(25, 'Yandi', NULL, '081323825406', 'Indramayu', 'CADANGAN', 'T 7565 TC', '[\"2\",\"3\",\"5\",\"6\"]', 0, 0, 'Y', '2022-11-09 04:08:07', '2022-11-09 11:15:58'),
(26, 'DEDE KOHAR', NULL, '085319186707', NULL, 'CADANGAN', 'T 7564 TC', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 1, 240000, 'Y', '2023-09-04 02:19:28', '2023-10-20 08:27:59'),
(27, 'ENTIS', NULL, '081617000745', NULL, 'CADANGAN', 'T 7566 TC', '[\"1\",\"2\",\"3\",\"5\",\"6\"]', 0, 0, 'Y', '2023-09-04 02:26:24', '2023-09-04 22:02:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_driver_saving`
--

CREATE TABLE `trvl_ref_driver_saving` (
  `id` bigint(20) NOT NULL,
  `type` enum('DRIVER','HELPER') NOT NULL,
  `user_id` int(5) NOT NULL,
  `booking_id` varchar(20) DEFAULT NULL,
  `trnsct_date` varchar(10) NOT NULL,
  `information` text DEFAULT NULL,
  `in` int(20) DEFAULT 0,
  `out` int(20) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_finance_account`
--

CREATE TABLE `trvl_ref_finance_account` (
  `id` int(11) NOT NULL,
  `group_id` varchar(10) NOT NULL,
  `is_parent` varchar(1) DEFAULT '',
  `parent` varchar(10) DEFAULT NULL,
  `account_id` varchar(25) NOT NULL,
  `name` varchar(45) NOT NULL,
  `post_report` varchar(15) DEFAULT NULL,
  `post_sub_report` varchar(35) DEFAULT NULL,
  `cash_flow_group` varchar(15) DEFAULT NULL,
  `debit` int(20) DEFAULT NULL,
  `credit` int(20) DEFAULT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_finance_account`
--

INSERT INTO `trvl_ref_finance_account` (`id`, `group_id`, `is_parent`, `parent`, `account_id`, `name`, `post_report`, `post_sub_report`, `cash_flow_group`, `debit`, `credit`, `active`, `created_at`, `updated_at`) VALUES
(1, '1-000', 'Y', NULL, '1-100', 'ASET LANCAR', 'NERACA', NULL, NULL, 0, 0, 'Y', '2021-04-06 19:54:05', '2021-04-06 19:54:05'),
(2, '1-000', 'Y', NULL, '1-200', 'ASET TETAP', 'NERACA', NULL, NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(3, '1-000', '', '1-100', '1-110', 'Kas Kantor', 'NERACA', 'AKTIVA_LANCAR', NULL, 71900000, 5800000, 'Y', '2021-04-06 20:06:32', '2021-04-06 20:06:32'),
(4, '1-000', '', '1-100', '1-120', 'Bank BRI', 'NERACA', 'AKTIVA_LANCAR', NULL, 113350000, 300000, 'Y', '2021-04-06 20:31:15', '2021-04-06 20:31:15'),
(5, '1-000', '', '1-100', '1-130', 'Uang Muka Sewa', 'NERACA', 'AKTIVA_LANCAR', NULL, 143750000, 152250000, 'Y', '2021-04-06 20:31:34', '2021-04-06 20:31:34'),
(6, '1-000', '', '1-100', '1-140', 'Piutang Bon', 'NERACA', 'AKTIVA_LANCAR', NULL, 0, 0, 'Y', '2021-04-06 20:32:06', '2021-04-06 20:32:06'),
(7, '2-000', 'Y', NULL, '2-100', 'Kewajiban Lancar', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-06 20:38:19', '2021-04-06 20:38:19'),
(8, '3-000', '', NULL, '3-100', 'Modal', 'NERACA', 'MODAL', NULL, 0, 0, 'Y', '2021-04-06 20:38:41', '2021-04-06 20:38:41'),
(9, '2-000', '', '2-100', '2-110', 'Hutang ke H. Ade', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-06 20:39:11', '2021-04-06 20:39:11'),
(10, '2-000', '', '2-100', '2-120', 'Hutang ke Hj. Im Inayah', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-06 20:40:09', '2021-04-06 20:40:09'),
(13, '3-000', '', NULL, '3-200', 'Laba Bersih', 'NERACA', 'MODAL', NULL, 300000, 172750000, 'Y', '2021-04-06 20:41:50', '2021-04-06 20:41:50'),
(14, '4-000', '', NULL, '4-100', 'Pendapatan Sewa', 'LABARUGI', 'PENDAPATAN', NULL, 0, 172750000, 'Y', '2021-04-06 20:42:05', '2021-04-06 20:42:05'),
(15, '5-000', '', NULL, '5-100', 'HPP Perjalanan', 'LABARUGI', 'HPP', NULL, 1800000, 0, 'Y', '2021-04-06 20:42:32', '2021-04-06 20:42:32'),
(16, '6-000', '', NULL, '6-011', 'Gaji', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:43:55', '2021-04-06 20:44:06'),
(17, '6-000', '', NULL, '6-012', 'Listrik, Wifi & Kuota', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:44:49', '2021-04-06 20:44:49'),
(18, '6-000', '', NULL, '6-013', 'Makanan & Minuman', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(19, '6-000', '', NULL, '6-014', 'ATK & Printing', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(20, '6-000', '', NULL, '6-015', 'Perlengkapan Mobil', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(21, '6-000', '', NULL, '6-016', 'Pengurusan Surat Mobil', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(22, '6-000', '', NULL, '6-017', 'Service Mobil', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(23, '6-000', '', NULL, '6-018', 'Perizinan & Legal', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:45:07'),
(24, '6-000', '', NULL, '6-019', 'Pajak', 'LABARUGI', 'BIAYA', NULL, 0, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:47:26'),
(25, '7-000', '', NULL, '7-100', 'Pendapatan Lainnya', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:20', '2021-04-07 19:42:20'),
(26, '7-000', '', NULL, '7-200', 'Fee Paket Biro', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(37, '8-000', '', NULL, '8-001', 'Admin Bank', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(38, '8-000', '', NULL, '8-002', 'Langganan Software', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(39, '8-000', '', NULL, '8-003', 'Tunjangan Hari raya', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(40, '8-000', '', NULL, '8-004', 'Upah Kerja', 'LABARUGI', 'BIAYA_LAIN', NULL, 0, 0, 'Y', '2021-04-07 19:42:40', '2021-04-07 19:42:40'),
(41, '1-000', '', '1-200', '1-210', 'Inventaris Kantor', 'NERACA', 'AKTIVA_TETAP', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(42, '1-000', '', '1-200', '1-220', 'Kendaraan', 'NERACA', 'AKTIVA_TETAP', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(43, '1-000', '', '1-200', '1-230', 'Tanah & Bangunan', 'NERACA', 'AKTIVA_TETAP', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(44, '1-000', '', '', '1-800', 'Depresiasi', 'NERACA', 'DEPRESIASI', NULL, 0, 0, 'Y', '2021-04-06 20:05:30', '2021-04-06 20:05:30'),
(45, '7-000', '', '7-100', '7-300', 'BUNGA BANK', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-17 00:20:54', '2021-04-17 00:20:54'),
(46, '2-000', '', '2-100', '2-130', 'Hutang Usaha', 'NERACA', 'KEWAJIBAN', NULL, 0, 0, 'Y', '2021-04-21 20:15:44', '2021-04-21 20:15:44'),
(47, '6-000', '', NULL, '6-020', 'Komisi Agen/Biro', 'LABARUGI', 'BIAYA', NULL, 300000, 0, 'Y', '2021-04-06 20:45:07', '2021-04-06 20:47:26'),
(48, '7-000', '', NULL, '7-400', 'Fee Refund', 'LABARUGI', 'PENDAPATAN_LAIN', NULL, 0, 0, 'Y', '2021-04-30 18:41:47', '2021-04-30 18:41:47'),
(50, '6-000', '', NULL, '6-021', 'Kebutuhan Kantor', 'LABARUGI', NULL, NULL, 0, 0, 'Y', '2022-10-18 20:57:14', '2022-10-18 20:57:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_finance_account_group`
--

CREATE TABLE `trvl_ref_finance_account_group` (
  `group_id` varchar(15) NOT NULL,
  `name` varchar(45) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_finance_account_group`
--

INSERT INTO `trvl_ref_finance_account_group` (`group_id`, `name`, `active`, `created_at`, `updated_at`) VALUES
('1-000', 'ASET', 'Y', '2021-04-06 19:51:05', '2021-04-06 19:51:05'),
('2-000', 'KEWAJIBAN', 'Y', '2021-04-06 19:51:16', '2021-04-06 19:51:16'),
('3-000', 'EKUITAS', 'Y', '2021-04-06 19:51:25', '2021-04-06 19:51:25'),
('4-000', 'PENDAPATAN', 'Y', '2021-04-06 19:51:37', '2021-04-06 19:51:37'),
('5-000', 'HARGA POKOK PENJUALAN', 'Y', '2021-04-06 19:51:50', '2021-04-06 19:51:50'),
('6-000', 'BIAYA USAHA', 'Y', '2021-04-06 19:52:10', '2021-04-06 19:52:10'),
('7-000', 'PENDAPATAN LAINNYA', 'Y', '2021-04-06 19:52:23', '2021-04-08 20:08:06'),
('8-000', 'BIAYA LAINNYA', 'Y', '2021-04-08 20:07:46', '2021-04-08 20:07:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_helper`
--

CREATE TABLE `trvl_ref_helper` (
  `helper_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` text DEFAULT NULL,
  `phone_num` varchar(25) DEFAULT NULL,
  `ttl_trnsct` int(5) DEFAULT NULL,
  `ttl_saving` int(25) DEFAULT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_helper`
--

INSERT INTO `trvl_ref_helper` (`helper_id`, `name`, `address`, `phone_num`, `ttl_trnsct`, `ttl_saving`, `active`, `created_at`, `updated_at`) VALUES
(1, 'ANDRI', 'Tanjung sari', '082117330910', 0, 0, 'Y', '2021-04-21 20:00:07', '2023-09-23 01:32:29'),
(2, 'IGUN', NULL, '085295333006', 1, 180000, 'Y', '2023-09-04 02:28:29', '2023-10-20 08:27:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_payment_id`
--

CREATE TABLE `trvl_ref_payment_id` (
  `id` int(11) NOT NULL,
  `month` varchar(7) NOT NULL,
  `serial_num` int(4) UNSIGNED ZEROFILL NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_payment_id`
--

INSERT INTO `trvl_ref_payment_id` (`id`, `month`, `serial_num`, `created_at`, `updated_at`) VALUES
(1, '2023-09', 0005, '2023-09-23 08:36:22', '2023-09-30 13:09:38'),
(2, '2023-10', 0010, '2023-10-07 10:34:32', '2023-10-20 08:33:08'),
(3, '2023-11', 0003, '2023-11-10 10:22:48', '2023-11-16 05:03:45'),
(4, '2023-12', 0004, '2023-12-26 11:01:15', '2023-12-26 11:02:16'),
(5, '2024-01', 0008, '2024-01-04 02:50:11', '2024-01-30 12:43:44'),
(6, '2024-02', 0018, '2024-02-01 10:51:51', '2024-02-27 05:33:19'),
(7, '2024-03', 0008, '2024-03-04 06:22:47', '2024-03-14 08:49:36'),
(8, '2024-04', 0007, '2024-04-25 07:38:14', '2024-04-29 09:21:36'),
(9, '2024-05', 0009, '2024-05-01 03:32:31', '2024-05-03 07:37:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_payment_method`
--

CREATE TABLE `trvl_ref_payment_method` (
  `payment_method_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_payment_method`
--

INSERT INTO `trvl_ref_payment_method` (`payment_method_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Cash', '2021-03-22 07:24:33', '2021-03-22 07:24:33'),
(2, 'Transfer BRI', '2021-03-22 07:24:33', '2021-03-22 07:24:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_spj_budget_account`
--

CREATE TABLE `trvl_ref_spj_budget_account` (
  `account_id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_spj_budget_account`
--

INSERT INTO `trvl_ref_spj_budget_account` (`account_id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Driver', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(2, 'Co. Driver', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(3, 'Jarak', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(4, 'BBM', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(5, 'Tol', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(6, 'Parkir', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(7, 'Lain-lain', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22'),
(8, 'M-Kir', 'Y', '2021-04-20 02:13:22', '2021-04-20 02:13:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_ref_spj_id`
--

CREATE TABLE `trvl_ref_spj_id` (
  `id` int(11) NOT NULL,
  `month` varchar(7) NOT NULL,
  `serial_num` int(4) UNSIGNED ZEROFILL NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_ref_spj_id`
--

INSERT INTO `trvl_ref_spj_id` (`id`, `month`, `serial_num`, `created_at`, `updated_at`) VALUES
(1, '2023-10', 0002, '2023-10-20 08:27:59', '2023-10-20 08:27:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_booking`
--

CREATE TABLE `trvl_trnsct_booking` (
  `booking_id` varchar(15) NOT NULL,
  `trnsct_date` varchar(30) NOT NULL,
  `cust_id` int(5) DEFAULT NULL,
  `cust_name` varchar(45) DEFAULT NULL,
  `cust_phone_num` varchar(25) DEFAULT NULL,
  `cust_address` text DEFAULT NULL,
  `booking_start_date` varchar(10) NOT NULL,
  `booking_end_date` varchar(10) NOT NULL,
  `date_not_for_sale` varchar(10) DEFAULT NULL,
  `destination` text NOT NULL,
  `pick_up_location` text NOT NULL,
  `standby_time` text NOT NULL,
  `information` text DEFAULT NULL,
  `booking_from` varchar(15) NOT NULL,
  `agent_id` int(3) DEFAULT NULL,
  `agent_commission` int(20) DEFAULT NULL,
  `agent_commission_paid` int(15) DEFAULT NULL,
  `st_commission` varchar(20) DEFAULT 'BELUM DICAIRKAN',
  `ttl_unit` int(3) NOT NULL,
  `ttl_trnsct` int(20) NOT NULL,
  `ttl_trnsct_paid` int(20) DEFAULT 0,
  `charge` int(20) DEFAULT NULL,
  `discount` int(20) DEFAULT NULL,
  `status` varchar(25) DEFAULT 'SUKSES',
  `cancel_date` varchar(10) DEFAULT NULL,
  `cancel_by` varchar(45) DEFAULT NULL,
  `money_refundable_plan` int(25) DEFAULT 0,
  `money_return` int(25) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_trnsct_booking`
--

INSERT INTO `trvl_trnsct_booking` (`booking_id`, `trnsct_date`, `cust_id`, `cust_name`, `cust_phone_num`, `cust_address`, `booking_start_date`, `booking_end_date`, `date_not_for_sale`, `destination`, `pick_up_location`, `standby_time`, `information`, `booking_from`, `agent_id`, `agent_commission`, `agent_commission_paid`, `st_commission`, `ttl_unit`, `ttl_trnsct`, `ttl_trnsct_paid`, `charge`, `discount`, `status`, `cancel_date`, `cancel_by`, `money_refundable_plan`, `money_return`, `created_at`, `updated_at`) VALUES
('BK2023090001', '2023-09-23 15:36:22', 132, 'PT Meiloon Technology Indonesia', '082130748197', 'PT Meiloon Technology Indonesi Pagaden', '2023-09-24', '2023-09-24', '2023-09-24', 'Cikarang', 'PT Meiloon Technology Indonesi Pagaden', '07.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 3700000, 3700000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2023-09-23 08:36:22', '2023-09-23 08:36:22'),
('BK2023090002', '2023-09-30 20:06:48', 133, 'BEM STIE Miftahul Huda', '085352983954', 'Rancasari', '2023-10-05', '2023-10-05', '2023-10-05', 'Bursa Efek Indonesia, Musium Bank Indonesia, Ancol', 'Rancasari', '03.00', 'Driver Bp. Asep K', 'AGENT', 24, 100000, NULL, 'BELUM DICAIRKAN', 1, 4300000, 4300000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2023-09-30 13:06:48', '2023-09-30 13:09:38'),
('BK2023100001', '2023-10-07 17:34:32', 134, 'Bp. Syafiq', '081319924140', 'Purwakarta', '2023-10-21', '2023-10-22', '2023-10-22', 'Vila istana bunga lembang, Cibiuk Resto Lembang', 'Purwakarta', '08.00', 'Sudah termasuk biaya BBM, Tol, Parkir, Tips Supir', 'UMUM', 0, 0, NULL, 'BELUM DICAIRKAN', 1, 4500000, 4500000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2023-10-07 10:34:32', '2023-10-20 08:12:04'),
('BK2023100002', '2023-10-14 09:03:34', 135, 'Fahrul', '083195550003', 'Jungklang', '2023-10-22', '2023-10-22', '2023-10-22', 'Ds. Ciranggen Kec. Jatigede, al kamil', 'Jungklang Indomaret', '03.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 2, 7900000, 7900000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2023-10-14 02:03:34', '2023-10-14 02:04:44'),
('BK2023110001', '2023-11-10 17:22:48', 136, 'Ibu Yeni Nuraeni', '081386356052/081320423030', 'Kantor Dinas Tenaga kerja transmigrasi subang', '2023-12-22', '2023-12-25', '2023-12-25', 'Dieng, Jogjakarta', 'Kantor Dinas Tenaga kerja transmigrasi subang', '16.00', 'Jok 2-2 seat 50 + Selimut', 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 10500000, 7500000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2023-11-10 10:22:48', '2023-11-16 05:03:45'),
('BK2023120001', '2023-12-26 18:00:07', 137, 'Bp. Wirta', '085782472223', 'Sukra', '2023-12-27', '2023-12-27', '2023-12-27', 'water park parahiyangan, al jabar bandung', 'Sukra', '05.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 4000000, 4000000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2023-12-26 11:00:07', '2023-12-26 11:02:16'),
('BK2024010001', '2024-01-04 09:50:11', 138, 'Bp. H. Yaya Ruhmaya', '085782472223', 'Pamanukan', '2024-01-13', '2024-01-13', '2024-01-13', 'RM. Sukahati Cipacing Sumedang - Janspark - Al Jabar', 'Mesjid Al Muhlisin Pamanukan', '06.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 2, 8000000, 8000000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-01-04 02:50:11', '2024-01-04 02:50:36'),
('BK2024010002', '2024-01-22 18:35:11', 139, 'Sekolah Bunda Maria', '085782472223', 'Pamanukan', '2024-01-24', '2024-01-24', '2024-01-24', 'Subang Kota', 'Sekolah Bunda Maria Pamanukan', '06.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 1000000, 1000000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-01-22 11:35:11', '2024-01-23 05:49:51'),
('BK2024010003', '2024-01-30 19:43:44', 140, 'Ahmad Arbah', '08219283572', 'Cidahu', '2024-01-29', '2024-01-30', '2024-01-30', 'Cikole Lembang', 'Cidahu', '12.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 2500000, 2500000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-01-30 12:43:44', '2024-01-30 12:43:44'),
('BK2024020002', '2024-02-01 17:51:51', 142, 'Fave Hotel Pamanukan', '085724427540', 'Pamanukan', '2024-02-02', '2024-02-02', '2024-02-02', 'Trans tudio Bandung', 'Pamanukan', '07.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 1700000, 1700000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-01 10:51:51', '2024-02-01 10:51:51'),
('BK2024020003', '2024-02-01 17:57:01', 143, 'Bp. Sugandi', '082127163192', 'Sukadana', '2024-02-03', '2024-02-04', '2024-02-04', 'Drajat pas garut', 'Sukadana', '05.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 4700000, 4700000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-01 10:57:01', '2024-02-01 10:57:01'),
('BK2024020004', '2024-02-13 10:02:53', 144, 'Bp. Uhe', '085716455986', 'cikande cilebar karawang', '2024-02-18', '2024-02-18', '2024-02-18', 'gunung jati, kraton, Cibulan kuningan', 'cikande cilebar karawang', '01.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 3, 11250000, 11250000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-13 03:02:53', '2024-02-13 03:06:02'),
('BK2024020005', '2024-02-17 18:14:38', 145, 'AKBAR', '083816387789', 'Ciasem', '2024-02-18', '2024-02-18', '2024-02-18', 'Tropicana Cirebon', 'PPN Ciasem', '05.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 4300000, 4300000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-17 11:14:38', '2024-02-17 11:15:41'),
('BK2024020006', '2024-02-22 15:27:15', 146, 'Astahanas', '082120357400', 'Binong', '2024-02-21', '2024-02-21', '2024-02-21', 'UNHAN sentul bogor', 'Binong', '04.00', NULL, 'BIRO', 25, 300000, 300000, 'SUDAH DICAIRKAN', 3, 12000000, 12000000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-22 08:27:15', '2024-02-22 09:04:36'),
('BK2024020007', '2024-02-22 18:00:43', 147, 'Fave Hotel Pamanukan', '085724427540', 'Pamanukan', '2024-02-23', '2024-02-23', '2024-02-23', 'Trans tudio Bandung', 'Fave Hotel Pamanukan', '07.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 1700000, 1700000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-22 11:00:43', '2024-02-22 11:00:43'),
('BK2024020008', '2024-02-24 13:12:19', 148, 'Bp. Jafar', '085782472223', 'Patrol', '2024-02-25', '2024-02-25', '2024-02-25', 'Jamblang Palimanan, Gunung Jati', 'Mekarsari Patrol', '06.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 2, 6600000, 6600000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-24 06:12:19', '2024-02-24 06:15:21'),
('BK2024020009', '2024-02-27 12:32:02', 149, 'SD Yusodarso', '08156119464', 'Subang', '2024-02-24', '2024-02-24', '2024-02-24', 'Cikole Lembang', 'radio benpas subang', '06.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 4200000, 4200000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-02-27 05:32:02', '2024-02-27 05:33:19'),
('BK2024030001', '2024-03-04 13:22:47', 150, 'Bp. Warsono', '085782472223', 'Karawang', '2024-04-11', '2024-04-12', '2024-04-12', 'sarimukti rt 12/03 sindangangin lakbok ciamis', 'suryacipta karawang', '13.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 3800000, 3800000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-03-04 06:22:47', '2024-03-04 06:23:16'),
('BK2024030002', '2024-03-12 11:10:57', 151, 'Gereja Katolik GKSP Subang Sie Pendidikan', '08156119464', 'Subang', '2024-03-16', '2024-03-16', '2024-03-16', 'Cikarang', 'Greja katolik/belakang gor subang Jln A. Nata Sukarya No 22', '05.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 4200000, 4200000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-03-12 04:10:57', '2024-03-14 08:46:02'),
('BK2024030003', '2024-03-14 15:49:36', 152, 'Gereja Katolik GKSP Subang Sie Pendidikan', '085782472223', 'Subang', '2024-03-16', '2024-03-16', '2024-03-16', 'Cikarang', 'Greja katolik/belakang gor subang Jln A. Nata Sukarya No 22', '05.00 wib', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 2700000, 2700000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-03-14 08:49:36', '2024-03-14 08:49:36'),
('BK2024040001', '2024-04-25 14:38:14', 153, 'SD YOSUDARSO', '08156119464', 'Subang', '2024-04-27', '2024-04-27', '2024-04-27', 'Bandung', 'Depan Radio  Benpas subang', '04.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 1, 2700000, 2700000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-04-25 07:38:14', '2024-04-25 07:38:41'),
('BK2024040002', '2024-04-29 13:06:18', 154, 'NCTZEN Subang', '085179522318', 'Subang', '2024-05-18', '2024-05-18', '2024-05-18', 'GBK, Lotte Avenue Kuningan Jakarta', 'Pamanukan - Subang', '05.00', NULL, 'UMUM', NULL, 0, NULL, 'BELUM DICAIRKAN', 2, 5300000, 5300000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-04-29 06:06:18', '2024-04-29 09:21:36'),
('BK2024050001', '2024-05-01 10:32:31', 155, 'Harum Tour', '081325567220', 'Patrol', '2024-05-23', '2024-05-23', '2024-05-23', 'Lembah dewata lembang', 'indramayu arahan bangkir', '04.30', NULL, 'BIRO', 9, 400000, NULL, 'BELUM DICAIRKAN', 2, 7600000, 1000000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-05-01 03:32:31', '2024-05-01 03:32:31'),
('BK2024050002', '2024-05-01 16:04:44', 156, 'Harum Tour', '081325567220', 'Patrol', '2024-05-02', '2024-05-02', '2024-05-02', 'Bandara', 'Patrol', '0', NULL, 'BIRO', 9, 200000, NULL, 'BELUM DICAIRKAN', 1, 4200000, 4200000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-05-01 09:04:44', '2024-05-01 09:06:09'),
('BK2024050003', '2024-05-03 09:34:59', 157, 'SMAN 1 Ciasem', '081328196867', 'Ciasem', '2024-05-06', '2024-05-08', '2024-05-09', 'Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis', 'SMAN 1 Ciasem', '14.00', NULL, 'UMUM', 0, 0, NULL, 'BELUM DICAIRKAN', 5, 67500000, 67500000, 0, 0, 'SUKSES', NULL, NULL, 0, NULL, '2024-05-03 02:34:59', '2024-05-03 07:37:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_booking_dtl`
--

CREATE TABLE `trvl_trnsct_booking_dtl` (
  `trnsctdtl_id` bigint(20) NOT NULL,
  `booking_id` varchar(25) NOT NULL,
  `booking_start_date` varchar(10) NOT NULL,
  `booking_end_date` varchar(10) NOT NULL,
  `date_not_for_sale` varchar(10) DEFAULT NULL,
  `category_id` varchar(10) NOT NULL,
  `unit_qty` int(3) NOT NULL DEFAULT 0,
  `unit_qty_spj` int(3) NOT NULL DEFAULT 0,
  `price` int(20) NOT NULL DEFAULT 0,
  `total` int(20) NOT NULL DEFAULT 0,
  `status` varchar(25) DEFAULT 'MENUNGGU',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_trnsct_booking_dtl`
--

INSERT INTO `trvl_trnsct_booking_dtl` (`trnsctdtl_id`, `booking_id`, `booking_start_date`, `booking_end_date`, `date_not_for_sale`, `category_id`, `unit_qty`, `unit_qty_spj`, `price`, `total`, `status`, `created_at`, `updated_at`) VALUES
(1, 'BK2023090001', '2023-09-24', '2023-09-24', '2023-09-24', '2', 1, 0, 3700000, 3700000, 'MENUNGGU', '2023-09-23 08:36:22', '2023-09-23 08:36:22'),
(2, 'BK2023090002', '2023-10-05', '2023-10-05', '2023-10-05', '1', 1, 0, 4300000, 4300000, 'MENUNGGU', '2023-09-30 13:06:48', '2023-09-30 13:06:48'),
(3, 'BK2023100001', '2023-10-21', '2023-10-22', '2023-10-22', '3', 1, 1, 4500000, 4500000, 'MENUNGGU', '2023-10-07 10:34:32', '2023-10-07 10:34:32'),
(4, 'BK2023100002', '2023-10-22', '2023-10-22', '2023-10-22', '1', 1, 0, 4200000, 4200000, 'MENUNGGU', '2023-10-14 02:03:34', '2023-10-14 02:03:34'),
(5, 'BK2023100002', '2023-10-22', '2023-10-22', '2023-10-22', '2', 1, 0, 3700000, 3700000, 'MENUNGGU', '2023-10-14 02:03:34', '2023-10-14 02:03:34'),
(6, 'BK2023110001', '2023-12-22', '2023-12-25', '2023-12-25', '2', 1, 0, 10500000, 10500000, 'MENUNGGU', '2023-11-10 10:22:48', '2023-11-10 10:22:48'),
(7, 'BK2023120001', '2023-12-27', '2023-12-27', '2023-12-27', '2', 1, 0, 4000000, 4000000, 'MENUNGGU', '2023-12-26 11:00:07', '2023-12-26 11:00:07'),
(8, 'BK2024010001', '2024-01-13', '2024-01-13', '2024-01-13', '1', 2, 0, 4000000, 8000000, 'MENUNGGU', '2024-01-04 02:50:11', '2024-01-04 02:50:11'),
(9, 'BK2024010002', '2024-01-24', '2024-01-24', '2024-01-24', '6', 1, 0, 1000000, 1000000, 'MENUNGGU', '2024-01-22 11:35:11', '2024-01-22 11:35:11'),
(10, 'BK2024010003', '2024-01-29', '2024-01-30', '2024-01-30', '6', 1, 0, 2500000, 2500000, 'MENUNGGU', '2024-01-30 12:43:44', '2024-01-30 12:43:44'),
(11, 'BK2024020002', '2024-02-02', '2024-02-02', '2024-02-02', '5', 1, 0, 1700000, 1700000, 'MENUNGGU', '2024-02-01 10:51:51', '2024-02-01 10:51:51'),
(12, 'BK2024020003', '2024-02-03', '2024-02-04', '2024-02-04', '3', 1, 0, 4700000, 4700000, 'MENUNGGU', '2024-02-01 10:57:01', '2024-02-01 10:57:01'),
(13, 'BK2024020004', '2024-02-18', '2024-02-18', '2024-02-18', '2', 3, 0, 3750000, 11250000, 'MENUNGGU', '2024-02-13 03:02:53', '2024-02-13 03:02:53'),
(14, 'BK2024020005', '2024-02-18', '2024-02-18', '2024-02-18', '1', 1, 0, 4300000, 4300000, 'MENUNGGU', '2024-02-17 11:14:38', '2024-02-17 11:14:38'),
(15, 'BK2024020006', '2024-02-21', '2024-02-21', '2024-02-21', '2', 3, 0, 4000000, 12000000, 'MENUNGGU', '2024-02-22 08:27:15', '2024-02-22 08:27:15'),
(16, 'BK2024020007', '2024-02-23', '2024-02-23', '2024-02-23', '5', 1, 0, 1700000, 1700000, 'MENUNGGU', '2024-02-22 11:00:43', '2024-02-22 11:00:43'),
(17, 'BK2024020008', '2024-02-25', '2024-02-25', '2024-02-25', '2', 2, 0, 3300000, 6600000, 'MENUNGGU', '2024-02-24 06:12:19', '2024-02-24 06:12:19'),
(18, 'BK2024020009', '2024-02-24', '2024-02-24', '2024-02-24', '1', 1, 0, 4200000, 4200000, 'MENUNGGU', '2024-02-27 05:32:02', '2024-02-27 05:32:02'),
(19, 'BK2024030001', '2024-04-11', '2024-04-12', '2024-04-12', '5', 1, 0, 3800000, 3800000, 'MENUNGGU', '2024-03-04 06:22:47', '2024-03-04 06:22:47'),
(20, 'BK2024030002', '2024-03-16', '2024-03-16', '2024-03-16', '1', 1, 0, 4200000, 4200000, 'MENUNGGU', '2024-03-12 04:10:57', '2024-03-12 04:10:57'),
(21, 'BK2024030003', '2024-03-16', '2024-03-16', '2024-03-16', '3', 1, 0, 2700000, 2700000, 'MENUNGGU', '2024-03-14 08:49:36', '2024-03-14 08:49:36'),
(22, 'BK2024040001', '2024-04-27', '2024-04-27', '2024-04-27', '3', 1, 0, 2700000, 2700000, 'MENUNGGU', '2024-04-25 07:38:14', '2024-04-25 07:38:14'),
(23, 'BK2024040002', '2024-05-18', '2024-05-18', '2024-05-18', '2', 1, 0, 3800000, 3800000, 'MENUNGGU', '2024-04-29 06:06:18', '2024-04-29 06:06:18'),
(24, 'BK2024040002', '2024-05-18', '2024-05-18', '2024-05-18', '6', 1, 0, 1500000, 1500000, 'MENUNGGU', '2024-04-29 06:06:18', '2024-04-29 06:06:18'),
(25, 'BK2024050001', '2024-05-23', '2024-05-23', '2024-05-23', '2', 2, 0, 3800000, 7600000, 'MENUNGGU', '2024-05-01 03:32:31', '2024-05-01 03:32:31'),
(26, 'BK2024050002', '2024-05-02', '2024-05-02', '2024-05-02', '1', 1, 0, 4200000, 4200000, 'MENUNGGU', '2024-05-01 09:04:44', '2024-05-01 09:04:44'),
(27, 'BK2024050003', '2024-05-06', '2024-05-08', '2024-05-09', '1', 5, 0, 13500000, 67500000, 'MENUNGGU', '2024-05-03 02:34:59', '2024-05-03 02:34:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_debt`
--

CREATE TABLE `trvl_trnsct_debt` (
  `id` bigint(20) NOT NULL,
  `sdm_type` enum('SDM','DRIVER','HELPER') NOT NULL DEFAULT 'DRIVER',
  `sdm_id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `amount` int(25) NOT NULL DEFAULT 0,
  `amount_paid` int(25) DEFAULT 0,
  `desc` varchar(225) NOT NULL,
  `account_id` varchar(25) NOT NULL,
  `status` enum('SUCCESS','CANCEL') DEFAULT 'SUCCESS',
  `admin` varchar(60) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_debt_payment`
--

CREATE TABLE `trvl_trnsct_debt_payment` (
  `id` int(11) NOT NULL,
  `debt_id` bigint(20) NOT NULL,
  `account_id` varchar(25) NOT NULL,
  `amount` int(25) NOT NULL DEFAULT 0,
  `admin` varchar(60) NOT NULL,
  `payment_date` varchar(30) NOT NULL,
  `payrol_payment_id` bigint(20) DEFAULT NULL,
  `status` enum('SUCCESS','CANCEL') NOT NULL DEFAULT 'SUCCESS',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_jurnal`
--

CREATE TABLE `trvl_trnsct_jurnal` (
  `jurnal_id` bigint(20) NOT NULL,
  `proof_id` varchar(15) NOT NULL,
  `trnsct_date` varchar(25) NOT NULL,
  `related_person` varchar(45) NOT NULL,
  `account_id` varchar(15) NOT NULL,
  `description` text NOT NULL,
  `debit` int(25) DEFAULT 0,
  `credit` int(25) DEFAULT 0,
  `type` varchar(15) NOT NULL,
  `trnsct_type` varchar(45) DEFAULT NULL,
  `post_net_profit` varchar(35) DEFAULT NULL,
  `is_main` enum('Y','N') NOT NULL,
  `is_refund` enum('N','Y') DEFAULT 'N',
  `user` varchar(45) NOT NULL,
  `status` varchar(10) DEFAULT 'OK',
  `status_booking` enum('OK','BATAL') DEFAULT 'OK',
  `booking_id` varchar(25) DEFAULT NULL,
  `spj_id` varchar(25) DEFAULT NULL,
  `payrol_id` bigint(20) DEFAULT NULL,
  `debt_id` bigint(20) DEFAULT NULL,
  `debt_payment_id` bigint(20) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_trnsct_jurnal`
--

INSERT INTO `trvl_trnsct_jurnal` (`jurnal_id`, `proof_id`, `trnsct_date`, `related_person`, `account_id`, `description`, `debit`, `credit`, `type`, `trnsct_type`, `post_net_profit`, `is_main`, `is_refund`, `user`, `status`, `status_booking`, `booking_id`, `spj_id`, `payrol_id`, `debt_id`, `debt_payment_id`, `updated_at`, `created_at`) VALUES
(1, 'INV2023090001', '2023-09-23', 'PT Meiloon Technology Indonesia', '1-120', 'Sewa Big Bus Setra Tgl 24-09-2023 Tujuan Cikarang A/N PT Meiloon Technology Indonesia', 3700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023090001', NULL, NULL, NULL, NULL, '2023-09-23 08:36:22', '2023-09-23 08:36:22'),
(2, 'INV2023090001', '2023-09-23', 'PT Meiloon Technology Indonesia', '4-100', 'Sewa Big Bus Setra Tgl 24-09-2023 Tujuan Cikarang A/N PT Meiloon Technology Indonesia', 0, 3700000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023090001', NULL, NULL, NULL, NULL, '2023-09-23 08:36:22', '2023-09-23 08:36:22'),
(3, 'INV2023090002', '2023-09-30', 'BEM STIE Miftahul Huda', '1-120', 'DP Big Bus SHD Tgl 05-10-2023 Tujuan Bursa Efek Indonesia, Musium Bank Indonesia, Ancol A/N BEM STIE Miftahul Huda', 400000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023090002', NULL, NULL, NULL, NULL, '2023-09-30 13:06:48', '2023-09-30 13:06:48'),
(4, 'INV2023090002', '2023-09-30', 'BEM STIE Miftahul Huda', '1-130', 'DP Big Bus SHD Tgl 05-10-2023 Tujuan Bursa Efek Indonesia, Musium Bank Indonesia, Ancol A/N BEM STIE Miftahul Huda', 0, 400000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023090002', NULL, NULL, NULL, NULL, '2023-09-30 13:06:48', '2023-09-30 13:06:48'),
(5, 'INV2023090004', '2023-09-30', 'BEM STIE Miftahul Huda', '1-130', 'Pelunasan Kode Booking BK2023090002', 4300000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023090002', NULL, NULL, NULL, NULL, '2023-09-30 13:09:38', '2023-09-30 13:09:38'),
(6, 'INV2023090004', '2023-09-30', 'BEM STIE Miftahul Huda', '4-100', 'Pelunasan Kode Booking BK2023090002', 0, 4300000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023090002', NULL, NULL, NULL, NULL, '2023-09-30 13:09:38', '2023-09-30 13:09:38'),
(7, 'INV2023090003', '2023-09-30', 'BEM STIE Miftahul Huda', '1-120', 'Pelunasan Big Bus SHD Tgl 05-10-2023 Tujuan Bursa Efek Indonesia, Musium Bank Indonesia, Ancol A/N BEM STIE Miftahul Huda', 3900000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023090002', NULL, NULL, NULL, NULL, '2023-09-30 13:09:38', '2023-09-30 13:09:38'),
(8, 'INV2023090003', '2023-09-30', 'BEM STIE Miftahul Huda', '1-130', 'Pelunasan Big Bus SHD Tgl 05-10-2023 Tujuan Bursa Efek Indonesia, Musium Bank Indonesia, Ancol A/N BEM STIE Miftahul Huda', 0, 3900000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023090002', NULL, NULL, NULL, NULL, '2023-09-30 13:09:38', '2023-09-30 13:09:38'),
(9, 'INV2023100001', '2023-10-07', 'Bp. Syafiq', '1-120', 'DP Medium Bus Tgl 21-10-2023 Tujuan Vila Istana Bunga Lembang A/N Bp. Syafiq', 500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', NULL, NULL, NULL, NULL, '2023-10-07 10:34:32', '2023-10-07 10:34:32'),
(10, 'INV2023100001', '2023-10-07', 'Bp. Syafiq', '1-130', 'DP Medium Bus Tgl 21-10-2023 Tujuan Vila Istana Bunga Lembang A/N Bp. Syafiq', 0, 500000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', NULL, NULL, NULL, NULL, '2023-10-07 10:34:32', '2023-10-07 10:34:32'),
(11, 'INV2023100002', '2023-10-14', 'Fahrul', '1-110', 'DP Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 1000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:03:34', '2023-10-14 02:03:34'),
(12, 'INV2023100002', '2023-10-14', 'Fahrul', '1-130', 'DP Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 0, 1000000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:03:34', '2023-10-14 02:03:34'),
(13, 'INV2023100003', '2023-10-02', 'Fahrul', '1-110', 'DP Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 5000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:04:18', '2023-10-14 02:04:18'),
(14, 'INV2023100003', '2023-10-02', 'Fahrul', '1-130', 'DP Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 0, 5000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:04:18', '2023-10-14 02:04:18'),
(15, 'INV2023100005', '2023-10-14', 'Fahrul', '1-130', 'Pelunasan Kode Booking BK2023100002', 7900000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:04:44', '2023-10-14 02:04:44'),
(16, 'INV2023100005', '2023-10-14', 'Fahrul', '4-100', 'Pelunasan Kode Booking BK2023100002', 0, 7900000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:04:44', '2023-10-14 02:04:44'),
(17, 'INV2023100004', '2023-10-14', 'Fahrul', '1-110', 'Pelunasan Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 1900000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:04:44', '2023-10-14 02:04:44'),
(18, 'INV2023100004', '2023-10-14', 'Fahrul', '1-130', 'Pelunasan Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 0, 1900000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100002', NULL, NULL, NULL, NULL, '2023-10-14 02:04:44', '2023-10-14 02:04:44'),
(19, 'INV2023100007', '2023-10-20', 'Bp. Syafiq', '1-130', 'Pelunasan Kode Booking BK2023100001', 4500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', NULL, NULL, NULL, NULL, '2023-10-20 08:07:31', '2023-10-20 08:07:31'),
(20, 'INV2023100007', '2023-10-20', 'Bp. Syafiq', '4-100', 'Pelunasan Kode Booking BK2023100001', 0, 4500000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', NULL, NULL, NULL, NULL, '2023-10-20 08:07:31', '2023-10-20 08:07:31'),
(21, 'INV2023100006', '2023-10-20', 'Bp. Syafiq', '1-110', 'Pelunasan Medium Bus Tgl 21-10-2023 Tujuan Vila Istana Bunga Lembang A/N Bp. Syafiq', 4000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', NULL, NULL, NULL, NULL, '2023-10-20 08:07:31', '2023-10-20 08:07:31'),
(22, 'INV2023100006', '2023-10-20', 'Bp. Syafiq', '1-130', 'Pelunasan Medium Bus Tgl 21-10-2023 Tujuan Vila Istana Bunga Lembang A/N Bp. Syafiq', 0, 4000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', NULL, NULL, NULL, NULL, '2023-10-20 08:07:31', '2023-10-20 08:07:31'),
(23, 'INV2023100008', '2023-10-20', 'WIRTA', '1-110', 'Kas Jalan Big Bus SHD Tgl 21-10-2023 Tujuan Vila istana bunga lembang, Cibiuk Resto Lembang A/N Bp. Syafiq', 0, 1800000, 'OUT', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', '0001/SPJ/OPR/PT.KME/10/23', NULL, NULL, NULL, '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(24, 'INV2023100008', '2023-10-20', 'WIRTA', '5-100', 'Kas Jalan Big Bus SHD Tgl 21-10-2023 Tujuan Vila istana bunga lembang, Cibiuk Resto Lembang A/N Bp. Syafiq', 1800000, 0, 'OUT', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023100001', '0001/SPJ/OPR/PT.KME/10/23', NULL, NULL, NULL, '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(25, 'INV2023100009', '2023-10-20', 'Admin', '1-110', 'Pemindahan uang dari kas ke BRI', 0, 4000000, 'DEPOSIT', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', NULL, NULL, NULL, NULL, NULL, '2023-10-20 08:33:08', '2023-10-20 08:33:08'),
(26, 'INV2023100009', '2023-10-20', 'Admin', '1-120', 'Pemindahan uang dari kas ke BRI', 4000000, 0, 'DEPOSIT', NULL, 'NERACA', 'Y', 'N', 'Yusup', 'OK', 'OK', NULL, NULL, NULL, NULL, NULL, '2023-10-20 08:33:08', '2023-10-20 08:33:08'),
(27, 'INV2023110001', '2023-11-10', 'Ibu Yeni Nuraeni', '1-120', 'DP Big Bus Setra Tgl 22-12-2023 Tujuan Dieng, Jogjakarta A/N Ibu Yeni Nuraeni', 5500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023110001', NULL, NULL, NULL, NULL, '2023-11-10 10:22:48', '2023-11-10 10:22:48'),
(28, 'INV2023110001', '2023-11-10', 'Ibu Yeni Nuraeni', '1-130', 'DP Big Bus Setra Tgl 22-12-2023 Tujuan Dieng, Jogjakarta A/N Ibu Yeni Nuraeni', 0, 5500000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023110001', NULL, NULL, NULL, NULL, '2023-11-10 10:22:48', '2023-11-10 10:22:48'),
(29, 'INV2023110002', '2023-11-16', 'Ibu Yeni Nuraeni', '1-120', 'DP Big Bus Setra Tgl 22-12-2023 Tujuan Dieng, Jogjakarta A/N Ibu Yeni Nuraeni', 2000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023110001', NULL, NULL, NULL, NULL, '2023-11-16 05:03:45', '2023-11-16 05:03:45'),
(30, 'INV2023110002', '2023-11-16', 'Ibu Yeni Nuraeni', '1-130', 'DP Big Bus Setra Tgl 22-12-2023 Tujuan Dieng, Jogjakarta A/N Ibu Yeni Nuraeni', 0, 2000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023110001', NULL, NULL, NULL, NULL, '2023-11-16 05:03:45', '2023-11-16 05:03:45'),
(31, 'INV2023120001', '2023-12-26', 'Bp. Wirta', '1-110', 'DP Big Bus Setra Tgl 27-12-2023 Tujuan water park parahiyangan, al jabar bandung A/N Bp. Wirta', 0, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023120001', NULL, NULL, NULL, NULL, '2023-12-26 11:01:15', '2023-12-26 11:01:15'),
(32, 'INV2023120001', '2023-12-26', 'Bp. Wirta', '1-130', 'DP Big Bus Setra Tgl 27-12-2023 Tujuan water park parahiyangan, al jabar bandung A/N Bp. Wirta', 0, 0, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023120001', NULL, NULL, NULL, NULL, '2023-12-26 11:01:15', '2023-12-26 11:01:15'),
(33, 'INV2023120003', '2023-12-26', 'Bp. Wirta', '1-130', 'Pelunasan Kode Booking BK2023120001', 4000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023120001', NULL, NULL, NULL, NULL, '2023-12-26 11:02:16', '2023-12-26 11:02:16'),
(34, 'INV2023120003', '2023-12-26', 'Bp. Wirta', '4-100', 'Pelunasan Kode Booking BK2023120001', 0, 4000000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023120001', NULL, NULL, NULL, NULL, '2023-12-26 11:02:16', '2023-12-26 11:02:16'),
(35, 'INV2023120002', '2023-12-26', 'Bp. Wirta', '1-110', 'Pelunasan Big Bus Setra Tgl 27-12-2023 Tujuan water park parahiyangan, al jabar bandung A/N Bp. Wirta', 4000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2023120001', NULL, NULL, NULL, NULL, '2023-12-26 11:02:16', '2023-12-26 11:02:16'),
(36, 'INV2023120002', '2023-12-26', 'Bp. Wirta', '1-130', 'Pelunasan Big Bus Setra Tgl 27-12-2023 Tujuan water park parahiyangan, al jabar bandung A/N Bp. Wirta', 0, 4000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2023120001', NULL, NULL, NULL, NULL, '2023-12-26 11:02:16', '2023-12-26 11:02:16'),
(37, 'INV2024010001', '2024-01-04', 'Bp. H. Yaya Ruhmaya', '1-110', 'DP Big Bus SHD Tgl 13-01-2024 Tujuan RM. Sukahati Cipacing Sumedang - Janspark - Al Jabar A/N Bp. H. Yaya Ruhmaya', 2000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024010001', NULL, NULL, NULL, NULL, '2024-01-04 02:50:11', '2024-01-04 02:50:11'),
(38, 'INV2024010001', '2024-01-04', 'Bp. H. Yaya Ruhmaya', '1-130', 'DP Big Bus SHD Tgl 13-01-2024 Tujuan RM. Sukahati Cipacing Sumedang - Janspark - Al Jabar A/N Bp. H. Yaya Ruhmaya', 0, 2000000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024010001', NULL, NULL, NULL, NULL, '2024-01-04 02:50:11', '2024-01-04 02:50:11'),
(39, 'INV2024010003', '2024-01-04', 'Bp. H. Yaya Ruhmaya', '1-130', 'Pelunasan Kode Booking BK2024010001', 8000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024010001', NULL, NULL, NULL, NULL, '2024-01-04 02:50:36', '2024-01-04 02:50:36'),
(40, 'INV2024010003', '2024-01-04', 'Bp. H. Yaya Ruhmaya', '4-100', 'Pelunasan Kode Booking BK2024010001', 0, 8000000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024010001', NULL, NULL, NULL, NULL, '2024-01-04 02:50:36', '2024-01-04 02:50:36'),
(41, 'INV2024010002', '2024-01-04', 'Bp. H. Yaya Ruhmaya', '1-110', 'Pelunasan Big Bus SHD Tgl 13-01-2024 Tujuan RM. Sukahati Cipacing Sumedang - Janspark - Al Jabar A/N Bp. H. Yaya Ruhmaya', 6000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024010001', NULL, NULL, NULL, NULL, '2024-01-04 02:50:36', '2024-01-04 02:50:36'),
(42, 'INV2024010002', '2024-01-04', 'Bp. H. Yaya Ruhmaya', '1-130', 'Pelunasan Big Bus SHD Tgl 13-01-2024 Tujuan RM. Sukahati Cipacing Sumedang - Janspark - Al Jabar A/N Bp. H. Yaya Ruhmaya', 0, 6000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024010001', NULL, NULL, NULL, NULL, '2024-01-04 02:50:36', '2024-01-04 02:50:36'),
(43, 'INV2024010004', '2024-01-22', 'Sekolah Bunda Maria', '1-120', 'DP Hiace Tgl 24-01-2024 Tujuan Subang Kota A/N Sekolah Bunda Maria', 200000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024010002', NULL, NULL, NULL, NULL, '2024-01-22 11:35:11', '2024-01-22 11:35:11'),
(44, 'INV2024010004', '2024-01-22', 'Sekolah Bunda Maria', '1-130', 'DP Hiace Tgl 24-01-2024 Tujuan Subang Kota A/N Sekolah Bunda Maria', 0, 200000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024010002', NULL, NULL, NULL, NULL, '2024-01-22 11:35:11', '2024-01-22 11:35:11'),
(45, 'INV2024010006', '2024-01-23', 'Sekolah Bunda Maria', '1-130', 'Pelunasan Kode Booking BK2024010002', 1000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024010002', NULL, NULL, NULL, NULL, '2024-01-23 05:49:51', '2024-01-23 05:49:51'),
(46, 'INV2024010006', '2024-01-23', 'Sekolah Bunda Maria', '4-100', 'Pelunasan Kode Booking BK2024010002', 0, 1000000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024010002', NULL, NULL, NULL, NULL, '2024-01-23 05:49:51', '2024-01-23 05:49:51'),
(47, 'INV2024010005', '2024-01-23', 'Sekolah Bunda Maria', '1-120', 'Pelunasan Hiace Tgl 24-01-2024 Tujuan Subang Kota A/N Sekolah Bunda Maria', 800000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024010002', NULL, NULL, NULL, NULL, '2024-01-23 05:49:51', '2024-01-23 05:49:51'),
(48, 'INV2024010005', '2024-01-23', 'Sekolah Bunda Maria', '1-130', 'Pelunasan Hiace Tgl 24-01-2024 Tujuan Subang Kota A/N Sekolah Bunda Maria', 0, 800000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024010002', NULL, NULL, NULL, NULL, '2024-01-23 05:49:51', '2024-01-23 05:49:51'),
(49, 'INV2024010007', '2024-01-30', 'Ahmad Arbah', '1-120', 'Sewa Hiace Tgl 29-01-2024 Tujuan Cikole Lembang A/N Ahmad Arbah', 2500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024010003', NULL, NULL, NULL, NULL, '2024-01-30 12:43:44', '2024-01-30 12:43:44'),
(50, 'INV2024010007', '2024-01-30', 'Ahmad Arbah', '4-100', 'Sewa Hiace Tgl 29-01-2024 Tujuan Cikole Lembang A/N Ahmad Arbah', 0, 2500000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024010003', NULL, NULL, NULL, NULL, '2024-01-30 12:43:44', '2024-01-30 12:43:44'),
(51, 'INV2024020001', '2024-02-01', 'Fave Hotel Pamanukan', '1-120', 'Sewa Elf Long Tgl 02-02-2024 Tujuan Trans tudio Bandung A/N Fave Hotel Pamanukan', 1700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020002', NULL, NULL, NULL, NULL, '2024-02-01 10:51:51', '2024-02-01 10:51:51'),
(52, 'INV2024020001', '2024-02-01', 'Fave Hotel Pamanukan', '4-100', 'Sewa Elf Long Tgl 02-02-2024 Tujuan Trans tudio Bandung A/N Fave Hotel Pamanukan', 0, 1700000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020002', NULL, NULL, NULL, NULL, '2024-02-01 10:51:51', '2024-02-01 10:51:51'),
(53, 'INV2024020002', '2024-02-01', 'Bp. Sugandi', '1-120', 'Sewa Medium Bus Tgl 03-02-2024 Tujuan Drajat pas garut A/N Bp. Sugandi', 4700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020003', NULL, NULL, NULL, NULL, '2024-02-01 10:57:01', '2024-02-01 10:57:01'),
(54, 'INV2024020002', '2024-02-01', 'Bp. Sugandi', '4-100', 'Sewa Medium Bus Tgl 03-02-2024 Tujuan Drajat pas garut A/N Bp. Sugandi', 0, 4700000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020003', NULL, NULL, NULL, NULL, '2024-02-01 10:57:01', '2024-02-01 10:57:01'),
(55, 'INV2024020003', '2024-02-13', 'Bp. Uhe', '1-120', 'DP Big Bus Setra Tgl 18-02-2024 Tujuan gunung jati, kraton, Cibulan kuningan A/N Bp. Uhe', 5250000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020004', NULL, NULL, NULL, NULL, '2024-02-13 03:02:53', '2024-02-13 03:02:53'),
(56, 'INV2024020003', '2024-02-13', 'Bp. Uhe', '1-130', 'DP Big Bus Setra Tgl 18-02-2024 Tujuan gunung jati, kraton, Cibulan kuningan A/N Bp. Uhe', 0, 5250000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020004', NULL, NULL, NULL, NULL, '2024-02-13 03:02:53', '2024-02-13 03:02:53'),
(57, 'INV2024020005', '2024-02-12', 'Bp. Uhe', '1-130', 'Pelunasan Kode Booking BK2024020004', 11250000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020004', NULL, NULL, NULL, NULL, '2024-02-13 03:06:02', '2024-02-13 03:06:02'),
(58, 'INV2024020005', '2024-02-12', 'Bp. Uhe', '4-100', 'Pelunasan Kode Booking BK2024020004', 0, 11250000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020004', NULL, NULL, NULL, NULL, '2024-02-13 03:06:02', '2024-02-13 03:06:02'),
(59, 'INV2024020004', '2024-02-12', 'Bp. Uhe', '1-120', 'Pelunasan Big Bus Setra Tgl 18-02-2024 Tujuan gunung jati, kraton, Cibulan kuningan A/N Bp. Uhe', 6000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020004', NULL, NULL, NULL, NULL, '2024-02-13 03:06:02', '2024-02-13 03:06:02'),
(60, 'INV2024020004', '2024-02-12', 'Bp. Uhe', '1-130', 'Pelunasan Big Bus Setra Tgl 18-02-2024 Tujuan gunung jati, kraton, Cibulan kuningan A/N Bp. Uhe', 0, 6000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020004', NULL, NULL, NULL, NULL, '2024-02-13 03:06:02', '2024-02-13 03:06:02'),
(61, 'INV2024020006', '2024-02-17', 'AKBAR', '1-110', 'DP Big Bus SHD Tgl 18-02-2024 Tujuan Tropicana Cirebon A/N AKBAR', 500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020005', NULL, NULL, NULL, NULL, '2024-02-17 11:14:38', '2024-02-17 11:14:38'),
(62, 'INV2024020006', '2024-02-17', 'AKBAR', '1-130', 'DP Big Bus SHD Tgl 18-02-2024 Tujuan Tropicana Cirebon A/N AKBAR', 0, 500000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020005', NULL, NULL, NULL, NULL, '2024-02-17 11:14:38', '2024-02-17 11:14:38'),
(63, 'INV2024020008', '2024-02-17', 'AKBAR', '1-130', 'Pelunasan Kode Booking BK2024020005', 4300000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020005', NULL, NULL, NULL, NULL, '2024-02-17 11:15:41', '2024-02-17 11:15:41'),
(64, 'INV2024020008', '2024-02-17', 'AKBAR', '4-100', 'Pelunasan Kode Booking BK2024020005', 0, 4300000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020005', NULL, NULL, NULL, NULL, '2024-02-17 11:15:41', '2024-02-17 11:15:41'),
(65, 'INV2024020007', '2024-02-17', 'AKBAR', '1-120', 'Pelunasan Big Bus SHD Tgl 18-02-2024 Tujuan Tropicana Cirebon A/N AKBAR', 3800000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020005', NULL, NULL, NULL, NULL, '2024-02-17 11:15:41', '2024-02-17 11:15:41'),
(66, 'INV2024020007', '2024-02-17', 'AKBAR', '1-130', 'Pelunasan Big Bus SHD Tgl 18-02-2024 Tujuan Tropicana Cirebon A/N AKBAR', 0, 3800000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020005', NULL, NULL, NULL, NULL, '2024-02-17 11:15:41', '2024-02-17 11:15:41'),
(67, 'INV2024020009', '2024-02-22', 'Astahanas', '1-120', 'Sewa Big Bus Setra Tgl 21-02-2024 Tujuan UNHAN sentul bogor A/N Astahanas', 12000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020006', NULL, NULL, NULL, NULL, '2024-02-22 08:27:15', '2024-02-22 08:27:15'),
(68, 'INV2024020009', '2024-02-22', 'Astahanas', '4-100', 'Sewa Big Bus Setra Tgl 21-02-2024 Tujuan UNHAN sentul bogor A/N Astahanas', 0, 12000000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020006', NULL, NULL, NULL, NULL, '2024-02-22 08:27:15', '2024-02-22 08:27:15'),
(69, 'INV2024020010', '2024-02-22', 'Bp. Asep Astahanas', '1-120', 'Pencairan Komisi BK2024020006', 0, 300000, 'OUT', 'DISBURSEMENT_COMMISSION', NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020006', NULL, NULL, NULL, NULL, '2024-02-22 09:04:36', '2024-02-22 09:04:36'),
(70, 'INV2024020010', '2024-02-22', 'Bp. Asep Astahanas', '6-020', 'Pencairan Komisi BK2024020006', 300000, 0, 'OUT', 'DISBURSEMENT_COMMISSION', 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020006', NULL, NULL, NULL, NULL, '2024-02-22 09:04:36', '2024-02-22 09:04:36'),
(71, 'INV2024020011', '2024-02-22', 'Fave Hotel Pamanukan', '1-120', 'Sewa Elf Long Tgl 23-02-2024 Tujuan Trans tudio Bandung A/N Fave Hotel Pamanukan', 1700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020007', NULL, NULL, NULL, NULL, '2024-02-22 11:00:43', '2024-02-22 11:00:43'),
(72, 'INV2024020011', '2024-02-22', 'Fave Hotel Pamanukan', '4-100', 'Sewa Elf Long Tgl 23-02-2024 Tujuan Trans tudio Bandung A/N Fave Hotel Pamanukan', 0, 1700000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020007', NULL, NULL, NULL, NULL, '2024-02-22 11:00:43', '2024-02-22 11:00:43'),
(73, 'INV2024020012', '2024-02-24', 'Bp. Jafar', '1-120', 'DP Big Bus Setra Tgl 25-02-2024 Tujuan Jamblang Palimanan, Gunung Jati A/N Bp. Jafar', 1500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020008', NULL, NULL, NULL, NULL, '2024-02-24 06:12:19', '2024-02-24 06:12:19'),
(74, 'INV2024020012', '2024-02-24', 'Bp. Jafar', '1-130', 'DP Big Bus Setra Tgl 25-02-2024 Tujuan Jamblang Palimanan, Gunung Jati A/N Bp. Jafar', 0, 1500000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020008', NULL, NULL, NULL, NULL, '2024-02-24 06:12:19', '2024-02-24 06:12:19'),
(75, 'INV2024020014', '2024-02-24', 'Bp. Jafar', '1-130', 'Pelunasan Kode Booking BK2024020008', 6600000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020008', NULL, NULL, NULL, NULL, '2024-02-24 06:15:21', '2024-02-24 06:15:21'),
(76, 'INV2024020014', '2024-02-24', 'Bp. Jafar', '4-100', 'Pelunasan Kode Booking BK2024020008', 0, 6600000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020008', NULL, NULL, NULL, NULL, '2024-02-24 06:15:21', '2024-02-24 06:15:21'),
(77, 'INV2024020013', '2024-02-24', 'Bp. Jafar', '1-120', 'Pelunasan Big Bus Setra Tgl 25-02-2024 Tujuan Jamblang Palimanan, Gunung Jati A/N Bp. Jafar', 5100000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020008', NULL, NULL, NULL, NULL, '2024-02-24 06:15:21', '2024-02-24 06:15:21'),
(78, 'INV2024020013', '2024-02-24', 'Bp. Jafar', '1-130', 'Pelunasan Big Bus Setra Tgl 25-02-2024 Tujuan Jamblang Palimanan, Gunung Jati A/N Bp. Jafar', 0, 5100000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020008', NULL, NULL, NULL, NULL, '2024-02-24 06:15:21', '2024-02-24 06:15:21'),
(79, 'INV2024020015', '2024-02-27', 'SD Yusodarso', '1-120', 'DP Big Bus SHD Tgl 24-02-2024 Tujuan Cikole Lembang A/N SD Yusodarso', 2000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020009', NULL, NULL, NULL, NULL, '2024-02-27 05:32:02', '2024-02-27 05:32:02'),
(80, 'INV2024020015', '2024-02-27', 'SD Yusodarso', '1-130', 'DP Big Bus SHD Tgl 24-02-2024 Tujuan Cikole Lembang A/N SD Yusodarso', 0, 2000000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020009', NULL, NULL, NULL, NULL, '2024-02-27 05:32:02', '2024-02-27 05:32:02'),
(81, 'INV2024020017', '2024-02-23', 'SD Yusodarso', '1-130', 'Pelunasan Kode Booking BK2024020009', 4200000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020009', NULL, NULL, NULL, NULL, '2024-02-27 05:33:19', '2024-02-27 05:33:19'),
(82, 'INV2024020017', '2024-02-23', 'SD Yusodarso', '4-100', 'Pelunasan Kode Booking BK2024020009', 0, 4200000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020009', NULL, NULL, NULL, NULL, '2024-02-27 05:33:19', '2024-02-27 05:33:19'),
(83, 'INV2024020016', '2024-02-23', 'SD Yusodarso', '1-120', 'Pelunasan Big Bus SHD Tgl 24-02-2024 Tujuan Cikole Lembang A/N SD Yusodarso', 2200000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024020009', NULL, NULL, NULL, NULL, '2024-02-27 05:33:19', '2024-02-27 05:33:19'),
(84, 'INV2024020016', '2024-02-23', 'SD Yusodarso', '1-130', 'Pelunasan Big Bus SHD Tgl 24-02-2024 Tujuan Cikole Lembang A/N SD Yusodarso', 0, 2200000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024020009', NULL, NULL, NULL, NULL, '2024-02-27 05:33:19', '2024-02-27 05:33:19'),
(85, 'INV2024030001', '2024-03-04', 'Bp. Warsono', '1-120', 'DP Elf Long Tgl 11-04-2024 Tujuan sarimukti rt 12/03 sindangangin lakbok ciamis A/N Bp. Warsono', 1800000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024030001', NULL, NULL, NULL, NULL, '2024-03-04 06:22:47', '2024-03-04 06:22:47'),
(86, 'INV2024030001', '2024-03-04', 'Bp. Warsono', '1-130', 'DP Elf Long Tgl 11-04-2024 Tujuan sarimukti rt 12/03 sindangangin lakbok ciamis A/N Bp. Warsono', 0, 1800000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024030001', NULL, NULL, NULL, NULL, '2024-03-04 06:22:47', '2024-03-04 06:22:47'),
(87, 'INV2024030003', '2024-03-04', 'Bp. Warsono', '1-130', 'Pelunasan Kode Booking BK2024030001', 3800000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024030001', NULL, NULL, NULL, NULL, '2024-03-04 06:23:16', '2024-03-04 06:23:16'),
(88, 'INV2024030003', '2024-03-04', 'Bp. Warsono', '4-100', 'Pelunasan Kode Booking BK2024030001', 0, 3800000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024030001', NULL, NULL, NULL, NULL, '2024-03-04 06:23:16', '2024-03-04 06:23:16'),
(89, 'INV2024030002', '2024-03-04', 'Bp. Warsono', '1-120', 'Pelunasan Elf Long Tgl 11-04-2024 Tujuan sarimukti rt 12/03 sindangangin lakbok ciamis A/N Bp. Warsono', 2000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024030001', NULL, NULL, NULL, NULL, '2024-03-04 06:23:16', '2024-03-04 06:23:16'),
(90, 'INV2024030002', '2024-03-04', 'Bp. Warsono', '1-130', 'Pelunasan Elf Long Tgl 11-04-2024 Tujuan sarimukti rt 12/03 sindangangin lakbok ciamis A/N Bp. Warsono', 0, 2000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024030001', NULL, NULL, NULL, NULL, '2024-03-04 06:23:16', '2024-03-04 06:23:16'),
(91, 'INV2024030004', '2024-03-12', 'Gereja Katolik GKSP Subang Sie Pendidikan', '1-120', 'DP Big Bus SHD Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 1000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024030002', NULL, NULL, NULL, NULL, '2024-03-12 04:10:57', '2024-03-12 04:10:57'),
(92, 'INV2024030004', '2024-03-12', 'Gereja Katolik GKSP Subang Sie Pendidikan', '1-130', 'DP Big Bus SHD Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 0, 1000000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024030002', NULL, NULL, NULL, NULL, '2024-03-12 04:10:57', '2024-03-12 04:10:57'),
(93, 'INV2024030006', '2024-03-14', 'Gereja Katolik GKSP Subang Sie Pendidikan', '1-130', 'Pelunasan Kode Booking BK2024030002', 4200000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024030002', NULL, NULL, NULL, NULL, '2024-03-14 08:46:02', '2024-03-14 08:46:02'),
(94, 'INV2024030006', '2024-03-14', 'Gereja Katolik GKSP Subang Sie Pendidikan', '4-100', 'Pelunasan Kode Booking BK2024030002', 0, 4200000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024030002', NULL, NULL, NULL, NULL, '2024-03-14 08:46:02', '2024-03-14 08:46:02'),
(95, 'INV2024030005', '2024-03-14', 'Gereja Katolik GKSP Subang Sie Pendidikan', '1-120', 'Pelunasan Big Bus SHD Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 3200000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024030002', NULL, NULL, NULL, NULL, '2024-03-14 08:46:02', '2024-03-14 08:46:02'),
(96, 'INV2024030005', '2024-03-14', 'Gereja Katolik GKSP Subang Sie Pendidikan', '1-130', 'Pelunasan Big Bus SHD Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 0, 3200000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024030002', NULL, NULL, NULL, NULL, '2024-03-14 08:46:02', '2024-03-14 08:46:02'),
(97, 'INV2024030007', '2024-03-14', 'Gereja Katolik GKSP Subang Sie Pendidikan', '1-120', 'Sewa Medium Bus Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 2700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024030003', NULL, NULL, NULL, NULL, '2024-03-14 08:49:36', '2024-03-14 08:49:36'),
(98, 'INV2024030007', '2024-03-14', 'Gereja Katolik GKSP Subang Sie Pendidikan', '4-100', 'Sewa Medium Bus Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 0, 2700000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024030003', NULL, NULL, NULL, NULL, '2024-03-14 08:49:36', '2024-03-14 08:49:36'),
(99, 'INV2024040001', '2024-04-25', 'SD YOSUDARSO', '1-120', 'DP Medium Bus Tgl 27-04-2024 Tujuan Bandung A/N SD YOSUDARSO', 1000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024040001', NULL, NULL, NULL, NULL, '2024-04-25 07:38:14', '2024-04-25 07:38:14'),
(100, 'INV2024040001', '2024-04-25', 'SD YOSUDARSO', '1-130', 'DP Medium Bus Tgl 27-04-2024 Tujuan Bandung A/N SD YOSUDARSO', 0, 1000000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024040001', NULL, NULL, NULL, NULL, '2024-04-25 07:38:14', '2024-04-25 07:38:14'),
(101, 'INV2024040003', '2024-04-25', 'SD YOSUDARSO', '1-130', 'Pelunasan Kode Booking BK2024040001', 2700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024040001', NULL, NULL, NULL, NULL, '2024-04-25 07:38:41', '2024-04-25 07:38:41'),
(102, 'INV2024040003', '2024-04-25', 'SD YOSUDARSO', '4-100', 'Pelunasan Kode Booking BK2024040001', 0, 2700000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024040001', NULL, NULL, NULL, NULL, '2024-04-25 07:38:41', '2024-04-25 07:38:41'),
(103, 'INV2024040002', '2024-04-25', 'SD YOSUDARSO', '1-120', 'Pelunasan Medium Bus Tgl 27-04-2024 Tujuan Bandung A/N SD YOSUDARSO', 1700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024040001', NULL, NULL, NULL, NULL, '2024-04-25 07:38:41', '2024-04-25 07:38:41'),
(104, 'INV2024040002', '2024-04-25', 'SD YOSUDARSO', '1-130', 'Pelunasan Medium Bus Tgl 27-04-2024 Tujuan Bandung A/N SD YOSUDARSO', 0, 1700000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024040001', NULL, NULL, NULL, NULL, '2024-04-25 07:38:41', '2024-04-25 07:38:41'),
(105, 'INV2024040004', '2024-04-29', 'NCTZEN Subang', '1-120', 'DP Big Bus SetraHiace Tgl 18-05-2024 Tujuan GBK, Lotte Avenue Kuningan Jakarta A/N NCTZEN Subang', 400000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024040002', NULL, NULL, NULL, NULL, '2024-04-29 06:06:18', '2024-04-29 06:06:18'),
(106, 'INV2024040004', '2024-04-29', 'NCTZEN Subang', '1-130', 'DP Big Bus SetraHiace Tgl 18-05-2024 Tujuan GBK, Lotte Avenue Kuningan Jakarta A/N NCTZEN Subang', 0, 400000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024040002', NULL, NULL, NULL, NULL, '2024-04-29 06:06:18', '2024-04-29 06:06:18'),
(107, 'INV2024040006', '2024-04-29', 'NCTZEN Subang', '1-130', 'Pelunasan Kode Booking BK2024040002', 5300000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024040002', NULL, NULL, NULL, NULL, '2024-04-29 09:21:36', '2024-04-29 09:21:36'),
(108, 'INV2024040006', '2024-04-29', 'NCTZEN Subang', '4-100', 'Pelunasan Kode Booking BK2024040002', 0, 5300000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024040002', NULL, NULL, NULL, NULL, '2024-04-29 09:21:36', '2024-04-29 09:21:36'),
(109, 'INV2024040005', '2024-04-29', 'NCTZEN Subang', '1-120', 'Pelunasan Big Bus SetraHiace Tgl 18-05-2024 Tujuan GBK, Lotte Avenue Kuningan Jakarta A/N NCTZEN Subang', 4900000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024040002', NULL, NULL, NULL, NULL, '2024-04-29 09:21:36', '2024-04-29 09:21:36'),
(110, 'INV2024040005', '2024-04-29', 'NCTZEN Subang', '1-130', 'Pelunasan Big Bus SetraHiace Tgl 18-05-2024 Tujuan GBK, Lotte Avenue Kuningan Jakarta A/N NCTZEN Subang', 0, 4900000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024040002', NULL, NULL, NULL, NULL, '2024-04-29 09:21:36', '2024-04-29 09:21:36'),
(111, 'INV2024050001', '2024-05-01', 'Harum Tour', '1-120', 'DP Big Bus Setra Tgl 23-05-2024 Tujuan Lembah dewata lembang A/N Harum Tour', 1000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050001', NULL, NULL, NULL, NULL, '2024-05-01 03:32:31', '2024-05-01 03:32:31'),
(112, 'INV2024050001', '2024-05-01', 'Harum Tour', '1-130', 'DP Big Bus Setra Tgl 23-05-2024 Tujuan Lembah dewata lembang A/N Harum Tour', 0, 1000000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050001', NULL, NULL, NULL, NULL, '2024-05-01 03:32:31', '2024-05-01 03:32:31'),
(113, 'INV2024050002', '2024-05-01', 'Harum Tour', '1-120', 'DP Big Bus SHD Tgl 02-05-2024 Tujuan Bandara A/N Harum Tour', 500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050002', NULL, NULL, NULL, NULL, '2024-05-01 09:04:44', '2024-05-01 09:04:44'),
(114, 'INV2024050002', '2024-05-01', 'Harum Tour', '1-130', 'DP Big Bus SHD Tgl 02-05-2024 Tujuan Bandara A/N Harum Tour', 0, 500000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050002', NULL, NULL, NULL, NULL, '2024-05-01 09:04:44', '2024-05-01 09:04:44'),
(115, 'INV2024050004', '2024-05-01', 'Harum Tour', '1-130', 'Pelunasan Kode Booking BK2024050002', 4200000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050002', NULL, NULL, NULL, NULL, '2024-05-01 09:06:09', '2024-05-01 09:06:09'),
(116, 'INV2024050004', '2024-05-01', 'Harum Tour', '4-100', 'Pelunasan Kode Booking BK2024050002', 0, 4200000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050002', NULL, NULL, NULL, NULL, '2024-05-01 09:06:09', '2024-05-01 09:06:09'),
(117, 'INV2024050003', '2024-05-01', 'Harum Tour', '1-120', 'Pelunasan Big Bus SHD Tgl 02-05-2024 Tujuan Bandara A/N Harum Tour', 3700000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050002', NULL, NULL, NULL, NULL, '2024-05-01 09:06:09', '2024-05-01 09:06:09'),
(118, 'INV2024050003', '2024-05-01', 'Harum Tour', '1-130', 'Pelunasan Big Bus SHD Tgl 02-05-2024 Tujuan Bandara A/N Harum Tour', 0, 3700000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050002', NULL, NULL, NULL, NULL, '2024-05-01 09:06:09', '2024-05-01 09:06:09'),
(119, 'INV2024050005', '2024-05-03', 'SMAN 1 Ciasem', '1-110', 'DP Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 10000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 02:34:59', '2024-05-03 02:34:59'),
(120, 'INV2024050005', '2024-05-03', 'SMAN 1 Ciasem', '1-130', 'DP Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 0, 10000000, 'IN', NULL, '', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 02:34:59', '2024-05-03 02:34:59'),
(121, 'INV2024050006', '2024-04-23', 'SMAN 1 Ciasem', '1-120', 'DP Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 20000000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 03:11:20', '2024-05-03 03:11:20'),
(122, 'INV2024050006', '2024-04-23', 'SMAN 1 Ciasem', '1-130', 'DP Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 0, 20000000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 03:11:20', '2024-05-03 03:11:20'),
(123, 'INV2024050008', '2024-05-03', 'SMAN 1 Ciasem', '1-130', 'Pelunasan Kode Booking BK2024050003', 67500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 07:37:59', '2024-05-03 07:37:59'),
(124, 'INV2024050008', '2024-05-03', 'SMAN 1 Ciasem', '4-100', 'Pelunasan Kode Booking BK2024050003', 0, 67500000, 'IN', NULL, 'LABARUGI', 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 07:37:59', '2024-05-03 07:37:59'),
(125, 'INV2024050007', '2024-05-03', 'SMAN 1 Ciasem', '1-110', 'Pelunasan Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 37500000, 0, 'IN', NULL, NULL, 'N', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 07:37:59', '2024-05-03 07:37:59'),
(126, 'INV2024050007', '2024-05-03', 'SMAN 1 Ciasem', '1-130', 'Pelunasan Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 0, 37500000, 'IN', NULL, NULL, 'Y', 'N', 'Yusup', 'OK', 'OK', 'BK2024050003', NULL, NULL, NULL, NULL, '2024-05-03 07:37:59', '2024-05-03 07:37:59');

--
-- Trigger `trvl_trnsct_jurnal`
--
DELIMITER $$
CREATE TRIGGER `cancel_jurnal` BEFORE UPDATE ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
IF NEW.status = 'BATAL' THEN
UPDATE trvl_ref_finance_account
SET credit = credit - old.credit, 
	debit = debit - old.debit
WHERE account_id = old.account_id;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_akun_laba_bersih` BEFORE INSERT ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
IF NEW.post_net_profit = 'LABARUGI' THEN
UPDATE trvl_ref_finance_account
SET credit = credit + NEW.credit, 
	debit = debit + NEW.debit
WHERE account_id = '3-200';
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_akun_laba_bersih_akibat_pembatalan_penapatan_sewa` AFTER UPDATE ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
IF NEW.status_booking = 'BATAL' THEN
UPDATE trvl_ref_finance_account
SET credit = credit - OLD.credit
WHERE account_id = '3-200';
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_saldo_neraca_insert` BEFORE INSERT ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
UPDATE trvl_ref_finance_account
SET debit = debit + NEW.debit, credit = credit + NEW.credit
WHERE account_id = NEW.account_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_saldo_neraca_update` BEFORE UPDATE ON `trvl_trnsct_jurnal` FOR EACH ROW BEGIN
UPDATE trvl_ref_finance_account
SET debit = (debit - OLD.debit) + NEW.debit, 
credit = (credit - OLD.credit) + NEW.credit
WHERE account_id = NEW.account_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_payment`
--

CREATE TABLE `trvl_trnsct_payment` (
  `payment_id` varchar(15) NOT NULL,
  `booking_id` varchar(20) DEFAULT NULL,
  `payment_date` varchar(35) NOT NULL,
  `received_from` varchar(45) NOT NULL,
  `amount` int(25) NOT NULL,
  `payment_method` varchar(15) NOT NULL,
  `information` text NOT NULL,
  `is_canceled` int(1) NOT NULL DEFAULT 0,
  `admin` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_trnsct_payment`
--

INSERT INTO `trvl_trnsct_payment` (`payment_id`, `booking_id`, `payment_date`, `received_from`, `amount`, `payment_method`, `information`, `is_canceled`, `admin`, `created_at`, `updated_at`) VALUES
('INV2023090001', 'BK2023090001', '2023-09-2315:36:22', 'PT Meiloon Technology Indonesia', 3700000, '1-120', 'Sewa Big Bus Setra Tgl 24-09-2023 Tujuan Cikarang A/N PT Meiloon Technology Indonesia', 0, 'Yusup', '2023-09-23 08:36:22', '2023-09-23 08:36:22'),
('INV2023090002', 'BK2023090002', '2023-09-0920:06:48', 'BEM STIE Miftahul Huda', 400000, '1-120', 'DP Big Bus SHD Tgl 05-10-2023 Tujuan Bursa Efek Indonesia, Musium Bank Indonesia, Ancol A/N BEM STIE Miftahul Huda', 0, 'Yusup', '2023-09-30 13:06:48', '2023-09-30 13:06:48'),
('INV2023090003', 'BK2023090002', '2023-09-30', 'BEM STIE Miftahul Huda', 3900000, '1-120', 'Pelunasan Big Bus SHD Tgl 05-10-2023 Tujuan Bursa Efek Indonesia, Musium Bank Indonesia, Ancol A/N BEM STIE Miftahul Huda', 0, 'Yusup', '2023-09-30 13:09:38', '2023-09-30 13:09:38'),
('INV2023100001', 'BK2023100001', '2023-10-0717:34:32', 'Bp. Syafiq', 500000, '1-120', 'DP Medium Bus Tgl 21-10-2023 Tujuan Vila Istana Bunga Lembang A/N Bp. Syafiq', 0, 'Yusup', '2023-10-07 10:34:32', '2023-10-07 10:34:32'),
('INV2023100002', 'BK2023100002', '2023-08-0509:03:34', 'Fahrul', 1000000, '1-110', 'DP Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 0, 'Yusup', '2023-10-14 02:03:34', '2023-10-14 02:03:34'),
('INV2023100003', 'BK2023100002', '2023-10-02', 'Fahrul', 5000000, '1-110', 'DP Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 0, 'Yusup', '2023-10-14 02:04:18', '2023-10-14 02:04:18'),
('INV2023100004', 'BK2023100002', '2023-10-14', 'Fahrul', 1900000, '1-110', 'Pelunasan Big Bus SHDBig Bus Setra Tgl 22-10-2023 Tujuan Ds. Ciranggen Kec. Jatigede, al kamil A/N Fahrul', 0, 'Yusup', '2023-10-14 02:04:44', '2023-10-14 02:04:44'),
('INV2023100006', 'BK2023100001', '2023-10-20', 'Bp. Syafiq', 4000000, '1-110', 'Pelunasan Medium Bus Tgl 21-10-2023 Tujuan Vila Istana Bunga Lembang A/N Bp. Syafiq', 0, 'Yusup', '2023-10-20 08:07:31', '2023-10-20 08:07:31'),
('INV2023110001', 'BK2023110001', '2023-11-1017:22:48', 'Ibu Yeni Nuraeni', 5500000, '1-120', 'DP Big Bus Setra Tgl 22-12-2023 Tujuan Dieng, Jogjakarta A/N Ibu Yeni Nuraeni', 0, 'Yusup', '2023-11-10 10:22:48', '2023-11-10 10:22:48'),
('INV2023110002', 'BK2023110001', '2023-11-16', 'Ibu Yeni Nuraeni', 2000000, '1-120', 'DP Big Bus Setra Tgl 22-12-2023 Tujuan Dieng, Jogjakarta A/N Ibu Yeni Nuraeni', 0, 'Yusup', '2023-11-16 05:03:45', '2023-11-16 05:03:45'),
('INV2023120001', 'BK2023120001', '2023-12-26', 'Bp. Wirta', 0, '1-110', 'DP Big Bus Setra Tgl 27-12-2023 Tujuan water park parahiyangan, al jabar bandung A/N Bp. Wirta', 0, 'Yusup', '2023-12-26 11:01:15', '2023-12-26 11:01:15'),
('INV2023120002', 'BK2023120001', '2023-12-26', 'Bp. Wirta', 4000000, '1-110', 'Pelunasan Big Bus Setra Tgl 27-12-2023 Tujuan water park parahiyangan, al jabar bandung A/N Bp. Wirta', 0, 'Yusup', '2023-12-26 11:02:16', '2023-12-26 11:02:16'),
('INV2024010001', 'BK2024010001', '2023-10-1309:50:11', 'Bp. H. Yaya Ruhmaya', 2000000, '1-110', 'DP Big Bus SHD Tgl 13-01-2024 Tujuan RM. Sukahati Cipacing Sumedang - Janspark - Al Jabar A/N Bp. H. Yaya Ruhmaya', 0, 'Yusup', '2024-01-04 02:50:11', '2024-01-04 02:50:11'),
('INV2024010002', 'BK2024010001', '2024-01-04', 'Bp. H. Yaya Ruhmaya', 6000000, '1-110', 'Pelunasan Big Bus SHD Tgl 13-01-2024 Tujuan RM. Sukahati Cipacing Sumedang - Janspark - Al Jabar A/N Bp. H. Yaya Ruhmaya', 0, 'Yusup', '2024-01-04 02:50:36', '2024-01-04 02:50:36'),
('INV2024010004', 'BK2024010002', '2024-01-1818:35:11', 'Sekolah Bunda Maria', 200000, '1-120', 'DP Hiace Tgl 24-01-2024 Tujuan Subang Kota A/N Sekolah Bunda Maria', 0, 'Yusup', '2024-01-22 11:35:11', '2024-01-22 11:35:11'),
('INV2024010005', 'BK2024010002', '2024-01-23', 'Sekolah Bunda Maria', 800000, '1-120', 'Pelunasan Hiace Tgl 24-01-2024 Tujuan Subang Kota A/N Sekolah Bunda Maria', 0, 'Yusup', '2024-01-23 05:49:51', '2024-01-23 05:49:51'),
('INV2024010007', 'BK2024010003', '2024-01-2819:43:44', 'Ahmad Arbah', 2500000, '1-120', 'Sewa Hiace Tgl 29-01-2024 Tujuan Cikole Lembang A/N Ahmad Arbah', 0, 'Yusup', '2024-01-30 12:43:44', '2024-01-30 12:43:44'),
('INV2024020001', 'BK2024020002', '2024-02-0117:51:51', 'Fave Hotel Pamanukan', 1700000, '1-120', 'Sewa Elf Long Tgl 02-02-2024 Tujuan Trans tudio Bandung A/N Fave Hotel Pamanukan', 0, 'Yusup', '2024-02-01 10:51:51', '2024-02-01 10:51:51'),
('INV2024020002', 'BK2024020003', '2024-01-3117:57:01', 'Bp. Sugandi', 4700000, '1-120', 'Sewa Medium Bus Tgl 03-02-2024 Tujuan Drajat pas garut A/N Bp. Sugandi', 0, 'Yusup', '2024-02-01 10:57:01', '2024-02-01 10:57:01'),
('INV2024020003', 'BK2024020004', '2024-01-2010:02:53', 'Bp. Uhe', 5250000, '1-120', 'DP Big Bus Setra Tgl 18-02-2024 Tujuan gunung jati, kraton, Cibulan kuningan A/N Bp. Uhe', 0, 'Yusup', '2024-02-13 03:02:53', '2024-02-13 03:02:53'),
('INV2024020004', 'BK2024020004', '2024-02-12', 'Bp. Uhe', 6000000, '1-120', 'Pelunasan Big Bus Setra Tgl 18-02-2024 Tujuan gunung jati, kraton, Cibulan kuningan A/N Bp. Uhe', 0, 'Yusup', '2024-02-13 03:06:02', '2024-02-13 03:06:02'),
('INV2024020006', 'BK2024020005', '2024-01-1518:14:38', 'AKBAR', 500000, '1-110', 'DP Big Bus SHD Tgl 18-02-2024 Tujuan Tropicana Cirebon A/N AKBAR', 0, 'Yusup', '2024-02-17 11:14:38', '2024-02-17 11:14:38'),
('INV2024020007', 'BK2024020005', '2024-02-17', 'AKBAR', 3800000, '1-120', 'Pelunasan Big Bus SHD Tgl 18-02-2024 Tujuan Tropicana Cirebon A/N AKBAR', 0, 'Yusup', '2024-02-17 11:15:41', '2024-02-17 11:15:41'),
('INV2024020009', 'BK2024020006', '2024-02-2015:27:15', 'Astahanas', 12000000, '1-120', 'Sewa Big Bus Setra Tgl 21-02-2024 Tujuan UNHAN sentul bogor A/N Astahanas', 0, 'Yusup', '2024-02-22 08:27:15', '2024-02-22 08:27:15'),
('INV2024020011', 'BK2024020007', '2024-02-2218:00:43', 'Fave Hotel Pamanukan', 1700000, '1-120', 'Sewa Elf Long Tgl 23-02-2024 Tujuan Trans tudio Bandung A/N Fave Hotel Pamanukan', 0, 'Yusup', '2024-02-22 11:00:43', '2024-02-22 11:00:43'),
('INV2024020012', 'BK2024020008', '2024-02-1713:12:19', 'Bp. Jafar', 1500000, '1-120', 'DP Big Bus Setra Tgl 25-02-2024 Tujuan Jamblang Palimanan, Gunung Jati A/N Bp. Jafar', 0, 'Yusup', '2024-02-24 06:12:19', '2024-02-24 06:12:19'),
('INV2024020013', 'BK2024020008', '2024-02-24', 'Bp. Jafar', 5100000, '1-120', 'Pelunasan Big Bus Setra Tgl 25-02-2024 Tujuan Jamblang Palimanan, Gunung Jati A/N Bp. Jafar', 0, 'Yusup', '2024-02-24 06:15:21', '2024-02-24 06:15:21'),
('INV2024020015', 'BK2024020009', '2024-02-2212:32:02', 'SD Yusodarso', 2000000, '1-120', 'DP Big Bus SHD Tgl 24-02-2024 Tujuan Cikole Lembang A/N SD Yusodarso', 0, 'Yusup', '2024-02-27 05:32:02', '2024-02-27 05:32:02'),
('INV2024020016', 'BK2024020009', '2024-02-23', 'SD Yusodarso', 2200000, '1-120', 'Pelunasan Big Bus SHD Tgl 24-02-2024 Tujuan Cikole Lembang A/N SD Yusodarso', 0, 'Yusup', '2024-02-27 05:33:19', '2024-02-27 05:33:19'),
('INV2024030001', 'BK2024030001', '2024-01-2313:22:47', 'Bp. Warsono', 1800000, '1-120', 'DP Elf Long Tgl 11-04-2024 Tujuan sarimukti rt 12/03 sindangangin lakbok ciamis A/N Bp. Warsono', 0, 'Yusup', '2024-03-04 06:22:47', '2024-03-04 06:22:47'),
('INV2024030002', 'BK2024030001', '2024-03-04', 'Bp. Warsono', 2000000, '1-120', 'Pelunasan Elf Long Tgl 11-04-2024 Tujuan sarimukti rt 12/03 sindangangin lakbok ciamis A/N Bp. Warsono', 0, 'Yusup', '2024-03-04 06:23:16', '2024-03-04 06:23:16'),
('INV2024030004', 'BK2024030002', '2024-03-1211:10:57', 'Gereja Katolik GKSP Subang Sie Pendidikan', 1000000, '1-120', 'DP Big Bus SHD Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 0, 'Yusup', '2024-03-12 04:10:57', '2024-03-12 04:10:57'),
('INV2024030005', 'BK2024030002', '2024-03-14', 'Gereja Katolik GKSP Subang Sie Pendidikan', 3200000, '1-120', 'Pelunasan Big Bus SHD Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 0, 'Yusup', '2024-03-14 08:46:02', '2024-03-14 08:46:02'),
('INV2024030007', 'BK2024030003', '2024-03-1415:49:36', 'Gereja Katolik GKSP Subang Sie Pendidikan', 2700000, '1-120', 'Sewa Medium Bus Tgl 16-03-2024 Tujuan Cikarang A/N Gereja Katolik GKSP Subang Sie Pendidikan', 0, 'Yusup', '2024-03-14 08:49:36', '2024-03-14 08:49:36'),
('INV2024040001', 'BK2024040001', '2024-04-2214:38:14', 'SD YOSUDARSO', 1000000, '1-120', 'DP Medium Bus Tgl 27-04-2024 Tujuan Bandung A/N SD YOSUDARSO', 0, 'Yusup', '2024-04-25 07:38:14', '2024-04-25 07:38:14'),
('INV2024040002', 'BK2024040001', '2024-04-25', 'SD YOSUDARSO', 1700000, '1-120', 'Pelunasan Medium Bus Tgl 27-04-2024 Tujuan Bandung A/N SD YOSUDARSO', 0, 'Yusup', '2024-04-25 07:38:41', '2024-04-25 07:38:41'),
('INV2024040004', 'BK2024040002', '2024-03-1113:06:18', 'NCTZEN Subang', 400000, '1-120', 'DP Big Bus SetraHiace Tgl 18-05-2024 Tujuan GBK, Lotte Avenue Kuningan Jakarta A/N NCTZEN Subang', 0, 'Yusup', '2024-04-29 06:06:18', '2024-04-29 06:06:18'),
('INV2024040005', 'BK2024040002', '2024-04-29', 'NCTZEN Subang', 4900000, '1-120', 'Pelunasan Big Bus SetraHiace Tgl 18-05-2024 Tujuan GBK, Lotte Avenue Kuningan Jakarta A/N NCTZEN Subang', 0, 'Yusup', '2024-04-29 09:21:36', '2024-04-29 09:21:36'),
('INV2024050001', 'BK2024050001', '2024-04-0410:32:31', 'Harum Tour', 1000000, '1-120', 'DP Big Bus Setra Tgl 23-05-2024 Tujuan Lembah dewata lembang A/N Harum Tour', 0, 'Yusup', '2024-05-01 03:32:31', '2024-05-01 03:32:31'),
('INV2024050002', 'BK2024050002', '2024-04-2216:04:44', 'Harum Tour', 500000, '1-120', 'DP Big Bus SHD Tgl 02-05-2024 Tujuan Bandara A/N Harum Tour', 0, 'Yusup', '2024-05-01 09:04:44', '2024-05-01 09:04:44'),
('INV2024050003', 'BK2024050002', '2024-05-01', 'Harum Tour', 3700000, '1-120', 'Pelunasan Big Bus SHD Tgl 02-05-2024 Tujuan Bandara A/N Harum Tour', 0, 'Yusup', '2024-05-01 09:06:09', '2024-05-01 09:06:09'),
('INV2024050005', 'BK2024050003', '2024-03-1809:34:59', 'SMAN 1 Ciasem', 10000000, '1-110', 'DP Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 0, 'Yusup', '2024-05-03 02:34:59', '2024-05-03 02:34:59'),
('INV2024050006', 'BK2024050003', '2024-04-23', 'SMAN 1 Ciasem', 20000000, '1-120', 'DP Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 0, 'Yusup', '2024-05-03 03:11:20', '2024-05-03 03:11:20'),
('INV2024050007', 'BK2024050003', '2024-05-03', 'SMAN 1 Ciasem', 37500000, '1-110', 'Pelunasan Big Bus SHD Tgl 06-05-2024 Tujuan Dieng, Kampus Universitas Negri Yogjakarta, Parangtritis A/N SMAN 1 Ciasem', 0, 'Yusup', '2024-05-03 07:37:59', '2024-05-03 07:37:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_payrol`
--

CREATE TABLE `trvl_trnsct_payrol` (
  `id` bigint(20) NOT NULL,
  `sdm_type` enum('SDM','DRIVER','HELPER') NOT NULL DEFAULT 'SDM',
  `sdm_id` bigint(20) NOT NULL,
  `year` int(4) NOT NULL,
  `month` varchar(2) NOT NULL,
  `upah_bulanan` int(25) DEFAULT 0,
  `upah_lainlain` int(25) DEFAULT 0,
  `potongan_kasbon` int(25) DEFAULT 0,
  `potongan_lainlain` int(25) DEFAULT 0,
  `total` int(25) NOT NULL DEFAULT 0,
  `total_paid` int(25) DEFAULT 0,
  `status` enum('SUCCESS','CANCEL') DEFAULT 'SUCCESS',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_payrol_payment`
--

CREATE TABLE `trvl_trnsct_payrol_payment` (
  `id` int(11) NOT NULL,
  `payrol_id` bigint(20) NOT NULL,
  `account_id` varchar(25) NOT NULL,
  `amount` int(25) NOT NULL DEFAULT 0,
  `admin` varchar(60) NOT NULL,
  `payment_date` varchar(30) NOT NULL,
  `status` enum('SUCCESS','CANCEL') NOT NULL DEFAULT 'SUCCESS',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_spj`
--

CREATE TABLE `trvl_trnsct_spj` (
  `id` bigint(20) NOT NULL,
  `spj_id` varchar(25) NOT NULL,
  `booking_id` varchar(25) NOT NULL,
  `police_num` varchar(10) NOT NULL,
  `category_id` varchar(10) NOT NULL,
  `main_driver` varchar(10) NOT NULL,
  `second_driver` varchar(10) DEFAULT NULL,
  `helper_id` varchar(10) DEFAULT NULL,
  `premi_main_driver` int(15) DEFAULT NULL,
  `premi_second_driver` int(15) DEFAULT NULL,
  `premi_helper` int(15) DEFAULT NULL,
  `budget` int(20) NOT NULL DEFAULT 0,
  `km_start` varchar(15) DEFAULT NULL,
  `km_end` varchar(15) DEFAULT NULL,
  `status` varchar(30) DEFAULT 'MENUNGGU',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_trnsct_spj`
--

INSERT INTO `trvl_trnsct_spj` (`id`, `spj_id`, `booking_id`, `police_num`, `category_id`, `main_driver`, `second_driver`, `helper_id`, `premi_main_driver`, `premi_second_driver`, `premi_helper`, `budget`, `km_start`, `km_end`, `status`, `created_at`, `updated_at`) VALUES
(1, '0001/SPJ/OPR/PT.KME/10/23', 'BK2023100001', 'T 7590 TC', '3', '26', NULL, '2', 240000, 0, 180000, 1800000, NULL, NULL, 'MENUNGGU', '2023-10-20 08:27:59', '2023-10-20 08:27:59');

--
-- Trigger `trvl_trnsct_spj`
--
DELIMITER $$
CREATE TRIGGER `cancel_premi_helper` BEFORE UPDATE ON `trvl_trnsct_spj` FOR EACH ROW BEGIN
IF NEW.status = 'BATAL' THEN
UPDATE trvl_ref_helper
SET ttl_trnsct = ttl_trnsct - 1, 
	ttl_saving = ttl_saving - old.premi_helper
WHERE driver_id = old.helper_id;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `cancel_premi_main_driver` BEFORE UPDATE ON `trvl_trnsct_spj` FOR EACH ROW BEGIN
IF NEW.status = 'BATAL' THEN
UPDATE trvl_ref_driver
SET ttl_trnsct = ttl_trnsct - 1, 
	ttl_saving = ttl_saving - old.premi_main_driver
WHERE driver_id = old.main_driver;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `cancel_premi_second_driver` BEFORE UPDATE ON `trvl_trnsct_spj` FOR EACH ROW BEGIN
IF NEW.status = 'BATAL' THEN
UPDATE trvl_ref_driver
SET ttl_trnsct = ttl_trnsct - 1, 
	ttl_saving = ttl_saving - old.premi_second_driver
WHERE driver_id = old.second_driver;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_qty_unit_spj` AFTER INSERT ON `trvl_trnsct_spj` FOR EACH ROW BEGIN
UPDATE trvl_trnsct_booking_dtl set unit_qty_spj = unit_qty_spj + 1
where category_id = NEW.category_id and booking_id = NEW.booking_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trvl_trnsct_spj_budget`
--

CREATE TABLE `trvl_trnsct_spj_budget` (
  `spjbudget_id` bigint(20) NOT NULL,
  `spj_id` varchar(25) NOT NULL,
  `account_id` int(10) NOT NULL,
  `qty` int(10) DEFAULT NULL,
  `duration` varchar(20) DEFAULT NULL,
  `amount` int(20) DEFAULT NULL,
  `ttl_amount` int(20) DEFAULT NULL,
  `information` text DEFAULT NULL,
  `is_saving` enum('Y','N') DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data untuk tabel `trvl_trnsct_spj_budget`
--

INSERT INTO `trvl_trnsct_spj_budget` (`spjbudget_id`, `spj_id`, `account_id`, `qty`, `duration`, `amount`, `ttl_amount`, `information`, `is_saving`, `created_at`, `updated_at`) VALUES
(1, '0001/SPJ/OPR/PT.KME/10/23', 1, 1, '2', 120000, 240000, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(2, '0001/SPJ/OPR/PT.KME/10/23', 2, 1, '2', 90000, 180000, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(3, '0001/SPJ/OPR/PT.KME/10/23', 3, 400, '0', 0, 0, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(4, '0001/SPJ/OPR/PT.KME/10/23', 4, 100, '1', 6800, 680000, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(5, '0001/SPJ/OPR/PT.KME/10/23', 5, 0, '0', 0, 0, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(6, '0001/SPJ/OPR/PT.KME/10/23', 6, 0, '0', 0, 0, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(7, '0001/SPJ/OPR/PT.KME/10/23', 7, 1, '1', 700000, 700000, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59'),
(8, '0001/SPJ/OPR/PT.KME/10/23', 8, 0, '0', 0, 0, NULL, 'N', '2023-10-20 08:27:59', '2023-10-20 08:27:59');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `rice_ref_car`
--
ALTER TABLE `rice_ref_car`
  ADD PRIMARY KEY (`car_id`);

--
-- Indeks untuk tabel `rice_ref_delivery_status`
--
ALTER TABLE `rice_ref_delivery_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indeks untuk tabel `rice_ref_driver`
--
ALTER TABLE `rice_ref_driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indeks untuk tabel `rice_ref_helper`
--
ALTER TABLE `rice_ref_helper`
  ADD PRIMARY KEY (`helper_id`);

--
-- Indeks untuk tabel `rice_ref_process`
--
ALTER TABLE `rice_ref_process`
  ADD PRIMARY KEY (`process_id`);

--
-- Indeks untuk tabel `rice_ref_product`
--
ALTER TABLE `rice_ref_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indeks untuk tabel `rice_ref_product_stock`
--
ALTER TABLE `rice_ref_product_stock`
  ADD PRIMARY KEY (`stok_id`);

--
-- Indeks untuk tabel `rice_ref_warehouse`
--
ALTER TABLE `rice_ref_warehouse`
  ADD PRIMARY KEY (`warehouse_id`);

--
-- Indeks untuk tabel `rice_trnsct_delivery`
--
ALTER TABLE `rice_trnsct_delivery`
  ADD PRIMARY KEY (`delivery_id`);

--
-- Indeks untuk tabel `rice_trnsct_delivery_item`
--
ALTER TABLE `rice_trnsct_delivery_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_id` (`delivery_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indeks untuk tabel `rice_trnsct_processing`
--
ALTER TABLE `rice_trnsct_processing`
  ADD PRIMARY KEY (`process_id`);

--
-- Indeks untuk tabel `rice_trnsct_processing_item`
--
ALTER TABLE `rice_trnsct_processing_item`
  ADD PRIMARY KEY (`procitem_id`);

--
-- Indeks untuk tabel `rice_trnsct_purchase`
--
ALTER TABLE `rice_trnsct_purchase`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `warehouse_id` (`warehouse_id`),
  ADD KEY `purchase_date` (`purchase_date`);

--
-- Indeks untuk tabel `rice_trnsct_purchase_item`
--
ALTER TABLE `rice_trnsct_purchase_item`
  ADD PRIMARY KEY (`purcitem_id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indeks untuk tabel `sys_ref_acl`
--
ALTER TABLE `sys_ref_acl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aktif` (`aktif`);

--
-- Indeks untuk tabel `sys_ref_group_menu`
--
ALTER TABLE `sys_ref_group_menu`
  ADD PRIMARY KEY (`id_group_menu`);

--
-- Indeks untuk tabel `sys_ref_module`
--
ALTER TABLE `sys_ref_module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_module` (`id_module`);

--
-- Indeks untuk tabel `sys_ref_role`
--
ALTER TABLE `sys_ref_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_role` (`id_role`);

--
-- Indeks untuk tabel `sys_ref_salary`
--
ALTER TABLE `sys_ref_salary`
  ADD PRIMARY KEY (`salary_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `month` (`month`),
  ADD KEY `nip` (`nip`);

--
-- Indeks untuk tabel `sys_ref_sdm`
--
ALTER TABLE `sys_ref_sdm`
  ADD PRIMARY KEY (`sdm_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `name` (`name`);

--
-- Indeks untuk tabel `sys_ref_sdm_position`
--
ALTER TABLE `sys_ref_sdm_position`
  ADD PRIMARY KEY (`position_id`),
  ADD KEY `module_id` (`module_id`);

--
-- Indeks untuk tabel `sys_ref_user`
--
ALTER TABLE `sys_ref_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `trvl_ref_agent`
--
ALTER TABLE `trvl_ref_agent`
  ADD PRIMARY KEY (`agent_id`);

--
-- Indeks untuk tabel `trvl_ref_booking_id`
--
ALTER TABLE `trvl_ref_booking_id`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trvl_ref_car`
--
ALTER TABLE `trvl_ref_car`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trvl_ref_car_category`
--
ALTER TABLE `trvl_ref_car_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trvl_ref_car_service`
--
ALTER TABLE `trvl_ref_car_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `police_num` (`police_num`);

--
-- Indeks untuk tabel `trvl_ref_customer`
--
ALTER TABLE `trvl_ref_customer`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indeks untuk tabel `trvl_ref_driver`
--
ALTER TABLE `trvl_ref_driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indeks untuk tabel `trvl_ref_driver_saving`
--
ALTER TABLE `trvl_ref_driver_saving`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trvl_ref_finance_account`
--
ALTER TABLE `trvl_ref_finance_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_id` (`account_id`);

--
-- Indeks untuk tabel `trvl_ref_finance_account_group`
--
ALTER TABLE `trvl_ref_finance_account_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indeks untuk tabel `trvl_ref_helper`
--
ALTER TABLE `trvl_ref_helper`
  ADD PRIMARY KEY (`helper_id`);

--
-- Indeks untuk tabel `trvl_ref_payment_id`
--
ALTER TABLE `trvl_ref_payment_id`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trvl_ref_payment_method`
--
ALTER TABLE `trvl_ref_payment_method`
  ADD PRIMARY KEY (`payment_method_id`);

--
-- Indeks untuk tabel `trvl_ref_spj_budget_account`
--
ALTER TABLE `trvl_ref_spj_budget_account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indeks untuk tabel `trvl_ref_spj_id`
--
ALTER TABLE `trvl_ref_spj_id`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trvl_trnsct_booking`
--
ALTER TABLE `trvl_trnsct_booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD UNIQUE KEY `trnsct_date` (`trnsct_date`),
  ADD KEY `booking_start_date` (`booking_start_date`),
  ADD KEY `booking_end_date` (`booking_end_date`),
  ADD KEY `date_not_for_sale` (`date_not_for_sale`);

--
-- Indeks untuk tabel `trvl_trnsct_booking_dtl`
--
ALTER TABLE `trvl_trnsct_booking_dtl`
  ADD PRIMARY KEY (`trnsctdtl_id`),
  ADD KEY `booking_end_date` (`booking_end_date`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `booking_start_date` (`booking_start_date`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `date_not_for_sale` (`date_not_for_sale`);

--
-- Indeks untuk tabel `trvl_trnsct_debt`
--
ALTER TABLE `trvl_trnsct_debt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sdm_id` (`sdm_id`);

--
-- Indeks untuk tabel `trvl_trnsct_debt_payment`
--
ALTER TABLE `trvl_trnsct_debt_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `debt_id` (`debt_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indeks untuk tabel `trvl_trnsct_jurnal`
--
ALTER TABLE `trvl_trnsct_jurnal`
  ADD PRIMARY KEY (`jurnal_id`);

--
-- Indeks untuk tabel `trvl_trnsct_payment`
--
ALTER TABLE `trvl_trnsct_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `booking_id` (`booking_id`);

--
-- Indeks untuk tabel `trvl_trnsct_payrol`
--
ALTER TABLE `trvl_trnsct_payrol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`);

--
-- Indeks untuk tabel `trvl_trnsct_payrol_payment`
--
ALTER TABLE `trvl_trnsct_payrol_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wages_id` (`payrol_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indeks untuk tabel `trvl_trnsct_spj`
--
ALTER TABLE `trvl_trnsct_spj`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `police_num` (`police_num`),
  ADD KEY `main_driver` (`main_driver`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `spj_id` (`spj_id`);

--
-- Indeks untuk tabel `trvl_trnsct_spj_budget`
--
ALTER TABLE `trvl_trnsct_spj_budget`
  ADD PRIMARY KEY (`spjbudget_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_car`
--
ALTER TABLE `rice_ref_car`
  MODIFY `car_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_delivery_status`
--
ALTER TABLE `rice_ref_delivery_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_driver`
--
ALTER TABLE `rice_ref_driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_helper`
--
ALTER TABLE `rice_ref_helper`
  MODIFY `helper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_process`
--
ALTER TABLE `rice_ref_process`
  MODIFY `process_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_product`
--
ALTER TABLE `rice_ref_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_product_stock`
--
ALTER TABLE `rice_ref_product_stock`
  MODIFY `stok_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rice_ref_warehouse`
--
ALTER TABLE `rice_ref_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `rice_trnsct_delivery`
--
ALTER TABLE `rice_trnsct_delivery`
  MODIFY `delivery_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rice_trnsct_delivery_item`
--
ALTER TABLE `rice_trnsct_delivery_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rice_trnsct_processing`
--
ALTER TABLE `rice_trnsct_processing`
  MODIFY `process_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `rice_trnsct_processing_item`
--
ALTER TABLE `rice_trnsct_processing_item`
  MODIFY `procitem_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rice_trnsct_purchase`
--
ALTER TABLE `rice_trnsct_purchase`
  MODIFY `purchase_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rice_trnsct_purchase_item`
--
ALTER TABLE `rice_trnsct_purchase_item`
  MODIFY `purcitem_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `sys_ref_acl`
--
ALTER TABLE `sys_ref_acl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT untuk tabel `sys_ref_group_menu`
--
ALTER TABLE `sys_ref_group_menu`
  MODIFY `id_group_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `sys_ref_module`
--
ALTER TABLE `sys_ref_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `sys_ref_role`
--
ALTER TABLE `sys_ref_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `sys_ref_salary`
--
ALTER TABLE `sys_ref_salary`
  MODIFY `salary_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sys_ref_sdm`
--
ALTER TABLE `sys_ref_sdm`
  MODIFY `sdm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `sys_ref_user`
--
ALTER TABLE `sys_ref_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_agent`
--
ALTER TABLE `trvl_ref_agent`
  MODIFY `agent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_booking_id`
--
ALTER TABLE `trvl_ref_booking_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_car`
--
ALTER TABLE `trvl_ref_car`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_car_category`
--
ALTER TABLE `trvl_ref_car_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_car_service`
--
ALTER TABLE `trvl_ref_car_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_customer`
--
ALTER TABLE `trvl_ref_customer`
  MODIFY `cust_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_driver`
--
ALTER TABLE `trvl_ref_driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_driver_saving`
--
ALTER TABLE `trvl_ref_driver_saving`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_finance_account`
--
ALTER TABLE `trvl_ref_finance_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_helper`
--
ALTER TABLE `trvl_ref_helper`
  MODIFY `helper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_payment_id`
--
ALTER TABLE `trvl_ref_payment_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_payment_method`
--
ALTER TABLE `trvl_ref_payment_method`
  MODIFY `payment_method_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_spj_budget_account`
--
ALTER TABLE `trvl_ref_spj_budget_account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `trvl_ref_spj_id`
--
ALTER TABLE `trvl_ref_spj_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_booking_dtl`
--
ALTER TABLE `trvl_trnsct_booking_dtl`
  MODIFY `trnsctdtl_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_debt`
--
ALTER TABLE `trvl_trnsct_debt`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_debt_payment`
--
ALTER TABLE `trvl_trnsct_debt_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_jurnal`
--
ALTER TABLE `trvl_trnsct_jurnal`
  MODIFY `jurnal_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_payrol`
--
ALTER TABLE `trvl_trnsct_payrol`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_payrol_payment`
--
ALTER TABLE `trvl_trnsct_payrol_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_spj`
--
ALTER TABLE `trvl_trnsct_spj`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `trvl_trnsct_spj_budget`
--
ALTER TABLE `trvl_trnsct_spj_budget`
  MODIFY `spjbudget_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
